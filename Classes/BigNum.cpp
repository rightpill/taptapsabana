//
//  BigNum.cpp
//  Zoobyssrium
//
//  Created by PACKSUNG PILL on 1/12/17.
//
//

#include "BigNum.h"
#include "GameManager.h"
//#include<sstream>

BigNum::BigNum(){
    numbers.clear();
    numbers.push_back(0);
}

void BigNum::resetNum(){
    numbers.clear();
    numbers.push_back(0);
}
void BigNum::setNumFromFullNumberByDotSplitString(std::string str){
    numbers.clear();
    ValueVector rows = GameManager::getInstance()->split(str, splitChar);
    std::string num;
    for (int i = 0; i < (int)rows.size(); i ++) {
        num = rows.at(i).asString();
//        Value(num).asString();
        numbers.push_back(atoi(num.c_str()));
    }
}
std::string BigNum::getNumForSave(){
    std::string str = "";
    std::string num;
    for (int i = 0; i < (int)numbers.size(); i ++) {
        num = cocos2d::StringUtils::toString(numbers.at(i));
        str.append(num);
        if (numbers.size() > i + 1) {
            str.append(splitChar);
        }
    }
    return str;
}

void BigNum::addNum(int amount, int unit){
    for (int i = (int)numbers.size(); i < unit+1; i++) { // create if now exist
        numbers.push_back(0);
    }
    numbers.at(unit) += amount;
    arrange();
}
void BigNum::addNum(BigNum* num){
    for (int i = (int)numbers.size(); i < num->numbers.size(); i++) { // create if now exist
        numbers.push_back(0);
    }
    
    for (int i = 0; i < num->numbers.size(); i++) {
        numbers.at(i) += num->numbers.at(i);
    }
    arrange();
}
void BigNum::subtractNum(int amount, int unit){
    addNum(-amount, unit);
}
void BigNum::subtractNum(BigNum* num){
    for (int i = (int)numbers.size(); i < num->numbers.size(); i++) { // create if now exist
        numbers.push_back(0);
    }
    
    for (int i = 0; i < num->numbers.size(); i++) {
        numbers.at(i) -= num->numbers.at(i);
    }
    arrange();
}
void BigNum::arrange(){
    bool positiveExist = false;
    bool negativeExist = false;
    bool biggestNumIsPositive = false;
    int num = 0;
    for (int i = 0; i < numbers.size(); i++) {
        num = numbers.at(i);
        if (num != 0) {
            biggestNumIsPositive = num > 0;
        }
        if (num > 0) {
            positiveExist = true;
        }
        if (num < 0) {
            negativeExist = true;
        }
        if (num > 1000 || num < -1000) {
            numbers[i] = num%1000;
            addNum(num/1000, i+1);
        }
    }
    
    if (biggestNumIsPositive && negativeExist) {
        for (int i = 0; i < numbers.size()-1; i++) {
            num = numbers.at(i);
            if (num < 0) {
                numbers[i+1]--;
                numbers[i] += 1000;
            }
        }
    }else if(!biggestNumIsPositive && positiveExist){
        for (int i = 0; i < numbers.size()-1; i++) {
            num = numbers.at(i);
            if (num > 0) {
                numbers[i+1]++;
                numbers[i] -= 1000;
            }
        }
    }
    RemoveUnusedHigherZero();
}
void BigNum::RemoveUnusedHigherZero(){
    while (numbers.at(numbers.size() - 1) == 0 && numbers.size() > 1) {
        numbers.erase(numbers.begin() + (int)(numbers.size() - 1));
    }
}
int BigNum::IsBiggerThanThis(BigNum* num){
    RemoveUnusedHigherZero();
    num->RemoveUnusedHigherZero();
    if (numbers.size() != num->numbers.size()) {
        if (numbers.size() > num->numbers.size()) {
            return 1;
        }else{
            return -1;
        }
    }
    int index = (int)numbers.size() - 1;
    while(index >= 0){
        if (numbers.at(index) != num->numbers.at(index)) {
            if (numbers.at(index) > num->numbers.at(index)) {
                return 1;
            }else{
                return -1;
            }
        }
        index--;
    }
    return 0;
}
void BigNum::multiply(float rate){
    int num;
    float rated;
    int integeredNum;
    int dataToAddLowerLevel = 0;
    for (int i = (int)numbers.size() - 1; i >= 0; i--) {
        num = numbers[i];
        rated = num*rate;
        integeredNum = (int)(rated + 0.5f); // round off
        numbers[i] = integeredNum;
        numbers[i] += dataToAddLowerLevel;
        if (i > 0 && integeredNum != rated) {
            dataToAddLowerLevel = (rated - integeredNum)*1000;
        }
    }
    arrange();
}
std::string BigNum::getUnitStr(int unit){
    unit = unit - 1;
    if (unit == 0) {
        return "k";
    }else if (unit == 1) {
        return "m";
    }else if (unit == 2) {
        return "b";
    }else if (unit == 3) {
        return "t";
    }
    unit = unit - 4;
    
    int devider = 26;
    int loop = 1 + unit/devider;
    int index = unit%devider;
    char ch = 'A' + index;
    std::string str = "";
    for (int i = 0; i < loop; i++) {
        str.push_back(ch);
    }
    return str;
}
std::string BigNum::getFullNumberStringByDotSplit(){
    std::string str;
    for (int i = 0; i < numbers.size(); i++) {
        str.append(Value(numbers.at(i)).asString());
        str.append(".");
    }
    return str.substr(0, str.size() - 1);
}

std::string BigNum::getExpression(){
    std::string str;
    if (numbers.size() > 1) {
        str = cocos2d::StringUtils::toString(numbers[numbers.size() - 1]);
        int firstDigitPlace = (int)str.length();
        str = str.append(splitChar);
        int digitPlaceLeft = 4 - firstDigitPlace;
        str = str.append(getZeroBaseNum(numbers[numbers.size() - 2], true).substr(0, digitPlaceLeft));
        str = str.append(getUnitStr((int)numbers.size() - 1));
    }else{
        str = cocos2d::StringUtils::toString(numbers[0]);
    }
    return str.c_str();
}
std::string BigNum::getZeroBaseNum(int num, bool removeNegative){
    std::string str = "";
    bool isNegative = num < 0;
    if (isNegative) {
        num *= -1;
        if (!removeNegative) {
            str = "-";
        }
    }
    if (num >= 100) {;
    }else if(num >= 10){
        str = str.append("0");
    }else{
        str = str.append("00");
    }
    
    return str.append(cocos2d::StringUtils::toString(num));
}

void BigNum::setNum(BigNum* num){
    numbers.clear();
    for (int i = 0; i < num->numbers.size(); i++) {
        numbers.push_back(num->numbers.at(i));
    }
}
