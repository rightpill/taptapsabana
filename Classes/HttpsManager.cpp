//
//  HttpsManager.cpp
//  PuzzleRoyale
//
//  Created by PACKSUNG PILL on 10/7/16.
//
//

#include "HttpsManager.h"
#include "NativeInterface.h"
#include "json/document.h"
#include "GameManager.h"
#include "TimeManager.h"

//#include "PluginSdkboxPlay/PluginSdkboxPlay.h"
//#include "Kakao/Common/GameUserInfo.h"
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//#include "UrlJni.h"
//#endif

using namespace cocos2d;
using namespace cocos2d::network;
HttpsManager* HttpsManager::m_mySingleton = NULL;
HttpsManager* HttpsManager::m_mySingletonForStage = NULL;

HttpsManager::HttpsManager()
{
    isTrying = false;
}

void HttpsManager::startThread(){
    this->unschedule(schedule_selector(HttpsManager::networkThread));
    this->schedule(schedule_selector(HttpsManager::networkThread), 0.1);
}
void HttpsManager::networkThread(float dt)
{
    if (lock || isInProcess) {
        return;
    }
    if (commandQueue.size() <= 0) {
        return;
    }
    log("network thread");
    int command = commandQueue.at(0);
    std::string url = urlQueue.at(0);
    std::string data = dataQueue.at(0);
    whichJob = jobQueue.at(0);
    commandQueue.erase(commandQueue.begin());
    dataQueue.erase(dataQueue.begin());
    urlQueue.erase(urlQueue.begin());
    jobQueue.erase(jobQueue.begin());
    
    isInProcess = true;
    if (command == COMMAND_GET) {
        char Data[4096],Key[4096];
        log("URL: %s\nDATA: %s", url.c_str(), data.c_str());
        cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
        request->setUrl(__String::createWithFormat("%s?%s", url.c_str(), data.c_str())->getCString());
        request->setRequestType(cocos2d::network::HttpRequest::Type::GET);
        //request->setResponseCallback( CC_CALLBACK_2(HttpsManager::onHttpRequestCompleted, this) );
        request->setResponseCallback(this, httpresponse_selector(HttpsManager::onHttpRequestCompleted));
        // write the post data
        //const char* postData = data.c_str();
        //request->setRequestData(postData, strlen(postData));
        
        request->setTag("GET test1");
        cocos2d::network::HttpClient::getInstance()->send(request);
        request->release();
    }else if(command == COMMAND_SEND) {
        isNetworkFail = false;
        char Data[4096],Key[4096];
        log("URL: %s\nDATA: %s", url.c_str(), data.c_str());
        cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
        //request->setUrl(__String::createWithFormat("%s?%s", url.c_str(), data.c_str())->getCString());
        request->setUrl(url.c_str());
        
        std::vector<std::string> headers;
        headers.push_back("Content-Type: text/plain; charset=utf-8");
        
        request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
        //request->setResponseCallback( CC_CALLBACK_2(HttpsManager::onHttpRequestCompleted, this) );
        request->setResponseCallback(this, httpresponse_selector(HttpsManager::onHttpRequestCompleted));
        // write the post data
        const char* postData = data.c_str();
        request->setRequestData(postData, strlen(postData));
        request->setTag("POST test1");
        cocos2d::network::HttpClient::getInstance()->send(request);
        log("request: %s", request->getRequestData());
        request->release();
    }
}
HttpsManager* HttpsManager::getInstance()
{
    if(NULL == m_mySingleton)
    {
        m_mySingleton = HttpsManager::create();
        m_mySingleton->webTime = -1;
        m_mySingleton->retain();
    }
    
    return m_mySingleton;
}
HttpsManager* HttpsManager::getInstanceForStage()
{
    if(NULL == m_mySingletonForStage)
    {
        m_mySingletonForStage = HttpsManager::create();
        m_mySingletonForStage->webTime = -1;
        m_mySingletonForStage->retain();
    }
    
    return m_mySingletonForStage;
}
bool HttpsManager::init()
{
    Layer::init();
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(15);
    return true;
}

/*void HttpsManager::getData(std::string url, int job)
{
    lock = true;
    commandQueue.push_back(COMMAND_GET);
    urlQueue.push_back(url);
    dataQueue.push_back(data);
    lock = false;
    
    isNetworkFail =false;
    isTrying = true;
    
    HttpRequest* request = new HttpRequest();
    request->setUrl(url.c_str());
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback(this, httpresponse_selector(HttpsManager::onHttpRequestCompleted));
    request->setTag("Http time from timeapi.org");
    HttpClient::getInstance()->send(request);
    request->release();
}*/

void HttpsManager::getData(std::string url, std::string data, int job){
    lock = true;
    commandQueue.push_back(COMMAND_GET);
    urlQueue.push_back(url);
    dataQueue.push_back(data);
    jobQueue.push_back(job);
    lock = false;
}

void HttpsManager::sendData(std::string url, std::string data, int job){
    lock = true;
    commandQueue.push_back(COMMAND_SEND);
    urlQueue.push_back(url);
    dataQueue.push_back(data);
    jobQueue.push_back(job);
    lock = false;
}

void HttpsManager::onHttpRequestCompleted(Ref *sender, void *data)
{
    isInProcess = false;
    log("complete request http");
    HttpResponse *response = (HttpResponse*)data;
    
    isTrying = false;
    if (!response)
    {
        log("invalied response ");
        isNetworkFail = true;
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag()))
    {
        log("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = (int)response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    log("** response code: %d", statusCode);
    if (!response->isSucceed())
    {
        log("response failed");
        log("** error buffer: %s", response->getErrorBuffer());
        isNetworkFail = true;
        if (statusCode == -1) {
            isConnected = false;
        }
        
        //        CCMessageBox("인터넷 연결 상태를 확인해주세요.", "인터넷 엑세스 오류");
        //        GameManager::sharedGameManager()->exitGame();
        return;
    }
    isNetworkFail = false;
    if(whichJob == NN_CROSS && statusCode != 200){
        promotionReceived = true;
        isWaitingForCross = false;
        initDone = true;
        return;
    }
    isConnected = true;
    std::vector<char> *buffer = response->getResponseData();
    if (buffer->size() > 0) {
        std::string res;
        res.insert(res.begin(), buffer->begin(), buffer->end());
        log("** res %s", res.c_str());
        //cocos2d::MessageBox(__String::createWithFormat("** res %s", res.c_str())->getCString(), "result");
        
        rapidjson::Document document;
        if(document.Parse(res.c_str()).HasParseError()){
            log("** https json parsing error");
            isNetworkFail = true;
            return;
        }
        
        if(whichJob == NN_PROVE_RECEIPT){
            proveResultCame = true;
            if (statusCode != 200) {
                isReceiptGood = false;
            }else{
                if(!document.HasMember("result")){
                    isNetworkFail = true;
                    return;
                }
                rapidjson::Value& value = document["result"];
                std::string result = value.GetString();
                receiptResult = result;
                if (result.compare("success") == 0) {
                    isReceiptGood = true;
                }else{
                    isReceiptGood = false;
                }
            }
        }else if (whichJob == NN_INIT) {
            log("** nnInit came");
            if(!document.HasMember("update")){
                isNetworkFail = true;
                return;
            }
            rapidjson::Value& value = document["update"];
            bool isUpdate = value.GetBool();
            if (isUpdate) {
                log("update available");
            }
            
            if (document["ad"].HasMember("applovin")) {
                value = document["ad"]["applovin"]["use"];
                bool useAppLovin = value.GetBool();
                if (useAppLovin) {
                    value = document["ad"]["applovin"]["percent"];
                    appLovinPercent = value.GetInt();
                }else{
                    appLovinPercent = 0;
                }
            }else{
                appLovinPercent = 0;
            }
            
            if (document["ad"].HasMember("unityads")) {
                value = document["ad"]["unityads"]["use"];
                bool useUnity = value.GetBool();
                if (useUnity) {
                    value = document["ad"]["unityads"]["percent"];
                    unityPercent = value.GetInt();
                }else{
                    unityPercent = 0;
                }
            }else{
                unityPercent = 0;
            }
            
            
            /*value = document["ad"]["tapjoy"]["use"];
            bool useTapjoy = value.GetBool();
            if (useTapjoy) {
                value = document["ad"]["tapjoy"]["percent"];
                tapjoyPercent = value.GetInt();
            }else{
                tapjoyPercent = 0;
            }*/
            
            value = document["crosspromotion"]["use"];
            bool useCross = value.GetBool();
            if (useCross) {
                value = document["crosspromotion"]["percent"];
                crossPercent = value.GetInt();
            }else{
                crossPercent = 0;
            }
            //crossPercent = 100;// test
            value = document["service"];
            useService = value.GetBool();
            
//            if (rand()%100 <= crossPercent && !GameManager::getInstance()->isFirstRun && UserDefault::getInstance()->getBoolForKey(KEY_TUTORIAL_DONE, false)) {
//                nnGetCrossPromotion();
//            }else{
//                initDone = true;
//            }
        }else if(whichJob == NN_CROSS){
            if(!document.HasMember("image")){
                isNetworkFail = true;
                return;
            }
            rapidjson::Value& value = document["image"];
            promotionImageLink = value.GetString();
            value = document["url"];
            promotionLink = value.GetString();
            promotionReceived = true;
            isWaitingForCross = false;
            initDone = true;
        }else if(whichJob == NN_POST_BOX_INIT){
            if(!document.HasMember("postbox_id")){
                isNetworkFail = true;
                return;
            }
            
            rapidjson::Value& value = document["postbox_id"];
            postBoxID = value.GetString();
            value = document["postbox_item_count"];
            std::string itemCount = value.GetString();
            postBoxItemss.clear();
            postBoxItemsExpire.clear();
            postBoxItemsDescription.clear();
            postBoxItemIDs.clear();
            if(Value(itemCount).asInt() > 0){
                isPostBoxGot = false;
                isWaitingForPostbox = true;
                
                long time = TimeManager::getInstance()->getCurrentTime();
                int counter = 0;
                std::string call = "234";
                call = Value((int)time).asString();
                hash = NativeInterface::NativeInterface::getHash((cliendId + postBoxID + call).c_str(), secretKey);
                log("hash: %s", hash.c_str());
                while(hash.find("+") != std::string::npos){
                    hash.replace( hash.find("+"), 1, "%2B");
                }
                log("hash encoded: %s", hash.c_str());
                requestData =
                "client_id=" + cliendId + "&" +
                "postbox_id=" + postBoxID + "&" +
                "section=" + "NONE" + "&" +
                "call=" + call + "&" +
                "hash=" + hash;
                function = "v20160401/postbox/item";
                getData( gameURL + function, requestData, NN_POST_BOX_ITEM);
            }else{
                isPostBoxGot = true;
            }
        }else if(whichJob == NN_POST_BOX_ITEM) {
            isWaitingForPostbox = false;
            isPostBoxGot = true;
            if (statusCode != 200) {
                isNetworkFail = true;
                return;
            }
            int arraySize = document.Size();
            for (int i = 0; i < arraySize; i++) {
                rapidjson::Value& postItem = document[i];
                if(!postItem.HasMember("item_code")){
                    isNetworkFail = true;
                    return;
                }
                rapidjson::Value& value = postItem["item_code"];
                std::string itemCode = value.GetString();
                value = postItem["item_count"];
                std::string itemCount = value.GetString();
                value = postItem["message_code"];
                postBoxItemsDescription.push_back(value.GetString());
                value = postItem["expired_sec"];
                postBoxItemsExpire.push_back(value.GetInt());
                postBoxItemss.push_back(itemCode);
                value = postItem["item_id"];
                postBoxItemIDs.push_back(value.GetString());
            }
        }else if(whichJob == NN_POST_BOX_RECEIVE){
            isPostBoxReceiveDone = true;
            isPostBoxReceiveStarted = false;
            
            if(!document.HasMember("result")){
                isNetworkFail = true;
                return;
            }
            rapidjson::Value& result = document["result"];
            if (std::string(result.GetString()).compare("success") == 0) {
                isPostBoxReceiveSuccess = true;
            }else{
                isPostBoxReceiveSuccess = false;
            }
        }else if(whichJob == NN_SAVE){
            saveComplete = true;
            if(!document.HasMember("result")){
                isNetworkFail = true;
                return;
            }
            rapidjson::Value& value = document["result"];
            std::string result = value.GetString();
            if (result.compare("success") == 0) {
                saveResult = NN_SAVE;
            }else if (result.compare("device") == 0) {
                saveResult = NN_SAVE_DEVICE;
            }else if (result.compare("time") == 0) {
                saveResult = NN_SAVE_TIME;
            }else{
                saveResult = NN_SAVE_FAIL;
            }
        }else if(whichJob == NN_LOAD){
            loadComplete = true;
            if(!document.HasMember("result")){
                isNetworkFail = true;
                return;
            }
            rapidjson::Value& value = document["result"];
            std::string result = value.GetString();
            if (result.compare("success") == 0) {
                loadResult = NN_LOAD;
                loadedData = document["data"]["game_data"].GetString();
            }else if (result.compare("limit") == 0) {
                loadResult = NN_LOAD_LIMIT;
            }else if (result.compare("time") == 0) {
                loadResult = NN_LOAD_TIME;
            }else if (result.compare("none") == 0) {
                loadResult = NN_LOAD_NONE;
            }else{
                loadResult = NN_LOAD_FAIL;
            }
        }else if(whichJob == NN_COUPON){
            if(!document.HasMember("result")){
                isNetworkFail = true;
                return;
            }
            rapidjson::Value& value = document["result"];
            std::string result = value.GetString();
            
            if (result.compare("success") == 0) {
                value = document["item"];
                couponItem = value.GetString();
                value = document["timestamp"];
                std::string ts = Value(value.GetInt()).asString();
                value = document["hash"];
                std::string hashYouMade = value.GetString();
                std::string hashIMade = NativeInterface::NativeInterface::getHash((cliendId + result + GameManager::getInstance()->uuid + couponID + couponItem + ts).c_str(), secretKey);
                if (hashYouMade.compare(hashIMade) == 0 || true) {
                    log("hash match");
                    isCouponeDone = true;
                }else{
                    log("HASH NO MATCH");
                    couponResult = NN_COUPON_FAIL;
                }
                
            }else if (result.compare("exist") == 0) {
                couponResult = NN_COUPON_USED;
            }else if (result.compare("expired") == 0) {
                couponResult = NN_COUPON_EXPIRE;
            }else{
                couponResult = NN_COUPON_FAIL;
            }
        }
    }
    whichJob = NN_NONE;
    return;
    //    uint64_t timeInInt = s.GetUint64();
}
void HttpsManager::nnInit(){
    log("** nnInit");
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(15);
    cliendId = "2TrJ3tk63lHu5E0BDs3XhFmiBhmQUbmpqkmlJr1jssVHtQ7Wk9";
    std::string str = UserDefault::getInstance()->getStringForKey(KEY_UUID, "");
    if (str.compare("Empty") == 0) {
        //uuid = NativeInterface::NativeInterface::getUUID();
        //UserDefault::getInstance()->setStringForKey(KEY_UUID, uuid);
    }else{
        //uuid = str;
    }
    
    
    
    udid = NativeInterface::NativeInterface::getUDID();
    version = GameManager::getInstance()->version;
    data = "theData";
    ts = "123";
    secretKey = "862r9xugm0077eKid1d1y11sCKmtg8Dv";
    
    
    long time = TimeManager::getInstance()->getCurrentTime();
    int counter = 0;
    /*while(true){
        ts = Value((int)time).asString();
        hash = NativeInterface::NativeInterface::getHash((cliendId + uuid + udid + version + ts).c_str(), secretKey);
        if (hash.find("+") == std::string::npos) {
            break;
        }
        time--;
        counter++;
        if (counter > 10000) {
            break;
        }
    }*/
    ts = Value((int)time).asString();
    hash = NativeInterface::NativeInterface::getHash((cliendId + GameManager::getInstance()->uuid + udid + version + ts).c_str(), secretKey);
    while(hash.find("+") != std::string::npos){
        hash.replace( hash.find("+"), 1, "%2B");
    }
    
    
    gameURL = "https://game-api.nanoo.so/";
    gameID = "puzzleroyale";

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    platform = "IOS";
#elif CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    platform = "ANDROID";
#else
    
#endif
    requestData =
    "client_id=" + cliendId + "&" +
    "version=" + version + "&" +
    "platform=" + platform;
    function = "/v1/init";
    getData( gameURL + gameID + function, requestData, NN_INIT);
    log("** nnInit done");
}
void HttpsManager::nnGetPostBox(){
    log("** post box");
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(15);
    isWaitingForPostbox = true;
    std::string call = "234";
    
    long time = TimeManager::getInstance()->getCurrentTime();
    int counter = 0;
    /*while(true){
        call = Value((int)time).asString();
        hash = NativeInterface::NativeInterface::getHash((cliendId + uuid + call).c_str(), secretKey);
        if (hash.find("+") == std::string::npos) {
            break;
        }
        time--;
        counter++;
        if (counter > 10000) {
            break;
        }
    }*/
    call = Value((int)time).asString();
    hash = NativeInterface::NativeInterface::getHash((cliendId + GameManager::getInstance()->uuid + call).c_str(), secretKey);
    log("hash: %s", hash.c_str());
    while(hash.find("+") != std::string::npos){
        hash.replace( hash.find("+"), 1, "%2B");
    }
    log("hash encoded: %s", hash.c_str());
    
    requestData =
    "client_id=" + cliendId + "&" +
    "uuid=" + GameManager::getInstance()->uuid + "&" +
    "platform=" + platform + "&" +
    "subscription=" + "N" + "&" +
    "version=" + version + "&" +
    "call=" + call + "&" +
    "hash=" + hash;
    function = "v20160401/postbox/init";
    getData( gameURL + function, requestData, NN_POST_BOX_INIT);
    log("get postbox: %s", requestData.c_str());
}
void HttpsManager::nnReceivePostBoxItem(std::string itemID, int count){
    log("** nnReceivePostBoxItem");
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(15);
    
    isPostBoxReceiveDone = false;
    isPostBoxReceiveStarted = true;
    isWaitingForPostbox = true;
    std::string call = "7";//Value(rand()%1024).asString();//"1024";//Value(count).asString();
    
    long time = TimeManager::getInstance()->getCurrentTime();
    int counter = 0;
    /*while(true){
        call = Value((int)time).asString();
        hash = NativeInterface::NativeInterface::getHash((cliendId + postBoxID + itemID + call).c_str(), secretKey);
        if (hash.find("+") != std::string::npos) {
            break;
        }
        break;
        time--;
        counter++;
        if (counter > 10000) {
            break;
        }
    }
    */
    call = Value((int)time).asString();
    hash = NativeInterface::NativeInterface::getHash((cliendId + postBoxID + itemID + call).c_str(), secretKey);
    while(hash.find("+") != std::string::npos){
        hash.replace( hash.find("+"), 1, "%2B");
    }
    
    requestData =
    "client_id=" + cliendId + "&" +
    "postbox_id=" + postBoxID + "&" +
    "item_id=" + itemID + "&" +
    "uuid=" + GameManager::getInstance()->uuid + "&" +
    "call=" + call + "&" +
    "hash=" + hash;
    function = "v20160401/postbox/receive";
    sendData( gameURL + function, requestData, NN_POST_BOX_RECEIVE);
}

void HttpsManager::nnCoupon(std::string coupon){
    log("** nnCoupon");
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(15);
    
    couponID = coupon;
    requestData =
    "client_id=" + cliendId + "&" +
    "coupon=" + coupon + "&" +
    "uuid=" + GameManager::getInstance()->uuid + "&" +
//    "nickname=" + UserDefault::getInstance()->getStringForKey(KEY_CHOSEN_NAME, "puzzleroyale") + "&" +
    "platform=" + data;
    function = "v20160401/coupon/check";
    sendData( gameURL + function, requestData, NN_COUPON);
}
void HttpsManager::nnSave(std::string data){
    log("** nnSave");
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(15);
    
    long time = TimeManager::getInstance()->getCurrentTime();
    int counter = 0;
    /*while(true){
        ts = Value((int)time).asString();
        hash = NativeInterface::NativeInterface::getHash((cliendId + uuid + udid + version + ts).c_str(), secretKey);
        if (hash.find("+") == std::string::npos) {
            break;
        }
        time--;
        counter++;
        if (counter > 10000) {
            break;
        }
    }*/
    ts = Value((int)time).asString();
    hash = NativeInterface::NativeInterface::getHash((cliendId + GameManager::getInstance()->uuid + udid + version + ts).c_str(), secretKey);
    while(hash.find("+") != std::string::npos){
        hash.replace( hash.find("+"), 1, "%2B");
    }
    
    requestData =
    "client_id=" + cliendId + "&" +
    "uuid=" + GameManager::getInstance()->uuid + "&" +
    "udid=" + udid + "&" +
    "version=" + version + "&" +
    "data=" + data + "&" +
    "ts=" + ts + "&" +
    "hash=" + hash;
    function = "/v1/cloud/save";
    gameID = "puzzleroyale";
    sendData( gameURL + gameID + function, requestData, NN_SAVE);
}
void HttpsManager::nnLoad(){
    log("** nnLoad");
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(15);
    
    long time = TimeManager::getInstance()->getCurrentTime();
    int counter = 0;
    /*while(true){
        ts = Value((int)time).asString();
        hash = NativeInterface::NativeInterface::getHash((cliendId + uuid + udid + ts).c_str(), secretKey);
        if (hash.find("+") == std::string::npos) {
            break;
        }
        time--;
        counter++;
        if (counter > 10000) {
            break;
        }
    }*/
    ts = Value((int)time).asString();
    hash = NativeInterface::NativeInterface::getHash((cliendId + GameManager::getInstance()->uuid + udid + ts).c_str(), secretKey);
    while(hash.find("+") != std::string::npos){
        hash.replace( hash.find("+"), 1, "%2B");
    }
    
    requestData =
    "client_id=" + cliendId + "&" +
    "uuid=" + GameManager::getInstance()->uuid + "&" +
    "udid=" + udid + "&" +
    "ts=" + ts + "&" +
    "hash=" + hash;
    function = "/v1/cloud/load";
    gameID = "puzzleroyale";
    sendData( gameURL + gameID + function, requestData, NN_LOAD);
}
void HttpsManager::nnProveReceipt(std::string purchase, std::string signature){
    log("** nnProveReceipt");
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(60);
    
    proveResultCame = false;
    
    while(purchase.find("+") != std::string::npos){
        purchase.replace( purchase.find("+"), 1, "%2B"); // replace all 'x' to 'y'
    }
    while(signature.find("+") != std::string::npos){
        signature.replace( signature.find("+"), 1, "%2B"); // replace all 'x' to 'y'
    }
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    function = "v20160401/purchase/receipt_validator_ios";
    requestData =
    "client_id=" + cliendId + "&" +
    "receipt=" + purchase;
#elif CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    function = "v20160401/purchase/receipt_validator_android";
    requestData =
    "client_id=" + cliendId + "&" +
    "purchase=" + purchase + "&" +
    "signature=" + signature;
#else
#endif
    
    sendData( gameURL + function, requestData, NN_PROVE_RECEIPT);
}
void HttpsManager::nnProveReceipt(std::string receipt){
    log("** nnProveReceipt");
    
    proveResultCame = false;
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    function = "v20160401/purchase/receipt_validator_ios";
    requestData =
    "client_id=" + cliendId + "&" +
    "receipt=" + receipt + "&" +
    "mode=" + "DEVELOPMENT";
#elif CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    //function = "/v20160401/purchase/receipt_validator_android";
#else
    
#endif
    
    requestData =
    "client_id=" + cliendId + "&" +
    "purchase=" + receipt + "&" +
    "signature=" + "DEVELOPMENT";
    
    sendData( gameURL + function, requestData, NN_PROVE_RECEIPT);
}
void HttpsManager::nnGetCrossPromotion(){
    log("** nnGetCrossPromotion");
    if (whichJob == NN_CROSS) {
        return;
    }
    log("** get cross");
    
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(15);
    isWaitingForCross = true;
    promotionReceived = false;
    const char* strLanguage;
    if (GameManager::getInstance()->getLanguageType() == LanguageType::ENGLISH) {
        strLanguage = "EN";
    }else if (GameManager::getInstance()->getLanguageType() == LanguageType::KOREAN) {
        strLanguage = "KO";
    }else if (GameManager::getInstance()->getLanguageType() == LanguageType::JAPANESE) {
        strLanguage = "JA";
//    }else if (GameManager::getInstance()->getLanguageType() == LanguageType::CHINESE_SIMPLIFIED) {
//        strLanguage = "CN";
//    }else if (GameManager::getInstance()->getLanguageType() == LanguageType::CHINESE_TRADITIONAL) {
//        strLanguage = "TW";
    }
    requestData =
    "client_id=" + cliendId + "&" +
    "platform=" + platform + "&" +
    "orientation=" + "VERTICAL" + "&" +
    "language=" + strLanguage;
    function = "v20160401/promotion/request";
    getData( gameURL + function, requestData, NN_CROSS);
    
}
bool HttpsManager::isNetworkConnected(){
    log("** isNetworkConnected");
    network::HttpRequest* request = new (std::nothrow) network::HttpRequest();
    request->setUrl("http://www.google.com");
    request->setRequestType(network::HttpRequest::Type::GET);
    request->setResponseCallback([=] (network::HttpClient* client, network::HttpResponse* response)
                                 {
                                     CCLOG("response is %s", response ? "true":"false");
                                     CCLOG("response->isSucceed() is %s", response->isSucceed() ? "true":"false");
                                     
                                     if(!response || !response->isSucceed())
                                     {
                                         // No internet connection
                                         isConnected = false;
                                         CCLOG("No internet connection");
                                     }
                                     else
                                     {
                                         // Internet available
                                         isConnected = true;
                                         CCLOG("Internet connection available");
                                     }
                                 });
    
    network::HttpClient::getInstance()->sendImmediate(request);
    request->release();
    
    return isConnected;
}
