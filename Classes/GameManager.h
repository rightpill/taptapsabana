#ifndef GameManager_h
#define GameManager_h

#include "cocos2d.h"

#ifdef SDKBOX_ENABLED
#include "PluginSdkboxPlay/PluginSdkboxPlay.h"
#include "PluginIAP/PluginIAP.h"
#include "PluginAdMob/PluginAdMob.h"
#endif
#include "HelloWorldScene.h"
#include "IntroScene.h"
#include "ui/UIImageView.h"
#include "ui/UIButton.h"
#include "ui/UIHelper.h"
#include "ui/UIButton.h"
#include "ui/UIListView.h"
#include "ui/UIText.h"
#include "ui/UITextField.h"
#include "ui/UIText.h"
#include "ui/UIImageView.h"
#include "ui/UILoadingBar.h"
#include "editor-support/cocostudio/CocoStudio.h"
//#include "GoogleBilling.h"

#include "ui/UITextBMFont.h"
#include "LogoSplash.h"

using namespace cocos2d;
using namespace cocos2d::ui;


#define MAX_LEVEL 100

#define STATE_STANDING 0
#define STATE_WALKING 1
#define STATE_JUMPING 2
#define STATE_FALLING 3
#define SCREEN_WIDTH 123
#define SCREEN_HEIGHT 220

#define ENEMY_MOVE_STAND 0
#define ENEMY_MOVE_WANDER_ON_A_PLATFORM 1
#define ENEMY_MOVE_WANDER_CROSS_PLATFORM 2
#define ENEMY_MOVE_WANDER_CROSS_PLATFORM_CRASH_BY_WALL 3
#define ENEMY_MOVE_WANDER_ON_CEILING 4
#define ENEMY_MOVE_WANDER_ON_WALL 5
#define ENEMY_MOVE_HANG 6
#define ENEMY_MOVE_DROP 7
#define ENEMY_MOVE_DROP_CRASH 8
#define ENEMY_MOVE_FLY_TO_HERO 9
#define ENEMY_MOVE_FLY_TO_HERO_HORIZONTALLY 10
#define ENEMY_MOVE_FLY_TO_HERO_FOLLOW 11
#define ENEMY_MOVE_FLY_TO_RIGHT 12
#define ENEMY_MOVE_FLY_TO_DOWN 13
#define ENEMY_MOVE_FLY_TO_LEFT 14
#define ENEMY_MOVE_FLY_TO_UP 15
#define ENEMY_MOVE_CUSTOM 16
#define ENEMY_MOVE_FLY_WANDER_STOP 17
#define ENEMY_MOVE_SWIM_TO_HERO_FOLLOW 18
#define ENEMY_MOVE_JUMP_STAND 19
#define ENEMY_MOVE_JUMP_WANDER 20

#define ENEMY_ACTION_NONE 0
#define ENEMY_ACTION_FIRE 1
#define ENEMY_ACTION_DEFENCE 2
#define ENEMY_ACTION_MOVE 3
#define ENEMY_ACTION_MOVE_TO_LEFT 4
#define ENEMY_ACTION_MOVE_TO_RIGHT 5



#define SOUND_SKILL_AURORA 0
#define SOUND_SKILL_WIND 1
#define SOUND_SKILL_RAINBOW 2
#define SOUND_CREATE_ANIMAL 3
#define SOUND_CREATE_DECO 4
#define SOUND_CREATE_RELIC 5
#define SOUND_POPUP_CREATE 6
#define SOUND_POPUP_NEGA 7
#define SOUND_POPUP_NORMAL 8
#define SOUND_UPGRADE 9
#define SOUND_ANIMAL_TAP_0 10
#define SOUND_ANIMAL_TAP_1 11
#define SOUND_GRASS_0 12
#define SOUND_GRASS_1 13
#define SOUND_GRASS_2 14
#define SOUND_GRASS_3 15
#define SOUND_MENU_DOWN 16
#define SOUND_MENU_UP 17
#define SOUND_BTN_POS 18
#define SOUND_BTN_NEG 19
#define SOUND_SCENE_0 20
#define SOUND_SCENE_1 21
#define SOUND_SCENE_2 22
#define SOUND_SCENE_3 23
#define SOUND_BGM_TITLE 24
#define SOUND_BGM_VILLAGE 25



#define EFFECT_TWINKLE 0
#define EFFECT_EXPLODE_SMALL 1
#define EFFECT_EXPLODE_MIDDLE 2
#define EFFECT_EXPLODE_BIG 3
#define EFFECT_EXPLODE_HUGE 4
#define EFFECT_SMOKE 5
#define EFFECT_GREEN_SMOKE 6
#define EFFECT_RED_SMOKE 7


#define VEHICLE_NONE 0
#define VEHICLE_GOLIATH 1
#define VEHICLE_BROOM 2
#define VEHICLE_CART 3
#define VEHICLE_TOP 4
#define VEHICLE_BIRD 5
#define VEHICLE_BALLOON 6


#define ACTION_TAG_ANIMATION 0
#define ACTION_TAG_ROTATION 1
#define ACTION_TAG_HIT 2
#define ACTION_TAG_ROTATE 3
#define ACTION_TAG_SWAP 4

#define GUN_ORDINARY 0

#define MARKET_APPSTORE_PAID 0
#define MARKET_PLAYSTORE_PAID 1
#define MARKET_APPSTORE_FREE 2
#define MARKET_PLAYSTORE_FREE 3
#define MARKET_NAVER_PAID 4
#define MARKET_TSTORE_PAID 5
#define MARKET_SAMSUNG_APPS 6
#define MARKET_MAC 7
#define MARKET_FUNBOX 8
#define MARKET_OUYA 9
#define MARKET_WINDOWS 10
#define MARKET_CHINA_NO_IAP 11

#define FONT_DEFAULT 0
#define FONT_BITDUST_ONE 1
#define FONT_BITDUST_TWO 2
#define FONT_ARIAL 3
#define FONT_CJK 4
#define FONT_DOHYUN 5

#define GOLD 0
#define GEM 1


#define MISSILE_EFFECT_NONE 0
#define MISSILE_EFFECT_SMOKE 1
#define MISSILE_EFFECT_RED_BALL 2
#define MISSILE_EFFECT_GREEN_SMOKE 3
#define MISSILE_EFFECT_RED_SMOKE 4

#define INVENTORY_MAX_COUNT 100


#define COIN_OFFSET 7973
#define GEM_OFFSET 9997
#define GAME_EXP_UNIT 37


#define DEFAULT_LIGHTNING_ITEM_COUNT 30
#define PPR 5.0f

#define ACHIEVEMENT_STAR_COLLECTER 0
#define ACHIEVEMENT_KEEPER 1
#define ACHIEVEMENT_FIRST_FRUIT 2
#define ACHIEVEMENT_RICH_MAN 3
#define ACHIEVEMENT_KILLER 4
#define ACHIEVEMENT_HOT_KETTLE 5
#define ACHIEVEMENT_WHOS_BOSS 6
#define ACHIEVEMENT_WEAPON_MASTER 7
#define ACHIEVEMENT_LEADERSHIP 8
#define ACHIEVEMENT_SOUL_LEGEND 9

#define PAGE_BEFORE_START -1
#define PAGE_TITLE 0

#define CLOSE_TO_NEXT 0
#define CLOSE_TO_RETRY 1
#define CLOSE_TO_STAGES 2
#define CLOSE_TO_MAIN 3
#define FAST_SPEED 2
#define DEFAULT_CRITICAL 5

#define SPRITE_EFFECT_BLUR 0
#define SPRITE_EFFECT_OUTLINE 1
#define SPRITE_EFFECT_NOISE 2
#define SPRITE_EFFECT_EDGE 3
#define ATTACK_DELAY 1

#define MONSTER_SKILL_NONE -1
#define MONSTER_SKILL_HP_N_PERCENT_INSCREASE_KIND 0
#define MONSTER_SKILL_HP_N_PERCENT_INSCREASE_ALL 1
#define MONSTER_SKILL_HP_N_INSCREASE_KIND 2
#define MONSTER_SKILL_HP_N_INSCREASE_ALL 3
#define MONSTER_SKILL_AP_N_PERCENT_INSCREASE_KIND 4
#define MONSTER_SKILL_AP_N_PERCENT_INSCREASE_ALL 5
#define MONSTER_SKILL_AP_N_INSCREASE_KIND 6
#define MONSTER_SKILL_AP_N_INSCREASE_ALL 7
#define MONSTER_SKILL_SUMMONER_HP_N 8
#define MONSTER_SKILL_SUMMONER_HP_N_PERCENT 9
#define MONSTER_SKILL_SUMMONER_AP_N 10
#define MONSTER_SKILL_SUMMONER_AP_N_PERCENT 11
#define MONSTER_SKILL_EXTRA_SUMMON_N_PERCENT_KIND 12
#define MONSTER_SKILL_EXTRA_SUMMON_N_PERCENT_ALL 13
#define MONSTER_SKILL_CRITICAL_N_PERCENT_KIND 14
#define MONSTER_SKILL_CRITICAL_N_PERCENT_ALL 15
#define MONSTER_SKILL_CRITICAL_N_KIND 16
#define MONSTER_SKILL_CRITICAL_N_ALL 17
#define MONSTER_SKILL_ATTACK_SPEED_KIND 18
#define MONSTER_SKILL_ATTACK_SPEED_ALL 19
#define MONSTER_SKILL_MOVE_SPEED_KIND 20
#define MONSTER_SKILL_MOVE_SPEED_ALL 21
#define MONSTER_SKILL_UPPER_CLASS_SUMMON_N_PERCENT_KIND 22
#define MONSTER_SKILL_UPPER_CLASS_SUMMON_N_PERCENT_ALL 23
#define MONSTER_SKILL_COMBO_N 24
#define MONSTER_SKILL_COMBO_TIME_N_PERCENT 25

#define COLLEAGUE_SKILL_NONE -1
#define COLLEAGUE_SKILL_HP_N_PERCENT_ME 0
#define COLLEAGUE_SKILL_HP_N_PERCENT_ALL 1
#define COLLEAGUE_SKILL_HP_N_ME 2
#define COLLEAGUE_SKILL_HP_N_ALL 3
#define COLLEAGUE_SKILL_AP_N_PERCENT_ME 4
#define COLLEAGUE_SKILL_AP_N_PERCENT_ALL 5
#define COLLEAGUE_SKILL_AP_N_ME 6
#define COLLEAGUE_SKILL_AP_N_ALL 7
#define COLLEAGUE_SKILL_CRITICAL_RATE_N_PERCENT_ME 8
#define COLLEAGUE_SKILL_CRITICAL_RATE_N_PERCENT_ALL 9
#define COLLEAGUE_SKILL_CRITICAL_DAMAGE_N_ME 10
#define COLLEAGUE_SKILL_CRITICAL_DAMAGE_N_ALL 11
#define COLLEAGUE_SKILL_ATTACK_SPEED_N_PERCENT_ME 12
#define COLLEAGUE_SKILL_ATTACK_SPEED_N_PERCENT_ALL 13
#define COLLEAGUE_SKILL_MOVE_SPEED_N_PERCENT_ME 14
#define COLLEAGUE_SKILL_MOVE_SPEED_N_PERCENT_ALL 15

#define VIDEO_RESET_SKILLS 0
#define VIDEO_FREE_GEM 1
#define VIDEO_FREE_RELIC 2
#define VIDEO_FREE_RAIN 3
#define VIDEO_500_TAP 4


#define MAX_TICKET_COUNT 7
#define MAX_HERO_COUNT 22

#define CSV_MONSTER_STAT 0
#define CSV_MONSTER_SKILL 1
#define CSV_COLLEAGUE_STAT 2
#define CSV_COLLEAGUE_SKILL 3

#define GREED_TYPE_WIN_RANKING 0
#define GREED_TYPE_LOSE_RANKING 1
#define GREED_TYPE_WIN_FREE 2
#define GREED_TYPE_LOSE_FREE 3

#define INIT_RANKING 20

#define KEY_REMOVE_ADS_GET "KEY_REMOVE_ADS_GET"
#define KEY_TOTAL_PLAY_TIME "TOTAL_PLAY_TIME"
#define KEY_TOTAL_RELIC_GET "KEY_TOTAL_RELIC_GET"
#define KEY_TOTAL_ANIMAL_CREATE "KEY_TOTAL_ANIMAL_CREATE"
#define KEY_TOTAL_RARE_ANIMAL_CREATE "KEY_TOTAL_RARE_ANIMAL_CREATE"
//#define KEY_TOTAL_TAP_COUNT "KEY_TOTAL_TAP_COUNT"
#define KEY_ANIMAL_BY_GEM "ANIMAL_BY_GEM"
#define KEY_BEST_HUNTER_LEVEL "KEY_BEST_HUNTER_LEVEL"
#define KEY_TOTAL_SKILL_USE "KEY_TOTAL_SKILL_USE"
#define KEY_LIFE "TheLife"
#define KEY_TAP_BUFF_LEVEL "TapBuffLevel"
#define KEY_TAP_LEVEL "Taplevel"
#define KEY_SELECTED_LANGUAGE "SelectedLanguage"
#define KEY_MUSIC_ON "MusicOn"
#define KEY_SOUND_ON "SoundOn"
#define KEY_MUSIC_VOLUMN "MusicVolumn"
#define KEY_SOUND_VOLUMN "SoundVolumn"
#define KEY_PUSH_ALARM "PushAlarm"
#define KEY_COIN_LEFT "CoinLeft"
#define KEY_GEM_LEFT "GemLeft"
#define KEY_UUID "KeyUUID"
#define KEY_AUTO_TAP_LEVEL "AutoTapLevel"
#define KEY_BIG_TAP_LEVEL "BigTapLevel"
#define KEY_BUFF_LEVEL "BuffLevel"
#define KEY_AUTO_TAP_USED_TIME "WhenAutoTapUsed"
#define KEY_BIG_TAP_USED_TIME "WhenBigTap"
#define KEY_BUFF_USED_TIME "WhenBuff"
#define KEY_RESET_USED_TIME "RESETBuff"
#define KEY_SANHO_UNLOCKED_FORAMT "SanhoUnlocked%d"
#define KEY_SANHO_LEVEL_FORAMT "SanhoLevel%d"
#define KEY_UNLOCKED_ANIMAL_FORMAT "AnimalUnlocked%d"
#define KEY_ANIMAL_COUNT_IN_AREA_FORMAT "AnimalCount%d_%d"
#define KEY_SELECTED_AREA "SelectedArea"
#define KEY_ANIMAL_COUNT_INVENTORY_FORMAT "AnimalCountInventory%d"
#define KEY_RELIC_HAVE_FORMAT "RelicHaveIt%d"
#define KEY_RELIC_READY_START_TIME_FORMAT "RelicReadyStartTime%d"
#define KEY_RELIC_IN_USE_FORMAT "RelicInUse%d"
#define KEY_RELIC_POINT "RelicPoint"
#define KEY_RELIC_SLOT_UNLOCKED_FORMAT "RelicSlotUnlockedFormat%d"
#define KEY_INVENTORY_SLOT_COUNT "KEY_INVENTORY_SLOT_COUNT"
#define KEY_LAST_ATTACK_CHECK_TIME "KEY_LAST_ATTACK_CHECK_TIME"
#define KEY_LAST_PLAY_DAY "KEY_LAST_PLAY_DAY"
#define KEY_ATTACKER_LEVEL "KEY_ATTACKER_LEVEL"
#define KEY_HUNTED_FORMAT "HUNTED_%d"
#define KEY_REWARD_RELIC_COUNT "KEY_REWARD_RELIC_COUNT"
//#define KEY_ANIMAL_LEVEL_FORMAT "KEY_ANIMAL_LEVEL_FORMAT_%d"
#define KEY_ACCUMULATED_ANIMAL_COUNT_FORMAT "KEY_ACCUMULATED_ANIMAL_COUNT_FORMAT_%d"
#define KEY_LAST_PAUSED_TIME "KEY_LAST_PAUSED_TIME"
#define KEY_NEXT_ID "KEY_NEXT_ID"
#define KEY_ANIMAL_INDEX_FOR_ID_FORMAT "KEY_INDEX_FOR_ID_%d"
#define KEY_ANIMAL_AREA_FOR_ID_FORMAT "KEY_AREA_FOR_ID_%d"
//static const Color3B greenColor = {5,126,45};
//static const Color3B yesColor = {241,74,1};
//static const Color3B noColor = {46,124,179};

#define IAP_GEM_0 0
#define IAP_GEM_1 1
#define IAP_GEM_2 2
#define IAP_GEM_3 3

#define IAP_BLUE_BIRD 5
#define IAP_PUPPY 6
#define IAP_STRONG_ODIN 7
#define IAP_GEM_SPECIAL_600 8
#define IAP_GEM_SPECIAL_1000 9

using namespace cocos2d::ui;

typedef struct PetInfo
{
    int maxLevel;
    int maxAttack;
    int petNumber;
    int levelRequired;
    const char* name;
    const char* description;
    int starCount;
}PET;
using namespace cocos2d;

class GameManager : public Node, public sdkbox::AdMobListener, public sdkbox::IAPListener
{
private:
    int bulletType;
    
    //Constructor
    GameManager();
    void loadLanguageSheet();
    void loadCSV(ValueVector& table, std::string fileName);
    //Instance of the singleton
    static GameManager* m_mySingleton;
    Layer* pauseLayer;
    Layer* optionLayer;
    Layer* gameOverLayer;
    Layer* achievementLayer;
    Scene* gameOverScene;
	HelloWorldScene* world = nullptr;
	Node* LoadingScene;
    
    int currentBGM;
    std::string userDefaultData;
	int _starCount;
public:
    IntroScene* intro = nullptr;
    LogoSplash* logoSplash = nullptr;
    bool isInIntro = false;
    bool isInLogo = false;
    std::string uuid="";
    bool isFirstRun=false;
    ValueMap languageTable;
    std::string getValueFromCSV(int table, int row, int column);
    //Get instance of singleton
    static GameManager* getInstance();
    cocos2d::LanguageType getLanguageType();
    void setLanguageType(LanguageType lang);
    std::string getText(const char* textId);
    std::string getText(const char* textId, cocos2d::LanguageType type);
    
    std::map<std::string, std::string> priceMap;
    void loadCSVFile();
    ValueVector split(const std::string &str, const std::string &delim);
    float getValueForFunction(std::string str);
	bool IsNewGame;
    bool isRankingFight;
    int jewelKind;
    
    bool isPlayServiceLogIn;
    void showDisposableMessage(const char* msg, Node* parent);
    void showUpEffect(Node* node);
    std::string getEnemyName(int stage, int ranking);
    void runAnimation(Node* node, const char* name, bool repeat);
    int getEnemyIndex(int stage, int ranking);
//    bool isShieldPurcahsed();
//    void setShield(long time);
//    long getShieldPurchasedTime();
    const char* getIconFileNameForMonsterSkill(int index, int round);
	int StageMode;
	int GetUpgradePrice(cocos2d::Point pos);
	int getCoin();
	void addCoin(int howMuch);
    void setCoin(int howMuch);
    bool isRewardAvailable;
    int getGem();
    void rewardTapjoy(int howMuch);
    void addGem(int howMuch);
    void setGem(int howMuch);
	void SetStringWithAutoLineBreak(TextBMFont* lbl, std::string str, float width);
    Layer* settingLayer;
    void onPlayServiceLogin(bool isLogin);
    const char* getFont(int font);
    void initGameManager();
    int iapIndex = -1;
    void rewardIAP(int index);
    int getIAPGemRewardCount(int index);
    
    
    int getColleaguePurchasePrice(int index);
    bool isColleaguePurchased(int index);
    void purchaseColleague(int index);
    std::string getVillageName(int stageIndex);
    Layer* titleLayer;
    bool isGuestPlay;
    bool isVideoReady;
    bool isUsingController;
    const char* convertMyUserDefaultToCharArray();
    void convertCharArrayToMyUserDefault(const char* data);
    void scrollToItem(ScrollView* scrollView, Node* node);
    void scrollToItem(ScrollView* scrollView, cocos2d::Point pos, cocos2d::Point offset);
    bool gameStarted;
    bool initComplete;
	Sprite* myPhoto;
    
    int getMaxExp();
    void addExp(int exp);
    void saveGameData();
    void loadGameData();
    void setLoadedData(const char* key, const char* value);
	void SpriteMoveDone(Node* node);
    cocos2d::Point getGemCountPosition();
    bool isPaidGame();
    bool isInMiddleOfGame;
    bool firstPlayed;
    static void callbackFromBilling(int result);
    void verifyReceipt(std::string reciept, std::string signature);
    void alertToUser(std::string str);
    
    const char* version;
    cocos2d::Size originalSize;
    const char* getLocalizedFont();
    cocos2d::Label* getLocalizedLabel();
    cocos2d::Label* getLocalizedLabel(const char* text, Color4B color);
    void setLocalizedString(std::string str, Text* lbl);
    void setLocalizedString(Text* lbl, std::string str);
    void setLocalizedString(Label* lbl, std::string str);
    void setStringWithLocalizedFont(std::string str, Text* lbl);
    int videoAdsIndex = 0;
    int videoCoinReward=1000;
    int videoColleagueIndex = 0;
    void showShining(Node* parent, int glowCount, const char* particleName, float scale, cocos2d::Point pos, int varDistance, Color3B color, Widget::TextureResType fileType);
    bool isStageSetOnce;
    int currentStageIndex;
    int currentRanking;
    void runSceneAnimation(Node* layer, std::string sceneName, bool loop);
    int market;
    int page = PAGE_BEFORE_START;
    bool developer;
    bool paidUser;
    bool appAlreadyLaunched;
    int theme;
    float musicVolumn;
    float soundVolumn;
    const char* currentUserID;
    void showVideoDone();
    void showVideoFailed();
    void showVideo(int index);
    void clickAdsDone();
    void clickAdsFailed();
    void exitGame();
    bool isVideoRewardEnergy;
    bool isVideoRewardAttack;
    bool isVideoRewardCoins;
    void setFontName(Label* lbl, const char* name, float fontSize);
    int getGemForCoin(int coinCount);
    //A function that returns zero "0"
    int ReturnZero(){return 0;}
    // another test function
    void runScene() { CCLOG("test");};
    void buttonDown(int buttonType);
    void buttonUp(int buttonType);
    void saveCoin();
    void setFontSize(Label* lbl, float size);

    bool checkDataSecure();
    bool justCleared = false;
    void preLoadAllSoundEffect();
    void playSoundEffect(int sound);
    void setMusicVolumn(float vol);
    void setSoundVolumn(float vol);
    void setNotificationOn(bool onOff);
    int getPotionCount();
    void setPotionCount(int bomb);
    float getMusicVolumn();
    float getSoundVolumn();
    bool getNotificationOn();
    double getAngle(cocos2d::Point pos1, cocos2d::Point pos2);
    void SendAchievement(const char* text, int count, int goal);
    void makeItSiluk(Node* node);
    void makeItSilukOnce(Node* node, float duration, float scale);
    void makeItSiluk(Node* node, float interval, float scale);
    void makeItSilukOccationally(Node* node, float interval, float animationDur);
    bool refreshOrientationNeeded = true;
    bool isPortrait = false;
    cocos2d::Point getRandomPointFromRect(cocos2d::Rect rec);
	std::string GetSlotKey(int index);
    void scrollTheLayer(ScrollView* scrollLayer, bool isLeft, bool isHorizontal, int howMuch);
//    HelloWorld* getStageLayer();
    void arrange(float startX, float endX, Vector<Button*> nodes);
    Scene* getTitleScene();
    Layer* getGameStartLayer();
    Scene* getGameStartScene();
    void alignToCenter(Node* node0, Node* node1, float gap, float centerX, float offsetX);

    Layer* getTitleLayer();
    Layer* getShopLayer();
    Layer* getPauseLayer();
//    Layer* getGameOverLayer();
//    Scene* getGameOverScene();
    const char* getBulletName(int weaponType, int playerMissileDemage);
    Layer* getOptionLayer();
    Layer* getAchievementLayer();
    void setWorld(HelloWorldScene* layer);
    const char* getShortenedKoreanString(std::string str, int length);
    HelloWorldScene* getWorld();
    
//    void setStageLayer(Layer* layer);
    void setStageScene(Scene* scene);
//    RepeatForever* getScaleUpDownAction(float scale);
    
    std::vector<std::string> nickNames;
    void loadNickNames();
    std::string getNickName(int stageIndex, int ranking);
    
    bool caseInsCompare(std::string& str1, std::string& str2);
    void pushLayer(Layer* parent, Layer* layer);
    void pushLayerWithoutDisable(Layer* parent, Layer* layer);
    void popLayer(Layer* layer);
    void popLayerWithoutAnimation(Layer* layer);
    void animateFadeIn(Node* layer, Node* parent);
    void animateFadeOut(Node* layer);
    void animationFadeInDone(Node* layer);
    void animationFadeOutDone(Node* layer);
    
//    void disableLayer(Layer* layer);
//    void enableLayer(Layer* layer);
    
	void showParticleExplosion(Node* prt, const char* sptName, cocos2d::Point pos, int distance, float scale, Widget::TextureResType type);
    void addYellowTurningBackground(Node* node, int backWidth);
    void scheduleLocalNotification(const char* title, const char* msg, int time);
    void scheduleLocalNotification();
    void unscheduleAllNotifications();
    long strToLong(const char* str);
    Sprite* getGrayScaleImage(const char* src, int devider = 3);
    Sprite* getGrayScaleSpriteWithSpriteFrameName(const char* src);
    RenderTexture* createAdditiveBorder( Sprite* label, int size, Color3B color, GLubyte opacity );
    RenderTexture* createAdditive( Sprite* label, Color3B color, GLubyte opacity, int additiveCount);
    Sprite* getSpriteShapeSolidColorSprite(const char* src, Color3B color);
    Layer* getGridLayer();
    void setSpriteEffect(Sprite* spt, int effect);
    void reset();
    void backup();
    void setTimeLeft(TextBMFont* lbl, long time);
    void setTimeLeft(Text* lbl, long time);
    void setTimeLeft(Label* lbl, long time);
    std::string getTimeLeftInString(long time);
    std::string getTimeLeftInStringHMS(long time);
    void initAchievement();
    void setAchievementGoal(int achievementIndex, int goalCount);
    bool setAchievement(int achievementIndex, int countToAdd);
    const char* getAchievementId(int achievementIndex);
    void googleSignInOrOut(bool sign);
    bool isGoogleSigned;
    int getAchievementGoalCount(int achievementIndex);
    int getAchievementCurrentCount(int achievementIndex);
     
	int getIndexOf(std::vector<int> vec, int index);
     
    void nativeControllerButtonEvent(int controller, int button, bool isPressed, float value, bool isAnalog);
    
    void nativeControllerLAxisXEvent(int controller, float value);
    void nativeControllerLAxisYEvent(int controller, float value);
    
    Color3B getRandomColor();
    
    void fillLayerWithTile(Layer* layer, std::string sptName);
    int getPointLeftToNextEffect();
    
    
    // plugin admob
    void adViewDidReceiveAd(const std::string &name) ;
    void adViewDidFailToReceiveAdWithError(const std::string &name, const std::string &msg) ;
    void adViewWillPresentScreen(const std::string &name) ;
    void adViewDidDismissScreen(const std::string &name) ;
    void adViewWillDismissScreen(const std::string &name) ;
    void adViewWillLeaveApplication(const std::string &name) ;
    void reward(const std::string &name, const std::string &currency, double amount) ;
    
    // plugin iap
    // iap methods
    std::string strPrices[9];
    virtual void onInitialized(bool ok) override;
    virtual void onSuccess(sdkbox::Product const& p) override;
    virtual void onFailure(sdkbox::Product const& p, const std::string &msg)
    override;
    virtual void onCanceled(sdkbox::Product const& p) override;
    virtual void onRestored(sdkbox::Product const& p) override;
    virtual void onProductRequestSuccess(std::vector<sdkbox::Product> const &products)
    override;
    virtual void onProductRequestFailure(const std::string &msg) override;
    virtual void onRestoreComplete(bool ok, const std::string &msg) override;
};

#endif
