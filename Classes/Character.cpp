//
//  Animal.cpp
//  Zoobyssrium
//
//  Created by PACKSUNG PILL on 1/14/17.
//
//

#include "Character.h"
#include "GameManager.h"
#include "TimeManager.h"
bool Character::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Widget::init() )
    {
        return false;
    }
    size = Director::getInstance()->getWinSize();
    forbiddenRect = Rect(1086, 201, 558, 1000);
    int offset = 30;
    entireRect = Rect(offset, offset, 2668 - offset*2, 364 - offset);
    moveRect0 = Rect(offset, offset, forbiddenRect.origin.x, entireRect.size.height - offset);
    moveRect1 = Rect(forbiddenRect.origin.x, 0, entireRect.size.width - forbiddenRect.origin.x - offset, forbiddenRect.origin.y);
    moveRect2 = Rect(offset, forbiddenRect.origin.y, forbiddenRect.origin.x - offset, entireRect.size.height - forbiddenRect.origin.y - offset);//moveRect0.size.height - (forbiddenRect.origin.y + forbiddenRect.size.height));
    moveRect3 = Rect(forbiddenRect.origin.x + forbiddenRect.size.width, forbiddenRect.origin.y, entireRect.size.width - (forbiddenRect.origin.x + forbiddenRect.size.width) - offset, entireRect.size.height - forbiddenRect.origin.y - offset);
    relicGiveTimer = 0;
    return true;
}
void Character::showTalkBox(int talkType, float persistTime, std::string text){
    removeTalkBox(0);
    std::string fileName = "";
    std::string iconName = "";
    float scale = 1;
    Point offset = Point::ZERO;
//    Node* child;
    if(talkType == TALK_BOX_LIFE){
        fileName = "talkBoxIcon.csb";
//        child = Sprite::create("text_buble_s_leaf.png");
        iconName = "text_buble_s_leaf.png";
    }else if(talkType == TALK_BOX_RELIC){
        fileName = "talkBoxIcon.csb";
        //        child = Sprite::create("text_buble_s_leaf.png");
        iconName = "icon_relic.png";
        offset += Point(0, 6);
        scale = 0.5f;
        
//    }else if(talkType == TALK_BOX_GEM){
//        fileName = "talkBoxIcon.csb";
//        iconName = "text_buble_s_exmark.png";
    }else{// if(talkType == TALK_BOX_TALK){
        if(text.size() > 19){
            fileName = "talkBox2.csb";
        }else if(text.size() > 9){
            fileName = "talkBox1.csb";
        }else {
            fileName = "talkBox0.csb";
        }
    }
    
    talkBox = CSLoader::createNode(fileName);
    talkBox->setPosition(this->getPosition() + Point(anim->getContentSize().width/2, anim->getBoundingBox().size.height));
//    this->getParent()->addChild(talkBox,88888880);
    this->addChild(talkBox,88888880);
    
    Node* bg = talkBox->getChildByName("sptBackground");
    
    if(iconName.size() > 0){
        ImageView* image = (ImageView*)talkBox->getChildByName("sptBackground")->getChildByName("sptIcon");
        image->loadTexture(iconName);
        image->setScale(image->getScale()*scale);
        image->setPosition(image->getPosition() + offset);
        Button* btn = (Button*)bg;
        btn->setTag(talkType);
        btn->addClickEventListener(CC_CALLBACK_1(Character::onTalkTouched, this));
    }
    if(text.size() > 0){
        Text* lbl = (Text*)bg->getChildByName("lblText");
        lbl->setString(text);
    }
    this->schedule(schedule_selector(Character::updateTalkboxPosition));
}
/*
void Character::showTalkBox(int talkType, float persistTime, std::string text){
    removeTalkBox(0);
    std::string fileName = "text_buble_fram_1.png";
    Node* child;
    if(talkType == TALK_BOX_LIFE){
//        fileName = "text_buble_fram_1.png";
        child = Sprite::create("text_buble_s_leaf.png");
    }else if(talkType == TALK_BOX_GEM){
        fileName = "text_buble_fram_1.png";
        child = Sprite::create("text_buble_s_exmark.png");
    }else{// if(talkType == TALK_BOX_TALK){
        if(text.size() > 19){
            fileName = "text_buble_fram_4.png";
        }else if(text.size() > 9){
            fileName = "text_buble_fram_3.png";
        }else {
            fileName = "text_buble_fram_2.png";
        }
        Label* lbl = GameManager::getInstance()->getLocalizedLabel();
        GameManager::getInstance()->setFontSize(lbl, 90);
        lbl->setTextColor(Color4B::BLACK);
        lbl->setString(text);
        child = lbl;
    }
    btnTalkBox = Button::create(fileName);
    btnTalkBox->addClickEventListener(CC_CALLBACK_1(Character::onTalkTouched, this));
    child->setPosition(btnTalkBox->getContentSize()/2);
    this->addChild(btnTalkBox);
    
    btnTalkBox->setPosition(Point(anim->getContentSize().width/2, anim->getBoundingBox().size.height));
    
    btnTalkBox->addChild(child);
    this->scheduleOnce(schedule_selector(Character::removeTalkBox), persistTime);
}*/
void Character::onTalkTouched(Ref* ref){
    int index =  ((Button*)ref)->getTag();
    if(index == TALK_BOX_LIFE){
        GameManager::getInstance()->getWorld()->onLifeOnAnimalTouched(this);
    }else if(index == TALK_BOX_GEM){
        GameManager::getInstance()->getWorld()->onGemOnAnimalTouched(this);
    }else if(index == TALK_BOX_RELIC){
        GameManager::getInstance()->getWorld()->onRelicOnAnimalTouched(this);
    }else if(index == TALK_BOX_TALK){
        
    }
    removeTalkBox(0);
}
void Character::removeTalkBox(float dt){
    this->unschedule(schedule_selector(Character::updateTalkboxPosition));
    if(talkBox != nullptr){
        talkBox->removeFromParentAndCleanup(true);
        talkBox = nullptr;
    }
}
void Character::setSpine(int index){
    float scale = 0.5f;
    std::string strScale = GameManager::getInstance()->getWorld()->animalScale.at(index);
    scale *= atoi(strScale.substr(0, strScale.size()-1).c_str())*0.01f;
    name = GameManager::getInstance()->getWorld()->animalJson.at(index);
    std::string atlas = GameManager::getInstance()->getWorld()->animalAtlas.at(index);
    if(name.size() <= 0){
        name = GameManager::getInstance()->getWorld()->animalJson.at(0);
        atlas = GameManager::getInstance()->getWorld()->animalAtlas.at(0);
    }
    anim = SkeletonAnimation::createWithJsonFile(name, atlas, scale);
    addChild(anim);
}
void Character::setSpine(std::string json, std::string atlas){
    float scale = 1.0f;
    anim = SkeletonAnimation::createWithJsonFile(json, atlas, scale);
    name = json;
    addChild(anim);
}
void Character::setSpineAnimation(int index){
    charIndex = index;
    /*if (index == 0) {
    anim = SkeletonAnimation::createWithJsonFile("ape_1.json.txt", "ape_1.atlas", 0.5f);
    }else if (index == 1) {
        anim = SkeletonAnimation::createWithJsonFile("ape_2.json.txt", "ape_2.atlas", 0.5f);
    }else if (index == 2) {
        anim = SkeletonAnimation::createWithJsonFile("ape_3.json.txt", "ape_3.atlas", 0.5f);
    }else if (index == 3) {
        anim = SkeletonAnimation::createWithJsonFile("hipo_1.json.txt", "hipo_1.atlas", 0.5f);
    }else if (index == 4) {
        anim = SkeletonAnimation::createWithJsonFile("hipo_2.json.txt", "hipo_2.atlas", 0.5f);
    }else if (index == 5) {
        anim = SkeletonAnimation::createWithJsonFile("hipo_3.json.txt", "hipo_3.atlas", 0.5f);
    }else if (index == 6) {
        anim = SkeletonAnimation::createWithJsonFile("rhino_1.json.txt", "rhino_1.atlas", 0.5f);
    }else if (index == 7) {
        anim = SkeletonAnimation::createWithJsonFile("rhino_2.json", "rhino_2.atlas", 0.5f);
//        anim = SkeletonAnimation::createWithJsonFile("spineboy.json", "spineboy.atlas", 0.5f);
    }else if (index == 8) {
        anim = SkeletonAnimation::createWithJsonFile("rhino_3.json.txt", "rhino_3.atlas", 0.5f);
    }else if (index == 9) {
        anim = SkeletonAnimation::createWithJsonFile("eleph_1.json.txt", "eleph_1.atlas", 0.5f);
    }else if (index == 10) {
        anim = SkeletonAnimation::createWithJsonFile("eleph_2.json.txt", "eleph_2.atlas", 0.5f);
    }else if (index == 11) {
        anim = SkeletonAnimation::createWithJsonFile("eleph_3.json.txt", "eleph_3.atlas", 0.5f);
    }*/
    float scale = 0.5f;
    std::string strScale = GameManager::getInstance()->getWorld()->animalScale.at(index);
    scale *= atoi(strScale.substr(0, strScale.size()-1).c_str())*0.01f;
    name = GameManager::getInstance()->getWorld()->animalJson.at(index);
    anim = SkeletonAnimation::createWithJsonFile(name, GameManager::getInstance()->getWorld()->animalAtlas.at(index), scale);
    
//    anim->setAnimation(0, "idle", true);
//    anim->setAnimation(1, "empty", false);
//    anim->addAnimation(1, "gungrab", false, 2);
    
    addChild(anim);
    setPosForFirstTime();
    startMove();

    anim->update(0);
//    height = anim->getBoundingBox().size.height;
//    anim->setPositionY(height/2);
    
//    DrawNode* draw = DrawNode::create();
//    draw->drawRect(Point(0, 0), anim->getBoundingBox().size, Color4F::RED);
//    this->addChild(draw);
    anim->setAnchorPoint(Point(0, 0));
    this->setAnchorPoint(Point(0, 0));
//    this->setPosition(getMovePoint());
    float extraY = 0;
    if (this->name.find("bird") != std::string::npos) {
        extraY += 400;
    }
    if (rand()%2==0) {
        this->setPosition(GameManager::getInstance()->getRandomPointFromRect(moveRect0) + Point(0, extraY));
    }else{
        this->setPosition(GameManager::getInstance()->getRandomPointFromRect(moveRect3) + Point(0, extraY));
    }
    
    
//    this->runAction(RepeatForever::create(RotateBy::create(1, 130)));
}
void Character::startMove(){
//    this->schedule(schedule_selector(Character::moveAuto), 0.1f);
}
void Character::moveAuto(float dt){
    
    relicGiveTimer -= dt;
    if (relicGiveTimer < 0) {
        relicGiveTimer = 120; // test
        //        std::string strKey = StringUtils::format("relic_get_today_%d", this->ID);
        //        std::string todayKey = "today_number";
        int todayNumber = UserDefault::getInstance()->getIntegerForKey("today_number", -1);
        int today = (int)(TimeManager::getInstance()->getCurrentTime()/(3600*24)); //
        //        int today = (int)(TimeManager::getInstance()->getCurrentTime()/(15)); // test
        //        log("what day: %d, %d", today, (int)((TimeManager::getInstance()->getCurrentTime() + 24*60*60)/(3600*24)));
        if (today != todayNumber) {
            int totalSpawnedAnimalCount = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
            for (int i = 0; i < totalSpawnedAnimalCount; i++) {
                UserDefault::getInstance()->setIntegerForKey(StringUtils::format("relic_get_today_%d", i).c_str(), 0);
            }
            
            log("edited relic: %d", UserDefault::getInstance()->getIntegerForKey(StringUtils::format("relic_get_today_%d", this->ID).c_str(), 0));
            UserDefault::getInstance()->setIntegerForKey("today_number", today);
        }
        int todayGiveCount = UserDefault::getInstance()->getIntegerForKey(StringUtils::format("relic_get_today_%d", this->ID).c_str(), 0);
        log("animal relic today: %d, %d, id: %d, today relic count: %d", todayNumber, today, this->ID, todayGiveCount);
        //        GameManager::getInstance()->getWorld()->showMessage(StringUtils::format("%d, %d, id:%d, %d",todayNumber, today, this->ID, todayGiveCount));
        if (todayGiveCount < 5 && !relicOnAndNotRecieved) {
            relicRequested = true;
            relicOnAndNotRecieved = true;
            //            todayGiveCount++;
            //            UserDefault::getInstance()->setIntegerForKey(strTodayGiveKey, todayGiveCount);
            //            UserDefault::getInstance()->flush();
        }
    }
    
    nextActionTimeLeft -= dt;
    if (nextActionTimeLeft > 0) {
        if (isMoving) {
//            Point source = getCurrentPoint();
//            anim->setScale(targetPos.x > source.x?-anim->getScaleY():anim->getScaleY(), anim->getScaleY());
            if(this->getParent() == nullptr){
                this->setPosition(this->getPosition() + moveBit*dt/moveTime);
            }else{
                if (this->getActionByTag(55) == nullptr) {
                    MoveBy* move = MoveBy::create(nextActionTimeLeft, moveBit*(nextActionTimeLeft)/moveTime);
                    move->setTag(55);
                    this->runAction(move);
                }
            }
        }
        return;
    }
//     test 
//    return;
//    nextActionTimeLeft = 5.0f;
//    this->runAction(MoveTo::create(nextActionTimeLeft, Point(90, this->getPosition().y> 200?0:400)));
//    return;
    
    int ani = rand()%4;
    if (stayCount > 0) {
        stayCount--;
        ani = rand()%3;
    }
    isMoving = false;
    if (this->name.find("bird") != std::string::npos) {
        ani = CHARACTER_ANI_WALK;
    }
    
    this->stopActionByTag(55);
    if (ani == CHARACTER_ANI_IDLE) {
        nextActionTimeLeft = runAnimation(ani, 1);
    }else if (ani == CHARACTER_ANI_IDLE2) {
        nextActionTimeLeft = runAnimation(ani, 1);
    }else if (ani == CHARACTER_ANI_IDLE3) {
        this->stopActionByTag(55);
    }else if (ani == CHARACTER_ANI_WALK) {
        Point targetPos = getMovePoint();
        isMoving = true;
        int speed = rand()%200 + 200;
        if (this->name.find("bird") != std::string::npos) {
            targetPos.y += 400;
            speed *= 2;
        }
        Point source = getCurrentPoint();
        float distance = targetPos.getDistance(source);
        
        float speedScale = 4;
        float dur = distance*speedScale/speed;
//        MoveTo* move = MoveTo::create(dur, targetPos);
//        move->setTag(55);
//        this->runAction(move);
        anim->setScale(targetPos.x > source.x?-anim->getScaleY():anim->getScaleY(), anim->getScaleY());
        moveSpeed = speed;
        moveTime = dur;
        moveBit = (targetPos - getPosition());
        nextActionTimeLeft = dur;
        runAnimation(ani, speed/200.0f);
    }
}
void Character::setID(){
    int nextID = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
    ID = nextID;
    nextID++;
    UserDefault::getInstance()->setIntegerForKey(KEY_NEXT_ID, nextID);
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(KEY_ANIMAL_INDEX_FOR_ID_FORMAT, ID).c_str(), charIndex);
}
void Character::setArea(int theArea){
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(KEY_ANIMAL_AREA_FOR_ID_FORMAT, ID).c_str(), theArea);
    this->area = theArea;
}
void Character::loadArea(){
    area = UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ANIMAL_AREA_FOR_ID_FORMAT, ID).c_str(), 0);
}
float Character::runAnimation(int ani, float timeScale){
    string aniName;
    int trackIndex =0;
    bool loop = true;
    if (ani == CHARACTER_ANI_IDLE) {
        aniName = "idle";
    }else if (ani == CHARACTER_ANI_IDLE2) {
        aniName = "idle2";
    }else if (ani == CHARACTER_ANI_IDLE3) {
        aniName = "idle3";
    }else if (ani == CHARACTER_ANI_WALK) {
        if(charIndex == 32){
            aniName = "run";
        }else{
            aniName = "walk";
        }
    }else if (ani == CHARACTER_ANI_RUN) {
        aniName = "run";
    }
//    log("run ani: %s, %s", name.c_str(), aniName.c_str());
    spTrackEntry* entry =  anim->setAnimation(trackIndex, aniName, loop);
    if (entry == nullptr) {
        entry =  anim->setAnimation(trackIndex, "fly_1", loop);
    }
    if (entry == nullptr) {
        entry =  anim->setAnimation(trackIndex, "idle", loop);
    }
    if (entry == nullptr) {
        GameManager::getInstance()->showDisposableMessage(StringUtils::format("Index: %d, There is no animation named \"%s\"", charIndex, aniName.c_str()).c_str(), this->getParent());
        return 0;
    }
    
    anim->setTimeScale(timeScale);
    return entry->endTime;
}

float Character::runAnimation(std::string aniName, bool loop){
    spTrackEntry* entry =  anim->setAnimation(0, aniName, loop);
    return entry->endTime;
}
float Character::runAnimation(std::string aniName){
    return runAnimation(aniName, false);
}

float Character::getIdleTime(int index){
    return 1;
}
void Character::setPosForFirstTime(){
    int index = rand()%4;
    Rect rect;
    if (index == 0) {
        rect = moveRect0;
    }else if (index == 1) {
        rect = moveRect1;
    }else if (index == 2) {
        rect = moveRect2;
    }else if (index == 3) {
        rect = moveRect3;
    }
    setCurrentPoint(Point(rect.origin.x + rand()%((int)rect.size.width), rect.origin.y + rand()%((int)rect.size.height)));
}
cocos2d::Point Character::getMovePoint(){
    int targetArea = rand()%4;
    if (targetArea == 0) {
        return GameManager::getInstance()->getRandomPointFromRect(moveRect0);
    }else if (targetArea == 1) {
        return GameManager::getInstance()->getRandomPointFromRect(moveRect1);
    }else if (targetArea == 2) {
        return GameManager::getInstance()->getRandomPointFromRect(moveRect2);
    }else if (targetArea == 3) {
        return GameManager::getInstance()->getRandomPointFromRect(moveRect3);
    }
    
    Point currentPos = getCurrentPoint();
    if (this->name.find("bird") != std::string::npos) {
        currentPos.y -= 400;
    }
    if (moveRect0.containsPoint(currentPos)) {
        if (currentPos.y > forbiddenRect.origin.y + forbiddenRect.size.height) {
            if (rand()%2 == 0) {
                return GameManager::getInstance()->getRandomPointFromRect(Rect(moveRect2.origin.x, moveRect2.origin.y, entireRect.size.width - moveRect0.origin.x, moveRect2.size.height));
            }else{
                return GameManager::getInstance()->getRandomPointFromRect(moveRect0);
            }
        }else if (currentPos.y < forbiddenRect.origin.y) {
            if (rand()%2 == 0) {
                return GameManager::getInstance()->getRandomPointFromRect(Rect(moveRect1.origin.x, moveRect1.origin.y, entireRect.size.width - moveRect0.origin.x-50, moveRect1.size.height));
            }else{
                return GameManager::getInstance()->getRandomPointFromRect(moveRect0);
            }
        }else{
            return GameManager::getInstance()->getRandomPointFromRect(moveRect0);
        }
    }else if (moveRect1.containsPoint(currentPos)) {
        if (rand()%3 == 0) {
            return GameManager::getInstance()->getRandomPointFromRect(Rect(moveRect0.origin.x, moveRect0.origin.y, moveRect0.size.width, moveRect1.size.height));
        }else if (rand()%3 == 0) {
            return GameManager::getInstance()->getRandomPointFromRect(Rect(moveRect3.origin.x, moveRect3.origin.y, moveRect3.size.width, moveRect1.size.height));
        }else{
            return GameManager::getInstance()->getRandomPointFromRect(moveRect1);
        }
    }else if (moveRect2.containsPoint(currentPos)) {
        if (rand()%3 == 0) {
            return GameManager::getInstance()->getRandomPointFromRect(Rect(moveRect0.origin.x, moveRect2.origin.y, moveRect0.size.width, moveRect2.size.height));
        }else if (rand()%3 == 0) {
            return GameManager::getInstance()->getRandomPointFromRect(Rect(moveRect3.origin.x, moveRect2.origin.y, moveRect3.size.width, moveRect2.size.height));
        }else{
            return GameManager::getInstance()->getRandomPointFromRect(moveRect2);
        }
    }else if (moveRect3.containsPoint(currentPos)) {
        if (currentPos.y > forbiddenRect.origin.y + forbiddenRect.size.height) {
            if (rand()%2 == 0) {
                return GameManager::getInstance()->getRandomPointFromRect(Rect(moveRect0.origin.x, moveRect2.origin.y, entireRect.size.width - moveRect3.origin.x, moveRect2.size.height));
            }else{
                return GameManager::getInstance()->getRandomPointFromRect(moveRect3);
            }
        }else if (currentPos.y < forbiddenRect.origin.y) {
            if (rand()%2 == 0) {
                return GameManager::getInstance()->getRandomPointFromRect(Rect(moveRect0.origin.x, moveRect0.origin.y, entireRect.size.width - moveRect3.origin.x, moveRect1.size.height));
            }else{
                return GameManager::getInstance()->getRandomPointFromRect(moveRect3);
            }
        }else{
            return GameManager::getInstance()->getRandomPointFromRect(moveRect3);
        }
    }
    return Point(400, 50);
}
void Character::setCurrentPoint(Point pos){
    this->setPosition(pos);
}
cocos2d::Point Character::getCurrentPoint(){
    return getPosition();
}
void Character::readyForFriend(){
//    this->unschedule(schedule_selector(Character::moveAuto));
//    freezed = true;
    this->stopActionByTag(55);
    Point pos = getPosition();
    int leftStartX = 1080;
    int leftEndX = 1150;
    int half = 1334;
    int rightStartX = half + (half -leftEndX);
    int rightEndX = half*2 - leftStartX;
    
    Point target;
    if (pos.x > half) {
        target = Point(rightStartX + rand()%(rightEndX - rightStartX), entireRect.origin.y+rand()%(int)entireRect.size.height);
    }else{
        target = Point(leftStartX + rand()%(leftEndX - leftStartX), entireRect.origin.y+rand()%(int)entireRect.size.height);
    }
    float distance = target.getDistance(pos);
    int speed = 600;
    float speedScale = 4;
    float dur = distance*speedScale/speed;
//    this->stopActionByTag(55);
    moveSpeed = speed;
    moveTime = dur;
    moveBit = (target - getPosition());
    this->stopAllActions();
//    this->runAction(Sequence::create(MoveTo::create(dur, target), CallFunc::create(CC_CALLBACK_0(Character::stayForAWhile, this)), NULL));
    this->runAction(Sequence::create(DelayTime::create(dur), CallFunc::create(CC_CALLBACK_0(Character::stayForAWhile, this)), NULL));
    stayCount = 5;
    anim->setScale(target.x > pos.x?-anim->getScaleY():anim->getScaleY(), anim->getScaleY());
    
    nextActionTimeLeft = dur;
    
    if (lastAnimation != CHARACTER_ANI_WALK) {
        runAnimation(CHARACTER_ANI_WALK, speed/200.0f);
    }
}
void Character::goBackToNormalLife(){
//    this->schedule(schedule_selector(Character::moveAuto), 0.1f);
//    freezed = false;
}
void Character::stayForAWhile(){
    stayCount = 5;
    runAnimation(CHARACTER_ANI_IDLE2, 1);
    anim->setScale(getPosition().x < 1334?-anim->getScaleY():anim->getScaleY(), anim->getScaleY());
    goBackToNormalLife();
}

void Character::updateTalkboxPosition(float dt){
    if(talkBox != nullptr){
        if (charIndex == 31 || charIndex == 42) {
//            talkBox->setPosition(this->getPosition() + Point(anim->getContentSize().width/2, anim->getBoundingBox().size.height/2));
            talkBox->setPosition(Point(anim->getContentSize().width/2, anim->getBoundingBox().size.height/2));
        }else{
//            talkBox->setPosition(this->getPosition() + Point(anim->getContentSize().width/2, anim->getBoundingBox().size.height));
            talkBox->setPosition(Point(anim->getContentSize().width/2, anim->getBoundingBox().size.height));
        }
    }
}
