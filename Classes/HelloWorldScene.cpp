#include "HelloWorldScene.h"
#include "TimeManager.h"
#include "SimpleAudioEngine.h"
#include "NativeInterface.h"
#include "GameManager.h"
#ifdef SDKBOX_ENABLED
#include "PluginSdkboxPlay/PluginSdkboxPlay.h"
#include "PluginAdMob/PluginAdMob.h"
#include "PluginIAP/PluginIAP.h"
#endif
USING_NS_CC;

Scene* HelloWorldScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorldScene::create();
    layer->setName("world");
    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorldScene::init()
{    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    sdkbox::PluginSdkboxPlay::init();
    sdkbox::PluginSdkboxPlay::signin();
    log("sdkboxplay isSignedIn: %d", sdkbox::PluginSdkboxPlay::isSignedIn());
//    UserDefault::getInstance()->setBoolForKey("share_tutorial", false); // test
    int selectedArea = UserDefault::getInstance()->getIntegerForKey(KEY_SELECTED_AREA, 0);
    loadAnimalCSV();
    loadRelicCSV();
    loadAnimalPriceCsv();
    GameManager::getInstance()->setWorld(this);
    
    absentReward = new BigNum();
    absentReward->resetNum();
//    TimeManager::getInstance()->getHttpTime();
    theLife = new BigNum();
    theLife->setNumFromFullNumberByDotSplitString(UserDefault::getInstance()->getStringForKey(KEY_LIFE, "0"));
    totalSanhoEffectPerSec = new BigNum();
    size = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
//    BigNum* num = new BigNum();
//    num->addNum(200, 0);
//    num->addNum(1, 1);
//    log("num: %s", num->getExpression().c_str());
//    num->multiply(1/3.0f);
//    log("num: %s", num->getExpression().c_str());
    
    tapReward = new BigNum();
    
    GameManager::getInstance()->refreshOrientationNeeded = true;
    btnDefense = nullptr;
    
    // closeCaptureMode.png
    // capture_mode.png
    // capture_on.png
    btnScreenCapture = Button::create("capture_mode.png", "capture_mode.png", "capture_mode.png");
    this->addChild(btnScreenCapture, 10);
    btnScreenCapture->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onScreenCaptureClick, this));
    btnScreenCapture->setPosition(Point(0, -50));
    
    btnScreenCaptureAll = Button::create("capture_off.png", "capture_on.png", "capture_on.png");
    this->addChild(btnScreenCaptureAll, 10);
    btnScreenCaptureAll->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onScreenCaptureAllClick, this));
    btnScreenCaptureAll->setVisible(false);
    
    // test
//    lblTimeTest = Label::createWithTTF("", "arial.ttf", 20);
//    this->addChild(lblTimeTest, 10);
//    lblTimeTest->setAnchorPoint(Point(0, 1));
//    lblTimeTest->setPosition(Point(20, 1334 - 300));
    
//    lblHunterTimeLeft = Label::createWithTTF("Hunter Time Left: ", "GodoB.ttf", 24);
//    lblHunterTimeLeft->setAnchorPoint(Point(0, 1));
//    lblHunterTimeLeft->setPosition(Point(20, 1334 - 200));
//    this->addChild(lblHunterTimeLeft, 10);
//    lblHunterTimeLeft->enableOutline(Color4B::BLACK, 2);
    
//    lblHunterLastTime = Label::createWithTTF("Hunter Last Hour: ", "GodoB.ttf", 24);
//    lblHunterLastTime->setAnchorPoint(Point(0, 1));
//    lblHunterLastTime->setPosition(Point(20, 1334 - 230));
//    this->addChild(lblHunterLastTime, 10);
//    lblHunterLastTime->enableOutline(Color4B::BLACK, 2);
//
//    lblHunterNextTime = Label::createWithTTF("Hunter Next Hour: ", "GodoB.ttf", 24);
//    lblHunterNextTime->setAnchorPoint(Point(0, 1));
//    lblHunterNextTime->setPosition(Point(20, 1334 - 260));
//    this->addChild(lblHunterNextTime, 10);
//    lblHunterNextTime->enableOutline(Color4B::BLACK, 2);
    
    btnQuest = Button::create("daily_quest_off.png","daily_quest_on.png","daily_quest_off.png");
    this->addChild(btnQuest, 10);
    btnQuest->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onDailyQuestClick, this));
    btnQuest->setPosition(Point(0, -50));
    addRedBean(btnQuest, Point(15, 30));
    
    btnSetting = Button::create("option_off.png","option_on.png","option_off.png");
    this->addChild(btnSetting, 10);
    btnSetting->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onSettingClick, this));
    btnSetting->setPosition(Point(0, -50));
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    btnAchievement = Button::create("achievement_2.png","achievement_2.png","achievement_2.png");
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC
    btnAchievement = Button::create("achievement_2.png","achievement_2.png","achievement_2.png");
#endif
    this->addChild(btnAchievement, 10);
    btnAchievement->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onAchievementServiceClick, this));
    btnAchievement->setPosition(Point(0, -50));
    
    btnRecord = Button::create("btnRecord.png");
    this->addChild(btnRecord, 99999);
    btnRecord->setVisible(false);
    btnRecord->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onRecordClick, this));
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
//    btnRecord->setVisible(true);
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC
    
#endif
    loadedLayer = CSLoader::createNode("HorizontalLayer.csb");
    loadedLayer->setContentSize(size);
    loadedLayer->setPosition(Point::ZERO);
    loadedLayer->setAnchorPoint(Point::ZERO);
    cocostudio::timeline::ActionTimeline* action = CSLoader::createTimeline("HorizontalLayer.csb");
    loadedLayer->runAction(action);
    action->gotoFrameAndPlay(0, true);
    
    ui::Helper::doLayout(loadedLayer);
    this->addChild(loadedLayer, 1);
    
//    btnBlocker = loadedLayer->getChildByName("btnBlocker");
//    btnBlocker->setVisible(false);
    
    Button* btnAppear = (Button*)loadedLayer->getChildByName("btnAppear");
    btnAppear->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::OnAppearClick, this));
    btnAppear->setPositionX(btnAppear->getPositionX() + 750);
    
    Button* btnHidden = (Button*)loadedLayer->getChildByName("btnHidden");
    btnHidden->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::OnHideClick, this));
    btnHidden->setPositionX(btnHidden->getPositionX() + 750);
    
    lblLife = (Text*)loadedLayer->getChildByName("pnlStatus")->getChildByName("lblLifeCount");
    
    itemTree = (ImageView*)loadedLayer->getChildByName("itemTree");
    ImageView* sptIcon = (ImageView*)itemTree->getChildByName("sptIcon");
    sptIcon->loadTexture("res/ui-icons/fild_0.png");
    Button* btn = (Button*)itemTree->getChildByName("btnUpgrade");
    tabObjects.pushBack(itemTree);
    btnUpgrade10 = (Button*)btn->clone();
    btnUpgrade10->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onUpgrade10Click, this));
    btnUpgrade10->setScale(0.8f);
    btnUpgrade10->setPosition(Point(-1000, 0));
    
    btnUpgrade100 = (Button*)btn->clone();
    btnUpgrade100->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onUpgrade100Click, this));
    btnUpgrade100->setScale(0.8f);
    btnUpgrade100->setPosition(Point(-1000, 0));
    
    itemRelic = (ImageView*)loadedLayer->getChildByName("itemRelic");
    for (int i = 0; i < 5; i++) {
        btn = (Button*)itemRelic->getChildByName(StringUtils::format("btn%d", i));
        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onRelicSlotClick, this));
        ((Text*)itemRelic->getChildByName(StringUtils::format("slot%d", i))->getChildByName("lblPrice"))->setString(Value(unlockRelicSlotPrice).asString());
    }
    btn = (Button*)itemRelic->getChildByName("btnUpgrade");
    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onGochaRelicClick, this));
    addRedBean(btn);
    btn = (Button*)itemRelic->getChildByName("btnManage");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onRelicManageClick, this));
    
    itemCharacter = (ImageView*)loadedLayer->getChildByName("itemAnimal");
    tabObjects.pushBack(itemCharacter);
    btn = (Button*)itemCharacter->getChildByName("btnManageAnimal");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onManageCharacterClick, this));
    btn = (Button*)itemCharacter->getChildByName("itemAnimal_0")->getChildByName("btnUpgrade");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onPickNormalAnimalClick, this));
    addRedBean(btn);
    btn = (Button*)itemCharacter->getChildByName("itemAnimal_1")->getChildByName("btnUpgrade");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onPickRareAnimalClick, this));
    addRedBean(btn);
    
    updateAnimalPickTab();
    Text* lbl = (Text*)itemCharacter->getChildByName("itemAnimal_0")->getChildByName("btnUpgrade")->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString(lbl, "create");
    lbl = (Text*)itemCharacter->getChildByName("itemAnimal_1")->getChildByName("btnUpgrade")->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString(lbl, "create");
    
    lbl = (Text*)itemCharacter->getChildByName("itemAnimal_0")->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString(lbl, "get normal animal");
    lbl = (Text*)itemCharacter->getChildByName("itemAnimal_0")->getChildByName("lblDescription_0");
    GameManager::getInstance()->setLocalizedString(lbl, "normal animal description");
    lbl = (Text*)itemCharacter->getChildByName("itemAnimal_1")->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString(lbl, "get rare animal");
    lbl = (Text*)itemCharacter->getChildByName("itemAnimal_1")->getChildByName("lblDescription_0");
    GameManager::getInstance()->setLocalizedString(lbl, "rare animal description");
    lbl = (Text*)itemCharacter->getChildByName("btnManageAnimal")->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString(lbl, "manage");
    log("manage: %s", lbl->getString().c_str());
    svSkill = (ScrollView*)loadedLayer->getChildByName("svSkill");
    loadedLayer->addChild(btnUpgrade10);
    loadedLayer->addChild(btnUpgrade100);
    Widget* sanhoItemTemplete = (Widget*)svSkill->getChildByName("sanhoItem");
    itemTemplete = (Widget*)svSkill->getChildByName("item0");
    itemTemplete->addChild(((Widget*)sanhoItemTemplete->getChildByName("sptCheck0"))->clone());
    itemTemplete->addChild(((Widget*)sanhoItemTemplete->getChildByName("sptCheck1"))->clone());
    itemTemplete->addChild(((Widget*)sanhoItemTemplete->getChildByName("lblCondition0"))->clone());
    itemTemplete->addChild(((Widget*)sanhoItemTemplete->getChildByName("lblCondition1"))->clone());
    itemTemplete->addChild(((Widget*)sanhoItemTemplete->getChildByName("sptLock"))->clone());
    itemGapY = 6;
    itemHeight = 104;
    int itemCount = 1;
    int sanhoCount = SANHO_COUNT;
    svSkill->setInnerContainerSize(Size(750, (sanhoCount)*(itemGapY + itemHeight)));
    svSkill->setClippingEnabled(true);
    // main tree button
    for (int i = 0; i < 0; i++) {
        Widget* clone = itemTemplete->clone();
        svSkill->getInnerContainer()->addChild(clone);
        clone->setPosition(Point(12, svSkill->getInnerContainerSize().height - i*(itemGapY + itemHeight)));
//        clone->setName(__String::createWithFormat("item%d", i)->getCString());
        clone->setName("mainTree");
        btn = (Button*)clone->getChildByName("btnUpgrade");
        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onUpgradeDecoClick, this));
        btn->setTag(10000); // main tree
    }
    tabObjects.pushBack(svSkill);
    DrawNode* draw;
    const char* persistingSptName;
    const char* coolingSptName;
    for (int i = 0; i < 4; i++) {
        btn = (Button*)loadedLayer->getChildByName(__String::createWithFormat("btnSkill%d", i)->getCString());
        if (i == 0) {
            btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onAutoTapSkillClick, this));
        }else if (i == 1){
            btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onBigTapSkillClick, this));
        }else if (i == 2){
            btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onBuffSkillClick, this));
        }else if (i == 3){
            btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onResetClick, this));
        }
        btnSkillList.pushBack(btn);
        draw = DrawNode::create();
        btn->addChild(draw);
        draw->setName("draw");
        draw->setVisible(false);
//        btn->setVisible(false);
//        continue; // test
        persistingSptName = __String::createWithFormat("skill%d.png", i)->getCString();
        coolingSptName = __String::createWithFormat("skill%d.png", i)->getCString();
        
        Sprite* spt = Sprite::create(persistingSptName);
//        spt->setColor(Color3B::RED);
        auto timer = ProgressTimer::create(spt);
        timer->setType(ProgressTimer::Type::RADIAL);
        btn->addChild(timer);
//        timer->setReverseProgress(true);
        timer->setPosition(Point(btn->getContentSize().width/2, btn->getContentSize().height/2));
        timerPersistingList.pushBack(timer);
        
        
        spt = Sprite::create(coolingSptName);
        spt->setColor(Color3B::GRAY);
        timer = ProgressTimer::create(spt);
        timer->setType(ProgressTimer::Type::RADIAL);
        btn->addChild(timer);
//        timer->setReverseProgress(true);
        timer->setPosition(Point(btn->getContentSize().width/2, btn->getContentSize().height/2));
        timerCoolingList.pushBack(timer);
        btn->getChildByName("lblTime")->setLocalZOrder(101);
    }
    
    Node* item = itemTree;
    btn = (Button*)item->getChildByName("btnUpgrade");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onUpgradeMainTreeClick, this));
    
    Button* btnBonus = Button::create("res/fild_bounus.png");
    item->addChild(btnBonus);
    btnBonus->setPosition(Point(51, 50));
    btnBonus->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onBonusMainTreeClick, this));
    btnBonus->setVisible(false);
    btnBonus->setName("btnBonus");
    
    svCharacter = (ScrollView*)loadedLayer->getChildByName("svCharacter");
    svCharacter->setClippingEnabled(true);
    tabObjects.pushBack(svCharacter);
    
//    svCharacter->setInnerContainerSize(Size(750, itemCount*(itemGapY + itemHeight)));
    
    ImageView* icon, *sptAnimalCountBack;
    std::string str, name;
    for (int i = 0; i < animalNames.size(); i++) {
        break; // tap changed to animal draw
        Widget* clone = itemTemplete->clone();
        svCharacter->getInnerContainer()->addChild(clone);
        clone->setPosition(Point(12, svCharacter->getInnerContainerSize().height - i*(itemGapY + itemHeight)));
        clone->setName(__String::createWithFormat("item%d", i)->getCString());
        btn = (Button*)clone->getChildByName("btnUpgrade");
        clone->setTag(i);
        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onCreateCharacterClick, this));
        btn->setTag(i);
        
        sptAnimalCountBack = ImageView::create("animalCountBg.png");
        clone->addChild(sptAnimalCountBack);
        sptAnimalCountBack->setOpacity(100);
        sptAnimalCountBack->setPosition(Point(544, 76));
        sptAnimalCountBack->setName("animalCountBg");
        lbl = (Text*)clone->getChildByName("lblDescription_0");
        lbl->setTextVerticalAlignment(TextVAlignment::CENTER);
        lbl->setTextHorizontalAlignment(TextHAlignment::CENTER);
        lbl->setAnchorPoint(Point(0.5, 0.5));
        lbl->setPosition(sptAnimalCountBack->getPosition() + Point(0, 40));
        lbl->setLocalZOrder(100);
        lbl->setString("23/99");
        lbl->setTextColor(Color4B(250, 248, 214, 255));
        icon = (ImageView*)clone->getChildByName("sptIcon");
        if(animalAtlas.at(i).size() > 0){
            name = animalAtlas.at(i).substr(0, animalAtlas.at(i).size() - 6);
        }
        icon->loadTexture(StringUtils::format("res/ui-icons/icon_%s.png", name.c_str()));
    }
    
    // sanho
    for (int i = 0; i < sanhoCount; i++) {
        Widget* clone = sanhoItemTemplete->clone();
        svSkill->getInnerContainer()->addChild(clone);
        clone->setPosition(Point(12, svSkill->getInnerContainerSize().height - (i)*(itemGapY + itemHeight)));
        clone->setName(__String::createWithFormat("sanhoItem%d", i)->getCString());
        btn = (Button*)clone->getChildByName("btnUpgrade");
        btn->setTag(i);
        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onUpgradeSanhoClick, this));
        icon = (ImageView*)clone->getChildByName("sptIcon");
        icon->loadTexture(StringUtils::format("res/ui-icons/fild_%d.png", i + 1).c_str());
        
        Button* btnBonus = Button::create("res/fild_bounus.png");
        clone->addChild(btnBonus);
        btnBonus->setPosition(Size(51, 50));
        btnBonus->setTag(i);
        btnBonus->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onBonusSanhoClick, this));
        btnBonus->setVisible(false);
        btnBonus->setName("btnBonus");
    }
    sanhoItemTemplete->removeFromParentAndCleanup(true);
    
    updateTotalSanhoEffect();
    
    pnlUIBG = (Widget*)loadedLayer->getChildByName("pnlUIBG");
    pnlUIBG->setContentSize(Size(size.width, 585));
    pnlUIBG->setLocalZOrder(-9996);

//    Node* sun = loadedLayer->getChildByName("sunMoon");
//    sun->setLocalZOrder(-9997);
//    Node* sky = loadedLayer->getChildByName("sky");
//    sky->setLocalZOrder(-9998);
    
    svField = (ScrollView*)loadedLayer->getChildByName("svField");
    svField->retain();
    svField->setSwallowTouches(false);
    svField->isHoldTimeEnabled = true;
    svField->removeFromParentAndCleanup(false);
    svField->setScrollBarEnabled(false);
    this->addChild(svField);
    svField->release();
//    svField->setTouchEnabled(false);
    
//    svField->getInnerContainer()->removeAllChildren();  // test
    
    Sprite* sptLandBottom = Sprite::create("land_2.png");
    svField->addChild(sptLandBottom);
    sptLandBottom->setAnchorPoint(Point(0, 1));
    sptLandBottom->setPosition(Point(0, 0));
    sptLandBottom->setScaleX(1334*2);
    sptLandBottom->setScaleY(2);
    
    
    svCollection = (ScrollView*)loadedLayer->getChildByName("svCollection");
    svCollection->setClippingEnabled(true);
    tabObjects.pushBack(svCollection);
    itemCount = 5;
    int animalCount = (int)animalNames.size();
    int animalItemSize = 120;           int animalItemGap = 20;
    svCollection->setInnerContainerSize(Size(750, ((animalCount - 1)/5 + 1)*(animalItemGap + animalItemSize) + 40));
    for (int i = 0; i < animalCount; i++) {
        ImageView* clone = ImageView::create("res/icon_frame_1_120X120.png");
        clone->setContentSize(Size(120, 120));
        
        if(animalAtlas.at(i).size() > 0){
            name = animalAtlas.at(i).substr(0, animalAtlas.at(i).size() - 6);
        }
        
        Sprite* sptIcon = Sprite::create(StringUtils::format("res/ui-icons/icon_%s.png", name.c_str()));
        clone->addChild(sptIcon);
        sptIcon->setName("sptIcon");
        int animalCount = getTotalAnimalCount(i);
        
        clone->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onAnimalDetailClick, this));
        
//        if(isAnimalCondition0Meet(i) && isAnimalCondition1Meet(i) && animalCount > 0){
        if (getAccumulatedAnimalCount(i) > 0) {
            
        }else{
            sptIcon->setTexture(GameManager::getInstance()->getGrayScaleImage(StringUtils::format("res/ui-icons/icon_%s.png", name.c_str()).c_str())->getTexture());
            sptIcon->setOpacity(125);
            sptIcon->setColor(Color3B(70, 70, 70));
        }
//        clone->setScale(120.0f/92.0f);
        sptIcon->setPosition(Point(60, 60));
        svCollection->getInnerContainer()->addChild(clone);
        clone->setPosition(Point(12 + 60 + 20 + (i%5)*140, svCollection->getInnerContainerSize().height + animalItemGap - (i/5 + 1)*(animalItemGap + animalItemSize) + 30));
        clone->setTag(i);
        //        addStarToIcon(sptIcon, i);
        
        clone->setTouchEnabled(true);
        
        Label* lbl = Label::createWithTTF("Lv.99", "GodoB.ttf", 18);
        lbl->setAnchorPoint(Point(0, 1));
        lbl->setPosition(Point(6, 120 - 5));
        clone->addChild(lbl);
        lbl->setName("lblLevel");
        lbl->enableOutline(Color4B::BLACK, 2);
        
        Sprite* sptProgressBack = Sprite::create("progressBarBack.png");
        sptProgressBack->setPosition(Point(60, 15));
        clone->addChild(sptProgressBack);
        
        ImageView* sptProgress = ImageView::create("progressBar.png");
        sptProgress->setAnchorPoint(Point(0, 0.5));
        sptProgress->setPosition(Point(60 - sptProgress->getContentSize().width/2, 15));
        sptProgress->setCapInsets(Rect(5, 5, sptProgress->getContentSize().width - 10, sptProgress->getContentSize().height - 10));
        sptProgress->setScale9Enabled(true);
        sptProgress->setContentSize(Size(110, 20));
        clone->addChild(sptProgress);
        sptProgress->setName("sptProgress");
        
        
        lbl = Label::createWithTTF("1/10", "GodoB.ttf", 16);
        lbl->setPosition(Point(60, 15));
        clone->addChild(lbl, 10);
        lbl->setName("lblExp");
        
    }
    
    Widget* lblClone;
//    for (int i = 0; i < itemCount; i++) {
//        Widget* clone = sanhoItemTemplete->clone();
//        svCollection->getInnerContainer()->addChild(clone);
//        clone->setTag(i);
//        clone->getChildByName("sptCheck0")->removeFromParentAndCleanup(true);
//        clone->getChildByName("sptCheck1")->removeFromParentAndCleanup(true);
//        clone->getChildByName("lblCondition0")->removeFromParentAndCleanup(true);
//        clone->getChildByName("lblCondition1")->removeFromParentAndCleanup(true);
//        clone->setPosition(Point(12, svCollection->getInnerContainerSize().height + itemGapY - i*(itemGapY + itemHeight)));
//        clone->setName(__String::createWithFormat("item%d", i)->getCString());
//        ImageView* sptIcon = (ImageView*)clone->getChildByName("sptIcon");
//        sptIcon->loadTexture(StringUtils::format("magic_%d.png", i+1));
//        clone->getChildByName("sptLock")->removeFromParent();
//        ImageView* timeIcon = (ImageView*)clone->getChildByName("timeIcon");
//        timeIcon->loadTexture("icon_life.png");
////        clone->getChildByName("sptIconBg")->removeFromParentAndCleanup(true);
//
//        
//        btn = (Button*)clone->getChildByName("btnUpgrade");
//        btn->setTag(i);
//        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMagicItemClick, this));
//        btn->setEnabled(false);
//        sptIcon = (ImageView*)btn->getChildByName("sptIcon");
//        sptIcon->loadTexture("icon_dia_white.png");
//        Text* lbl = (Text*)btn->getChildByName("lblPrice");
//        lbl->setString(Value(getMagicItemPrice(i)).asString());
//        GameManager::getInstance()->alignToCenter(sptIcon, lbl, 5, btn->getContentSize().width/2, 0);
//        if(i == 0){
//            GameManager::getInstance()->setLocalizedString("free", lbl);
//            lbl->setPositionX(btn->getContentSize().width/2);
//            sptIcon->setVisible(false);
//        }
//    }
    
//    // test for google indie festival
//    bool isGoogleIndieFestival = false;
//    if(isGoogleIndieFestival){
//        UserDefault::getInstance()->setIntegerForKey(KEY_TAP_LEVEL, 800);
//        for (int i = 0; i < 10; i++) {
//            UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_SANHO_UNLOCKED_FORAMT, i).c_str(), true);
//            UserDefault::getInstance()->setIntegerForKey(StringUtils::format(KEY_SANHO_LEVEL_FORAMT, i).c_str(), 530);
//        }
//        for (int i = 0; i < 10; i++) {
//            UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_SANHO_UNLOCKED_FORAMT, i).c_str(), true);
//            UserDefault::getInstance()->setIntegerForKey(StringUtils::format(KEY_SANHO_LEVEL_FORAMT, i).c_str(), 530);
//        }
//    }
//    
    
    
    svRelic = (ScrollView*)loadedLayer->getChildByName("svRelic");
    svRelic->setClippingEnabled(true);
    tabObjects.pushBack(svRelic);
    svRelic->setInnerContainerSize(Size(750, itemCount*(itemGapY + itemHeight)));
    resetRelic();
    
    svMap = (ScrollView*)loadedLayer->getChildByName("svMap");
    svMap->setClippingEnabled(true);
    tabObjects.pushBack(svMap);
    itemCount = 0;//AREA_KIND;
    svMap->setInnerContainerSize(Size(750, itemCount*(itemGapY + itemHeight)));
    itemMap = (ImageView*)loadedLayer->getChildByName("itemMap");
    itemMap->setVisible(false);
    Label* lblComingSoon = Label::createWithTTF("a", "GodoB.ttf", 25);
    GameManager::getInstance()->setLocalizedString(lblComingSoon, "coming soon");
    svMap->addChild(lblComingSoon);
    Size containerSize = svMap->getInnerContainerSize();
    lblComingSoon->setPosition(Point(containerSize.width/2, containerSize.height - lblComingSoon->getContentSize().height/2 - 20));
    lblComingSoon->setColor(Color3B(70, 70, 70));
    
    for (int i = 0; i < itemCount; i++) {
        Widget* clone = itemTemplete->clone();
        svMap->getInnerContainer()->addChild(clone);
        clone->setPosition(Point(12, svMap->getInnerContainerSize().height + itemGapY - i*(itemGapY + itemHeight)));
        clone->setName(__String::createWithFormat("item%d", i)->getCString());
        clone->getChildByName("lblDescription_0")->removeFromParentAndCleanup(true);
        clone->getChildByName("sptIconBg")->removeFromParentAndCleanup(true);
        lblClone = ((Widget*)clone->getChildByName("lblDescription"))->clone();
        lblClone->setPositionX(lblClone->getPositionX() + 200);
        lblClone->setName("lblAnimalCount");
        clone->addChild(lblClone);
        btn = (Button*)clone->getChildByName("btnUpgrade");
        btn->setTag(i);
        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMapButtonClick, this));
        sptIcon = (ImageView*)btn->getChildByName("sptIcon");
        sptIcon->loadTexture("icon_relic_white.png");
    }
    updateMap();
    svShop = (ScrollView*)loadedLayer->getChildByName("svShop");
    svShop->setClippingEnabled(true);
    tabObjects.pushBack(svShop);
    itemCount = shopItemCount;
    svShop->setInnerContainerSize(Size(750, (itemCount)*(itemGapY + itemHeight)));
    
    int itemCounter = 0;
    for (int i = 0; i < itemCount; i++) {
        itemCounter++;
        Widget* clone = sanhoItemTemplete->clone();
        svShop->getInnerContainer()->addChild(clone);
        clone->setTag(i);
        //        clone->getChildByName("sptIconBg")->removeFromParentAndCleanup(true);
        clone->getChildByName("sptIconBg")->setVisible(false);
        clone->getChildByName("sptCheck0")->removeFromParentAndCleanup(true);
        clone->getChildByName("sptCheck1")->removeFromParentAndCleanup(true);
        clone->getChildByName("lblCondition0")->removeFromParentAndCleanup(true);
        clone->getChildByName("lblCondition1")->removeFromParentAndCleanup(true);
        clone->setPosition(Point(12, svShop->getInnerContainerSize().height + itemGapY - i*(itemGapY + itemHeight)));
        clone->setName(__String::createWithFormat("item%d", i)->getCString());
        clone->getChildByName("sptLock")->removeFromParent();
        ImageView* sptIcon = (ImageView*)clone->getChildByName("sptIcon");
//        sptIcon->loadTexture("icon_dia_white.png");//StringUtils::format("magic_%d.png", i+1));
        sptIcon->setContentSize(Size(92, 92));
        ImageView* timeIcon = (ImageView*)clone->getChildByName("timeIcon");
//        timeIcon->setContentSize(Size(46, 38));
//        clone->getChildByName("sptIconBg")->removeFromParentAndCleanup(true);
        Text* lbl = (Text*)clone->getChildByName("lblDescription_0");
        Text* lblDescription = (Text*)clone->getChildByName("lblDescription");
        lblDescription->setTextColor(Color4B(57, 60, 51, 255));
        lbl->setTextColor(Color4B(105, 102, 83, 255));
        if (i == SHOP_FREE_RAIN) {
            sptIcon->loadTexture("UI/shop/blessing_shoure.png");
            timeIcon->loadTexture("UI/shop/leaf.png");
            lbl->setString(StringUtils::format(GameManager::getInstance()->getText("double life for min").c_str(), 3));
            GameManager::getInstance()->setLocalizedString(lblDescription, "free bless rain");
            clone->getChildByName("sptIconBg")->setVisible(true);
        }else if (i == SHOP_FREE_GEM) {
            sptIcon->loadTexture("UI/shop/gem_free.png");
            timeIcon->loadTexture("UI/shop/gem.png");
            int gemCount = 5;
            lbl->setString(StringUtils::format(GameManager::getInstance()->getText("get gem amount").c_str(), gemCount));
            GameManager::getInstance()->setLocalizedString(lblDescription, "free gem");
            clone->getChildByName("sptIconBg")->setVisible(true);
        }else if (i == SHOP_FREE_RELIC) {
            sptIcon->loadTexture("UI/shop/sunshine_free.png");
            timeIcon->loadTexture("UI/shop/sunshine.png");
            int relicCount = 5;
            lbl->setString(StringUtils::format(GameManager::getInstance()->getText("get relic amount").c_str(), relicCount));
            GameManager::getInstance()->setLocalizedString(lblDescription, "free relic");
            clone->getChildByName("sptIconBg")->setVisible(true);
        }else if (i == SHOP_RAIN) {
            sptIcon->loadTexture("UI/shop/blessing_shoure.png");
            timeIcon->loadTexture("UI/shop/leaf.png");
            lbl->setString(StringUtils::format(GameManager::getInstance()->getText("double life for min").c_str(), 15));
            GameManager::getInstance()->setLocalizedString(lblDescription, "bless rain");
        }else if (i == SHOP_ODIN_1) {
            sptIcon->loadTexture("UI/shop/shield of odin_1.png");
            timeIcon->loadTexture("UI/shop/attack.png");
            int dayCount = 1;
            lbl->setString(StringUtils::format(GameManager::getInstance()->getText("odin effect").c_str(), dayCount));
            GameManager::getInstance()->setLocalizedString(lblDescription, "odin");
        }else if (i == SHOP_GEM_SPECIAL_600 || i == SHOP_GEM_SPECIAL_1000){
            sptIcon->loadTexture(StringUtils::format("UI/shop/gem_%d.png", i - SHOP_GEM_SPECIAL_600 + 1));
            timeIcon->removeFromParent();
            lbl->setString(GameManager::getInstance()->getText("one time purchase only").c_str());
            lblDescription->setString(StringUtils::format(GameManager::getInstance()->getText("gem amount").c_str(), GameManager::getInstance()->getIAPGemRewardCount(IAP_GEM_SPECIAL_600 + i - SHOP_GEM_SPECIAL_600)));
        } else if (i >= SHOP_GEM_1 && i <= SHOP_GEM_4) {
            sptIcon->loadTexture(StringUtils::format("UI/shop/gem_%d.png", i - SHOP_GEM_1));
            lblDescription->setPositionY(52);//(lblDescription->getPositionY() + lbl->getPositionY())/2);
            timeIcon->removeFromParent();
            lbl->removeFromParent();
            lblDescription->setString(StringUtils::format(GameManager::getInstance()->getText("gem amount").c_str(), GameManager::getInstance()->getIAPGemRewardCount(i - SHOP_GEM_1)));
        }else if (i == SHOP_BIRD) {
            sptIcon->loadTexture("UI/shop/blue_bird_of_fortun.png");
            timeIcon->removeFromParent();
            lbl->setString(StringUtils::format("%s\n%s",GameManager::getInstance()->getText("get blue bird").c_str(), GameManager::getInstance()->getText("start raining").c_str()));
            GameManager::getInstance()->setLocalizedString(lblDescription, "blue bird");
        }else if (i == SHOP_PUPPY) {
            sptIcon->loadTexture("UI/shop/puppy_of_hapiness.png");
            timeIcon->removeFromParent();
            lbl->setString(StringUtils::format("%s\n%s",GameManager::getInstance()->getText("get puppy relic").c_str(), GameManager::getInstance()->getText("start raining").c_str()));
            GameManager::getInstance()->setLocalizedString(lblDescription, "puppy");
        }else if (i == SHOP_ODIN_7) {
            sptIcon->loadTexture("UI/shop/shield of odin_2.png");
            timeIcon->loadTexture("UI/shop/attack.png");
            int dayCount = 7;
            lbl->setString(StringUtils::format(GameManager::getInstance()->getText("odin effect").c_str(), dayCount));
            GameManager::getInstance()->setLocalizedString(lblDescription, "strong odin");
        }
        timeIcon->setPositionX(lblDescription->getPositionX() + timeIcon->getContentSize().width/2);
        if(clone->getChildByName("timeIcon") == nullptr){
            lbl->setPositionX(lblDescription->getPositionX());
        }else{
            lbl->setPositionX(timeIcon->getPositionX() + timeIcon->getContentSize().width/2 + 5);
        }
        
        btn = (Button*)clone->getChildByName("btnUpgrade");
        btn->setTag(i);
        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onShopItemClick, this));
//        btn->setEnabled(false);
        Text* lblPrice = (Text*)btn->getChildByName("lblPrice");
        Text* lblButtonText = (Text*)btn->getChildByName("lblDescription");
        ImageView* btnIcon = (ImageView*)btn->getChildByName("sptIcon");
        
        if( i == SHOP_FREE_RAIN || i == SHOP_FREE_GEM || i == SHOP_FREE_RELIC){
            GameManager::getInstance()->setLocalizedString(lblPrice, "daily quest 0");
            btnIcon->loadTexture("UI/shop/play.png");
            btnIcon->setContentSize(Size(16, 19));
            lblButtonText->removeFromParent();
            btnIcon->setPositionY(47);
            lblPrice->setAnchorPoint(Point(0.5, 0.5));
            lblPrice->setPositionY(47);
            GameManager::getInstance()->alignToCenter(btnIcon, lblPrice, 5, btn->getContentSize().width/2, 0);
            addRedBean(btn);
        }else if(i == SHOP_RAIN || i == SHOP_ODIN_1){
            int price = 100;
            lblPrice->setString(Value(price).asString());
            btnIcon->loadTexture("icon_dia_white.png");
//            btnIcon->setContentSize(Size(16, 19));
            GameManager::getInstance()->setLocalizedString(lblButtonText, "buy");
            GameManager::getInstance()->alignToCenter(btnIcon, lblPrice, 5, btn->getContentSize().width/2, 0);
        }else{
            int price = 100;
            if(i == 4){
                price = 1000;
            }else if(i == 5){
                price = 5000;
            }else if(i == 6){
                price = 10000;
            }else if(i == 7){
                price = 20000;
            }else if(i == 8){
                price = 50000;
            }else if(i == 9){
                price = 10000;
            }else if(i == 10){
                price = 5000;
            }else if(i == 11){
                price = 3000;
            }
            lblPrice->setString(Value(price).asString());
            btnIcon->loadTexture("UI/shop/cash.png");
            GameManager::getInstance()->setLocalizedString(lblButtonText, "buy");
            GameManager::getInstance()->alignToCenter(btnIcon, lblPrice, 5, btn->getContentSize().width/2, 0);
        }
        
        if( i == SHOP_FREE_RAIN || i == SHOP_FREE_GEM || i == SHOP_FREE_RELIC){
            Text* lblTimer = (Text*)lblPrice->clone();
            sptIcon->getParent()->addChild(lblTimer);
            lblTimer->setName("lblTimer");
            lblTimer->enableOutline(Color4B(57, 60, 51, 255));
            lblTimer->setPosition(Point(92/2, lblTimer->getContentSize().height));
            lblTimer->setVisible(false);
        }
        
        if (btn->getChildByName("lblDescription") != nullptr){
            lblButtonText->setTextColor(Color4B(255, 241, 146, 255));
        }
    }
    
    itemAchievement = (ImageView*)loadedLayer->getChildByName("itemAchievement");
    tabObjects.pushBack(itemAchievement);
    
    svAchievement = (ScrollView*)itemAchievement->getChildByName("svAchievement");
    svAchievement->setClippingEnabled(true);
//    tabObjects.pushBack(svShop);
    itemCount = achievementCount;
    Button* btnTemp = (Button*)svAchievement->getChildByName("btnTemp");
    float achieveItemHeight = 232;//btnTemp->getContentSize().height;
    svAchievement->setInnerContainerSize(Size(750, (itemCount/4 + (itemCount%4 != 0?1:0))*(itemGapY + achieveItemHeight)));
    
    for (int i = 0; i < itemCount; i++) {
//        Widget* clone = sanhoItemTemplete->clone();
        int index = getAchievementIndex(i);
        Button* imageView = (Button*)btnTemp->clone();
//        imageView->setContentSize(Size(160, 120));
        svAchievement->addChild(imageView);
        imageView->setTag(i);
        //        clone->getChildByName("sptIconBg")->removeFromParentAndCleanup(true);
        float gap = 29.0f;
        imageView->setAnchorPoint(Point(0, 1));
        imageView->setPosition(Point((i%4)*(imageView->getContentSize().width + gap), svAchievement->getInnerContainerSize().height - (i/4)*(itemGapY + achieveItemHeight) - 200));
        imageView->setName(__String::createWithFormat("item%d", i)->getCString());
        
//        ImageView* sptIcon = imageView::create("menu_1.png");
//        imageView->addChild(sptIcon);
//        sptIcon->setPosition(ImageView->getContentSize()/2);
//        sptIcon->setContentSize(Size(92, 92));
        Text* lblDescription = (Text*)imageView->getChildByName("lblDescription_0");
        lblDescription->setName("lblDescription_0");
//        lblDescription->setPosition(Point(imageView->getContentSize().width/2, 120));
//        imageView->addChild(lblDescription);
        Text* lblTitle = (Text*)imageView->getChildByName("lblTitle");
        lblTitle->setName("lblDescription");
//        lblTitle->setTextColor(Color4B(57, 60, 51, 255));
//        lblTitle->setPosition(imageView->getContentSize().width/2, 70);
//        imageView->addChild(lblTitle);
//        lblDescription->setTextColor(Color4B(105, 102, 83, 255));
        
//        btn = (Button*)clone->getChildByName("btnUpgrade");
//        btn->setTag(i);
//        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onAchievementItemClick, this));
//        btn->setEnabled(false);
        imageView->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onAchievementItemClick, this));
        addRedBean(imageView, Point(37.52f - 14, 177.56f));
        
        Text* lblPrice = (Text*)imageView->getChildByName("lblPrice");
//        imageView->addChild(lblPrice);
        lblPrice->setName("lblPrice");
//        lblPrice->setPosition(imageView->getContentSize().width/2, 20);
        Text* lblButtonText = (Text*)imageView->getChildByName("lblButtonText");
        ImageView* btnIcon = (ImageView*)imageView->getChildByName("sptIcon");
        
//        int price = 100;
//        lblPrice->setString(Value(price).asString());
//        btnIcon->loadTexture("icon_dia_white.png");
        GameManager::getInstance()->setLocalizedString(lblButtonText, "receive");
//        GameManager::getInstance()->alignToCenter(btnIcon, lblPrice, 5, btn->getContentSize().width/2, 0);
        
        if (index == ACHIEVE_MAIN_TREE_500 || index == ACHIEVE_MAIN_TREE_777 || index == ACHIEVE_MAIN_TREE_1000) {
            lblPrice->setVisible(false);
            btnIcon->setVisible(false);
//            lblButtonText->setString(StringUtils::format("%s +200%%", GameManager::getInstance()->getText("life per tap").c_str()));
            lblButtonText->setString("+200%");
            lblButtonText->setPositionX(imageView->getContentSize().width/2 + 10);
            Sprite* sptArrow = Sprite::create("arrow.png");
            lblButtonText->addChild(sptArrow);
            sptArrow->setPosition(Point(lblButtonText->getContentSize().width + 10, lblButtonText->getContentSize().height/2));
            Sprite* sptHand = Sprite::create("hand 1_w.png");
            lblButtonText->addChild(sptHand);
            sptHand->setPosition(Point(-sptHand->getContentSize().width/2 - 2, lblButtonText->getContentSize().height/2));
            sptHand->setScale(0.9f);
        }
    }

    itemTemplete->retain();
    itemTemplete->removeFromParentAndCleanup(false);
    
    sunMoonBatch = SpriteBatchNode::create("rockBg.png");
    this->addChild(sunMoonBatch, -9989);
    rockBgBatch = SpriteBatchNode::create("rockBg.png");
    svField->getInnerContainer()->addChild(rockBgBatch, -9988);
    rockBgBatch->setPositionY(-1);
//    rockBgBatch->getTexture()->setAntiAliasTexParameters();
    
    starBatch = SpriteBatchNode::create("star.png");
    svField->addChild(starBatch, -9988);
    treeBatch = SpriteBatchNode::create("tree.png");
    svField->getInnerContainer()->addChild(treeBatch, -9987);
    mainTreeBatch = SpriteBatchNode::create("mainTree.png");
    svField->addChild(mainTreeBatch, -360);
//    meerkatBatch = SpriteBatchNode::create("meerkat.png");
//    svField->getInnerContainer()->addChild(meerkatBatch, -9985);
    
    frontRockBatch = SpriteBatchNode::create("frontRock.png");
    svField->getInnerContainer()->addChild(frontRockBatch, 500);
    // meerkat

    // set background
    sky = Sprite::createWithSpriteFrameName("sky.png");
    sunMoonBatch->addChild(sky, -98);
    sky->setAnchorPoint(Point(0, 1));
    sky->setPosition(Point(0, 750));
    sky->setScaleX(1334);
    sky->setScaleY((sky->getContentSize().height + 164)/sky->getContentSize().height);
//    Sprite* spt2 = Sprite::createWithSpriteFrameName("sky.png");
//    sky->addChild(spt2, -98);
//    spt2->setAnchorPoint(Point(0, 0));
//    spt2->setPosition(Point(0, 1440));
//    spt2->setScale(1, -1);
    
    sunMoon = Sprite::createWithSpriteFrameName("sun.png");
    sunMoon->setPosition(Point(0, 320));
    sunMoon->setAnchorPoint(Point(0, -1));
    sunMoon->getTexture()->setAntiAliasTexParameters();
    for (int i = 0; i < 20; i++) {
        Sprite* emit = Sprite::createWithSpriteFrameName("sun.png");
        emit->setPosition(Point(600 + rand()%600 - 300, 920));
        emit->setScale((1 + rand()%5)*1.0f/(5 + rand()%31), 3.0f);
        emit->setOpacity(rand()%255);
        emit->setRotation(30);
        emit->runAction(RepeatForever::create(Sequence::create(DelayTime::create((rand()%100)*0.01f + 0.1f), FadeTo::create((rand()%100)*0.01f + 0.1f, 100), DelayTime::create((rand()%100)*0.01f + 0.1f), FadeOut::create((rand()%100)*0.01f + 0.1f), NULL)));
        sunMoonBatch->addChild(emit);
        shineEmitList.pushBack(emit);
    }
    for (int i = 0; i < 100; i++) {
        Sprite* twinkle = Sprite::createWithSpriteFrameName("whiteDot.png");
        if (rand()%3>0) {
            twinkle->setPosition(Point(rand()%1336, size.height - rand()%200));
        }else{
            twinkle->setPosition(Point(rand()%1336, size.height - rand()%100 - 200));
        }
        twinkle->setScale((5 + rand()%5)*0.05f);
        twinkle->setOpacity(rand()%255);
        twinkle->getTexture()->setAntiAliasTexParameters();
        twinkle->runAction(RepeatForever::create(Sequence::create(DelayTime::create((rand()%100)*0.01f + 0.1f), FadeTo::create((rand()%100)*0.01f + 0.1f, 100), DelayTime::create((rand()%100)*0.01f + 0.1f), FadeTo::create((rand()%100)*0.01f + 0.1f, 10), NULL)));
        sunMoonBatch->addChild(twinkle);
        twinkleList.pushBack(twinkle);
    }
    Sprite* spt = Sprite::createWithSpriteFrameName("moon.png");
    sunMoon->addChild(spt);
    spt->setPosition(Point(sunMoon->getContentSize().width/2, -sunMoon->getContentSize().height*2.5f));
    spt->getTexture()->setAntiAliasTexParameters();
    sunMoonBatch->addChild(sunMoon, 10);
    
    
    spt = Sprite::createWithSpriteFrameName("ground.png");
    rockBgBatch->addChild(spt, -99);
    spt->setAnchorPoint(Point(0,0));
    spt->setScaleX(1334);
    spt->getTexture()->setAliasTexParameters();
    
    ScrollView* layout = (ScrollView*)svField;
    layout->addTouchEventListener(CC_CALLBACK_2(HelloWorldScene::onFieldTouch, this)); // test
    
    treeBack = Sprite::createWithSpriteFrameName("treeBack.png");
    mainTreeBatch->addChild(treeBack);
    treeBack->setAnchorPoint(Point(0.5, 0));
    treeBack->setPosition(1341, 280);
    
    
    treeFront = Sprite::createWithSpriteFrameName("treeFront.png");
    treeBack->addChild(treeFront);
    treeFront->setPosition(128, 202);
    
    treeFoot = Sprite::createWithSpriteFrameName("treeFoot.png");
    treeBack->addChild(treeFoot);
    treeFoot->setPosition(127, 214);
    
    
    for (int i = 0; i < 4; i++) {
        spt = Sprite::createWithSpriteFrameName(StringUtils::format("leaf%d.png", i));
        spt->setAnchorPoint(Point(0.5f, 0));
        if (i == 0) {
            spt->setPosition(Point(32, 177));
        }else if (i == 1) {
            spt->setPosition(Point(93 , 229 ));
        }else if (i == 2) {
            spt->setPosition(Point(170 , 231 ));
        }else if (i == 3) {
            spt->setPosition(Point(231 , 181 ));
        }
        
        leafList.pushBack(spt);
        treeFront->addChild(spt);
    }
    
    float previousWidth = 0;
    for (int i = 0; i < 4; i++) {
        spt = Sprite::createWithSpriteFrameName(StringUtils::format("mountain%d.png", i));
        spt->setScale(2);
        if (i == 0) {
            spt->setPosition(Point(0, 363));
            spt->setAnchorPoint(Point(0, 0));
        }else if (i == 1) {
            spt->setPosition(Point(previousWidth, 361));
            spt->setAnchorPoint(Point(0, 0));
        }else if (i == 2) {
            spt->setAnchorPoint(Point(1, 0));
            spt->setPosition(Point(2668 - 558, 366));
        }else if (i == 3) {
            spt->setAnchorPoint(Point(1, 0));
            spt->setPosition(Point(2668, 364));
        }
        previousWidth = spt->getContentSize().width*2;
        rockBgBatch->addChild(spt);
    }

    // front rocks
    for (int i = 0; i < 3; i++) {
        spt = Sprite::createWithSpriteFrameName(StringUtils::format("frontRock%d.png", i));
        frontRockBatch->addChild(spt);
        if (i == 0) {
            spt->setAnchorPoint(Point::ZERO);
            spt->setPosition(Point::ZERO);
        }else if (i == 1) {
            spt->setAnchorPoint(Point::ZERO);
            spt->setPosition(Point(534, 0));
        }else if (i == 2) {
            spt->setAnchorPoint(Point(1, 0));
            spt->setPosition(Point(2668, 0));
        }
    }
    
    svSkill->setScrollBarEnabled(false);
    svCollection->setScrollBarEnabled(false);
    svRelic->setScrollBarEnabled(false);
    svMap->setScrollBarEnabled(false);
    svCharacter->setScrollBarEnabled(false);
    
    for (int i = 0; i < 7; i++) {
        Button* btn = (Button*)loadedLayer->getChildByName(__String::createWithFormat("mainMenu%d", i)->getCString());
        btnList.pushBack(btn);
        btn->setTag(i);
        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMainMenuClick, this));
        if (i == 1 || i == 5 || i == 6 || i == 3) {
            Node* icon = btn->getChildByName("icon");
            addRedBean(btn, Point(30, icon->getPosition().y));
        }
    }
    
    uiTop = loadedLayer->getChildByName("uiTop");
    uiBackground = loadedLayer->getChildByName("uiBackground");
    
    
    
    // test
    /*setMainTree(5, 5);
    setLion(5);
    setMeerkat(5);
    setMonkey(5);
    setDuck(5);
    setStar0(5);
    setStar1(5);
    setStar1(5);
    setGrass(70);
    */
    
    // performance test
    for (int i = 0; i < 0; i++) {
        Character* ch = Character::create();
        ch->setSpineAnimation(i/2);
        charList.pushBack(ch);
        //        ch->setScale(3);
        svField->addChild(ch, 0);
        ch->showTalkBox(TALK_BOX_RELIC, 60, "");
        selectedNode = ch;
    }
    int totalSpawnedAnimalCount = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
//    for (int i = 0; i < animalNames.size(); i++) {
    for (int i = 0; i < totalSpawnedAnimalCount; i++) {
//        int count = getAnimalCountInArea(selectedArea, i);
//        int inven = UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ANIMAL_COUNT_INVENTORY_FORMAT, i).c_str(), 0);
//        if (count - inven > 0) {
//            for (int j = 0; j < count; j++) {
//                if (isHunted(i)) {
//                    continue;
//                }
//        int area = UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ANIMAL_AREA_FOR_ID_FORMAT, i).c_str(), 0);
        int area = getArea(i);
        if (area != AREA_AFRICA) {
            continue;
        }
        Character* ch = Character::create();
        int index = getAnimalIndexForID(i);
        ch->setName(StringUtils::format("animal_%d", index));
        ch->setTag(index);
        ch->ID = i;
        ch->area = area;
        ch->setSpineAnimation(index);
        charList.pushBack(ch);
        svField->addChild(ch, 0);
        selectedNode = ch;
//            }
//        }
    }
//    updateAnimalEffect();
    updateTapReward();

    
//    sptBackground = Sprite::create("fieldBackground0.png");
    Node* container = svField->getInnerContainer();
//    container->addChild(sptBackground, -1);
//    sptBackground->setPosition(Point(container->getContentSize().width/2, container->getContentSize().height/2));
    
//    sptBackground->setBlendFunc(BlendFunc::ADDITIVE);
    
    sliderR = (Slider*)loadedLayer->getChildByName("testNode")->getChildByName("sliderR");
    sliderG = (Slider*)loadedLayer->getChildByName("testNode")->getChildByName("sliderG");
    sliderB = (Slider*)loadedLayer->getChildByName("testNode")->getChildByName("sliderB");
    lblR = (Text*)loadedLayer->getChildByName("testNode")->getChildByName("lblR");
    lblG = (Text*)loadedLayer->getChildByName("testNode")->getChildByName("lblG");
    lblB = (Text*)loadedLayer->getChildByName("testNode")->getChildByName("lblB");
    
    sliderR_color_subtractive = (Slider*)loadedLayer->getChildByName("testNode")->getChildByName("sliderR_0");
    sliderG_color_subtractive = (Slider*)loadedLayer->getChildByName("testNode")->getChildByName("sliderG_0");
    sliderB_color_subtractive = (Slider*)loadedLayer->getChildByName("testNode")->getChildByName("sliderB_0");
    lblR_color_subtractive = (Text*)loadedLayer->getChildByName("testNode")->getChildByName("lblR_0");
    lblG_color_subtractive = (Text*)loadedLayer->getChildByName("testNode")->getChildByName("lblG_0");
    lblB_color_subtractive = (Text*)loadedLayer->getChildByName("testNode")->getChildByName("lblB_0");
    
    loadedLayer->getChildByName("testNode")->setVisible(false);
    
    sptR = Sprite::create("redBackground.jpg");
    container->addChild(sptR);
    sptR->setPosition(getHalfPos(svField->getContentSize()));
    sptR->setBlendFunc(BlendFunc::ADDITIVE);
    sptR->setOpacity(0);
    
    sptG = Sprite::create("greenBackground.jpg");
    container->addChild(sptG);
    sptG->setPosition(getHalfPos(svField->getContentSize()));
    sptG->setBlendFunc(BlendFunc::ADDITIVE);
    sptG->setOpacity(0);
    
    sptB = Sprite::create("blueBackground.jpg");
    container->addChild(sptB);
    sptB->setPosition(getHalfPos(svField->getContentSize()));
    sptB->setBlendFunc(BlendFunc::ADDITIVE);
    sptB->setOpacity(0);
    
    // test
//    btn = (Button*)loadedLayer->getChildByName("btnUp");
//    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMoveSelectedNode, this));
//    btn = (Button*)loadedLayer->getChildByName("btnRight");
//    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMoveSelectedNode, this));
//    btn = (Button*)loadedLayer->getChildByName("btnDown");
//    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMoveSelectedNode, this));
//    btn = (Button*)loadedLayer->getChildByName("btnLeft");
//    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMoveSelectedNode, this));
//    btn = (Button*)loadedLayer->getChildByName("btnUp_0");
//    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMoveSelectedNode, this));
//    btn = (Button*)loadedLayer->getChildByName("btnRight_0");
//    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMoveSelectedNode, this));
//    btn = (Button*)loadedLayer->getChildByName("btnDown_0");
//    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMoveSelectedNode, this));
//    btn = (Button*)loadedLayer->getChildByName("btnLeft_0");
//    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMoveSelectedNode, this));
//    btn = (Button*)loadedLayer->getChildByName("btnChange");
//    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMoveSelectedNode, this));
//    btn = (Button*)loadedLayer->getChildByName("btnChange_0");
//    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onMoveSelectedNode, this));
    
//    Text* lblWinReward = (Text*)loadedLayer->getChildByName("lblWinReward");
//    ImageView* sptheroBack = (ImageView*)loadedLayer->getChildByName("sptHero");
//    ScrollView* scrollView = (ScrollView*)loadedLayer->getChildByName("svMap");
//    scrollView->setScrollBarEnabled(false);
    
    // arrange main tree zorder
    
    
    // arrange main tree zorder end
    labelPoolCount = 10;
    labelPoolIndex = 0;
    for (int i = 0; i < labelPoolCount; i++) {
        Label* lbl = Label::createWithTTF("a", "GodoB.ttf", 25);
        lbl->retain();
//        lbl->enableOutline(Color4B::BLACK, 2);
//        lbl->setTextColor(Color4B(202, 116, 39, 255));
        labelPool.pushBack(lbl);
    }
//    addListener();
    
    this->setContentSize(Size(1000, 1000));
    
    updateTapReward();
    
    
    this->schedule(schedule_selector(HelloWorldScene::updateQuest), 2.1f);
    
    currentTab = TAB_SKILL;
    updateMainButtons();
    
    updateMainTreeItem();
    
    updateSanhoImages();
    updateSanhoItems();
    
    CheckSkillTime();
    if (buffTimeElapse >0) {
        showRainbow();
    }
    if (autoTapTimeElapse>0) {
        showAurora();
    }
    _keyListener = EventListenerKeyboard::create();
    _keyListener->onKeyReleased = CC_CALLBACK_2(HelloWorldScene::onKeyReleased, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_keyListener, this);
    
    updateNotification();
    log("updateNotification: helloworld");
    
    tapCounter = getQuestProgress(3);
//    tapCounter = 0; // test 
    tapQuestDone = tapCounter >= getQuestMaxCount(3);
    
    updateNextAnimalPrice();
//    updateAnimalEffect();
    updateTotalSanhoEffect();
    updateCurrencyLabels();
    
    runTreeIdleAnimation();
    
    if(!UserDefault::getInstance()->getBoolForKey("prologueDone", false)){ // test 
        UserDefault::getInstance()->setBoolForKey("prologueDone", true);
        prologueLayer = CSLoader::createNode("Prologue.csb");
        this->addChild(prologueLayer, 10);
        
        float dur = 4;
        float appearDur = 0.5f;
        float afterDelay = 2;
        Button* btn = (Button*)prologueLayer->getChildByName("btnCover");
        btn->runAction(MoveBy::create(dur, Point(0, -600)));
        
        btn = (Button*)prologueLayer->getChildByName("btnOk");
        btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onOkPrologue, this));
        btn->setTitleText(GameManager::getInstance()->getText("ok"));
        
        Text* lbl = (Text*)prologueLayer->getChildByName("lblTitle");
        GameManager::getInstance()->setLocalizedString(lbl, "prologue 0");
        lbl->runAction(Sequence::create(DelayTime::create(dur+afterDelay), FadeIn::create(appearDur), NULL));
        
        lbl = (Text*)prologueLayer->getChildByName("lblPrologue0");
        GameManager::getInstance()->setLocalizedString(lbl, "prologue 1");
        lbl->runAction(Sequence::create(DelayTime::create(dur-appearDur+afterDelay), FadeOut::create(appearDur), NULL));
        
        lbl = (Text*)prologueLayer->getChildByName("lblPrologue1");
        lbl->setString(StringUtils::format("%s\n\n%s", GameManager::getInstance()->getText("prologue 2").c_str(), GameManager::getInstance()->getText("prologue 3").c_str()));
        lbl->runAction(Sequence::create(DelayTime::create(dur-appearDur+afterDelay), FadeOut::create(appearDur), NULL));
        
        log("std: %s", GameManager::getInstance()->getText("prologue 2").c_str());
        
        ScrollView* scrollView = (ScrollView*)prologueLayer->getChildByName("scrollView");
        scrollView->addTouchEventListener(CC_CALLBACK_2(HelloWorldScene::onSignatureTouch, this));
        scrollView->runAction(Sequence::create(DelayTime::create(dur + appearDur + afterDelay), FadeIn::create(appearDur), NULL));
        scrollView->setScrollBarEnabled(false);
        
        DrawNode* drawNode = DrawNode::create();
        scrollView->addChild(drawNode);
        drawNode->setName("drawNode");
        drawNode->setPosition(-scrollView->getPosition());
        drawNode->setLineWidth(10);
        
        signatureArray = PointArray::create(100);
        signatureArray->retain();
        
        lastSignaturePoint = Point::ZERO;
        updateOrientation();
    }else{
        if (true || !UserDefault::getInstance()->getBoolForKey("tutorial_done", false)) {
            btnSkipTutorial = Button::create("skip_button.png");
            this->addChild(btnSkipTutorial, 100);
            btnSkipTutorial->setPosition(Point(size.width, 0));
            btnSkipTutorial->setAnchorPoint(Point(1, 0));
            btnSkipTutorial->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onSkipTutorialClick, this));
            
            isTutorialOn = true;
            isReadyToProceedTutorial = true;
            tutorialStep = -1;
        }
        
        float bigger = size.width > size.height?size.width:size.height;
        float smaller = size.width < size.height?size.width:size.height;
        Size theSize;
        float fieldY = 0;
        if (GameManager::getInstance()->isPortrait) {
            theSize = Size(smaller, bigger);
            
            if (btnAppear->isVisible()) {
                fieldY = 584-164;
            }else{
                
                fieldY = 584;
            }
            
        }else{
            theSize = Size(bigger, smaller) ;
        }
        svField->setPosition(Point(theSize.width/2, fieldY));
        
        this->schedule(schedule_selector(HelloWorldScene::gameUpdate));
    }
    
    long now = TimeManager::getInstance()->getCurrentTime();
    int endTime = UserDefault::getInstance()->getIntegerForKey("odin_end_time", -1);
    if (endTime > now) {
        createOdinEffect();
    }
    
    now = TimeManager::getInstance()->getCurrentTime();
    endTime = UserDefault::getInstance()->getIntegerForKey("raining_end_time", -1);
    if (endTime > now) {
        createRainingEffect();
    }
    
    // attend achievement
    int today = (int)(TimeManager::getInstance()->getCurrentTime()/(3600*24)); //
    int savedDay = UserDefault::getInstance()->getIntegerForKey("attend_day", 0);
    if(today != savedDay){
        UserDefault::getInstance()->setIntegerForKey("attend_day", today);
        if (addAchievementCurrent(ACHIEVE_ATTEND_3, 1) >= 3){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQDg");
        }
        if(addAchievementCurrent(ACHIEVE_ATTEND_7, 1) >= 7){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQDw");
        }
        if(addAchievementCurrent(ACHIEVE_ATTEND_14, 1) >= 14){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQEA");
        }
        if(addAchievementCurrent(ACHIEVE_ATTEND_30, 1) >= 30){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQEQ");
        }
    }
    
    this->schedule(schedule_selector(HelloWorldScene::moveUpdate));
//    addAchievementCurrent(ACHIEVE_TAP_10000000, 10000000); // test
    
    this->scheduleOnce(schedule_selector(HelloWorldScene::onPresentUpdate), 60); // test

    sptAnimalProductAlertL = Sprite::create("text_buble_fram_1.png");
    this->addChild(sptAnimalProductAlertL, 9);
    sptAnimalProductAlertL->setAnchorPoint(Point(0.5, 0));
    sptAnimalProductAlertL->setRotation(90);
    sptAnimalProductAlertL->setPosition(Point(0, size.height*3/4));
    Sprite* sptCurrencyIcon = Sprite::create("text_buble_s_leaf.png");
    sptAnimalProductAlertL->addChild(sptCurrencyIcon);
    sptCurrencyIcon->setRotation(-90);
    sptCurrencyIcon->setPosition(Point(36, 38));
    sptAnimalProductAlertL->setVisible(false);
    
    sptAnimalProductAlertR = Sprite::create("text_buble_fram_1.png");
    this->addChild(sptAnimalProductAlertR, 9);
    sptAnimalProductAlertR->setAnchorPoint(Point(0.5, 0));
    sptAnimalProductAlertR->setRotation(-90);
    sptAnimalProductAlertR->setPosition(Point(size.width, size.height*3/4));
    sptCurrencyIcon = Sprite::create("text_buble_s_leaf.png");
    sptAnimalProductAlertR->addChild(sptCurrencyIcon);
    sptCurrencyIcon->setRotation(90);
    sptCurrencyIcon->setPosition(Point(24, 38));
    sptAnimalProductAlertR->setVisible(false);
    
    sptAnimalRelicAlertL = Sprite::create("text_buble_fram_1.png");
    this->addChild(sptAnimalRelicAlertL, 10);
    sptAnimalRelicAlertL->setAnchorPoint(Point(0.5, 0));
    sptAnimalRelicAlertL->setRotation(90);
    sptAnimalRelicAlertL->setPosition(Point(0, size.height*3/4));
    sptAnimalRelicAlertL->setVisible(false);
    
    sptCurrencyIcon = Sprite::create("icon_relic.png");
    sptAnimalRelicAlertL->addChild(sptCurrencyIcon);
    sptCurrencyIcon->setRotation(-90);
    sptCurrencyIcon->setPosition(Point(29, 37));
    
    sptAnimalRelicAlertR = Sprite::create("text_buble_fram_1.png");
    this->addChild(sptAnimalRelicAlertR, 10);
    sptAnimalRelicAlertR->setAnchorPoint(Point(0.5, 0));
    sptAnimalRelicAlertR->setRotation(-90);
    sptAnimalRelicAlertR->setPosition(Point(size.width, size.height*3/4));
    sptCurrencyIcon = Sprite::create("icon_relic.png");
    sptAnimalRelicAlertR->addChild(sptCurrencyIcon);
    sptCurrencyIcon->setRotation(90);
    sptCurrencyIcon->setPosition(Point(29, 37));
    sptAnimalRelicAlertR->setVisible(false);
    
    log("init done");
    isInitDone = true;
    return true;
}
void HelloWorldScene::onAchievementServiceClick(){
    if(sdkbox::PluginSdkboxPlay::isSignedIn()){
        log("sdkboxplay signed in");
        sdkbox::PluginSdkboxPlay::showAchievements();
    }else{
        log("sdkboxplay sign in");
        sdkbox::PluginSdkboxPlay::signin();
    }
    
    log("showAchievements");
}
void HelloWorldScene::onPresentClick(){
    present->removeFromParent();
    present = nullptr;
    this->scheduleOnce(schedule_selector(HelloWorldScene::onPresentUpdate), 60); // test
    
    presentAdsIndex = rand()%4;
    
    firstPopup = CSLoader::createNode("dialog.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    firstPopup->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(firstPopup, 10);
    Node* frame = firstPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    std::string reward;
    if (presentAdsIndex == 0) {
        reward = StringUtils::format(GameManager::getInstance()->getText("double life for min").c_str(), 3);
    }else if (presentAdsIndex == 1) {
        reward = StringUtils::format(GameManager::getInstance()->getText("get gem amount").c_str(), 5);
    }else if (presentAdsIndex == 2) {
        reward = StringUtils::format(GameManager::getInstance()->getText("get relic amount").c_str(), 5);
    }else if (presentAdsIndex == 3) {
        reward = StringUtils::format(GameManager::getInstance()->getText("skill description 1").c_str(), 500);
    }
    std::string description = GameManager::getInstance()->getText("watch video for good");
    
    lbl->setString(StringUtils::format("%s\n[%s]", description.c_str(), reward.c_str()));
    ddiyong(firstPopup);
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    btn = (Button*)frame->getChildByName("btnOk");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onPresentConfirmed, this));
    btn->setTitleText(GameManager::getInstance()->getText("yes"));
    
    btn = (Button*)frame->getChildByName("btnCancel");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn->setTitleText(GameManager::getInstance()->getText("no"));
}
void HelloWorldScene::onPresentConfirmed(){
    closePopup();
    if (presentAdsIndex == 0) {
        GameManager::getInstance()->showVideo(VIDEO_FREE_RAIN);
    }else if (presentAdsIndex == 1) {
        GameManager::getInstance()->showVideo(VIDEO_FREE_GEM);
    }else if (presentAdsIndex == 2) {
        GameManager::getInstance()->showVideo(VIDEO_FREE_RELIC);
    }else if (presentAdsIndex == 3){
        GameManager::getInstance()->showVideo(VIDEO_500_TAP);
    }
    int total = UserDefault::getInstance()->getIntegerForKey("key_present_use_count", 0);
    total++;
    UserDefault::getInstance()->setIntegerForKey("key_present_use_count", total);
    if(total%10 == 0){
        NativeInterface::NativeInterface::trackEvent("ad", "두더쥐 선물", Value(total).asString().c_str(), "", "video", total);
    }
}
void HelloWorldScene::onPresentUpdate(float dt){
    if (present == nullptr) {
        present = Character::create();
        present->anim = SkeletonAnimation::createWithJsonFile("present.json.txt", "present.atlas", 1);
        present->setLocalZOrder(-present->getPosition().y);
        present->addChild(present->anim);
        present->setPosForFirstTime();
        present->startMove();
        
        present->anim->update(0);
        present->anim->setAnchorPoint(Point(0, 0));
        present->setAnchorPoint(Point(0, 0));
        
        //    ch->stayCount = 60000;
        present->setPosition(Point(1420, 160));//ndCenter->getPosition() + Point(0, -110));
        present->unscheduleAllCallbacks();
        present->setName("animal");
        svField->addChild(present, 0);
//        present->runAnimation(CHARACTER_ANI_RUN, 1);
        
        spTrackEntry* entry =  present->anim->setAnimation(0, "show_up", false);
        float time = entry->endTime;
        present->runAction(Sequence::create(DelayTime::create(time), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::onPresentAppearDone, this)), NULL));
        
        Sprite* sptRect = Sprite::create("icon_frame.png");
        sptRect->setName("sptRect");
        present->addChild(sptRect);
        sptRect->setPosition(Point(0, 30));
        sptRect->setVisible(false);
        sptRect->setContentSize(Size(80, 80));
    }
}
void HelloWorldScene::onPresentAppearDone(){
    present->anim->setAnimation(0, "idle", true);
}
int HelloWorldScene::getAchievementIndex(int i){
    if( i == 0) return ACHIEVE_RARE_ANIMAL_1;
    if( i == 1) return ACHIEVE_RARE_ANIMAL_10 ;
    if( i == 2) return ACHIEVE_LION ;
    if( i == 3) return ACHIEVE_BIRD ;
    if( i == 4) return ACHIEVE_4_STAR_RELIC ;
    if( i == 5) return ACHIEVE_5_STAR_RELIC ;
    if( i == 6) return ACHIEVE_UNIQUE_ANIMAL ;
    if( i == 7) return ACHIEVE_LIMITED_ANIMAL ;
    if( i == 8) return ACHIEVE_PURCHASE ;
    if( i == 9) return ACHIEVE_TAP_10000 ;
    if( i == 10) return ACHIEVE_TAP_100000 ;
    if( i == 11) return ACHIEVE_TAP_1000000 ;
    if( i == 12) return ACHIEVE_TAP_10000000 ;
    if( i == 13) return ACHIEVE_ATTEND_3 ;
    if( i == 14) return ACHIEVE_ATTEND_7 ;
    if( i == 15) return ACHIEVE_ATTEND_14 ;
    if( i == 16) return ACHIEVE_ATTEND_30 ;
    if( i == 17) return ACHIEVE_SANHO_GOLD_TREE_UNLOCK ;
    if( i == 18) return ACHIEVE_SANHO_WATER_FALL_UNLOCK ;
    if( i == 19) return ACHIEVE_SANHO_MONKEY_STAR_UNLOCK ;
    if( i == 20) return ACHIEVE_SHARE_1 ;
    if( i == 21) return ACHIEVE_SHARE_10 ;
    if( i == 22) return ACHIEVE_SHARE_30 ;
    if( i == 23) return ACHIEVE_MAIN_TREE_500 ;
    if( i == 24) return ACHIEVE_MAIN_TREE_777 ;
    if( i == 25) return ACHIEVE_MAIN_TREE_1000 ;
    return -1;
}
void HelloWorldScene::onAchievementItemClick(Ref* ref){
    Button* btn = (Button*)ref;
    int tag = btn->getTag();
    int index = getAchievementIndex(tag);
    setAchievementRewardReceivedForHowMuchProgress(index, getAchievementMax(index));
    int reward = getAchievementReward(index, getAchievementMax(index));
    if(reward < 0){
        addMainTreeBonusLevel();
        updateTapReward();
    }else{
//        GameManager::getInstance()->addGem(reward);
        int iconCount = reward;
        float delyTime= 0;
        Point offset = Point(80, -300);
        if(reward > 999){
            for (int i = 0; i < reward/100; i++) {
                moveIconToPosition(ICON_GEM_100, getPosInWorld(btn) + offset, getPosInWorld(loadedLayer->getChildByName("pnlStatus")->getChildByName("iconGem")), delyTime);
                delyTime += 0.03f;
            }
        }else if(reward > 99){
            for (int i = 0; i < reward/10; i++) {
                moveIconToPosition(ICON_GEM_10, getPosInWorld(btn) + offset, getPosInWorld(loadedLayer->getChildByName("pnlStatus")->getChildByName("iconGem")), delyTime);
                delyTime += 0.03f;
            }
        }else{
            for (int i = 0; i < reward; i++) {
                moveIconToPosition(ICON_GEM, getPosInWorld(btn) + offset, getPosInWorld(loadedLayer->getChildByName("pnlStatus")->getChildByName("iconGem")), delyTime);
                delyTime += 0.03f;
            }
        }
        
        log("reward: %d", reward);
    }
    updateCurrencyLabels();
    updateAchievementItems();
}
int HelloWorldScene::getAchievementCurrent(int index){
    return UserDefault::getInstance()->getIntegerForKey(StringUtils::format("achievement_current_%d", index).c_str(), 0);
}
int HelloWorldScene::addAchievementCurrent(int index, int amount){
    int total = UserDefault::getInstance()->getIntegerForKey(StringUtils::format("achievement_current_%d", index).c_str(), 0);
    total += amount;
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format("achievement_current_%d", index).c_str(), total);
    return total;
}
int HelloWorldScene::getAchievementMax(int index){
    int level = 0;
    int lastMax = -1;
    while(true){
        int amount = getAchievementRewardReceivedForHowMuchProgress(index);
        int max = getAchievementMax(index, level);
        if(max < 0){
            return amount;
        }
        if(amount < max || lastMax == max){
            return max;
        }
        lastMax = max;
        level++;
    }
}
int HelloWorldScene::getAchievementReward(int index, int maxProgress){
    if (index == ACHIEVE_RARE_ANIMAL_1){ // rare animal create
        
    }else if (index == ACHIEVE_RARE_ANIMAL_10){ // rare animal create
        
    }else if (index == ACHIEVE_LION){ // lion create
        
    }else if (index == ACHIEVE_BIRD){ // bird create
        
    }else if (index == ACHIEVE_4_STAR_RELIC){ // 4 star create
        
    }else if (index == ACHIEVE_5_STAR_RELIC){ // 5 star create
        
    }else if (index == ACHIEVE_UNIQUE_ANIMAL){ // unique animal
        
    }else if (index == ACHIEVE_LIMITED_ANIMAL){ // limit animal
        
    }else if (index == ACHIEVE_PURCHASE){ // purchase
        return 500;
    }else if (index == ACHIEVE_TAP_10000){ // tap
        
    }else if (index == ACHIEVE_ATTEND_3){ // attend
        if(maxProgress == 3){
            
        }else if(maxProgress == 7){
            
        }else if(maxProgress == 14){
            
        }
    }else if (index == ACHIEVE_ATTEND_30){ // attend
        return 1000;
    }else if (index == ACHIEVE_SANHO_GOLD_TREE_UNLOCK){ // sanho unlock
        if(maxProgress == 6){
            
        }else if(maxProgress == 8){
            
        }else if(maxProgress == 13){
            
        }
    }else if (index == ACHIEVE_SANHO_MONKEY_STAR_UNLOCK){ // sanho unlock
        return 500;
    }else if (index == ACHIEVE_SHARE_1){ // share
        if(maxProgress == 1){
            
        }else if(maxProgress == 10){
            
        }else if(maxProgress == 30){
            
        }
    }else if (index == ACHIEVE_SHARE_30){ // share
        return 1000;
    }else if (index == ACHIEVE_MAIN_TREE_500){ // main tree
        return -1;
    }else if (index == ACHIEVE_MAIN_TREE_777){ // main tree
        return -1;
    }else if (index == ACHIEVE_MAIN_TREE_1000){ // main tree
        return -1;
    }
    return 100;
}
int HelloWorldScene::getAchievementMax(int index, int level){
    if (index == ACHIEVE_RARE_ANIMAL_1){ // rare animal create
        return 1;
    }else if (index == ACHIEVE_RARE_ANIMAL_10){ // rare animal create
        return 10;
    }else if (index == ACHIEVE_LION){ // lion create
        return 5;
    }else if (index == ACHIEVE_BIRD){ // bird create
        return 10;
    }else if (index == ACHIEVE_4_STAR_RELIC){ // 4 star create
        return 1;
    }else if (index == ACHIEVE_5_STAR_RELIC){ // 5 star create
        return 1;
    }else if (index == ACHIEVE_UNIQUE_ANIMAL){ // unique animal
        return 1;
    }else if (index == ACHIEVE_LIMITED_ANIMAL){ // limit animal
        return 1;
    }else if (index == ACHIEVE_PURCHASE){ // purchase
        return 1;
    }else if (index == ACHIEVE_TAP_10000){ // tap
        return 10000;
    }else if (index == ACHIEVE_TAP_100000){ // tap
        return 100000;
    }else if (index == ACHIEVE_TAP_1000000){ // tap
        return 1000000;
    }else if (index == ACHIEVE_TAP_10000000){ // tap
        return 10000000;
    }else if (index == ACHIEVE_ATTEND_3){ // attend
        return 3;
    }else if (index == ACHIEVE_ATTEND_7){ // attend
        return 7;
    }else if (index == ACHIEVE_ATTEND_14){ // attend
        return 14;
    }else if (index == ACHIEVE_ATTEND_30){ // attend
        return 30;
    }else if (index == ACHIEVE_SANHO_GOLD_TREE_UNLOCK){ // sanho unlock
        return 6;
    }else if (index == ACHIEVE_SANHO_WATER_FALL_UNLOCK){ // sanho unlock
        return 8;
    }else if (index == ACHIEVE_SANHO_MONKEY_STAR_UNLOCK){ // sanho unlock
        return 13;
    }else if (index == ACHIEVE_SHARE_1){ // share
        return 1;
    }else if (index == ACHIEVE_SHARE_10){ // sanho unlock
        return 10;
    }else if (index == ACHIEVE_SHARE_30){ // sanho unlock
        return 30;
    }else if (index == ACHIEVE_MAIN_TREE_500){ // main tree
        return 500;
    }else if (index == ACHIEVE_MAIN_TREE_777){ // main tree
        return 777;
    }else if (index == ACHIEVE_MAIN_TREE_1000){ // main tree
        return 1000;
    }
    return -1;
}
int HelloWorldScene::getAchievementRewardReceivedForHowMuchProgress(int index){
    return UserDefault::getInstance()->getIntegerForKey(StringUtils::format("achievement_current_%d_reward", index).c_str(), 0);
}
void HelloWorldScene::setAchievementRewardReceivedForHowMuchProgress(int index, int amount){
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format("achievement_current_%d_reward", index).c_str(), amount);
}
void HelloWorldScene::addRedBean(Node* node, Point pos){
    ImageView* spt = ImageView::create("notice_point_2.png");
    node->addChild(spt);
//    float width = 30;
//    spt->setColor(Color3B::RED);
//    spt->setScale(width/spt->getContentSize().width);
    if(pos == Point::ZERO){
//        spt->setPosition(Point(node->getContentSize().width - spt->getBoundingBox().size.width/2, node->getContentSize().height - spt->getBoundingBox().size.height/2));
        spt->setPosition(Point(-spt->getContentSize().width/2, node->getContentSize().height/2));
    }else{
        spt->setPosition(pos);
    }
    
    spt->setName("redBean");
    spt->setVisible(false);
}
void HelloWorldScene::showRedBean(Node* node, bool visible){
    if (node->getChildByName("redBean") != nullptr) {
        node->getChildByName("redBean")->setVisible(visible);
    }
}
void HelloWorldScene::onMainMenuTouch(Ref* pSender, Widget::TouchEventType type){
    if (type == Widget::TouchEventType::BEGAN) {
        lastMainMenuPoint = ((Widget*)pSender)->getTouchBeganPosition();
    }
    if (type == Widget::TouchEventType::MOVED) {
        Point pos = ((Widget*)pSender)->getTouchMovePosition();
        
        if (!GameManager::getInstance()->isPortrait && loadedLayer->getChildByName("btnAppear")->isVisible() && pos.y - lastMainMenuPoint.y > 50) {
            OnAppearClick();
        }
    }
}
void HelloWorldScene::onSignatureTouch(Ref* pSender, Widget::TouchEventType type){
    if (type == Widget::TouchEventType::BEGAN) {
        lastSignaturePoint = ((Widget*)pSender)->getTouchBeganPosition();
    }
    if (type == Widget::TouchEventType::MOVED) {
        Point pos = ((Widget*)pSender)->getTouchMovePosition();
        prologueLayer->getChildByName("btnOk")->setVisible(true);
        
        DrawNode* drawNode = (DrawNode*)prologueLayer->getChildByName("scrollView")->getChildByName("drawNode");
        drawNode->drawLine(lastSignaturePoint, pos, Color4F(49/255.0f, 4/255.0f, 2/255.0f, 1));
        
        lastSignaturePoint = pos;
    }
}
void HelloWorldScene::onOkPrologue(){
    float dur = 0.5f;
    float delayStep = 0.1f;
//    loadedLayer->runAction(Sequence::create(DelayTime::create(delayStep*0), MoveTo::create(dur, Point::ZERO), NULL));
//    svField->runAction(Sequence::create(DelayTime::create(delayStep*1), MoveTo::create(dur, GameManager::getInstance()->isPortrait?Point(0, 584):Point::ZERO), NULL));
//    sunMoon->runAction(Sequence::create(DelayTime::create(delayStep*2), MoveTo::create(dur, Point::ZERO), NULL));
//    prologueLayer->runAction(Sequence::create(MoveBy::create(dur, Point(0, -size.height)), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, prologueLayer)), NULL));
    prologueLayer->runAction(Sequence::create(MoveBy::create(0, Point(0, -size.height)), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, prologueLayer)), NULL));
    prologueLayer = nullptr;
//    sunMoonBatch->runAction(MoveTo::create(dur, Point::ZERO));
    sunMoonBatch->setPosition(Point::ZERO);
    this->runAction(Sequence::create(DelayTime::create(0), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::startGame, this)), NULL));
}
void HelloWorldScene::startGame(){
    GameManager::getInstance()->refreshOrientationNeeded = true;
    if (!UserDefault::getInstance()->getBoolForKey("tutorial_done", false)) {
        isTutorialOn = true;
        isReadyToProceedTutorial = true;
        tutorialStep = -1;
    }
    this->schedule(schedule_selector(HelloWorldScene::gameUpdate));
    
    float bigger = size.width > size.height?size.width:size.height;
    float smaller = size.width < size.height?size.width:size.height;
    float svX = 0;
    float svY = 0;
    float fieldY = 0;
    float skillY = 664;
    Node* btnAppear = loadedLayer->getChildByName("btnAppear");
    Node* btnHidden = loadedLayer->getChildByName("btnHidden");
    Size theSize;
    if (GameManager::getInstance()->isPortrait) {
        theSize = Size(smaller, bigger);
        
        float startX = 0;
        float endX = theSize.width;
        GameManager::getInstance()->arrange(startX, endX, btnList);
        if (btnAppear->isVisible()) {
            svY = -420;
            fieldY = 584-164;
        }else{
            
            fieldY = 584;
        }
        btnAppear->setPositionX(250);
        btnHidden->setPositionX(250);
        
    }else{
        theSize = Size(bigger, smaller);
        float startX = size.width - 750;//svSkill->getPosition().x + svSkill->getContentSize().width;
        float endX = theSize.width;
        GameManager::getInstance()->arrange(startX, endX, btnList);
        svX = theSize.width - 750;
        skillY = 86;
        if (btnAppear->isVisible()) {
            svY = -420;
        }else{
            
        }
    }
    svField->setPosition(Point(theSize.width/2, fieldY));
    loadedLayer->setPosition(Point(0, svY));
    
}
void HelloWorldScene::captureScreen(){
    
//    loadedLayer->runAction(Sequence::create(DelayTime::create(0.1f), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::captureLater, this)), NULL));
    captureLater();
}
void HelloWorldScene::onScreenCaptureAllClick(){
    captureScreen();
}
void HelloWorldScene::onScreenCaptureClick(){
    if (isCreatingAnimal) {
        return;
    }
    float moveY = 5000;
    if (isCaptureMode) {
        OnAppearClick();
        moveY *= -1;
    }else{
        OnHideClick();
//        loadedLayer->setPosition(Point(0, -moveY));
    }
    isCaptureMode = !isCaptureMode;
    btnScreenCapture->loadTextureNormal(isCaptureMode?"closeCaptureMode.png":"capture_mode.png");
    btnScreenCaptureAll->setVisible(isCaptureMode);
    
//    btnScreenCapture->runAction(Sequence::create(MoveBy::create(0, Point(0, -moveY)), nullptr));
    loadedLayer->setVisible(!isCaptureMode);
    btnQuest->setVisible(!isCaptureMode);
    btnSetting->setVisible(!isCaptureMode);
    btnAchievement->setVisible(!isCaptureMode);
    if (questLayer) {
        questLayer->setVisible(!isCaptureMode);
    }
//    btnSetting->runAction(Sequence::create(MoveBy::create(0, Point(0, -moveY)), nullptr));
    if(btnDefense != nullptr){
        btnDefense->setVisible(!isCaptureMode);
    }
    
    
    if (!UserDefault::getInstance()->getBoolForKey("share_tutorial", false)) { // test
        isTutorialOn = true;
        UserDefault::getInstance()->setBoolForKey("share_tutorial", true);
        tutorialStep = 4;
        removeAndCreateTutorialNode();
    }
    
//    capture(0);
//    this->runAction(Sequence::create(DelayTime::create(1.2f), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::askShare, this)), NULL));
    
}

//void HelloWorldScene::afterCaptured(bool succeed, const std::string& outputFile)
//{
//    if (succeed)
//    {
//        filePath = outputFile;
//        captureSuccess = true;
//    }
//    else
//    {
//        log("Capture screen failed.");
//    }
//}
//void HelloWorldScene::capture(float dt)
//{
//    captureSuccess = false;
//    char buf[40];
//    sprintf(buf, "screenShot_%d.png", 0);
//    utils::captureScreen(CC_CALLBACK_2(HelloWorldScene::afterCaptured, this),buf);
//}
void HelloWorldScene::askShare(){
    if(!captureSuccess) return;
    
    closePopup();
    Node* node = CSLoader::createNode("screenShare.csb");
    setAsPopup(node);
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    node->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(node, 10);
    Node* ndCenter = node->getChildByName("sptCenter");
    Point screenShotPos = ndCenter->getPosition();
    ndCenter->removeFromParent();
    
    auto sp = Sprite::create(filePath);
    node->addChild(sp);
    sp->setScale(406/sp->getContentSize().height);
    sp->setPosition(screenShotPos);
    
    Node* frame0 = node->getChildByName("frame_0");
    frame0->setContentSize(Size(sp->getBoundingBox().size.width + 10, frame0->getContentSize().height));
    
    Node* frame = node->getChildByName("frame");
    if(frame0->getContentSize().width > frame->getContentSize().width){
        frame->getChildByName("btnCloseBig")->setPositionX(frame->getChildByName("btnCloseBig")->getPositionX() + (frame0->getContentSize().width - frame->getContentSize().width)/2);
        frame->getChildByName("btnClose")->setPositionX(frame->getChildByName("btnClose")->getPositionX() + (frame0->getContentSize().width - frame->getContentSize().width)/2);
    }
                                                                                                               
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString("your sabana", lbl);
    ddiyong(firstPopup);
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    btn = (Button*)frame->getChildByName("btnShare");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::doShare, this));
    lbl = (Text*)btn->getChildByName("lblText");
    GameManager::getInstance()->setLocalizedString(lbl, "share");
}
void HelloWorldScene::doShare(){
    int counter = getQuestProgress(4);
    if (counter < getQuestMaxCount(4)) {
        counter++;
        setQuestProgress(4, counter);
        if (questLayer != nullptr) {
            updateQuest(0);
        }
    }
    if(addAchievementCurrent(ACHIEVE_SHARE_1, 1) >= 1){
        sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQFQ");
    }
    if(addAchievementCurrent(ACHIEVE_SHARE_10, 1) >= 10){
        sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQFg");
    }
    if(addAchievementCurrent(ACHIEVE_SHARE_30, 1) >= 30){
        sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQFw");
    }
    NativeInterface::NativeInterface::sharePhoto(filePath.c_str(), "Share your sabana!");
}

void HelloWorldScene::onDailyQuestClick(){
    if (isCreatingAnimal) {
        return;
    }
    if (questLayer != nullptr) {
        questLayer->removeFromParent();
        questLayer = nullptr;
        updateQuest(0);
        return;
    }
    questLayer = CSLoader::createNode("Quest.csb");
    this->addChild(questLayer, 10);
    questLayer->setPosition(Point(-42.27f, size.height));
    questLayer->setScale(1.3f);
    
    Text* lbl = (Text*)questLayer->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString(lbl, "daily quest");
    
    ScrollView* scrollView = (ScrollView*)questLayer->getChildByName("scrollView");
    scrollView->setScrollBarEnabled(false);
    setQuestProgress(3, tapCounter);
    
    Button* btn = (Button*)questLayer->getChildByName("btnClose");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onDailyQuestClick, this));
    
    for (int i = 0; i < 7; i++) {
        lbl = (Text*)scrollView->getChildByName(StringUtils::format("lblQuest_%d", i));
        GameManager::getInstance()->setLocalizedString(lbl, StringUtils::format("%s (%d/%d)", StringUtils::format("daily quest %d", i).c_str(), getQuestProgress(i), getQuestMaxCount(i)));
        Button* btn = (Button*)lbl->getChildByName("btnGet");
        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onRecevieQuestRewardClick, this));
        btn->setTag(i);
    }
    updateQuest(0);
}
void HelloWorldScene::onRecevieQuestRewardClick(Ref* ref){
    Button* btn = (Button*)ref;
    setQuestRewardReceived(btn->getTag(), true);
    
    int iconCount = 0;
    int iconType = ICON_GEM;
    switch (btn->getTag()) {
        case 0:
            iconType = ICON_RELIC;
            iconCount = 10;
//            addRelicPoint(iconCount);
            break;
        case 1:
            iconType = ICON_RELIC;
            iconCount = 5;
//            addRelicPoint(iconCount);
            break;
        case 2:
            iconType = ICON_RELIC;
            iconCount = 5;
//            addRelicPoint(iconCount);
            break;
        case 3:
            iconType = ICON_RELIC;
            iconCount = 10;
//            addQuestTapBuffLevel();
//            updateTapReward();
            break;
        case 4:
            iconType = ICON_RELIC;
            iconCount = 10;
//            addRelicPoint(iconCount);
            break;
        case 5:
            iconType = ICON_GEM;
            iconCount = 50;
//            GameManager::getInstance()->addGem(iconCount);
            break;
        case 6:
            iconType = ICON_GEM;
            iconCount = 50;
//            GameManager::getInstance()->addGem(iconCount);
            break;
            
        default:
            break;
    }
    
    float delyTime = 0;
    for (int i = 0; i < iconCount; i++) {
        if (iconType == ICON_RELIC) {
            moveIconToPosition(iconType, getPosInWorld(btn), getPosInWorld(loadedLayer->getChildByName("pnlStatus")->getChildByName("iconRelic")), delyTime);
        }else{
            moveIconToPosition(iconType, getPosInWorld(btn), getPosInWorld(loadedLayer->getChildByName("pnlStatus")->getChildByName("iconGem")), delyTime);
        }
        delyTime += 0.05f;
    }
    btn->setEnabled(false);
}
cocos2d::Point HelloWorldScene::getPosInWorld(Node* node){
    Point pos = node->getPosition();
    log("pos: %f, %f", pos.x, pos.y);
    Node* parent = node->getParent();
    while(true){
        if (parent == this) {
            break;
        }
        pos += parent->getPosition();
        log("pos: %f, %f", pos.x, pos.y);
        parent = parent->getParent();
    }
    return pos;
}
void HelloWorldScene::moveIconToPosition(int iconType, cocos2d::Point startPos, cocos2d::Point endPos, float delayTime){
    Sprite* sptIcon = nullptr;
    if (iconType == ICON_GEM ||iconType == ICON_GEM_10 ||iconType == ICON_GEM_100 ) {
        sptIcon = Sprite::create("icon_dia.png");
    }else if (iconType == ICON_RELIC) {
        sptIcon = Sprite::create("icon_relic.png");
    }
    if(sptIcon == nullptr) return;
    sptIcon->setPosition(startPos);
    int diffVar = 40;
    sptIcon->runAction(Sequence::create(DelayTime::create(delayTime), EaseInOut::create(MoveBy::create(0.3f, Point(rand()%diffVar - diffVar/2, diffVar + rand()%diffVar)), 3), DelayTime::create(0.3f), EaseInOut::create(MoveTo::create(0.6f, endPos), 2), CallFuncN::create(CC_CALLBACK_1(HelloWorldScene::onIconMoveDone, this)), nullptr));
    sptIcon->setTag(iconType);
    this->addChild(sptIcon, 100);
}
void HelloWorldScene::onIconMoveDone(Ref* ref){
    Sprite* sptIcon = (Sprite*)ref;
    if (sptIcon->getTag() == ICON_RELIC) {
        addRelicPoint(1);
    }else if (sptIcon->getTag() == ICON_GEM) {
        GameManager::getInstance()->addGem(1);
    }else if (sptIcon->getTag() == ICON_GEM_10) {
        GameManager::getInstance()->addGem(10);
    }else if (sptIcon->getTag() == ICON_GEM_100) {
        GameManager::getInstance()->addGem(100);
    }
    sptIcon->removeFromParent();
    updateCurrencyLabels();
}
void HelloWorldScene::addQuestTapBuffLevel(){
    GameManager::getInstance()->showDisposableMessage("quest level up", this);
    int level = getQuestTapBuffLevel();
    level++;
    UserDefault::getInstance()->setIntegerForKey("QuestTapBuffLevel", level);
}
int HelloWorldScene::getQuestTapBuffLevel(){
    return UserDefault::getInstance()->getIntegerForKey("QuestTapBuffLevel", 0);
}
int HelloWorldScene::getQuestProgress(int index){
    return UserDefault::getInstance()->getIntegerForKey(StringUtils::format("quest progress %d", index).c_str(), 0);
}
void HelloWorldScene::setQuestProgress(int index, int count){
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format("quest progress %d", index).c_str(), count);
}
int HelloWorldScene::getQuestMaxCount(int index){
    switch (index) {
        case 0:
            return 5;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 10;
            break;
        case 3:
            return 1000;
            break;
        case 4:
            return 5;
            break;
        case 5:
            return 5;
            break;
        case 6:
            return 1;
            break;
            
        default:
            break;
    }
}
void HelloWorldScene::onSettingClick(){
    for (int i = 0; i < 100; i++) {
        theLife->addNum(theLife); // test
    }
    GameManager::getInstance()->addGem(5000); // test now
    
    if (isCreatingAnimal) {
        return;
    }
    Node* node = CSLoader::createNode("Setting.csb");
    setAsPopup(node);
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)node->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    node->setPosition(size.width/2, size.height/2);
    this->addChild(node, 10);
    Node* frame = node->getChildByName("frame");
//    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString((Text*)frame->getChildByName("lblTitle"), "setting");
    GameManager::getInstance()->setLocalizedString((Text*)frame->getChildByName("lblMusic"), "music");
    GameManager::getInstance()->setLocalizedString((Text*)frame->getChildByName("lblEffect"), "sound effect");
    GameManager::getInstance()->setLocalizedString((Text*)frame->getChildByName("lblAlert"), "alert");
    
    
    bool isMusicOn = UserDefault::getInstance()->getBoolForKey(KEY_MUSIC_ON, true);
    ((Button*)frame->getChildByName("btnMusic"))->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onMusicClick, this));
//    ((Button*)frame->getChildByName("btnMusic"))->loadTextureNormal(isMusicOn?"sptAnimalIn.png":"sptAnimalOut.png");
    changeSettingState(0, isMusicOn, false);
    bool isSoundOn = UserDefault::getInstance()->getBoolForKey(KEY_SOUND_ON, true);
    ((Button*)frame->getChildByName("btnEffect"))->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onSoundEffectClick, this));
//    ((Button*)frame->getChildByName("btnEffect"))->loadTextureNormal(isSoundOn?"sptAnimalIn.png":"sptAnimalOut.png");
    changeSettingState(1, isSoundOn, false);
    bool isAlertOn = UserDefault::getInstance()->getBoolForKey(KEY_PUSH_ALARM, true);
    ((Button*)frame->getChildByName("btnAlert"))->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onAlertClick, this));
//    ((Button*)frame->getChildByName("btnAlert"))->loadTextureNormal(isAlertOn?"sptAnimalIn.png":"sptAnimalOut.png");
    changeSettingState(2, isAlertOn, false);
    
    btn = (Button*)frame->getChildByName("btnOk");
    btn->setTitleText(GameManager::getInstance()->getText("ok"));
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
}
void HelloWorldScene::changeSettingState(int index, bool isOn, bool animate){
    std::string strName;
    if(index == 0){
        strName = "btnMusic";
    }else if(index == 1){
        strName = "btnEffect";
    }else if(index == 2){
        strName = "btnAlert";
    }
    Button* btn = (Button*)firstPopup->getChildByName("frame")->getChildByName(strName);
    if (animate) {
        ((ImageView*)btn->getChildByName("sptOnOff"))->stopAllActions();
        ((ImageView*)btn->getChildByName("sptOnOff"))->runAction(Sequence::create(DelayTime::create(0.2f), FadeOut::create(0.2f), CallFuncN::create(isOn?CC_CALLBACK_1(HelloWorldScene::changeItToSettingOn, this):CC_CALLBACK_1(HelloWorldScene::changeItToSettingOff, this)), FadeIn::create(0.2f), nullptr));
        ((ImageView*)btn->getChildByName("sptButton"))->stopAllActions();
        ((ImageView*)btn->getChildByName("sptButton"))->runAction(EaseBackOut::create(MoveTo::create(0.2f, Point(isOn?23.21f:88.15f, 19.97f))));
        ((ImageView*)btn->getChildByName("sptButton"))->loadTexture("button_2.png");
        ((ImageView*)btn->getChildByName("sptButton"))->runAction(Sequence::create(DelayTime::create(0.2f), CallFuncN::create(CC_CALLBACK_1(HelloWorldScene::changeSettingButtonToOn, this)), NULL));
    }else{
        ((ImageView*)btn->getChildByName("sptOnOff"))->loadTexture(isOn?"frame_on.png":"frame_off.png");
        ((ImageView*)btn->getChildByName("sptButton"))->setPositionX(isOn?23.21f:88.16f);
    }
}
void HelloWorldScene::changeItToSettingOn(Ref* ref){
    ImageView* spt = (ImageView*)ref;
    spt->loadTexture("frame_on.png");
}
void HelloWorldScene::changeItToSettingOff(Ref* ref){
    ImageView* spt = (ImageView*)ref;
    spt->loadTexture("frame_off.png");
}

void HelloWorldScene::changeSettingButtonToOn(Ref* ref){
    ImageView* spt = (ImageView*)ref;
    spt->loadTexture("button_1.png");
}

void HelloWorldScene::onMusicClick(){
    bool isMusicOn = UserDefault::getInstance()->getBoolForKey(KEY_MUSIC_ON, true);
    isMusicOn = !isMusicOn;
    UserDefault::getInstance()->setBoolForKey(KEY_MUSIC_ON, isMusicOn);
//    ((Button*)firstPopup->getChildByName("frame")->getChildByName("btnMusic"))->loadTextureNormal(isMusicOn?"sptAnimalIn.png":"sptAnimalOut.png");
    GameManager::getInstance()->setMusicVolumn(isMusicOn?1:0);
    if (isMusicOn) {
        GameManager::getInstance()->playSoundEffect(SOUND_BGM_VILLAGE);
    }else{
        CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    }
    
    changeSettingState(0, isMusicOn, true);
}
void HelloWorldScene::onSoundEffectClick(){
    bool isSoundOn = UserDefault::getInstance()->getBoolForKey(KEY_SOUND_ON, true);
    isSoundOn = !isSoundOn;
    UserDefault::getInstance()->setBoolForKey(KEY_SOUND_ON, isSoundOn);
//    ((Button*)firstPopup->getChildByName("frame")->getChildByName("btnEffect"))->loadTextureNormal(isSoundOn?"sptAnimalIn.png":"sptAnimalOut.png");
    GameManager::getInstance()->setSoundVolumn(isSoundOn?1:0);
    changeSettingState(1, isSoundOn, true);
}
void HelloWorldScene::onAlertClick(){
    bool isAlertOn = UserDefault::getInstance()->getBoolForKey(KEY_PUSH_ALARM, true);
    isAlertOn = !isAlertOn;
    UserDefault::getInstance()->setBoolForKey(KEY_PUSH_ALARM, isAlertOn);
//    ((Button*)firstPopup->getChildByName("frame")->getChildByName("btnAlert"))->loadTextureNormal(isAlertOn?"sptAnimalIn.png":"sptAnimalOut.png");
    GameManager::getInstance()->setNotificationOn(isAlertOn);
    if (isAlertOn) {
        updateNotification();
    }else{
        NativeInterface::NativeInterface::cancelAllLocalNotification();
    }
    changeSettingState(2, isAlertOn, true);
}
void HelloWorldScene::updateNextAnimalPrice(){
    nextAnimalPrice = new BigNum();
    nextAnimalPrice->addNum(5000, 0);
    
//    for (int i = 0; i < getTotalAnimalCountInArea(selectedArea); i++) {
//    for (int i = 0; i < charList.size(); i++) {
    int totalSpawnedAnimalCount = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
    //    for (int i = 0; i < animalNames.size(); i++) {
    int animalCounter = 0;
    for (int i = 0; i < totalSpawnedAnimalCount; i++) {
        int area = getArea(i);
        if (area != AREA_AFRICA && area != AREA_INVENTORY_AFRICA ) {
            continue;
        }
        
//        if (!isHunted(i)) {
//        if (animalRank.at(charList.at(i)->charIndex).compare("normal") == 0) {
        int index = getAnimalIndexForID(i);
        if (animalRank.at(index).compare("normal") == 0) {
            int multiNum = 12;
            if (animalPrices.size() > animalCounter) {
                multiNum = Value(animalPrices.at(animalCounter)).asInt();
            }else{
                multiNum = Value(animalPrices.at(animalPrices.size() - 1)).asInt();
            }
            nextAnimalPrice->multiply(multiNum);
//            if (animalCounter > 40) {
//                nextAnimalPrice->multiply(7);
//            }else if (animalCounter > 30) {
//                nextAnimalPrice->multiply(8);
//            }else if (animalCounter > 20) {
//                nextAnimalPrice->multiply(9);
//            }else if (animalCounter > 12) {
//                nextAnimalPrice->multiply(10);
//            }else{
//                nextAnimalPrice->multiply(12);
//            }
            animalCounter++;
        }
    }
}
void HelloWorldScene::updateAnimalPickTab(){
    Text* lbl = (Text*)itemCharacter->getChildByName("itemAnimal_0")->getChildByName("btnUpgrade")->getChildByName("lblPrice");
    Sprite* sptLeaf = (Sprite*)itemCharacter->getChildByName("itemAnimal_0")->getChildByName("btnUpgrade")->getChildByName("sptIcon");
    
    updateNextAnimalPrice();
    
    lbl->setString(nextAnimalPrice->getExpression());
    
    GameManager::getInstance()->alignToCenter(sptLeaf, lbl, 10, 69, 0);
}
void HelloWorldScene::zoomInScrolllView(Node* node, float scale){
    if(!isInitDone){
        return;
    }
    
//    Point offset = GameManager::getInstance()->isPortrait?Point::ZERO:Point(-300, 0);
    Point offset = Point::ZERO;
    svField->stopAllActions();
    svField->getInnerContainer()->stopAllActions();
    GameManager::getInstance()->scrollToItem(svField, node->getPosition(), offset);
    
//    if (GameManager::getInstance()->isPortrait) {
        svField->getInnerContainer()->runAction(Sequence::create(DelayTime::create(2), MoveTo::create(0.2f, Point(-1334 + size.width/2, 0)), nullptr));
//    }else{
//        svField->getInnerContainer()->runAction(Sequence::create(DelayTime::create(2), MoveTo::create(0.2f, Point(-1674 + size.width/2, 0)), nullptr));
//    }
    
    showParticle(node, node->getBoundingBox().size.width);
    
//    svField->setAnchorPoint(Point(offset.x > 0?0.25:0.5, 0));
    
    float y = node->getPositionY()*scale;
    float centerY = svField->getContentSize().height/2;
    float defaultY = 0;
    if (GameManager::getInstance()->isPortrait) {
        defaultY = 584;
    }
    float moveY = defaultY + centerY - y;
    if (moveY < defaultY - centerY*scale) {
        moveY = defaultY - centerY*scale;
    }
    svField->runAction(MoveTo::create(0.3f, Point(size.width/2, moveY)));
    
    svField->runAction(Sequence::create(ScaleTo::create(0.3f, scale), DelayTime::create(1), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::zoomInEnded, this)),  ScaleTo::create(0.3f, 1),nullptr));
//    svField->runAction(Sequence::create(MoveBy::create(0.3f, 2), DelayTime::create(1), MoveTo::create(0.3f, Point::ZERO), nullptr));
}
void HelloWorldScene::zoomInEnded(){
//    svField->setAnchorPoint(Point(0.5, 0));
    float y = 0;
    if (GameManager::getInstance()->isPortrait) {
        y =584;
    }
//    svField->runAction(MoveBy::create(0.3f, Point(0, yMove)));
//    svField->setAnchorPoint(Point::ZERO);
        svField->runAction(MoveTo::create(0.3f, Point(size.width/2, y)));
}
void HelloWorldScene::ddiyong(Node* node){
    node->setScale(0.1f);
    node->runAction(Sequence::create(ScaleTo::create(0.2, 1.1), ScaleTo::create(0.05, 0.95),ScaleTo::create(0.05, 1), NULL));
    node->setPosition(size/2);
}
cocos2d::Point HelloWorldScene::getHalfPos(cocos2d::Size theSize){
    return Point(theSize.width/2, theSize.height/2);
}
int HelloWorldScene::getMagicItemPrice(int index){
    if(index == 0){
        return 0;
    }else if(index == 1){
        return 100;
    }else if(index == 2){
        return 200;
    }else if(index == 3){
        return 400;
    }else if(index == 4){
        return 800;
    }
    return 100;
}
void HelloWorldScene::showRainbow(){
    btnSkillList.at(2)->getChildByName("sptEffect")->setVisible(true);
    btnSkillList.at(2)->getChildByName("sptEffect")->setLocalZOrder(100);
    GameManager::getInstance()->playSoundEffect(SOUND_SKILL_RAINBOW);
    donottouchrainbow = true;
    rainbowNode = CSLoader::createNode("rainbow.csb");
    this->addChild(rainbowNode, -9979);
    rainbowNode->setPosition(Point(740, 400 + svField->getPositionY()));
    cocostudio::timeline::ActionTimeline* action = CSLoader::createTimeline("rainbow.csb");
    rainbowNode->runAction(action);
    action->gotoFrameAndPlay(0, 15, false);
    this->scheduleOnce(schedule_selector(HelloWorldScene::startRainbowShin), 15.0f*1/60);
    float angle = 3;
    float time = 0.25f;
    rainbowNode->runAction(RepeatForever::create(Sequence::create(RotateBy::create(time, angle), RotateBy::create(time, -angle), NULL)));
}
void HelloWorldScene::startRainbowShin(float dt){
    cocostudio::timeline::ActionTimeline* action = CSLoader::createTimeline("rainbow.csb");
    rainbowNode->runAction(action);
    action->gotoFrameAndPlay(16, 57, true);
    donottouchrainbow = false;
}
void HelloWorldScene::showAurora(){
    btnSkillList.at(0)->getChildByName("sptEffect")->setVisible(true);
    btnSkillList.at(0)->getChildByName("sptEffect")->setLocalZOrder(100);
    GameManager::getInstance()->playSoundEffect(SOUND_SKILL_AURORA);
    float y = 0;
    while(true){
        Sprite* spt = Sprite::create("Aurora_loop.png");
        this->addChild(spt, -9979);
        auroraList.pushBack(spt);
        spt->setScale(2);
        spt->setPosition(530, y);
        y += spt->getScale()*spt->getContentSize().height;
        if(y > 750){
            break;
        }
    }
    this->schedule(schedule_selector(HelloWorldScene::updatingAurora));
}
void HelloWorldScene::updatingAurora(float dt){
    for(int i = 0;i<auroraList.size();i++){
        Sprite* spt = auroraList.at(i);
        if(spt->getPositionY() < -spt->getContentSize().height){
            auroraList.eraseObject(spt);
            spt->removeFromParent();
            break;
        }
    }
    float y = 0;
    float speed = 180;
    int sptOverSceeenCount = 0;
    for(int i = 0;i<auroraList.size();i++){
        Sprite* spt = auroraList.at(i);
        spt->setPositionY(spt->getPositionY() - speed*dt);
        spt->setAnchorPoint(Point(0.5, 0));
        y = spt->getPositionY() + spt->getContentSize().height*spt->getScale();
        if(y > 750){
            sptOverSceeenCount++;
        }
    }
    if(sptOverSceeenCount <= 1){
        Sprite* spt = Sprite::create("Aurora_loop.png");
        spt->setAnchorPoint(Point(0.5, 0));
        spt->setPosition(530, y);
        this->addChild(spt, -9979);
        auroraList.pushBack(spt);
        spt->setScale(2);
    }
}
void HelloWorldScene::showWind(){
    GameManager::getInstance()->playSoundEffect(SOUND_SKILL_WIND);
    for(int i = 0; i < 30; i++){
        int randIndex = rand()%5;
        Sprite* spt = Sprite::create(StringUtils::format("note_%d.png", randIndex));
        this->addChild(spt);
        spt->setOpacity(0);
        spt->setPosition(Point(-rand()%1500, rand()%1334));
        spt->runAction(Sequence::create(FadeIn::create(0.5f), DelayTime::create(3-0.5f), FadeOut::create(0.5f), nullptr));
        spt->runAction(Sequence::create(MoveBy::create(3+(rand()%100)*0.01f, Point(2000, 0)), CallFuncN::create(CC_CALLBACK_1(Sprite::removeFromParentAndCleanup, spt)), nullptr));
        spt->runAction(RepeatForever::create(Sequence::create(MoveBy::create(1+(rand()%40)*0.01f, Point(0, 40)), MoveBy::create(1+(rand()%40)*0.01f, Point(0, -40)), NULL)));
    }
}

std::string HelloWorldScene::getAreaIconName(int area){
    if(area == 0){
        return "africaIcon.png";
    }else if(area == 1){
        return "asiaIcon.png";
    }else{
        return "asiaIcon.png";
    }
}
void HelloWorldScene::updateMap(){
    return;
//    Node* item;
//    Node* itemMap = svMap->getChildByName("itemMap");
    Button* btn;
    ImageView* sptSmallIcon, *sptIcon, *sptIconInButton, *item;
    std::string btnImageName, btnImageDownName;
    Text* lblCount, *lblDescription;
    bool condition0, condition1;
    for (int i = 0; i < AREA_KIND; i++) {
        sptSmallIcon = (ImageView*)itemMap->getChildByName(StringUtils::format("sptSmallArea%d", i).c_str());
        GameManager::getInstance()->setLocalizedString(StringUtils::format("area%d", i), (Text*)itemMap->getChildByName(StringUtils::format("lblSmallArea%d", i)));
        item = (ImageView*)svMap->getChildByName(StringUtils::format("item%d" ,i).c_str());
        sptIcon = (ImageView*)item->getChildByName("sptIcon");
        lblDescription = (Text*)item->getChildByName("lblDescription");
        GameManager::getInstance()->setLocalizedString(StringUtils::format("area%d", i), lblDescription);
        lblCount = (Text*)item->getChildByName("lblAnimalCount");
        lblCount->setString(StringUtils::format("%d/%d", getTotalAnimalCountInArea(i), getMaxAnimalPossessCount()));
        btn = (Button*)item->getChildByName("btnUpgrade");
        Text* lblTitleInButton =(Text*)btn->getChildByName("lblDescription"), lblPriceInButton;
        Text* lblPrice = (Text*)btn->getChildByName("lblPrice");
        sptIconInButton = (ImageView*)btn->getChildByName("sptIcon");
        bool isButtonEnabled = true;
        if(isAreaUnlocked(i)){
            sptIcon->loadTexture(getAreaIconName(i));
            if(item->getChildByName("sptCheck0") != nullptr){
                item->getChildByName("sptCheck0")->removeFromParentAndCleanup(true);
                item->getChildByName("sptCheck1")->removeFromParentAndCleanup(true);
                item->getChildByName("lblCondition0")->removeFromParentAndCleanup(true);
                item->getChildByName("lblCondition1")->removeFromParentAndCleanup(true);
            }
            if(selectedArea != i){
                btnImageName = "btnUpgrade.png";
                btnImageDownName = "btnUpgradeDown.png";
                GameManager::getInstance()->setLocalizedString("move", lblTitleInButton);
            }else{
                GameManager::getInstance()->setLocalizedString("selected area", lblTitleInButton);
            }
            lblTitleInButton->setPositionY(btn->getContentSize().height/2);
            if(lblPrice != nullptr){
                lblPrice->removeFromParentAndCleanup(true);
            }
            if(sptIconInButton != nullptr){
                sptIconInButton->removeFromParentAndCleanup(true);
            }
            lblCount->setTextColor(Color4B(94, 93, 73, 255));
            lblDescription->setTextColor(Color4B(94, 93, 73, 255));
            item->loadTexture("ui_list_bg.png");
        }else{
            sptIcon->loadTexture("lockedArea.png");
            sptSmallIcon->loadTexture("lockedArea.png");
            condition0 = false;
            condition1 = true;
            sptIcon = (ImageView*)item->getChildByName("sptCheck0");
            sptIcon->loadTexture(condition0?"sptChecked.png":"sptUnchecked.png");
            sptIcon = (ImageView*)item->getChildByName("sptCheck1");
            sptIcon->loadTexture(condition1?"sptChecked.png":"sptUnchecked.png");
            // lblCondition0 text
            // lblCondition1 text
            int relicCount = getRelicPoint();
            int price = getRelicPrice(i);
            if(condition0 && condition1 && relicCount >= price){
                btnImageName = "btnUnlock.png";
                btnImageDownName = "btnUnlockDown.png";
            }else{
                btnImageName = "btnDisabled.png";
                btnImageDownName = "btnDisabled.png";
                isButtonEnabled = false;
            }
            lblPrice->setString(Value(price).asString());
            GameManager::getInstance()->alignToCenter(sptIconInButton, lblPrice, 0, btn->getContentSize().width/2, -4);
            GameManager::getInstance()->setLocalizedString("open area", lblTitleInButton);
            lblCount->setTextColor(Color4B(250, 248, 214, 255));
            lblDescription->setTextColor(Color4B(250, 248, 214, 255));
            item->loadTexture("ui_list_bg_locked.png");
        }
        if(selectedArea == i){
            btnImageName = "btnSelected.png";
            btnImageDownName = "btnSelectedDown.png";
        }
        btn->loadTextureNormal(btnImageName);
        btn->loadTexturePressed(btnImageDownName);
        btn->setEnabled(isButtonEnabled);
    }
}
int HelloWorldScene::getRelicPoint(){
    return UserDefault::getInstance()->getIntegerForKey(KEY_RELIC_POINT, 0);
}
void HelloWorldScene::addRelicPoint(int amount){
    int point = UserDefault::getInstance()->getIntegerForKey(KEY_RELIC_POINT, 0);;
    point += amount;
    UserDefault::getInstance()->setIntegerForKey(KEY_RELIC_POINT, point);
    if(amount > 0){
        int total = UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_RELIC_GET, 0);
        total += amount;
        UserDefault::getInstance()->setIntegerForKey(KEY_TOTAL_RELIC_GET, total);
    }
}
int HelloWorldScene::getRelicPrice(int relicIndex){
    return 99;
}
bool HelloWorldScene::isAreaUnlocked(int areaIndex){
    if(areaIndex == 0){
        return true;
    }else{
        return UserDefault::getInstance()->getBoolForKey(StringUtils::format("AreaUnlocked%d", areaIndex).c_str(), false);
    }
}
void HelloWorldScene::unlockArea(int areaIndex){
    UserDefault::getInstance()->setBoolForKey(StringUtils::format("AreaUnlocked%d", areaIndex).c_str(), true);
}
int HelloWorldScene::getAnimalCountInArea(int areaIndex, int animalIndex){
    animalIndex = getAnimalIndex(animalIndex);
    return UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ANIMAL_COUNT_IN_AREA_FORMAT, areaIndex, animalIndex).c_str(), 0);
}
void HelloWorldScene::addAnimalCountInArea(int areaIndex, int animalIndex, int amount){
    animalIndex = getAnimalIndex(animalIndex);
    int count = UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ANIMAL_COUNT_IN_AREA_FORMAT, areaIndex, animalIndex).c_str(), 0);
    count += amount;
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(KEY_ANIMAL_COUNT_IN_AREA_FORMAT, areaIndex, animalIndex).c_str(), count);
}
int HelloWorldScene::getTotalAnimalCount(int index){
//    int count = 0;
//    count += getAnimalCountInArea(AREA_INVENTORY_AFRICA, index);
//    count += getAnimalCountInArea(selectedArea, index);
//    return count;
    int totalSpawnedAnimalCount = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
    int animalCount = 0;
    for (int i = 0; i < totalSpawnedAnimalCount; i++) {
        int area = getArea(i);
        if (area == AREA_INVENTORY_AFRICA || area == AREA_AFRICA) {
            animalCount++;
        }
    }
    return animalCount;
}
int HelloWorldScene::getTotalAnimalCountThatIHave(){
    int totalSpawnedAnimalCount = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
    int total = 0;
    for (int i = 0; i < totalSpawnedAnimalCount; i++) {
        int area = getArea(i);
        if (area == AREA_AFRICA || area == AREA_INVENTORY_AFRICA) {
            total++;
        }
    }
    return total;
}
int HelloWorldScene::getTotalAnimalCountInArea(int areaIndex){
    int total = 0;
//    for (int i = 0; i < animalNames.size(); i++) {
//        total += getAnimalCountInArea(areaIndex, i);
//    }
    int totalSpawnedAnimalCount = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
    for (int i = 0; i < totalSpawnedAnimalCount; i++) {
        int area = getArea(i);
        if (area == areaIndex) {
            total++;
        }
    }
    return total;
}
void HelloWorldScene::onMapButtonClick(Ref* ref){
    
}
void HelloWorldScene::updateMainTree(){
    for(auto deco : decoList){
        deco->removeFromParentAndCleanup(true);
    }
    decoList.clear();
    silukList.clear();
    shakingList.clear();
    int unlockedCount = 0;
    for (int i = 0; i < SANHO_COUNT; i++) {
        if (isSanhoUnlocked(i)) {
            unlockedCount++;
        }else{
            break;
        }
    }
    selectedNode= treeBack;
    
    float y = 280;
    float x = 1341;
    if (unlockedCount == 0) {
        treeBack->setPosition(1341, 280);
    }else if (unlockedCount == 1) {
        treeBack->setPosition(1341, 303);
    }else if (unlockedCount == 2) {
        treeBack->setPosition(1341, 311);
    }else if (unlockedCount == 3) {
        treeBack->setPosition(1341, 331);
    }else if (unlockedCount >= 4 && unlockedCount < 7) {
        treeBack->setPosition(1341, 338);
    }else if (unlockedCount >= 7 && unlockedCount < 9) {
        treeBack->setPosition(1341, 343);
    }else if (unlockedCount >= 9) {
        treeBack->setPosition(1341, 358);
    }
//    log("(%f, %f)", treeBack->getPosition().x, treeBack->getPosition().y);
    
    if (unlockedCount > 0) {
        Point pos;
        int level = 0;
        if (unlockedCount == 1) {
            pos = (Point(x, y));
        }else if (unlockedCount == 2) {
            pos = (Point(1338, 272));
            level = 1;
        }else if (unlockedCount == 3) {
            pos = (Point(1342, 272));
            level = 2;
        }else if (unlockedCount >= 4 && unlockedCount < 7) {
            pos = (Point(1340, 252));
            level = 3;
        }else if (unlockedCount >= 7 && unlockedCount < 9) {
            pos = (Point(1340, 245));
            level = 4;
        }else if (unlockedCount >= 9) {
            pos = (Point(1340, 243));
            level = 5;
        }
        Sprite* bottom = Sprite::createWithSpriteFrameName(StringUtils::format("treeBottomHill%d.png", level));
        bottom->setPosition(pos);
        bottom->setAnchorPoint(Point(0.5, 0));
        decoList.pushBack(bottom);
        
        mainTreeBatch->addChild(bottom, -6);
    }
    
    Sprite* bottomGlow=nullptr;
    if (unlockedCount > 1) {
        Point pos;
        int level = 0;
        if (unlockedCount == 1) {
            pos = (Point(1338, 238));
        }else if (unlockedCount == 2) {
            pos = (Point(1342, 235));
            level = 1;
        }else if (unlockedCount == 3) {
            pos = (Point(1341, 227));
            level = 2;
        }else if (unlockedCount >= 4 && unlockedCount < 7) {
            pos = (Point(1341, 195));
            level = 3;
        }else if (unlockedCount >= 7 && unlockedCount < 9) {
            pos = (Point(1344, 218));
            level = 4;
        }else if (unlockedCount >= 9) {
            pos = (Point(1329, 171));
            level = 5;
        }
        bottomGlow = Sprite::createWithSpriteFrameName(StringUtils::format("treeBottomHill%dBottom.png", level));
        bottomGlow->setAnchorPoint(Point(0.5, 0));
        bottomGlow->setPosition(pos);
        decoList.pushBack(bottomGlow);
    }
    Vector<Sprite*> flowers;
    if (unlockedCount > 0) {
        int flowerCount = getSanhoLevel(0)/100 + 1;
        if (flowerCount > 5) {
            flowerCount = 5;
        }
        for (int i = (int)flowerList.size(); i < flowerCount; i++) {
            std::string name = StringUtils::format("treeBottomDeco0_%d.png", i);
            if (mainTreeBatch->getChildByName(name) != nullptr) {
                continue;
            }
            Sprite* spt = Sprite::createWithSpriteFrameName(name);
            spt->setName(name);
            flowers.pushBack(spt);
            flowerList.pushBack(spt);
            mainTreeBatch->addChild(spt, 1);
            spt->setTag(0);
            shakingList.pushBack(spt);
//            float extraY = (unlockedCount-1)*16;
            if (i == 0) {
                spt->setPosition(Point(-39, 26) + treeBack->getPosition());
            }else if (i == 1) {
                spt->setPosition(Point(-48, 38) + treeBack->getPosition());
            }else if (i == 2) {
                spt->setPosition(Point(-62, 38) + treeBack->getPosition());
            }else if (i == 3) {
                spt->setPosition(Point(50, 38) + treeBack->getPosition());
            }else if (i == 4) {
                spt->setPosition(Point(33, 30) + treeBack->getPosition());
            }
            spt->setPositionY(spt->getPositionY() - spt->getContentSize().height/2);
            spt->setAnchorPoint(Point(0.5f, 0));
            zoomInScrolllView(spt, 2);
        }
    }
    if (unlockedCount > 1) {    // mushrooms
        int decoCount = getSanhoLevel(1)/100 + 1;
        if (decoCount > 5) {
            decoCount = 5;
        }
        for (int i = (int)mushroomList.size(); i < decoCount; i++) {
            std::string name = StringUtils::format("treeBottomDeco1_%d.png", i);
            if (mainTreeBatch->getChildByName(name) != nullptr) {
                continue;
            }
            Sprite* spt = Sprite::createWithSpriteFrameName(name);
            spt->setName(name);
            mainTreeBatch->addChild(spt, 1);
//            decoList.pushBack(spt);
            mushroomList.pushBack(spt);
            spt->setTag(1);
            silukList.pushBack(spt);
            if (i == 0) {
                spt->setPosition(1356, 215);
            }else if (i == 1) {
                spt->setPosition(1402, 222);
            }else if (i == 2) {
                spt->setPosition(1458, 233);
            }else if (i == 3) {
                spt->setPosition(1516, 241);
            }else if (i == 4) {
                spt->setPosition(1555, 266);
            }
            zoomInScrolllView(spt, 2);
        }
    }
    if (unlockedCount > 2) {    // blue grass
        int decoCount = getSanhoLevel(2)/100 + 1;
        if (decoCount > 5) {
            decoCount = 5;
        }
        for (int i = (int)blueLeafList.size(); i < decoCount; i++) {
            std::string name = StringUtils::format("treeBottomDeco2_%d.png", i);
            if (mainTreeBatch->getChildByName(name) != nullptr) {
                continue;
            }
            Sprite* spt = Sprite::createWithSpriteFrameName(name);
            spt->setName(name);
            mainTreeBatch->addChild(spt, -i);
//            decoList.pushBack(spt);
            blueLeafList.pushBack(spt);
            spt->setTag(2);
            silukList.pushBack(spt);
            if (i == 0) {
                spt->setPosition(1296, 231);
            }else if (i == 1) {
                spt->setPosition(1238, 248);
            }else if (i == 2) {
                spt->setPosition(1183, 255);
            }else if (i == 3) {
                spt->setPosition(1135, 289);
            }else if (i == 4) {
                spt->setPosition(1101, 309);
            }
            zoomInScrolllView(spt, 2);
        }
    }
    if (unlockedCount > 3) {    // pink bush
        int decoCount = getSanhoLevel(3)/100 + 1;
        if (decoCount > 5) {
            decoCount = 5;
        }
        for (int i = (int)pinkBushList.size(); i < decoCount; i++) {
            std::string name = StringUtils::format("treeBottomDeco3_%d.png", i);
            if (mainTreeBatch->getChildByName(name) != nullptr) {
                continue;
            }
            Sprite* spt = Sprite::createWithSpriteFrameName(name);
            spt->setName(name);
            mainTreeBatch->addChild(spt, -i-7);
//            decoList.pushBack(spt);
            pinkBushList.pushBack(spt);
            spt->setTag(3);
            shakingList.pushBack(spt);
            if (i == 0) {
                spt->setPosition(1573, 314);
            }else if (i == 1) {
                spt->setPosition(1562, 328);
            }else if (i == 2) {
                spt->setPosition(1527, 355);
            }else if (i == 3) {
                spt->setPosition(1496, 374);
            }else if (i == 4) {
                spt->setPosition(1451, 372);
            }
            spt->setPositionY(spt->getPositionY() - spt->getContentSize().height/2);
            spt->setAnchorPoint(Point(0.5f, 0));
            zoomInScrolllView(spt, 2);
        }
    }
    if (unlockedCount > 6) {    // yellow leafs
        int decoCount = getSanhoLevel(6)/100 + 1;
        if (decoCount > 5) {
            decoCount = 5;
        }
        for (int i = (int)yellowLeafList.size(); i < decoCount; i++) {
            std::string name = StringUtils::format("treeBottomDeco4_%d.png", i);
            if (mainTreeBatch->getChildByName(name) != nullptr) {
                continue;
            }
            Sprite* spt = Sprite::createWithSpriteFrameName(name);
            spt->setName(name);
            mainTreeBatch->addChild(spt, i-11);
//            decoList.pushBack(spt);
            yellowLeafList.pushBack(spt);
            spt->setTag(6);
            shakingList.pushBack(spt);
            if (i == 0) {
                spt->setPosition(1233, 366);
            }else if (i == 1) {
                spt->setPosition(1187, 377);
            }else if (i == 2) {
                spt->setPosition(1159, 393);
            }else if (i == 3) {
                spt->setPosition(1104, 352);
            }else if (i == 4) {
                spt->setPosition(1126, 409);
                spt->setLocalZOrder(i-11-2);
            }
            spt->setPositionY(spt->getPositionY() - spt->getContentSize().height/2);
            spt->setAnchorPoint(Point(0.5f, 0));
            zoomInScrolllView(spt, 2);
        }
    }
    if (unlockedCount > 8) {
        int decoCount = getSanhoLevel(6)/100 + 1;
        if (decoCount > 5) {
            decoCount = 5;
        }
        if (rootFallIndex < decoCount) {
            rootFallIndex = decoCount;
            Sprite* spt = Sprite::createWithSpriteFrameName(StringUtils::format("treeBottomDeco5_%d.png", decoCount-1));
            mainTreeBatch->addChild(spt, -6);
            decoList.pushBack(spt);
            if (decoCount == 1) {
                spt->setPosition(1434, 281);
            }else if (decoCount == 2) {
                spt->setPosition(1438, 289);
            }else if (decoCount == 3) {
                spt->setPosition(1446, 296);
            }else if (decoCount == 4) {
                spt->setPosition(1353, 297);
            }else if (decoCount == 5) {
                spt->setPosition(1349, 290);
            }
            zoomInScrolllView(spt, 2);
        }
    }
    if (bottomGlow != nullptr) {
        mainTreeBatch->addChild(bottomGlow, -7);
    }
}
void HelloWorldScene::onFieldTouch(Ref* pSender, Widget::TouchEventType type){
    if (type == Widget::TouchEventType::BEGAN) {
        isTouchedForLife = true;
        touchBeganFieldPos = svField->getInnerContainer()->getPosition();
        shakeTree();
        
        
        for (int i = 0; i < 5; i++) {
            Sprite* spt = Sprite::createWithSpriteFrameName("eff_ptc.png");
            mainTreeBatch->addChild(spt);
            Point pos = ((Widget*)pSender)->getTouchBeganPosition() - svField->getPosition() - svField->getInnerContainerPosition() + Point(svField->getContentSize().width/2, 0);
            spt->setPosition(pos);
            spt->setOpacity(0);
            spt->runAction(FadeIn::create(0.16f + (rand()%20)*0.01f));
            float dur = 0.5f + (rand()%20)*0.01f;
            spt->setOpacity(rand()%150 + 100);
            spt->runAction(MoveBy::create(dur, Point(rand()%30 - 15, 30 + rand()%20)));
            spt->runAction(Sequence::create(DelayTime::create(dur*2/3), FadeOut::create(dur/3), CallFuncN::create(CC_CALLBACK_1(Sprite::removeFromParentAndCleanup, spt)), nullptr));
        }
        
        
        if (!tapQuestDone) {
            tapCounter++;
            if (tapCounter >= getQuestMaxCount(3)) {
                tapQuestDone = true;
                setQuestProgress(3, tapQuestDone);
            }
            if (questLayer != nullptr) {
                setQuestProgress(3, tapCounter);
                updateQuest(0);
            }
            
        }
//        oneSecTimeElapse = 0;
        int index = rand()%4;
        if(index == 0){
            GameManager::getInstance()->playSoundEffect(SOUND_GRASS_0);
        }else if(index == 1){
            GameManager::getInstance()->playSoundEffect(SOUND_GRASS_1);
        }else if(index == 2){
            GameManager::getInstance()->playSoundEffect(SOUND_GRASS_2);
        }else if(index == 3){
            GameManager::getInstance()->playSoundEffect(SOUND_GRASS_3);
        }
        if(addAchievementCurrent(ACHIEVE_TAP_10000, 1) >= 10000){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQCg");
        }
        if(addAchievementCurrent(ACHIEVE_TAP_100000, 1) >= 100000){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQCw");
        }
        if(addAchievementCurrent(ACHIEVE_TAP_1000000, 1) >= 1000000){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQDA");
        }
        if(addAchievementCurrent(ACHIEVE_TAP_10000000, 1) >= 10000000){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQDQ");
        }
//        int total = UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_TAP_COUNT, 0);
//        total++;
//        UserDefault::getInstance()->setIntegerForKey(KEY_TOTAL_TAP_COUNT, total);
    }else if(type == Widget::TouchEventType::ENDED){
        if (touchBeganFieldPos.distance(svField->getInnerContainer()->getPosition()) < 50 && isCaptureMode) {
            for(auto animal: charList){
                Point pos = ((Widget*)pSender)->getTouchBeganPosition() - svField->getPosition() - svField->getInnerContainerPosition() + Point(svField->getContentSize().width/2, 0);
                Rect rect = animal->getBoundingBox();
                float radius = 200;
                Rect modifiedRect = Rect(rect.origin.x - radius/2, rect.origin.y - radius/2, radius, radius);
                if (modifiedRect.containsPoint(pos)){
                    onAnimalOnFieldClick(animal);
                }
            }
        }
        
        if (present != nullptr) {
            Point pos = ((Widget*)pSender)->getTouchBeganPosition() - svField->getPosition() - svField->getInnerContainerPosition() + Point(svField->getContentSize().width/2, 0);
            Rect rect = Rect(present->getPosition() + present->getChildByName("sptRect")->getPosition() + Point(-46, -46), Size(92, 92));
            if (rect.containsPoint(pos)){
                onPresentClick();
            }
        }
    }
}
void HelloWorldScene::backToLabelPool(Ref* ref){
    Label* lbl = (Label*)ref;
    lbl->removeFromParentAndCleanup(false);
}
void HelloWorldScene::showLabelFromPool(Node* parent, cocos2d::Point pos, std::string text, int moveHeight){
    Label* lbl = labelPool.at(labelPoolIndex);
    if (lbl->getParent()) {
        lbl->removeFromParentAndCleanup(false);
    }
    parent->addChild(lbl, 10);
    float visibleTime = 0.5f;
    float fadingTime = 1;
    lbl->setString(text);
    lbl->stopAllActions();
    lbl->setPosition(pos);
    lbl->setOpacity(255);
    lbl->runAction(MoveBy::create(visibleTime+fadingTime, Point(0, moveHeight)));
    lbl->runAction(Sequence::create(DelayTime::create(visibleTime), FadeOut::create(fadingTime), CallFuncN::create(CC_CALLBACK_1(HelloWorldScene::backToLabelPool ,   this)), NULL));
    labelPoolIndex++;
    if (labelPoolIndex >= labelPoolCount) {
        labelPoolIndex = 0;
    }
}
void HelloWorldScene::addListener(){
    auto listener = EventListenerTouchAllAtOnce::create();
    listener->onTouchesEnded = CC_CALLBACK_2(HelloWorldScene::onTouchesEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}
void HelloWorldScene::onTabChange(int tab){
    if (tab == TAB_SKILL) {
        onTreeClick();
    }else if (tab == TAB_CHARACTER) {
        onAnimalClick();
    }else if (tab == TAB_COLLECTION) {
        onCollectionClick();
    }else if (tab == TAB_RELIC) {
        onRelicClick();
//        showAttackResult(doAttackIsSuccess(true), true); // test
    }else if (tab == TAB_MAP) {
        onMapClick();
    }else if (tab == TAB_SHOP) {
        onShopClick();
    }else if (tab == TAB_ACHIEVEMENT){
        onAchievementClick();
    }
    btnUpgrade10->setOpacity(0);
    btnUpgrade100->setOpacity(0);
    selectedTapIndex = tab;
//    oneSecTimeElapse = 0;
    updateMainButtons();
    updateMainMenuRedBean();
}
void HelloWorldScene::onAchievementClick(){
    showScrollView(TAB_ACHIEVEMENT);
    updateAchievementItems();
}
void HelloWorldScene::updateAchievementItems(){
    Text* lbl = (Text*)itemAchievement->getChildByName("lblPlayTime");
    lbl->setString(StringUtils::format("%s: %s", GameManager::getInstance()->getText("play time").c_str(), GameManager::getInstance()->getTimeLeftInString(UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_PLAY_TIME, 0)).c_str()));
    lbl = (Text*)itemAchievement->getChildByName("lblRelicPoint");
    lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("relic earn count").c_str(), UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_RELIC_GET, 0)));
    lbl = (Text*)itemAchievement->getChildByName("lblAnimalCreate");
    lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("animal create count").c_str(), UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_ANIMAL_CREATE, 0)));
    lbl = (Text*)itemAchievement->getChildByName("lblRareAnimalCreate");
    lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("rare animal create count").c_str(), UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_RARE_ANIMAL_CREATE, 0)));
    lbl = (Text*)itemAchievement->getChildByName("lblTotalTap");
    lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("total tap count").c_str(), getAchievementCurrent(ACHIEVE_TAP_10000)));
    lbl = (Text*)itemAchievement->getChildByName("lblBestHunterLevel");
    lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("hunter best level").c_str(), UserDefault::getInstance()->getIntegerForKey(KEY_BEST_HUNTER_LEVEL, 0)));
    lbl = (Text*)itemAchievement->getChildByName("lblSkillUse");
    lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("skill use count").c_str(), UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_SKILL_USE, 0)));
    Button* clone;
    for (int i = 0; i < achievementCount; i++) {
//        clone = svAchievement->getInnerContainer()->getChildByTag(i);
        int index = getAchievementIndex(i);
        clone = (Button*)svAchievement->getInnerContainer()->getChildByName(__String::createWithFormat("item%d", i)->getCString());
        Text* lblDescription = (Text*)clone->getChildByName("lblDescription_0");
        Text* lblTitle = (Text*)clone->getChildByName("lblDescription");
//        lblTitle->setTextColor(Color4B(57, 60, 51, 255));
        if(lblDescription != nullptr){
//            lblDescription->setTextColor(Color4B(105, 102, 83, 255));
            
            std::string strFormat;
            strFormat = GameManager::getInstance()->getText(StringUtils::format("achievement name %d", index/10 - 1).c_str());
//            if(index/10 == 0){
//                strFormat = GameManager::getInstance()->getText(StringUtils::format("achievement name %d", 0).c_str());
//            }else if(i == 13){
//                strFormat = GameManager::getInstance()->getText(StringUtils::format("achievement name %d", 0).c_str());
//            }else if(i >= 14 && i <= 16){
//                strFormat = GameManager::getInstance()->getText(StringUtils::format("achievement name %d", 8).c_str());
//            }else if(i >= 17 && i <= 19){
//                strFormat = GameManager::getInstance()->getText(StringUtils::format("achievement name %d", 9).c_str());
//            }else if(i >= 20 && i <= 21){
//                strFormat = GameManager::getInstance()->getText(StringUtils::format("achievement name %d", 10).c_str());
//            }else if(i >= 22 && i <= 23){
//                strFormat = GameManager::getInstance()->getText(StringUtils::format("achievement name %d", 11).c_str());
//            }else if(i >= 24 && i <= 23){
//                strFormat = GameManager::getInstance()->getText(StringUtils::format("achievement name %d", 12).c_str());
//            }
            if(index/10 == 11){
                lblTitle->setString(StringUtils::format(strFormat.c_str(), GameManager::getInstance()->getText(StringUtils::format("plant%d", getAchievementMax(index) + 1).c_str()).c_str()));
            }else if(strFormat.find("%d") != std::string::npos){
                int maxCount = getAchievementMax(index);
                if( index/10 == 9 ){
                    maxCount /= 10000;
                }
                lblTitle->setString(StringUtils::format(strFormat.c_str(), maxCount));
            }else{
                lblTitle->setString(strFormat);
            }
            
            if (index/10  == 11){
                int current = getAchievementCurrent(index);
                int max = getAchievementMax(index);
                lblDescription->setString(StringUtils::format("%d/1", current >= max?1:0));
            }else{
                int max = getAchievementMax(index);
                int current = getAchievementCurrent(index);
                std::string strMax;
                std::string strCurrent;
                if(current >= 1000){
                    BigNum* num = new BigNum();
                    num->addNum(current, 0);
                    strCurrent = num->getExpression();
                    delete num;
                }else{
                    strCurrent = Value(current).asString();
                }
                if(max >= 1000){
                    BigNum* num = new BigNum();
                    num->addNum(max, 0);
                    strMax = num->getExpression();
                    delete num;
                }else{
                    strMax = Value(current).asString();
                }
                lblDescription->setString(StringUtils::format("%s/%s", strCurrent.c_str(), strMax.c_str()));
            }
        }
        
//        Button* btn = (Button*)clone->getChildByName("btnUpgrade");
        clone->setEnabled(getAchievementCurrent(index) >= getAchievementMax(index) && getAchievementRewardReceivedForHowMuchProgress(index) < getAchievementMax(index));
        
        Text* lblPrice = (Text*)clone->getChildByName("lblPrice");
        Text* lblButtonText = (Text*)clone->getChildByName("lblButtonText");
        ImageView* btnIcon = (ImageView*)clone->getChildByName("sptIcon");
        lblButtonText->setVisible(false);
        if(!clone->isEnabled() && getAchievementRewardReceivedForHowMuchProgress(index) == getAchievementMax(index)){
            lblPrice->setVisible(false);
            btnIcon->setVisible(false);
            GameManager::getInstance()->setLocalizedString(lblButtonText, "received");
            clone->loadTextureDisabled("btnSelected.png");
            clone->setContentSize(Size(160, 32));
            lblButtonText->setVisible(true);
            lblButtonText->setTextColor(Color4B(74, 66, 51, 255));
//            lblButtonText->setPositionY(clone->getContentSize().height/2);
        }else if(i == 12 || i == 24 || i == 25){
            
        }else{//} if(i < achievementCount - 1){
            int price = getAchievementReward(i, getAchievementMax(i));
            lblPrice->setString(Value(price).asString());
            btnIcon->loadTexture("icon_dia_white.png");
            GameManager::getInstance()->setLocalizedString(lblButtonText, "receive");
            GameManager::getInstance()->alignToCenter(btnIcon, lblPrice, 5, clone->getContentSize().width/2, 0);
        }
        if(index/10 == 13){
            lblButtonText->setVisible(true);
        }
        
        showRedBean(clone, clone->isEnabled());
    }
}
bool HelloWorldScene::isAchievementRedBeanEnabled(){
    for (int i = 0; i < achievementCount; i++) {
        int index = getAchievementIndex(i);
        if(getAchievementCurrent(index) >= getAchievementMax(index) && getAchievementRewardReceivedForHowMuchProgress(index) < getAchievementMax(index)){
            return true;
        }
    }
    return false;
}
void HelloWorldScene::onMainMenuClick(Ref* ref){
    if (isCreatingAnimal) {
        return;
    }
    
    if (rand()%100 < 20) {
        sdkbox::PluginAdMob::show("interstitial");
    }
    
    setAbsentRewardTime();
    
    Button* btn = (Button*)ref;
    int button = btn->getTag();
    onTabChange(button);
    
    if (loadedLayer->getChildByName("btnAppear")->isVisible()) {
        OnAppearClick();
    }
}
void HelloWorldScene::onUpgradeDecoClick(Ref* ref){
    Button* btn = (Button*)ref;
    int button = btn->getTag();
}
void HelloWorldScene::onBonusMainTreeClick(){
    log("bonus main tree");
    addMainTreeBonusLevel();
    updateMainTreeItem();
    updateTapReward();
    updateCurrencyLabels();
    GameManager::getInstance()->showDisposableMessage(StringUtils::format("%s +200%%", GameManager::getInstance()->getText("life per tap").c_str()).c_str(), this);
}
void HelloWorldScene::onUpgradeMainTreeClick(){
    BigNum* price = getUpgradeTapRewardPrice();
    if (theLife->IsBiggerThanThis(price) >= 0) {
        theLife->subtractNum(price);
        upgradeTapReward(1);
        handleTheLifeChanged();
//        updateSanhoItems();
    }
    delete price;
}

int HelloWorldScene::getAnimalIndex(int index){
    return Value(animalIndices.at(index)).asInt();
}
void HelloWorldScene::onExpandInventoryClick(){
    
}
int HelloWorldScene::getInventorySlotCount(){
    return (int)charList.size();
//    return UserDefault::getInstance()->getIntegerForKey(KEY_INVENTORY_SLOT_COUNT, 10);
}
void HelloWorldScene::addInventorySlotCount(){
    int count = getInventorySlotCount();
    count++;
    UserDefault::getInstance()->setIntegerForKey(KEY_INVENTORY_SLOT_COUNT, count);
}
int HelloWorldScene::getArea(int ID){
    return UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ANIMAL_AREA_FOR_ID_FORMAT, ID).c_str(), 0);
}
int HelloWorldScene::getAnimalIndexForID(int ID){
    return UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ANIMAL_INDEX_FOR_ID_FORMAT, ID).c_str(), 0);
}
void HelloWorldScene::onManageCharacterClick(){
//    NativeInterface::NativeInterface::cancelAllLocalNotification(); // test
//    TimeManager::getInstance()->testDay++;//test
    if (isCreatingAnimal) {
        return;
    }
    closePopup();
    firstPopup = CSLoader::createNode("animalManage.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    firstPopup->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(firstPopup, 10);
//    GameManager::getInstance()->pushLayer(this, (Layer*)firstPopup);
    Node* frame = firstPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString("manage animal", lbl);
    ddiyong(firstPopup);
    int totalAnimalCount = 0;
//    int totalActiveCount = 0;
//    int totalUnlockdKind = 0;
//    ImageView* sptIcon;
//    int animalCount = (int)animalNames.size();
//    ddiyong(firstPopup);
    
//    for (int i = 0; i < 6; i++) {
//        totalActiveCount += getTotalAnimalCountInArea(i);
//        
//        ImageView* item = (ImageView*)frame->getChildByName(StringUtils::format("sptArea%d", i));
//        lbl = (Text*)item->getChildByName("lblCount");
//        if(isAreaUnlocked(i)){
//            item->loadTexture(getAreaIconName(i));
//        }else{
//            item->loadTexture("lockedArea.png");
//            lbl->setVisible(false);
//        }
//        lbl->setString(Value(getTotalAnimalCountInArea(i)).asString());
//    }
    int inventoryCount = 0;
    int alpha = 1;
    int totalSpawnedAnimalCount = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
    for (int i = 0; i < totalSpawnedAnimalCount; i++) {
        int area = getArea(i);
        if (!(area == AREA_AFRICA || area == AREA_INVENTORY_AFRICA)) {
            continue;
        }
        inventoryCount++;
    }
    
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    ScrollView* svAnimals = (ScrollView*)frame->getChildByName("svAnimals");
    
    int detailItemHeight = 120;
    
    std::string name;
    svAnimals->setClippingEnabled(true);
//    int totalInventoryItemCount = 0;
//    int hiddenCount = getTotalAnimalCountInArea(AREA_INVENTORY_AFRICA);
//    int inventoryCount = hiddenCount + (int)charList.size();
    
    float scrollHeight = (inventoryCount/5 + (inventoryCount%5==0?0:1))*(itemGapY + detailItemHeight) + 40;
    svAnimals->setInnerContainerSize(Size(713, scrollHeight));
    int hiddenIndex = -1;
    int ID = 0;
    int animalCounter = 0;
    int area;
    for (int i = 0; i < totalSpawnedAnimalCount; i++) {
        area = getArea(i);
        if (!(area == AREA_AFRICA || area == AREA_INVENTORY_AFRICA)) {
            continue;
        }
        ID = i;
        int animalIndex = getAnimalIndexForID(ID);
        ImageView* clone = ImageView::create("UI/icon/icon_frame_2_120X120.png");
        clone->setContentSize(Size(120, 120));
        
//        if (i >= totalSpawnedAnimalCount) {
//            if (i == animalCount) {
//                alpha = inventoryCount - totalInventoryItemCount + 1;
//                alpha = (int)charList.size() + 1;
//            }else if (i == animalCount + alpha - 1) {
//                ImageView* sptIcon = ImageView::create("UI/icon/iconPlus.png");
//                clone->addChild(sptIcon);
            
//                totalAnimalCount += animalCount;
//                clone->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onExpandInventoryClick, this));
//
//                sptIcon->setPosition(Point(60, 60));
//                sptIcon->setTouchEnabled(false);
            
//                alpha = 5 - (totalInventoryItemCount + 1)%5;
//            }else{
//                clone->setColor(Color3B(50, 50, 50));
//            }
//        }else{
//            int unitCount = getAnimalCountInArea(AREA_INVENTORY_AFRICA, i);
//            if(unitCount <= 0){
//                continue;
//            }
//            if(animalAtlas.at(i).size() > 0){
//                name = animalAtlas.at(i).substr(0, animalAtlas.at(i).size() - 6);
//            }
        std::string labelName;
        if (animalRank.at(animalIndex).compare("rare") == 0) {
            labelName = "label_r.png";
        }else if (animalRank.at(animalIndex).compare("unique") == 0) {
            labelName = "label_u.png";
        }else if (animalRank.at(animalIndex).compare("limited") == 0) {
            labelName = "label_l.png";
        }else{
            labelName = "label_n.png";
        }
        Sprite* sptLabel = Sprite::create(labelName);
        sptLabel->setPosition(Point(0, 120));
        sptLabel->setAnchorPoint(Point(0, 1));
        clone->addChild(sptLabel, 1);
        
        name = animalAtlas.at(animalIndex).substr(0, animalAtlas.at(animalIndex).size() - 6);
        ImageView* sptIcon = ImageView::create(StringUtils::format("res/ui-icons/icon_%s.png", name.c_str()));
        clone->addChild(sptIcon);
        
//            totalAnimalCount += animalCount;
        //        if(isAnimalCondition0Meet(i) && isAnimalCondition1Meet(i) && animalCount > 0){
//        if(animalCount > 0){
        clone->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onAnimalDetailClickFromInventory, this));
//        totalUnlockdKind++;
//        }else{
//            sptIcon->setOpacity(80);
//        }
        
        //        clone->setScale(120.0f/92.0f);
        sptIcon->setPosition(Point(60, 60));
        sptIcon->setTouchEnabled(false);
        
        if (area == AREA_INVENTORY_AFRICA) {
            Sprite* sptLock = Sprite::create("sptLockBorder.png");
            sptLock->setPosition(Point(20, 20));
            clone->addChild(sptLock);
        }
//        }
        
        svAnimals->getInnerContainer()->addChild(clone);
        int gapY = 10;
        clone->setPosition(Point(78 + (animalCounter%5)*140, svAnimals->getInnerContainerSize().height - (animalCounter/5 + 1)*(gapY + detailItemHeight) + detailItemHeight/2));
        clone->setTag(ID);
        clone->setTouchEnabled(true);
        animalCounter++;
//        totalInventoryItemCount++;
    }
    
//    lbl = (Text*)frame->getChildByName("lblUnlockedCount");
//    lbl->setString(StringUtils::format("%s: %d/%d", GameManager::getInstance()->getText("animal have kind").c_str(), totalUnlockdKind, animalCount));
//    lbl = (Text*)frame->getChildByName("lblAnimalCount");
//    lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("animal have").c_str(), totalAnimalCount));
    
    for (int i = 0; i < 6; i++) {
        lbl = (Text*)frame->getChildByName(StringUtils::format("lbl%d", i));
        if(i == 0){
            lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("max animal possess count").c_str(), getMaxAnimalPossessCount()));
        }else if(i == 1){
            lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("animal have").c_str(), inventoryCount));
        }else if(i == 2){
            lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("animal protected").c_str(), getProtectedAnimalCount()));//, getMaxProtectedAnimalCount()));// test
        }else if(i == 3){
            lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("total survival power").c_str(), getTotalSurvivalPower()));
        }else if(i == 4){
            lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("next hunter level").c_str(), UserDefault::getInstance()->getIntegerForKey(KEY_ATTACKER_LEVEL, 0)));
        }else if(i == 5){
            lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("next hunter attack").c_str(), getAttackerPower(UserDefault::getInstance()->getIntegerForKey(KEY_ATTACKER_LEVEL, 0))));
        }
    }
}
int HelloWorldScene::getMaxAnimalPossessCount(){
    int count = 0;
    for (int i = 0; i < SANHO_COUNT; i++) {
        if (isSanhoUnlocked(i)) {
            count++;
        }else{
//            break;
            continue;
        }
    }
    return 7 + count*7;
}
void HelloWorldScene::onPickNormalAnimalClick(){
//    showAnimalDrawPopup(false);
    
    onGetNormalAnimalByPoint();
}
void HelloWorldScene::onPickRareAnimalClick(){
    showAnimalDrawPopup(true);
}
void HelloWorldScene::showAnimalDrawPopup(bool isRare){
    if (isCreatingAnimal) {
        return;
    }
    closePopup();
    firstPopup = CSLoader::createNode("rareAnimalGet.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    firstPopup->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(firstPopup, 10);
    Node* frame = firstPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString(isRare?"pick rare":"pick normal", lbl);
    lbl = (Text*)frame->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString(isRare?"get rare animal":"get normal animal", lbl);
    ddiyong(firstPopup);
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    btn = (Button*)frame->getChildByName("btnRelic");
    btn->addClickEventListener(isRare?CC_CALLBACK_0(HelloWorldScene::onGetRareAnimalByPoint, this):CC_CALLBACK_0(HelloWorldScene::onGetNormalAnimalByPoint, this));
    lbl = (Text*)btn->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString("create", lbl);
    lbl = (Text*)btn->getChildByName("lblPrice");
    lbl->setString(Value(gerRareAnimalPointPrice()).asString());
    GameManager::getInstance()->alignToCenter(btn->getChildByName("sptIcon"), lbl, 5, btn->getContentSize().width/2, 0);
    
    btn = (Button*)frame->getChildByName("btnGem");
    btn->addClickEventListener(isRare?CC_CALLBACK_0(HelloWorldScene::onGetRareAnimalByGem, this):CC_CALLBACK_0(HelloWorldScene::onGetNormalAnimalByGem, this));
    lbl = (Text*)btn->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString("create", lbl);
    lbl = (Text*)btn->getChildByName("lblPrice");
    lbl->setString(Value(rareAnimalPickDiaPrice).asString());
    GameManager::getInstance()->alignToCenter(btn->getChildByName("sptIcon"), lbl, 5, btn->getContentSize().width/2, 0);
}
int HelloWorldScene::gerRareAnimalPointPrice(){
    int animalCounter = 0;
    int totalSpawnedAnimalCount = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
    for (int i = 0; i < totalSpawnedAnimalCount; i++) {
        int area = getArea(i);
        if (area != AREA_AFRICA && area != AREA_INVENTORY_AFRICA ) {
            continue;
        }
        int index = getAnimalIndexForID(i);
        if (animalRank.at(index).compare("rare") == 0) {
            animalCounter++;
        }
    }
    int rareAnimalByGemCount = UserDefault::getInstance()->getIntegerForKey(KEY_ANIMAL_BY_GEM, 0);
    animalCounter -= rareAnimalByGemCount;
    
    if (animalCounter < animalRarePrices.size()) {
        return Value(animalRarePrices.at(animalCounter)).asInt();
    }else{
        return Value(animalRarePrices.at(animalRarePrices.size() - 1)).asInt();
    }
}
void HelloWorldScene::onGetRareAnimalByPoint(){
    if (getTotalAnimalCountInArea(selectedArea) >= getMaxAnimalPossessCount()) {
        GameManager::getInstance()->showDisposableMessage("exceed max animal possess count", this);
        return;
    }
    
    if (getRelicPoint() >= gerRareAnimalPointPrice()) {
        addRelicPoint(-gerRareAnimalPointPrice());
        int index = 0;
//        if(selectedArea == AREA_AFRICA){
//            index = 33 + rand()%8;
//        }
        while(true){
            index = rand()%(int)animalRank.size();
            if (animalRank.at(index).compare("rare") == 0) {
                break;
            }
        }
        createAnimal(index);
        int total = UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_RARE_ANIMAL_CREATE, 0);
        total++;
        UserDefault::getInstance()->setIntegerForKey(KEY_TOTAL_RARE_ANIMAL_CREATE, total);
        closePopup();
    }else{
        GameManager::getInstance()->showDisposableMessage("not enough relic point", this);
        GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
    }
}
void HelloWorldScene::onGetRareAnimalByGem(){
    if (isCreatingAnimal) {
        return;
    }
    
    if (getTotalAnimalCountInArea(selectedArea) >= getMaxAnimalPossessCount()) {
        GameManager::getInstance()->showDisposableMessage("exceed max animal possess count", this);
        return;
    }
    
    if (GameManager::getInstance()->getGem() >= rareAnimalPickDiaPrice) {
        GameManager::getInstance()->addGem(-rareAnimalPickDiaPrice);
        updateCurrencyLabels();
        int index = 0;
//        if(selectedArea == AREA_AFRICA){
//            index = 33 + rand()%8;
//        }
        
        while(true){
            index = rand()%(int)animalRank.size();
            if (animalRank.at(index).compare("rare") == 0) {
                break;
            }
        }
        int rareAnimalByGem = UserDefault::getInstance()->getIntegerForKey(KEY_ANIMAL_BY_GEM, 0);
        rareAnimalByGem++;
        UserDefault::getInstance()->setIntegerForKey(KEY_ANIMAL_BY_GEM, rareAnimalByGem);
        
        createAnimal(index);
        
        int total = UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_RARE_ANIMAL_CREATE, 0);
        total++;
        UserDefault::getInstance()->setIntegerForKey(KEY_TOTAL_RARE_ANIMAL_CREATE, total);
        closePopup();
    }else{
        GameManager::getInstance()->showDisposableMessage("not enough gem", this);
        GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
    }
}
void HelloWorldScene::onGetNormalAnimalByPoint(){
    if (isCreatingAnimal) {
        return;
    }
    
    if (getTotalAnimalCountInArea(selectedArea) >= getMaxAnimalPossessCount()) {
        GameManager::getInstance()->showDisposableMessage("exceed max animal possess count", this);
        return;
    }
    
    if (theLife->IsBiggerThanThis(nextAnimalPrice) >= 0) {
        theLife->subtractNum(nextAnimalPrice);
//        addRelicPoint(-normalPointPrice);
        int index = 0;
        if(selectedArea == AREA_AFRICA){
            while(true){
                index = rand()%(int)animalRank.size();
                if (animalRank.at(index).compare("normal") == 0) {
                    break;
                }
            }
        }

        createAnimal(index);
    }else{
        GameManager::getInstance()->showDisposableMessage("not enough life", this);
        GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
        return;
    }
    return;
    if (getRelicPoint() >= normalPointPrice) {
        addRelicPoint(-normalPointPrice);
        int index = 0;
        if(selectedArea == AREA_AFRICA){
            index = 0 + rand()%33;
        }
        createAnimal(index);
        closePopup();
    }else{
        GameManager::getInstance()->showDisposableMessage("not enough relic point", this);
        GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
    }
}
void HelloWorldScene::onGetNormalAnimalByGem(){
    if (getTotalAnimalCountInArea(selectedArea) >= getMaxAnimalPossessCount()) {
        GameManager::getInstance()->showDisposableMessage("exceed max animal possess count", this);
        return;
    }
    
    if (GameManager::getInstance()->getGem() >= normalDiaPrice) {
        GameManager::getInstance()->addGem(-normalDiaPrice);
        updateCurrencyLabels();
        int index = 0;
        if(selectedArea == AREA_AFRICA){
            index = 0 + rand()%33;
        }
        createAnimal(index);
        closePopup();
    }else{
        GameManager::getInstance()->showDisposableMessage("not enough gem", this);
        GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
    }
}
void HelloWorldScene::onCreateCharacterClick(Ref* ref){
    if (getTotalAnimalCountInArea(selectedArea) >= getMaxAnimalPossessCount()) {
        GameManager::getInstance()->showDisposableMessage("exceed max animal possess count", this);
        return;
    }
    
    if (isCreatingAnimal) {
        return;
    }
    Button* btn = (Button*)ref;
    int index = btn->getTag();
    
    bool condition0 = isAnimalCondition0Meet(index);
    bool condition1 = isAnimalCondition1Meet(index);
    bool forceUnlocked = !(condition0 && condition1) && getTotalAnimalCount(index) > 0;
    if ((condition0 && condition1) || forceUnlocked) {
        if (theLife->IsBiggerThanThis(nextAnimalPrice) >= 0) {
            theLife->subtractNum(nextAnimalPrice);
        }else{
            GameManager::getInstance()->showDisposableMessage("not enough life", this);
            GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
            return;
        }
    }else{
        int gem = GameManager::getInstance()->getGem();
        int price = 200; // replace this with force unlock price
        if (gem > price) {
            GameManager::getInstance()->addGem(-price);
            updateCurrencyLabels();
        }else{
            GameManager::getInstance()->showDisposableMessage("not enough gem", this);
            GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
            return;
        }
    }
    
    createAnimal(index);
}
void HelloWorldScene::createAnimal(int index){
    GameManager::getInstance()->playSoundEffect(SOUND_CREATE_ANIMAL);
    addAccumulatedAnimalCount(index);
    int totalAnimalCount = getAccumulatedAnimalCount(index);
//    if (totalAnimalCount > 1 && !isHunted(index)) {
//        showAnimalGot(index);
//        onAnimalClick();
//        return;
//    }
    setQuestProgress(1, 1);
    for (int i = 0; i < charList.size(); i++) {
        charList.at(i)->readyForFriend();
    }
    indexToCreateAnimal = index;
    setHunted(index, false);
    animalCreateEffect = Character::create();
    animalCreateEffect->setSpine("eff_call.json.txt", "eff_call.atlas");
    svField->addChild(animalCreateEffect);
    animalCreateEffect->setPosition(Point(1334, 130));
    float time = animalCreateEffect->runAnimation("eff_call_f");
    this->runAction(Sequence::create(DelayTime::create(time), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::createAnimalLater, this)), NULL));

    isCreatingAnimal = true;
    addAnimalCountInArea(selectedArea, index, 1);
    
    if (GameManager::getInstance()->isPortrait) {
        svField->getInnerContainer()->runAction(MoveTo::create(0.2f, Point(-1334 + size.width/2, 0)));
    }else{
        svField->getInnerContainer()->runAction(MoveTo::create(0.2f, Point(-1674 + size.width/2, 0)));
    }
    
    loadedLayer->runAction(Sequence::create(DelayTime::create(5), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::createAnimalDone, this)), NULL));
    
    updateMainTreeItem();
    
    int total = UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_ANIMAL_CREATE, 0);
    total++;
    UserDefault::getInstance()->setIntegerForKey(KEY_TOTAL_ANIMAL_CREATE, total);
    if(total == 1 || total%10 == 0){
        NativeInterface::NativeInterface::trackEvent("animal", "get", Value(total).asString().c_str(), "", "create", total);
    }
}
void HelloWorldScene::createAnimalDone(){
    isCreatingAnimal = false;
    loadedLayer->runAction(FadeIn::create(0.3f));
//    showAnimalInputText();
    log("createAnimalDone");
    showAnimalGot(indexToCreateAnimal, true);
    onTabChange(TAB_CHARACTER);
    updateAnimalPickTab();
    updateTapReward();
    updateTotalSanhoEffect();
    updateCurrencyLabels();
}
void HelloWorldScene::createAnimalLater(){
    Character* ch = Character::create();
    ch->setSpineAnimation(indexToCreateAnimal);
    ch->stayCount = 6;
    charList.pushBack(ch);
    ch->setPosition(Point(1334, 130));
    ch->setName(StringUtils::format("animal_%d", indexToCreateAnimal));
    ch->setTag(indexToCreateAnimal);
    svField->addChild(ch, 0);
    ch->anim->setOpacity(0);
    ch->setID();
    ch->setArea(AREA_AFRICA);
    ch->anim->runAction(FadeIn::create(1));
    setHunted(indexToCreateAnimal, false);
    
    if (animalRank.at(indexToCreateAnimal).compare("rare") == 0) {
        if(addAchievementCurrent(ACHIEVE_RARE_ANIMAL_1, 1) >= 1){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQAQ");
        }
        if(addAchievementCurrent(ACHIEVE_RARE_ANIMAL_10, 1) >= 10){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQAg");
        }
    }else if (animalRank.at(indexToCreateAnimal).compare("unique") == 0) {
        if(addAchievementCurrent(ACHIEVE_UNIQUE_ANIMAL, 1) >= 1){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQBw");
        }
    }else if (animalRank.at(indexToCreateAnimal).compare("limited") == 0) {
        if(addAchievementCurrent(ACHIEVE_LIMITED_ANIMAL, 1) >= 1){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQCA");
        }
    }else{
        
    }
    
    if (animalAtlas.at(indexToCreateAnimal).find("bird") != std::string::npos) {
        if(addAchievementCurrent(ACHIEVE_BIRD, 1) >= 10){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQCA");
        }
    }else if (animalAtlas.at(indexToCreateAnimal).find("bigcat_1") != std::string::npos ||
              animalAtlas.at(indexToCreateAnimal).find("bigcat_2") != std::string::npos) {
        if(addAchievementCurrent(ACHIEVE_LION, 1) >= 1){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQAw");
        }
    }
    
    animalCreateEffect->runAnimation("eff_call_1");
//    updateAnimalEffect();
}
void HelloWorldScene::onMagicItemClick(Ref* ref){
    Button* btn = (Button*)ref;
    int button = btn->getTag();
}

void HelloWorldScene::onRelicSlotClick(Ref* ref){
    Node* btn = (Node*)ref;
    int index = btn->getTag();
    if(!isRelicSlotUnlocked(index)){
        tryUnlockRelicSlot(index);
    }
}
void HelloWorldScene::tryUnlockRelicSlot(int index){
    if (GameManager::getInstance()->getGem() >= unlockRelicSlotPrice) {
        GameManager::getInstance()->addGem(-unlockRelicSlotPrice);
        updateCurrencyLabels();
        UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_RELIC_SLOT_UNLOCKED_FORMAT, index).c_str(), true);
        onRelicClick();
        if(firstPopup != nullptr){
            closePopup();
            onRelicManageClick();
        }
        GameManager::getInstance()->showDisposableMessage("relic slot unlocked", this);
    }else{
        GameManager::getInstance()->showDisposableMessage("not enough gem", this);
        GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
    }
}
void HelloWorldScene::onRelicItemClick(Ref* ref){
    Button* btn = (Button*)ref;
    int index = btn->getTag();
    if (isRelicInUse(index)) {
        unequipRelic(index);
    }else{
        long startedTime = getRelicReadyStartedTime(index);
        bool isGettingReady = startedTime > 0;
        if (isGettingReady) {
            return;
        }else{
            int slotAvailableCount = 0;
            for (int i = 0; i < 5; i++) {
                if(isRelicSlotUnlocked(i)){
                    slotAvailableCount++;
                }
            }
            int slotOccupiedCount = 0;
            for (int i = 0; i < relicEffects.size(); i++) {
                if (isRelicInUse(i)) {
                    slotOccupiedCount++;
                }
            }
            if(slotAvailableCount > slotOccupiedCount){
                equipRelic(index);
            }else{
                // slot not availble
                GameManager::getInstance()->showDisposableMessage("out of relic slot", this);
            }
        }
    }
    resetRelic();
    updateCurrencyLabels();
}
void HelloWorldScene::onGochaRelicClick(Ref* ref){
//    Button* btn = (Button*)ref;
    //    int button = btn->getTag();
    closePopup();
    firstPopup = CSLoader::createNode("relicGet.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    firstPopup->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(firstPopup, 10);
    Node* frame = firstPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString("discover relic", lbl);
    lbl = (Text*)frame->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString("get relic description", lbl);
    ddiyong(firstPopup);
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    btn = (Button*)frame->getChildByName("btnRelic");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onGochaWithRelicClick, this));
    lbl = (Text*)btn->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString("discover relic", lbl);
    lbl = (Text*)btn->getChildByName("lblPrice");
    int relicPickPointPrice = getRelicPrice();
    lbl->setString(Value(relicPickPointPrice).asString());
    GameManager::getInstance()->alignToCenter(btn->getChildByName("sptIcon"), lbl, 5, btn->getContentSize().width/2, 0);
    
    btn = (Button*)frame->getChildByName("btnGem");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onGochaWithGemClick, this));
    lbl = (Text*)btn->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString("discover relic", lbl);
    lbl = (Text*)btn->getChildByName("lblPrice");
    lbl->setString(Value(relicPickDiaPrice).asString());
    GameManager::getInstance()->alignToCenter(btn->getChildByName("sptIcon"), lbl, 5, btn->getContentSize().width/2, 0);
}
int HelloWorldScene::getRelicPrice(){
    int counter = 0;
    for (int i = 0; i < relicEffects.size(); i++) {
        if(isRelicUnlocked(i)){
            counter++;
        }
    }
    if (counter < relicPrices.size()) {
        return Value(relicPrices.at(counter)).asInt();
    }else{
        return Value(relicPrices.at(relicPrices.size() - 1)).asInt();
    }
}
//void HelloWorldScene::closePopup(){
//    GameManager::getInstance()->playSoundEffect(SOUND_BTN_POS);
//    if(firstPopup != nullptr){
//        firstPopup->removeFromParentAndCleanup(true);
//        firstPopup = nullptr;
//    }
//}
void HelloWorldScene::onGochaWithGemClick(){
    if(isThereRelicLeftToPick()){
        int price = relicPickDiaPrice;
        if(GameManager::getInstance()->getGem() >= price){
            GameManager::getInstance()->addGem(-price);
            updateCurrencyLabels();
            Node* frame = firstPopup->getChildByName("frame");
            Button* btn = (Button*)frame->getChildByName("btnGem");
            btn->setVisible(false);
            btn = (Button*)frame->getChildByName("btnRelic");
            btn->setVisible(false);
            //animation
            GameManager::getInstance()->playSoundEffect(SOUND_CREATE_RELIC);
            cocostudio::timeline::ActionTimeline* action = CSLoader::createTimeline("relicGet.csb");
            firstPopup->runAction(action);
            action->gotoFrameAndPlay(5, false);
            this->scheduleOnce(schedule_selector(HelloWorldScene::onRelicPickLater), 65.0f*1/50);
        }else{
            GameManager::getInstance()->showDisposableMessage("not enough gem", this);
            GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
        }
    }else{
        GameManager::getInstance()->showDisposableMessage("no relic to pick anymore", this);
    }
}
void HelloWorldScene::onGochaWithRelicClick(){
    if(isThereRelicLeftToPick()){
        int relicPickPointPrice = getRelicPrice();
        int price = relicPickPointPrice;
        if(getRelicPoint() >= price){
            addRelicPoint(-price);
            updateCurrencyLabels();
            Node* frame = firstPopup->getChildByName("frame");
            Button* btn = (Button*)frame->getChildByName("btnGem");
            btn->setVisible(false);
            btn = (Button*)frame->getChildByName("btnRelic");
            btn->setVisible(false);
            cocostudio::timeline::ActionTimeline* action = CSLoader::createTimeline("relicGet.csb");
            GameManager::getInstance()->playSoundEffect(SOUND_CREATE_RELIC);
            firstPopup->runAction(action);
            action->gotoFrameAndPlay(5, false);
            this->scheduleOnce(schedule_selector(HelloWorldScene::onRelicPickLater), 65.0f*1/50);
        }else{
            GameManager::getInstance()->showDisposableMessage("not enough relic point", this);
            GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
        }
    }else{
        GameManager::getInstance()->showDisposableMessage("no relic to pick anymore", this);
    }
}
void HelloWorldScene::onRelicPickLater(float dt){
    closePopup();
    int index = gochaRelic();
    int starCount = Value(relicEffects.at(index).at(3)).asInt();
    if(starCount == 4){
        if(addAchievementCurrent(ACHIEVE_4_STAR_RELIC, 1) >= 1){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQBQ");
        }
    }else if(starCount == 5){
        if(addAchievementCurrent(ACHIEVE_5_STAR_RELIC, 1) >= 1){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQBg");
        }
    }
    int total = 0;
    for (int i = 0; i < relicEffects.size(); i++) {
        if (!isRelicUnlocked(i)) {
            continue;
        }
        total++;
    }
    if(total == 1 || total%10 == 0){
        NativeInterface::NativeInterface::trackEvent("legacy", "get", Value(total).asString().c_str(), "", "create", total);
    }
    
    showRelicDetail(index);
    resetRelic();
    onRelicClick();
}
void HelloWorldScene::onPuppyPickLater(float dt){
    closePopup();
    int index = 27;
    UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_RELIC_HAVE_FORMAT, index).c_str(), index);
    showRelicDetail(index);
    resetRelic();
    onRelicClick();
}
void HelloWorldScene::showRelicDetail(int index){
    // make detail popup
//    closePopup();
    secondPopup = CSLoader::createNode("relicGot.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
//    secondPopup->setAnchorPoint(Point(0.5, 0.5));
    secondPopup->setPosition(size.width/2, size.height/2);
    this->addChild(secondPopup, 10);
    Node* frame = secondPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString("relic got", lbl);
    lbl = (Text*)frame->getChildByName("lblRelicName");
    GameManager::getInstance()->setLocalizedString(StringUtils::format("relic_%d", index).c_str(), lbl);
    lbl = (Text*)frame->getChildByName("lblEffect0");
    lbl->setString(getRelicEffect0(index));
    lbl = (Text*)frame->getChildByName("lblEffect1");
    lbl->setString(getRelicEffect1(index));
    ddiyong(secondPopup);
    // icon
    ImageView* sptIconBg = (ImageView*)frame->getChildByName("sptIconBg");
    ImageView* sptIcon = ImageView::create(StringUtils::format("res/ui-icons/%s.png", relicEffects.at(index).at(2).c_str()));
    sptIconBg->addChild(sptIcon);
    frame->setScale(120.0f/92.0f);
    sptIcon->setPosition(Point(60, 70));
    addStarToIcon(sptIcon, index);
    
    Button* btn = (Button*)secondPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    btn = (Button*)frame->getChildByName("btnOk");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn->setTitleText(GameManager::getInstance()->getText("ok"));
}
void HelloWorldScene::onRelicManageClick(){
    closePopup();
    firstPopup = CSLoader::createNode("relicManage.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
//    firstPopup->setAnchorPoint(Point(0.5, 0.5));
    firstPopup->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(firstPopup, 10);
    Node* frame = firstPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString("relic management", lbl);
    ddiyong(firstPopup);
    for (int i = 3; i < 5; i++) {
        ImageView* imgSlot = (ImageView*)frame->getChildByName(StringUtils::format("sptSlot%d", i));
        imgSlot->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onRelicSlotClick, this));
        ((Text*)imgSlot->getChildByName("lblPrice"))->setString(Value(unlockRelicSlotPrice).asString());
    }
    
    std::vector<int> relicListInUse;
    for (int i = 0; i < relicEffects.size(); i++) {
        if (isRelicInUse(i)) {
            relicListInUse.push_back(i);
        }
    }
    
    ImageView* sptIcon;
    for (int i = 0; i < 5; i++) {
        ImageView* item = (ImageView*)frame->getChildByName(StringUtils::format("sptSlot%d", i));
        
        sptIcon = (ImageView*)item->getChildByName("sptLock");
        if(sptIcon == nullptr){
            sptIcon = ImageView::create();
            item->addChild(sptIcon);
        }
        if(isRelicSlotUnlocked(i)){
            if(item->getChildByName("sptIcon") != nullptr){
                item->getChildByName("sptIcon")->removeFromParentAndCleanup(true);
                item->getChildByName("lblPrice")->removeFromParentAndCleanup(true);
            }
            if (relicListInUse.size() > i) {
                sptIcon->loadTexture(StringUtils::format("res/ui-icons/%s.png", relicEffects.at(relicListInUse.at(i)).at(2).c_str()));
                sptIcon->setContentSize(Size(92, 92));
                sptIcon->setScale(1.2f);
                sptIcon->setPosition(Point(60, 70));
                addStarToIcon(sptIcon, relicListInUse.at(i));
                item->setTag(relicListInUse.at(i));
                item->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onRelicDetailClick, this));
                item->setTouchEnabled(true);
                sptIcon->setVisible(true);
            }else{
                sptIcon->setVisible(false);
            }
        }else{
            
        }
    }
    
    lbl = (Text*)frame->getChildByName("lblInnerTitle");
    GameManager::getInstance()->setLocalizedString("equipped relic effect", lbl);
    
    int visibleLabelCount = 0;
    for (int j = 0; j < 11; j++) {
        lbl = (Text*)frame->getChildByName("svEffects")->getChildByName(StringUtils::format("lblEffect%d", j));
        int relicRate = 0;
        for (int i = 0; i < relicEffects.size(); i++) {
            if (isRelicInUse(i)) {
                std::string str = relicEffects.at(i).at(j+5);
                if(str.size() > 0){
                    relicRate += Value(str).asInt();
                }
            }
        }
        lbl->setString(StringUtils::format(GameManager::getInstance()->getText(StringUtils::format("relic effect format%d", j).c_str()).c_str(), relicRate));
        if(relicRate <= 0){
            lbl->setVisible(false);
        }else{
            visibleLabelCount++;
        }
    }
    if(visibleLabelCount <= 6){
        ((ScrollView*)frame->getChildByName("svEffects"))->setEnabled(false);
    }
    lbl = (Text*)frame->getChildByName("svEffects")->getChildByName(StringUtils::format("lblEffect%d", 11));
    lbl->setVisible(false);
    
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    ScrollView* svRelicList = (ScrollView*)frame->getChildByName("svRelics");
    int relicCount = (int)relicEffects.size();
    int detailItemHeight = 120;
    float scrollHeight = (relicCount/5 + (relicCount%5==0?0:1))*(itemGapY + detailItemHeight) + 40;
    svRelicList->setInnerContainerSize(Size(713, scrollHeight));
    for (int i = 0; i < relicCount; i++) {
        ImageView* clone = ImageView::create("icon_frame.png");
        clone->setContentSize(Size(120, 120));
        ImageView* sptIcon = ImageView::create(StringUtils::format("res/ui-icons/%s.png", relicEffects.at(i).at(2).c_str()));
        clone->addChild(sptIcon);
        
        clone->setScale(120.0f/92.0f);
        sptIcon->setPosition(Point(46, 52));
        sptIcon->setTouchEnabled(false);
        svRelicList->getInnerContainer()->addChild(clone);
        clone->setPosition(Point(78 + (i%5)*140, svRelicList->getInnerContainerSize().height + itemGapY - (i/5 + 1)*(itemGapY + detailItemHeight) + 30));
        clone->setTag(i);
        addStarToIcon(sptIcon, i);
        if(isRelicUnlocked(i)){
            clone->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onRelicDetailClick, this));
        }else{
            sptIcon->setOpacity(50);
        }
        
        clone->setTouchEnabled(true);
    }
}
void HelloWorldScene::onRelicDetailClick(Ref* ref){
    Node* img = (Node*)ref;
    showRelicDetail(img->getTag());
}
void HelloWorldScene::onAnimalDetailClickFromInventory(Ref* ref){
    Node* img = (Node*)ref;
    showAnimalDetailFromInventory(img->getTag());
}
int HelloWorldScene::getProtectedAnimalCount(){
    int hiddenCount = 0;
    int totalSpawnedAnimalCount = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
    for (int i = 0; i < totalSpawnedAnimalCount; i++) {
        int area = getArea(i);
        if (area == AREA_INVENTORY_AFRICA) {
            hiddenCount++;
        }
    }
    return hiddenCount;
}
int HelloWorldScene::getMaxProtectedAnimalCount(){
    return 5;
}
void HelloWorldScene::onAnimalDetailClick(Ref* ref){
    Node* img = (Node*)ref;
    if(getAccumulatedAnimalCount(img->getTag()) <= 0){
        return;
    }
    showAnimalGot(img->getTag());
//    showAnimalDetail(img->getTag());
}
void HelloWorldScene::showAnimalDetailFromInventory(int ID){
    secondPopup = CSLoader::createNode("animalDetail.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    secondPopup->setTag(ID);
    bool isHidden = getArea(ID) == AREA_INVENTORY_AFRICA;
    int index = getAnimalIndexForID(ID);
    //    secondPopup->setAnchorPoint(Point(0.5, 0.5));
    secondPopup->setPosition(size.width/2, size.height/2);
    this->addChild(secondPopup, 10);
    Node* frame = secondPopup->getChildByName("frame");
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    int hour = now->tm_hour;
    bool isDay = hour > 6 && hour < 18;
    ImageView* imgFrame = (ImageView*)frame->getChildByName("innerFrame");
    Size frameSize = imgFrame->getContentSize();
    imgFrame->loadTexture(isDay?"capture_bg_day.png":"capture_bg_night.png");
    imgFrame->setScale9Enabled(false);
    imgFrame->setContentSize(frameSize);
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    lbl->setString(GameManager::getInstance()->getText(animalNames.at(index).c_str()));
    ddiyong(secondPopup);
    Character* ch = Character::create();
    ch->setSpine(index);
    ch->setScale(1.5f);
    Node* innerFrame = frame->getChildByName("innerFrame");
    ch->setPosition(getHalfPos(innerFrame->getContentSize()) + Size(0, -100));
    innerFrame->addChild(ch, 0);
    
    //    ImageView* sptCheck0 = (ImageView*)frame->getChildByName("sptCheck0");
    //    ImageView* sptCheck1 = (ImageView*)frame->getChildByName("sptCheck1");
    //    Text* lblCondition0 = (Text*)frame->getChildByName("lblCondition0");
    //    Text* lblCondition1 = (Text*)frame->getChildByName("lblCondition1");
    //
    //    bool condition0 = isAnimalCondition0Meet(index);
    //    bool condition1 = isAnimalCondition1Meet(index);
    //
    //    sptCheck0->loadTexture(condition0?"sptChecked.png":"sptUnchecked.png");
    //    sptCheck1->loadTexture(condition1?"sptChecked.png":"sptUnchecked.png");
    //    lblCondition0->setString(getAnimalCondition0(index).c_str());
    //    lblCondition1->setString(getAnimalCondition1(index).c_str());
    //    if(lblCondition1->getString().size() <= 0){
    //        sptCheck1->setVisible(false);
    //    }
    
    Button* btn = (Button*)secondPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    lbl = (Text*)frame->getChildByName("lblLevel");
    lbl->setString(StringUtils::format("Lv.%d", getAnimalLevel(index)));
    lbl = (Text*)frame->getChildByName("lblSurvivalPower");
    lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("survival power").c_str(), getSurvivalPower(index)));
    
    Sprite* sptLock = (Sprite*)frame->getChildByName("sptLock");
    sptLock->setVisible(isHidden);
    btn = (Button*)frame->getChildByName("btnInput");
    
    lbl = (Text*)btn->getChildByName("sptDescription");
    if (isHidden) {
        ((ImageView*)btn->getChildByName("sptIcon"))->loadTexture("sptAnimalOut.png");
        GameManager::getInstance()->setLocalizedString(lbl, "show");
        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::showAnimal, this));
    }else{
        ((ImageView*)btn->getChildByName("sptIcon"))->loadTexture("sptAnimalIn.png");
        GameManager::getInstance()->setLocalizedString(lbl, "hide");
        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::hideAnimal, this));
    }
    
    btn = (Button*)frame->getChildByName("btnReturn");
    btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onToNatureClick, this));
    lbl = (Text*)btn->getChildByName("sptDescription");
    GameManager::getInstance()->setLocalizedString(lbl, "to nature");
    
    btn = (Button*)frame->getChildByName("btnOk");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onToNatureConfirmed, this));
    lbl = (Text*)btn->getChildByName("sptDescription");
    GameManager::getInstance()->setLocalizedString(lbl, "ok");
    btn = (Button*)frame->getChildByName("btnCancel");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onToNatureCanceled, this));
    lbl = (Text*)btn->getChildByName("sptDescription");
    GameManager::getInstance()->setLocalizedString(lbl, "cancel");
    
    
    
    //    int totalCount = getTotalAnimalCount(index);
    //    int inAreaCount = getAnimalCountInArea(selectedArea, index);
    //    int inInventoryCount = getAnimalCountInArea(AREA_INVENTORY_AFRICA, index);
    //    btn = (Button*)frame->getChildByName("btnInput");
    //    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onAnimalPutInClick, this));
    //    lbl = (Text*)btn->getChildByName("sptDescription");
    //    GameManager::getInstance()->setLocalizedString("put in", lbl);
    //    lbl = (Text*)btn->getChildByName("lblCount");
    //    lbl->setString(StringUtils::format("%d/%d", inInventoryCount, totalCount));
    //
    //    btn = (Button*)frame->getChildByName("btnOutput");
    //    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onAnimalPutOutClick, this));
    //    lbl = (Text*)btn->getChildByName("sptDescription");
    //    GameManager::getInstance()->setLocalizedString("put in", lbl);
    //    lbl = (Text*)btn->getChildByName("lblCount");
    //    lbl->setString(StringUtils::format("%d/%d", inAreaCount, totalCount));
}
void HelloWorldScene::showAnimal(Ref* ref){
    int ID = secondPopup->getTag();
//    addAnimalCountInArea(AREA_INVENTORY_AFRICA, index, -1);
//    addAnimalCountInArea(selectedArea, index, 1);
    setAnimalArea(ID, AREA_AFRICA);
    
    // move to center
    if (GameManager::getInstance()->isPortrait) {
        svField->getInnerContainer()->runAction(MoveTo::create(0, Point(-1334 + size.width/2, 0)));
    }else{
        svField->getInnerContainer()->runAction(MoveTo::create(0, Point(-1674 + size.width/2, 0)));
    }
    int index = getAnimalIndexForID(ID);
    Character* ch = Character::create();
    ch->setSpineAnimation(index);
    ch->stayCount = 1;
    
    charList.pushBack(ch);
    ch->ID = ID;
    ch->area = AREA_AFRICA;
    ch->setName(StringUtils::format("animal_%d", index));
    ch->setTag(index);
    ch->setPosition(Point(1334, 130));
    svField->addChild(ch, 0);
    ch->anim->runAction(FadeIn::create(1));
    
    closePopup();
    if (firstPopup != nullptr) {
        closePopup();
        onManageCharacterClick();
    }
}
void HelloWorldScene::hideAnimal(Ref* ref){
    int ID = secondPopup->getTag();
//    if (getProtectedAnimalCount() >= getMaxProtectedAnimalCount()) {
//        GameManager::getInstance()->showDisposableMessage("exceed max animal protect count", this);
//        return;
//    }
    setAnimalArea(ID, AREA_INVENTORY_AFRICA);
//    addAnimalCountInArea(AREA_INVENTORY_AFRICA, index, 1);
//    addAnimalCountInArea(selectedArea, index, -1);
    
    for(auto ch: charList){
//        if (ch->getName().compare(StringUtils::format("animal_%d", index)) == 0) {
        if(ch->ID == ID){
            Node* parent = ch->getParent();
            charList.eraseObject(ch);
            if(parent != nullptr){
                ch->removeFromParent();
            }
            log("hide animal : %d", ch->ID);
            break;
        }
    }
    closePopup();
    if (firstPopup != nullptr) {
        closePopup();
        onManageCharacterClick();
    }
}
void HelloWorldScene::onToNatureClick(Ref* ref){
    Node* frame = secondPopup->getChildByName("frame");
    frame->getChildByName("returnToNatureFrame")->setVisible(true);
    Text* lbl = (Text*)frame->getChildByName("lblReturnDescription");
    GameManager::getInstance()->setLocalizedString(lbl, "return to nature warning");
    lbl->setVisible(true);
    frame->getChildByName("btnOk")->setVisible(true);
    frame->getChildByName("btnCancel")->setVisible(true);
}
void HelloWorldScene::setAnimalArea(int ID, int theArea){
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(KEY_ANIMAL_AREA_FOR_ID_FORMAT, ID).c_str(), theArea);
}
void HelloWorldScene::onToNatureConfirmed(){
    int ID = secondPopup->getTag();
    for(auto ch: charList){
        //        if (ch->getName().compare(StringUtils::format("animal_%d", index)) == 0) {
        if(ch->ID == ID){
            Node* parent = ch->getParent();
            charList.eraseObject(ch);
            if(parent != nullptr){
                ch->removeFromParent();
            }
            
            break;
        }
    }
    setAnimalArea(ID, AREA_NATURE);
    subtractAccumulatedAnimalCount(getAnimalIndexForID(ID));
    GameManager::getInstance()->addGem(2);
    updateCurrencyLabels();
    closePopup();
    closePopup();
    onManageCharacterClick();
//    updateAnimalEffect();
    updateTapReward();
}
void HelloWorldScene::onToNatureCanceled(){
    Node* frame = secondPopup->getChildByName("frame");
    frame->getChildByName("returnToNatureFrame")->setVisible(false);
    Text* lbl = (Text*)frame->getChildByName("lblReturnDescription");
    lbl->setVisible(false);
    frame->getChildByName("btnOk")->setVisible(false);
    frame->getChildByName("btnCancel")->setVisible(false);
}
int HelloWorldScene::getSurvivalPower(int index){
    int level = getAnimalLevel(index);
    return (100 + 10*index)*(1 + 0.01f*(level - 1));
}
int HelloWorldScene::getAnimalExp(int index){
    int totalCount = getAccumulatedAnimalCount(index);
    int currentLevel = getAnimalLevel(index);
    for (int i = 1; i < currentLevel; i++) {
        totalCount -= getAnimalCountForLevelUp(i);
    }
    return totalCount;
}
int HelloWorldScene::getAccumulatedAnimalCount(int index){
    return UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ACCUMULATED_ANIMAL_COUNT_FORMAT, index).c_str(), 0);
}
void HelloWorldScene::subtractAccumulatedAnimalCount(int index){
    int count = getAccumulatedAnimalCount(index);
    if (count > 0) {
        count--;
    }
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(KEY_ACCUMULATED_ANIMAL_COUNT_FORMAT, index).c_str(), count);
}
void HelloWorldScene::addAccumulatedAnimalCount(int index){
    int count = getAccumulatedAnimalCount(index);
    count++;
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(KEY_ACCUMULATED_ANIMAL_COUNT_FORMAT, index).c_str(), count);
}
int HelloWorldScene::getAnimalLevel(int index){
//    return UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ANIMAL_LEVEL_FORMAT, index).c_str(), 1);
    int count = getAccumulatedAnimalCount(index);
    int level = 1;
    while(count >= getAnimalCountForLevelUp(level)){
        level++;
        count -= getAnimalCountForLevelUp(level);
    }
    return level;
}
int HelloWorldScene::getAnimalCountForLevelUp(int level){
    int totalCount = level + 1;
    if (totalCount > 10) {
        totalCount = 10;
    }
    return totalCount;
}
void HelloWorldScene::showAnimalGot(int index, bool showBackLight){
    if (getAccumulatedAnimalCount(index) <= 0 && false) { // test
        return;
    }
    closePopup();
    Node* node = CSLoader::createNode("animalGot.csb");
    setAsPopup(node);
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    node->setTag(index);
    //    node->setAnchorPoint(Point(0.5, 0.5));
    node->setPosition(size.width/2, size.height/2);
    this->addChild(node, 10);
    Node* frame = node->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    int hour = now->tm_hour;
    bool isDay = hour > 6 && hour < 18;
    ImageView* imgFrame = (ImageView*)frame->getChildByName("innerFrame");
    Size frameSize = imgFrame->getContentSize();
    imgFrame->loadTexture(isDay?"capture_bg_day.png":"capture_bg_night.png");
    imgFrame->setScale9Enabled(false);
    imgFrame->setContentSize(frameSize);
    lbl->setString(GameManager::getInstance()->getText(animalNames.at(index).c_str()));
    ddiyong(node);
    lbl = (Text*)frame->getChildByName("lblLevel");
    int level = getAnimalLevel(index);
    lbl->setString(StringUtils::format("Lv.%d", level));
    lbl = (Text*)frame->getChildByName("lblSurvivalPower");
    int survivalPower = getSurvivalPower(index);
    lbl->setString(StringUtils::format("%s: %d", GameManager::getInstance()->getText("survival power").c_str(), survivalPower));
    std::string rank;
    
    if (animalRank.at(index).compare("rare") == 0) {
        rank = GameManager::getInstance()->getText("rare species");
    }else if (animalRank.at(index).compare("unique") == 0) {
        rank = GameManager::getInstance()->getText("unique species");
    }else if (animalRank.at(index).compare("limited") == 0) {
        rank = GameManager::getInstance()->getText("limited species");
    }else{
        rank = GameManager::getInstance()->getText("normal species");
    }
    
    lbl = (Text*)frame->getChildByName("lblRareness");
    lbl->setString(StringUtils::format("%s: %s", GameManager::getInstance()->getText("rareness").c_str(), rank.c_str()));
    
    lbl = (Text*)frame->getChildByName("lblBonusEffect");
    lbl->setString(StringUtils::format(GameManager::getInstance()->getText("life get increase effect from animal").c_str(), Value(animalTouchEffect.at(index)).asInt()));
    
    Character* ch = Character::create();
    ch->setSpine(index);
    ch->setScale(1.5f);
    Node* innerFrame = frame->getChildByName("innerFrame");
    ch->setPosition(getHalfPos(innerFrame->getContentSize()) + Size(0, -100));
    innerFrame->addChild(ch, 1);
    
    if (showBackLight) {
        Sprite* sptBackLight = Sprite::create("backLight.png");
        innerFrame->addChild(sptBackLight, 0);
        sptBackLight->setPosition(ch->getPosition() + Point(0, 80));
        sptBackLight->runAction(RepeatForever::create(RotateBy::create(1, 50)));
        sptBackLight->setScale(1.7f);
        
        sptBackLight = Sprite::create("backLight.png");
        innerFrame->addChild(sptBackLight, 0);
        sptBackLight->setPosition(ch->getPosition() + Point(0, 80));
        sptBackLight->runAction(RepeatForever::create(RotateBy::create(1, -90)));
        sptBackLight->setScale(1.7f);
    }
    
    Button* btn = (Button*)node->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    bool isOnStage = false;
    
    for(auto ch: charList){
        if (ch->getName().compare(StringUtils::format("animal_%d", index)) == 0) {
            isOnStage = true;
            break;
        }
    }
    
    int totalCount = getTotalAnimalCount(index);
    int inAreaCount = getAnimalCountInArea(selectedArea, index);
    int inInventoryCount = getAnimalCountInArea(AREA_INVENTORY_AFRICA, index);
    btn = (Button*)frame->getChildByName("btnInput");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onAnimalPutInClick, this));
    btn->setEnabled(isOnStage && !isHunted(index));
    lbl = (Text*)btn->getChildByName("sptDescription");
    GameManager::getInstance()->setLocalizedString("put in", lbl);
//    lbl = (Text*)btn->getChildByName("lblCount");
//    lbl->setString(StringUtils::format("%d/%d", inInventoryCount, totalCount));
    
    btn = (Button*)frame->getChildByName("btnOutput");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onAnimalPutOutClick, this));
    btn->setEnabled(!isOnStage && !isHunted(index));
    lbl = (Text*)btn->getChildByName("sptDescription");
    GameManager::getInstance()->setLocalizedString("put out", lbl);
//    lbl = (Text*)btn->getChildByName("lblCount");
//    lbl->setString(StringUtils::format("%d/%d", inAreaCount, totalCount));
}

void HelloWorldScene::updateAttackReady(float dt){
    if (secondPopup == nullptr) {
        return;
    }
    Node* frame = secondPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    
    lbl->setString(StringUtils::format("%s %s", GameManager::getInstance()->getText("time left").c_str(), GameManager::getInstance()->getTimeLeftInStringHMS(3600 - TimeManager::getInstance()->getCurrentTime()%3600).c_str()));
}
void HelloWorldScene::startDefense(){
    showAttackResult(doAttackIsSuccess(false), false);
}
void HelloWorldScene::showAttackReady(){
    closePopup();
    secondPopup = CSLoader::createNode("defenseReady.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    
    this->schedule(schedule_selector(HelloWorldScene::updateAttackReady), 1);
    
    secondPopup->setPosition(size.width/2, size.height/2);
    this->addChild(secondPopup, 10);
    Node* frame = secondPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    lbl->setString(StringUtils::format("%s %s", GameManager::getInstance()->getText("time left").c_str(), GameManager::getInstance()->getTimeLeftInStringHMS(3600 - TimeManager::getInstance()->getCurrentTime()%3600).c_str()));
    ddiyong(secondPopup);
    
    Button* btn = (Button*)secondPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)secondPopup->getChildByName("btnOk");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::startDefense, this));
    if (sptShield != nullptr) {
        Sprite* spt = Sprite::create("UI/shop/odin.png");
        btn->addChild(spt);
        spt->setPosition(Point(0, btn->getContentSize().height));
    }
    lbl = (Text*)btn->getChildByName("lblText");
    GameManager::getInstance()->setLocalizedString(lbl, "get off");
    
    int level = UserDefault::getInstance()->getIntegerForKey(KEY_ATTACKER_LEVEL, 0);
    Text* lblValue = (Text*)frame->getChildByName("lblResultValue0");
    lblValue->setString(StringUtils::format("Lv.%d", level + 1));
    lblValue = (Text*)frame->getChildByName("lblResultValue1");
    lblValue->setString(Value(getAttackerPower(level)).asString());
    lblValue = (Text*)frame->getChildByName("lblResultValue2");
    lblValue->setString(Value((int)((false?0.8f:1)*getTotalSurvivalPower())).asString());
    lblValue = (Text*)frame->getChildByName("lblResultValue3");
    lblValue->setString(StringUtils::format("%d (-30%%)",(int)((true?0.8f:1)*getTotalSurvivalPower())));
    
    
//    UserDefault::getInstance()->setIntegerForKey(KEY_ATTACKER_LEVEL, level);
    
    ((Text*)frame->getChildByName("lblResultTitle0"))->setString(GameManager::getInstance()->getText("hunter"));
    ((Text*)frame->getChildByName("lblResultTitle1"))->setString(GameManager::getInstance()->getText("attack power"));
    ((Text*)frame->getChildByName("lblResultTitle2"))->setString(GameManager::getInstance()->getText("survival power when defense"));
    ((Text*)frame->getChildByName("lblResultTitle3"))->setString(GameManager::getInstance()->getText("survival power when unattended"));
    
    if (!UserDefault::getInstance()->getBoolForKey("tutorial_attack_done", false)) {
        isTutorialOn = true;
        UserDefault::getInstance()->setBoolForKey("tutorial_attack_done", true);
        tutorialStep = 3;
        removeAndCreateTutorialNode();
        
        Node* sptFinger = tutorialNode->getChildByName("imgFinger");
        sptFinger->removeFromParentAndCleanup(false);
        btn->addChild(sptFinger);
        sptFinger->setVisible(true);
        sptFinger->setPosition(Point(-sptFinger->getContentSize().width/2, btn->getContentSize().height/2));
    }
}
void HelloWorldScene::updateNotification(){
    if (!GameManager::getInstance()->getNotificationOn()) {
        return;
    }
    NativeInterface::NativeInterface::cancelAllLocalNotification();
    int timeRemain = TimeManager::getInstance()->getCurrentTime()%3600;
    int currentHour = (TimeManager::getInstance()->getCurrentTime()/(60*60))%24;
//    NativeInterface::NativeInterface::Push_Notification(GameManager::getInstance()->getText(StringUtils::format("attack alert %d", rand()%3).c_str()).c_str(), 60*60 - timeRemain);
//    NativeInterface::NativeInterface::Push_Notification(GameManager::getInstance()->getText(StringUtils::format("attack alert %d", rand()%3).c_str()).c_str(), 5);
//    NativeInterface::NativeInterface::Push_Notification(GameManager::getInstance()->getText(StringUtils::format("attack alert %d", rand()%3).c_str()).c_str(), 10); // test
    int lastCheckHour = UserDefault::getInstance()->getIntegerForKey(KEY_LAST_ATTACK_CHECK_TIME, -1);
    int hour = (int)(TimeManager::getInstance()->getCurrentTime()/3600);
    int attackCount = hour - lastCheckHour;
    log("hour: %d, lastCheckHour: %d", hour, lastCheckHour);
    if(lastCheckHour == -1 || lastCheckHour <= hour){
        NativeInterface::NativeInterface::Push_Notification(GameManager::getInstance()->getText(StringUtils::format("attack alert %d", rand()%3).c_str()).c_str(), 60*60 - timeRemain);
    }else{
        NativeInterface::NativeInterface::Push_Notification(GameManager::getInstance()->getText(StringUtils::format("attack alert %d", rand()%3).c_str()).c_str(), 60*60 - timeRemain + 60*60);
    }
    
//    NativeInterface::NativeInterface::Push_Notification(GameManager::getInstance()->getText(StringUtils::format("attack alert %d", rand()%3).c_str()).c_str(), 60*60*2 - timeRemain);
    for (int i = 1; i < 48; i++) {
//        if((currentHour + i) < 9 || (currentHour + i) > 22){
        int sec = 60*60*i - timeRemain;
//            NativeInterface::NativeInterface::Push_Notification(GameManager::getInstance()->getText(StringUtils::format("attack alert %d", rand()%3).c_str()).c_str(), sec);
//        }
//        log("sec left: %d, timeRemain: %d", sec + timeRemain, timeRemain);
    }
}
void HelloWorldScene::showAttackResult(bool isSuccess, bool debuf){
    closePopup();
    GameManager::getInstance()->playSoundEffect(SOUND_BGM_VILLAGE);
    // hunt
    int accumulatedRelic = 0;
    std::string removedAnimalName = "";
    if(isSuccess){
        accumulatedRelic = UserDefault::getInstance()->getIntegerForKey(KEY_REWARD_RELIC_COUNT);
        if (accumulatedRelic < 100) {
            accumulatedRelic++;
            UserDefault::getInstance()->setIntegerForKey(KEY_REWARD_RELIC_COUNT, accumulatedRelic);
        }
    }else if (!isSuccess) {
        if (rand()%100 < 5 && charList.size() > 3) {
//            int indexToRemove = rand()%((int)charList.size());
//            UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_HUNTED_FORMAT, indexToRemove).c_str(), true);
////            setHunted(indexToRemove, true);
//            Character* ch = charList.at(indexToRemove);
//            removedAnimalName = StringUtils::format("%s %d", animalNames.at(ch->getTag()).c_str(), 1);
//            charList.eraseObject(ch);
////            if (ch->talkBox != nullptr) {
////                ch->talkBox->removeFromParent();
////            }
//            setAnimalArea(ch->ID, AREA_HUNTED);
////            updateAnimalEffect();
//            ch->removeFromParent();
            huntAnimal();
            updateTapReward();
            
        }else{
            removedAnimalName = GameManager::getInstance()->getText("none");
        }
    }
    int hour = (int)(TimeManager::getInstance()->getCurrentTime()/3600) + 1;
    UserDefault::getInstance()->setIntegerForKey(KEY_LAST_ATTACK_CHECK_TIME, hour);
    log("**showAttackResult");
    secondPopup = CSLoader::createNode("defenseFailed.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    
    updateNotification();
    
    secondPopup->setPosition(size.width/2, size.height/2);
    this->addChild(secondPopup, 10);
    Node* frame = secondPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    lbl->setString(GameManager::getInstance()->getText(isSuccess?"defense success":"defense failed"));
    ddiyong(secondPopup);
    
    Button* btn = (Button*)secondPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)secondPopup->getChildByName("btnOk");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    lbl = (Text*)btn->getChildByName("lblText");
    GameManager::getInstance()->setLocalizedString(lbl, isSuccess?"defense success ok":"defense failed ok");
    
    lbl = (Text*)frame->getChildByName("lblResultLostAnimalName");
    lbl->setVisible(!isSuccess);
    int level = UserDefault::getInstance()->getIntegerForKey(KEY_ATTACKER_LEVEL, 0);
    Text* lblValue = (Text*)frame->getChildByName("lblResultValue0");
    lblValue->setString(StringUtils::format("Lv.%d", level + 1));
    lblValue = (Text*)frame->getChildByName("lblResultValue1");
    lblValue->setString(Value(getAttackerPower(level)).asString());
    lblValue = (Text*)frame->getChildByName("lblResultValue2");
    lblValue->setString(Value((int)((debuf?0.8f:1)*getTotalSurvivalPower())).asString());
    if (!isSuccess) {
        ImageView* sptImage = (ImageView*)secondPopup->getChildByName("sptImage");
        sptImage->loadTexture("attack_failed.png");
        
        GameManager::getInstance()->setLocalizedString(lbl, removedAnimalName.c_str());// test
        frame->getChildByName("lblResultValue3")->setVisible(false);
        lblValue = (Text*)frame->getChildByName("lblResultLostAnimalName");
        lblValue->setString(removedAnimalName);
//        lblValue->setPositionX(lbl->getPositionX() + lbl->getContentSize().width + 10);
    }else{
        ((Text*)frame->getChildByName("lblResultValue3"))->setString(Value(1).asString());
    }
    secondPopup->getChildByName("sptRelic")->setVisible(isSuccess);
    if (isSuccess) {
//        moveIconToPosition(ICON_RELIC, getPosInWorld(secondPopup->getChildByName("sptRelic")), getPosInWorld(loadedLayer->getChildByName("pnlStatus")->getChildByName("iconRelic")));
        addRelicPoint(1);
        updateCurrencyLabels();
    }
    
    if (isSuccess) {
        level++;
        
    }else{
        level-=2;
    }
    if (sptShield == nullptr) {
        UserDefault::getInstance()->setIntegerForKey(KEY_ATTACKER_LEVEL, level);
    }
    
    ((Text*)frame->getChildByName("lblResultTitle0"))->setString(GameManager::getInstance()->getText("hunter"));
    ((Text*)frame->getChildByName("lblResultTitle1"))->setString(GameManager::getInstance()->getText("attack power"));
    ((Text*)frame->getChildByName("lblResultTitle2"))->setString(GameManager::getInstance()->getText("defense survival power"));
    ((Text*)frame->getChildByName("lblResultTitle3"))->setString(GameManager::getInstance()->getText(isSuccess?"receive":"animal lost"));
    
    int hunterLevelChange = UserDefault::getInstance()->getIntegerForKey(KEY_ATTACKER_LEVEL, 0);
    ((Text*)frame->getChildByName("lblResultTitle4"))->setString(StringUtils::format(GameManager::getInstance()->getText(isSuccess?"hunter level rose":"hunter level reduce").c_str(), hunterLevelChange + 1));
    
    
    int best = UserDefault::getInstance()->getIntegerForKey(KEY_BEST_HUNTER_LEVEL, 0);
    if (best < hunterLevelChange) {
        UserDefault::getInstance()->setIntegerForKey(KEY_BEST_HUNTER_LEVEL, hunterLevelChange);
    }
}
void HelloWorldScene::showAnimalDetail(int index){
    closePopup();
    secondPopup = CSLoader::createNode("animalDetail.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    secondPopup->setTag(index);
//    secondPopup->setAnchorPoint(Point(0.5, 0.5));
    secondPopup->setPosition(size.width/2, size.height/2);
    this->addChild(secondPopup, 10);
    Node* frame = secondPopup->getChildByName("frame");
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    int hour = now->tm_hour;
    bool isDay = hour > 6 && hour < 18;
    ImageView* imgFrame = (ImageView*)frame->getChildByName("innerFrame");
    Size frameSize = imgFrame->getContentSize();
    imgFrame->loadTexture(isDay?"capture_bg_day.png":"capture_bg_night.png");
    imgFrame->setContentSize(frameSize);
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    lbl->setString(GameManager::getInstance()->getText(animalNames.at(index).c_str()));
    ddiyong(secondPopup);
    Character* ch = Character::create();
    ch->setSpine(index);
    ch->setScale(1.5f);
    Node* innerFrame = frame->getChildByName("innerFrame");
    ch->setPosition(getHalfPos(innerFrame->getContentSize()) + Size(0, -100));
    innerFrame->addChild(ch, 0);
    
    ImageView* sptCheck0 = (ImageView*)frame->getChildByName("sptCheck0");
    ImageView* sptCheck1 = (ImageView*)frame->getChildByName("sptCheck1");
    Text* lblCondition0 = (Text*)frame->getChildByName("lblCondition0");
    Text* lblCondition1 = (Text*)frame->getChildByName("lblCondition1");
    
    bool condition0 = isAnimalCondition0Meet(index);
    bool condition1 = isAnimalCondition1Meet(index);
    
    sptCheck0->loadTexture(condition0?"sptChecked.png":"sptUnchecked.png");
    sptCheck1->loadTexture(condition1?"sptChecked.png":"sptUnchecked.png");
    lblCondition0->setString(getAnimalCondition0(index).c_str());
    lblCondition1->setString(getAnimalCondition1(index).c_str());
    if(lblCondition1->getString().size() <= 0){
        sptCheck1->setVisible(false);
    }
    
    Button* btn = (Button*)secondPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    int totalCount = getTotalAnimalCount(index);
    int inAreaCount = getAnimalCountInArea(selectedArea, index);
    int inInventoryCount = getAnimalCountInArea(AREA_INVENTORY_AFRICA, index);
    btn = (Button*)frame->getChildByName("btnInput");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onAnimalPutInClick, this));
    lbl = (Text*)btn->getChildByName("sptDescription");
    GameManager::getInstance()->setLocalizedString("put in", lbl);
    lbl = (Text*)btn->getChildByName("lblCount");
    lbl->setString(StringUtils::format("%d/%d", inInventoryCount, totalCount));
    
    btn = (Button*)frame->getChildByName("btnOutput");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onAnimalPutOutClick, this));
    lbl = (Text*)btn->getChildByName("sptDescription");
    GameManager::getInstance()->setLocalizedString("put in", lbl);
    lbl = (Text*)btn->getChildByName("lblCount");
    lbl->setString(StringUtils::format("%d/%d", inAreaCount, totalCount));
}
void HelloWorldScene::onAnimalOnFieldClick(Ref* ref){
    Character* animal = (Character*)ref;
    
//    if(!captureSuccess) return;
//    OnAppearClick();
    closePopup();
    Node* node = CSLoader::createNode("animalShare.csb");
    setAsPopup(node);
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    node->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(node, 10);
    Node* frame = node->getChildByName("frame");
//    Node* ndCenter = frame->getChildByName("frameDay");
//    Point screenShotPos = frame->getChildByName("scrollView")->getPosition();
    
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    int hour = now->tm_hour;
    bool isDay = hour > 6 && hour < 18;
//    frame->getChildByName("frameDay")->setVisible(isDay);
//    frame->getChildByName("frameNight")->setVisible(!isDay);
    ScrollView* scrollView = (ScrollView*)frame->getChildByName("scrollView");
    frame->getChildByName("sptBubble")->setLocalZOrder(2);
    frame->getChildByName("sptLogo")->setLocalZOrder(2);
    
    ImageView* imgFrame = (ImageView*)scrollView->getChildByName("frameBackground");
    Size frameSize = imgFrame->getContentSize();
    imgFrame->loadTexture(isDay?"capture_bg_day.png":"capture_bg_night.png");
    imgFrame->setScale9Enabled(false);
    imgFrame->setContentSize(frameSize);
    
    imgFrame = (ImageView*)node->getChildByName("frame2");
    frameSize = imgFrame->getContentSize();
    imgFrame->loadTexture(isDay?"capture_bg_day.png":"capture_bg_night.png");
    imgFrame->setScale9Enabled(false);
    imgFrame->setContentSize(frameSize);
    
    Character* ch = Character::create();
    ch->setSpineAnimation(animal->charIndex);
//    ch->stayCount = 60000;
    ch->setPosition(Point(200, 90));//ndCenter->getPosition() + Point(0, -110));
    ch->unscheduleAllCallbacks();
    ch->setName("animal");
    frame->getChildByName("scrollView")->addChild(ch, 0);
    ch->runAnimation(CHARACTER_ANI_RUN, 1);
//    ch->anim->setAnimation(0, "run", true);
    
//    auto sp = Sprite::create(filePath);
//    node->addChild(sp);
//    sp->setScale(406/sp->getContentSize().height);
//    sp->setPosition(screenShotPos);
        
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString("your sabana", lbl);
    ddiyong(firstPopup);
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    btn = (Button*)frame->getChildByName("btnLine");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::modifyShareLine, this));
    ((Text*)btn->getChildByName("lblText"))->setString(GameManager::getInstance()->getText("input text"));
    btn = (Button*)frame->getChildByName("btnTake");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::takePicture, this));
    ((Text*)btn->getChildByName("lblText"))->setString(GameManager::getInstance()->getText("take picture"));
    btn = (Button*)frame->getChildByName("btnShare");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onShareAnimalClick, this));
    ((Text*)btn->getChildByName("lblText"))->setString(GameManager::getInstance()->getText("share"));
    btn = (Button*)node->getChildByName("btnChange");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onChangeShareModeClick, this));

    ((Widget*)firstPopup->getChildByName("Image_6"))->addTouchEventListener(CC_CALLBACK_2(HelloWorldScene::onAnimalShareTouch, this));
    
//    firstPopup->getChildByName("frame")->getChildByName("sptZoomLens")->setVisible(false);
    firstPopup->getChildByName("frame")->getChildByName("sptZoomRect")->runAction(RepeatForever::create(Sequence::create(ScaleTo::create(0.5f, 1.3f), DelayTime::create(0.5f), ScaleTo::create(0, 1), nullptr)));
    firstPopup->getChildByName("frame")->getChildByName("sptZoomRect")->runAction(RepeatForever::create(Sequence::create(FadeOut::create(1), DelayTime::create(1), FadeIn::create(0), nullptr)));
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowLeft0")->runAction(RepeatForever::create(Sequence::create(FadeOut::create(1), FadeIn::create(1), NULL)));
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowLeft1")->runAction(RepeatForever::create(Sequence::create(FadeIn::create(0.5f), FadeOut::create(1), FadeTo::create(0.5f, 155), NULL)));
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowLeft2")->runAction(RepeatForever::create(Sequence::create(FadeIn::create(1), FadeOut::create(1), NULL)));
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowRight0")->runAction(RepeatForever::create(Sequence::create(FadeOut::create(1), FadeIn::create(1), NULL)));
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowRight1")->runAction(RepeatForever::create(Sequence::create(FadeIn::create(0.5f), FadeOut::create(1), FadeTo::create(0.5f, 155), NULL)));
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowRight2")->runAction(RepeatForever::create(Sequence::create(FadeIn::create(1), FadeOut::create(1), NULL)));
    
    Sprite* sptAnimal = (Sprite*)firstPopup->getChildByName("frame2")->getChildByName("sptAnimal");
    std::string strName = StringUtils::format("res/ui-icons/icon_%s.png", animalAtlas.at(animal->charIndex).substr(0, animalAtlas.at(animal->charIndex).size() - 6).c_str());
    sptAnimal->setTexture(strName);
//    LayerColor* animalZoomer = LayerColor::create(Color4B::RED);
//    animalZoomer->setContentSize(size);
//    this->addChild(animalZoomer, 100);
//    
//    _animalZoomerTouchListener = EventListenerTouchAllAtOnce::create();
////    CC_SAFE_RETAIN(_animalZoomerTouchListener);
////    _animalZoomerTouchListener->setSha
//    _animalZoomerTouchListener->onTouchesBegan = CC_CALLBACK_2(HelloWorldScene::onTouchesBegan, this);
//    _animalZoomerTouchListener->onTouchesMoved = CC_CALLBACK_2(HelloWorldScene::onTouchesMoved, this);
//    _animalZoomerTouchListener->onTouchesEnded = CC_CALLBACK_2(HelloWorldScene::onTouchesEnded, this);
////    _animalZoomerTouchListener->onTouchesCancelled = CC_CALLBACK_2(Widget::onTouchCancelled, this);
//    _eventDispatcher->addEventListenerWithSceneGraphPriority(_animalZoomerTouchListener, firstPopup->getChildByName("Image_6"));
}
void HelloWorldScene::onAnimalShareTouch(Ref* pSender, Widget::TouchEventType type){
    log("sdfff: %d", (int)_eventDispatcher->_touches.size());
    
    hideZoomerIcon();
    zoomStarted = _eventDispatcher->_touches.size() == 2;
    if (zoomStarted) {
        moveEnabled = false;
    }
    if (zoomStarted && zoomEnabled && type == Widget::TouchEventType::MOVED) {
        zoomEndPoint = (_eventDispatcher->_touches.at(0)->getLocation() + _eventDispatcher->_touches.at(1)->getLocation())/2;
        zoomEndDistance = _eventDispatcher->_touches.at(0)->getLocation().distance(_eventDispatcher->_touches.at(1)->getLocation());
        float scaleDiff = zoomEndDistance - zoomStartDistance;
        float moveDiff = zoomEndPoint.distance(zoomStartPoint);
        log("scaleDiff: %f, moveDiff: %f", scaleDiff, moveDiff);
        Node* animal = firstPopup->getChildByName("frame")->getChildByName("scrollView")->getChildByName("animal");
        animal->setScale(animal->getScale() + scaleDiff*0.01f);
        animal->setPosition(animal->getPosition() + (zoomEndPoint - zoomStartPoint));
        zoomStartPoint = zoomEndPoint;
        zoomStartDistance = zoomEndDistance;
    }
    
    if (zoomStarted && !zoomEnabled) {
        Point pos = _eventDispatcher->_touches.at(0)->getLocation();
        zoomStartPoint = (pos + _eventDispatcher->_touches.at(1)->getLocation())/2;
        zoomStartDistance = _eventDispatcher->_touches.at(0)->getLocation().distance(_eventDispatcher->_touches.at(1)->getLocation());
        zoomEnabled = true;
    }
    
    if (_eventDispatcher->_touches.size() < 2) {
        zoomEnabled = false;
    }
    
    // move
    if(_eventDispatcher->_touches.size() == 1 && moveEnabled && type == Widget::TouchEventType::MOVED){
        Point pos = _eventDispatcher->_touches.at(0)->getLocation();
        Node* animal = firstPopup->getChildByName("frame")->getChildByName("scrollView")->getChildByName("animal");
        animal->setPosition(animal->getPosition() + (pos - moveStartPoint));
        moveStartPoint = pos;
    }
    
    if(_eventDispatcher->_touches.size() == 1 && !moveEnabled && type == Widget::TouchEventType::BEGAN){
        moveEnabled = true;
        moveStartPoint = _eventDispatcher->_touches.at(0)->getLocation();
    }
    
    if(_eventDispatcher->_touches.size() == 1 && type == Widget::TouchEventType::ENDED){
        moveEnabled = false;
    }
}
void HelloWorldScene::hideZoomerIcon(){
    firstPopup->getChildByName("frame")->getChildByName("sptZoomLens")->setVisible(false);
    firstPopup->getChildByName("frame")->getChildByName("sptZoomRect")->setVisible(false);
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowLeft0")->setVisible(false);
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowLeft1")->setVisible(false);
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowLeft2")->setVisible(false);
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowRight0")->setVisible(false);
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowRight1")->setVisible(false);
    firstPopup->getChildByName("frame")->getChildByName("sptZoomArrowRight2")->setVisible(false);
}
void HelloWorldScene::onTouchesBegan(const std::vector<Touch*>& touches, Event* event){
    log("sdg");
    hideZoomerIcon();
    zoomStarted = touches.size() == 2;
    if (zoomStarted) {
        zoomStartPoint = (touches.at(0)->getLocation() + touches.at(1)->getLocation())/2;
        zoomStartDistance = touches.at(0)->getLocation().distance(touches.at(1)->getLocation());
    }
}
void HelloWorldScene::onTouchesMoved(const std::vector<Touch*>& touches, Event* event){
    if (zoomStarted) {
        zoomEndPoint = (touches.at(0)->getLocation() + touches.at(1)->getLocation())/2;
        zoomEndDistance = touches.at(0)->getLocation().distance(touches.at(1)->getLocation());
        float scaleDiff = zoomEndDistance - zoomStartDistance;
        float moveDiff = zoomEndPoint.distance(zoomStartPoint);
        log("scaleDiff: %f, moveDiff: %f", scaleDiff, moveDiff);
        zoomStartPoint = zoomEndPoint;
        zoomStartDistance = zoomEndDistance;
    }
}
void HelloWorldScene::onTouchesEnded(const std::vector<Touch*>& touches, Event* event){
    zoomStarted = false;
}
void HelloWorldScene::onShareAnimalClick(){
    hideZoomerIcon();
    isAnimalShare = true;
    capture(0);
//    this->runAction(Sequence::create(DelayTime::create(0.3f), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::onShareAnimalDone, this)), NULL));
}
void HelloWorldScene::onShareAnimalDone(){
    auto originalSp = Sprite::create(filePath);
    log("sp width = %f", originalSp->getContentSize().width);
    
    float x = firstPopup->getPositionX() - 200;
    float y = firstPopup->getPositionY() - 130.21f;// -351.54f + 310.21f - 180;
    float theWidth = 400;
    float theHeight = 360;
//    x *= originalSp->getContentSize().width/size.width;
//    y *= originalSp->getContentSize().height/size.height;
//    theWidth *= originalSp->getContentSize().width/size.width;
//    theHeight *= originalSp->getContentSize().height/size.height;
    
    
//    auto testSp = Sprite::create(filePath, Rect(x, y, theWidth, theHeight));
////    testSp->setScale(400/testSp->getContentSize().height);
//    firstPopup->addChild(testSp);
    
//    auto sp = Sprite::create(filePath, Rect(x, y, theWidth, theHeight));
    
//    sp->setScale(406/sp->getContentSize().height);
//    this->addChild(sp, 1000);
//    sp->setPosition(Point(60,0));
    
    int width = theWidth;//sp->getContentSize().width;//getTexture()->getPixelsWide();
    int height = theHeight;//sp->getContentSize().height;//getTexture()->getPixelsHigh();
    
    auto renderTexture = RenderTexture::create(width, height, Texture2D::PixelFormat::RGBA8888);
    
    renderTexture->begin();
    this->setPosition(-x, -y);
    this->visit();
    renderTexture->end();
    renderTexture->saveToFile(StringUtils::format("tapSabanaScreenshot%d.png", captureCounter-1), Image::Format::PNG);
    this->runAction(Sequence::create(DelayTime::create(0.1f), MoveTo::create(0, Point::ZERO), NULL));
    
    this->runAction(Sequence::create(DelayTime::create(1.5f), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::doShare, this)), NULL));
}
void HelloWorldScene::onChangeShareModeClick(){
    bool isSpineTurn = firstPopup->getChildByName("frame2")->isVisible();
    firstPopup->getChildByName("frame2")->setVisible(!isSpineTurn);
//    if (isSpineTurn) {
//        time_t t = time(0);   // get time now
//        struct tm * now = localtime( & t );
//        int hour = now->tm_hour;
//        bool isDay = hour > 6 && hour < 18;
//        firstPopup->getChildByName("frame")->getChildByName("frameDay")->setVisible(isDay);
//        firstPopup->getChildByName("frame")->getChildByName("frameNight")->setVisible(!isDay);
//    }else{
//        firstPopup->getChildByName("frame")->getChildByName("frameDay")->setVisible(false);
//        firstPopup->getChildByName("frame")->getChildByName("frameNight")->setVisible(false);
//    }
//    firstPopup->getChildByName("frame")->getChildByName("animal")->setVisible(isSpineTurn);
}
void HelloWorldScene::onModifyLineDone(){
    std::string theLine = ((TextField*)secondPopup->getChildByName("tfInput"))->getString();
    closePopup();
    ((Text*)firstPopup->getChildByName("frame2")->getChildByName("lblText"))->setString(theLine);
    ((Text*)firstPopup->getChildByName("frame")->getChildByName("sptBubble")->getChildByName("lblText"))->setString(theLine);
}
void HelloWorldScene::modifyShareLine(){
    secondPopup = CSLoader::createNode("InputDialog.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    //    secondPopup->setAnchorPoint(Point(0.5, 0.5));
    secondPopup->setPosition(size.width/2, size.height/2);
    this->addChild(secondPopup, 10);
    Node* frame = secondPopup->getChildByName("frame");
    TextField* tf = (TextField*)secondPopup->getChildByName("tfInput");
    tf->setFontName(GameManager::getInstance()->getLocalizedFont());
    //    tf->setPlaceHolder(GameManager::getInstance()->getText("input sentence"));
    tf->setPlaceHolder("Input Text");
    Button* btn = (Button*)secondPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    ddiyong(secondPopup);
    btn = (Button*)frame->getChildByName("btnOk");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onModifyLineDone, this));
    
    btn->setTitleText(GameManager::getInstance()->getText("ok"));
}
void HelloWorldScene::takePicture(){
    Character* animal = (Character*)firstPopup->getChildByName("frame")->getChildByName("scrollView")->getChildByName("animal");
    
    if (isShareAnimalPaused) {
        animal->anim->pause();
        ((Text*)firstPopup->getChildByName("frame")->getChildByName("btnTake")->getChildByName("lblText"))->setString(GameManager::getInstance()->getText("take picture again"));
    }else{
        animal->anim->resume();
        ((Text*)firstPopup->getChildByName("frame")->getChildByName("btnTake")->getChildByName("lblText"))->setString(GameManager::getInstance()->getText("take picture"));
    }
    isShareAnimalPaused = !isShareAnimalPaused;
}
void HelloWorldScene::showAnimalInputText(){
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_CREATE);
    closePopup();
    firstPopup = CSLoader::createNode("animalTeach.csb");
    firstPopup->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(firstPopup, 10);
    ddiyong(firstPopup);
    Node* frame = firstPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString("input text", lbl);
    lbl = (Text*)frame->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString("want to learn", lbl);
    
    Character* ch = Character::create();
    ch->setSpine(indexToCreateAnimal);
    ch->setScale(1.5f);
    Node* innerFrame = frame->getChildByName("innerFrame");
    ch->setPosition(getHalfPos(innerFrame->getContentSize()) + Point(0, -180));
    innerFrame->addChild(ch, 0);
    
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    btn = (Button*)frame->getChildByName("btnInput");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onAnimalInputTextClick, this));
    btn->setTitleText(GameManager::getInstance()->getText("input text"));
    
    btn = (Button*)frame->getChildByName("btnShare");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onAnimalShareClick, this));
    btn->setTitleText(GameManager::getInstance()->getText("share"));
}
void HelloWorldScene::onAnimalInputTextClick(){
    closePopup();
    secondPopup = CSLoader::createNode("InputDialog.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
//    secondPopup->setAnchorPoint(Point(0.5, 0.5));
    secondPopup->setPosition(size.width/2, size.height/2);
    this->addChild(secondPopup, 10);
    Node* frame = secondPopup->getChildByName("frame");
    TextField* tf = (TextField*)secondPopup->getChildByName("tfInput");
    tf->setFontName(GameManager::getInstance()->getLocalizedFont());
//    tf->setPlaceHolder(GameManager::getInstance()->getText("input sentence"));
    tf->setPlaceHolder("Input Text");
    Button* btn = (Button*)secondPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    ddiyong(secondPopup);
    btn = (Button*)frame->getChildByName("btnOk");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onInputTextOk, this));
    
    btn->setTitleText(GameManager::getInstance()->getText("ok"));
}
void HelloWorldScene::onInputTextOk(){
    Node* frame = secondPopup->getChildByName("frame");
    TextField* tf = (TextField*)secondPopup->getChildByName("tfInput");
    std::string text = tf->getString();
    log("input text: %s", text.c_str());
    closePopup();
}
void HelloWorldScene::captureLater(){
    isAnimalShare = false;
    btnScreenCaptureAll->setOpacity(0);
    btnScreenCapture->setOpacity(0);
    capture(0);
    this->runAction(Sequence::create(DelayTime::create(1.2f), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::askShare, this)), NULL));
    
    Sprite* sptLogo = Sprite::create("logo_3.png");
    this->addChild(sptLogo);
    sptLogo->setPosition(size.width - 110, 60);
    sptLogo->runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, sptLogo)), nullptr));
    
    btnScreenCaptureAll->runAction(Sequence::create(DelayTime::create(0.5f), FadeIn::create(0), nullptr));
    btnScreenCapture->runAction(Sequence::create(DelayTime::create(0.5f), FadeIn::create(0), nullptr));
}
void HelloWorldScene::onAnimalShareClick(){
    float moveY = 5000;
    this->runAction(Sequence::create(DelayTime::create(0.31f), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::captureLater, this)), NULL));
    loadedLayer->runAction(Sequence::create(MoveTo::create(0, Point(0, -moveY)), DelayTime::create(3.2f), MoveTo::create(0, Point::ZERO), nullptr));
    btnScreenCapture->runAction(Sequence::create(MoveBy::create(0, Point(0, -moveY)), nullptr));
    btnQuest->runAction(Sequence::create(MoveBy::create(0, Point(0, -moveY)), nullptr));
    btnSetting->runAction(Sequence::create(MoveBy::create(0, Point(0, -moveY)), nullptr));
    btnAchievement->runAction(Sequence::create(MoveBy::create(0, Point(0, -moveY)), nullptr));
    if(btnDefense != nullptr){
        btnDefense->runAction(Sequence::create(MoveBy::create(0, Point(0, -moveY)), DelayTime::create(3.2f), MoveBy::create(0, Point(0, moveY)), nullptr));
    }
}
void HelloWorldScene::capture(float dt)
{
    captureSuccess = false;
    utils::captureScreen(CC_CALLBACK_2(HelloWorldScene::afterCaptured, this), StringUtils::format("tapSabanaScreenshot%d.png", captureCounter));
    captureCounter++;
}
void HelloWorldScene::afterCaptured(bool succeed, const std::string& outputFile)
{
    if (succeed)
    {
        filePath = outputFile;
        captureSuccess = true;
        
        if (isAnimalShare) {
//            doShare();
            onShareAnimalDone();
        }
    }
    else
    {
        log("Capture screen failed.");
    }
    
    try {
//        NativeInterface::NativeInterface::sharePhoto(filePath.c_str(), "공유해서 친구들에게 알려 줄까요? http://onelink.to/treeoflife");
    } catch (...) {
        
    }
}
void HelloWorldScene::onAnimalPutInClick(){
    int index = secondPopup->getTag();
    addAnimalCountInArea(AREA_INVENTORY_AFRICA, index, 1);
    addAnimalCountInArea(selectedArea, index, -1);

    for(auto ch: charList){
        if (ch->getName().compare(StringUtils::format("animal_%d", index)) == 0) {
            Node* parent = ch->getParent();
            charList.eraseObject(ch);
            if(parent != nullptr){
                ch->removeFromParent();
            }
            break;
        }
    }
    closePopup();
    if (firstPopup != nullptr) {
        closePopup();
        onManageCharacterClick();
    }
    
//    Node* dummy = Node::create();
//    dummy->setTag(index);
//    onAnimalDetailClick(dummy);
}
void HelloWorldScene::onAnimalToNature(){
    int index = secondPopup->getTag();
    addAnimalCountInArea(selectedArea, index, -1);
    
    for(auto ch: charList){
        if (ch->getName().compare(StringUtils::format("animal_%d", index)) == 0) {
            Node* parent = ch->getParent();
            charList.eraseObject(ch);
            if(parent != nullptr){
                ch->removeFromParent();
            }
            break;
        }
    }
    closePopup();
    if (firstPopup != nullptr) {
        closePopup();
        onManageCharacterClick();
    }

    subtractAccumulatedAnimalCount(index);
    GameManager::getInstance()->addGem(2);
}
void HelloWorldScene::onAnimalPutOutClick(){
    int index = secondPopup->getTag();
    addAnimalCountInArea(AREA_INVENTORY_AFRICA, index, -1);
    addAnimalCountInArea(selectedArea, index, 1);
    
    // move to center
    if (GameManager::getInstance()->isPortrait) {
        svField->getInnerContainer()->runAction(MoveTo::create(0, Point(-1334 + size.width/2, 0)));
    }else{
        svField->getInnerContainer()->runAction(MoveTo::create(0, Point(-1674 + size.width/2, 0)));
    }
    
    Character* ch = Character::create();
    ch->setSpineAnimation(index);
    ch->stayCount = 1;
    charList.pushBack(ch);
    ch->setName(StringUtils::format("animal_%d", index));
    ch->setTag(index);
    ch->setPosition(Point(1334, 130));
    svField->addChild(ch, 0);
    ch->anim->runAction(FadeIn::create(1));
    
    closePopup();
    if (firstPopup != nullptr) {
        closePopup();
        onManageCharacterClick();
    }
//    Node* dummy = Node::create();
//    dummy->setTag(index);
//    onAnimalDetailClick(dummy);
}
void HelloWorldScene::onAnimalManageAreaClick(){
    
}

void HelloWorldScene::onMoveToRegionClick(Ref* ref){
    Button* btn = (Button*)ref;
    int button = btn->getTag();
}
void HelloWorldScene::startRaining(int min){
    int endTime = UserDefault::getInstance()->getIntegerForKey("raining_end_time", -1);
    if(endTime > TimeManager::getInstance()->getCurrentTime()){
        UserDefault::getInstance()->setIntegerForKey("raining_end_time", endTime + min*60 - 1);
    }else{
        UserDefault::getInstance()->setIntegerForKey("raining_end_time", (int)TimeManager::getInstance()->getCurrentTime() + min*60 - 1);
    }
    createRainingEffect();
}
void HelloWorldScene::createRainingEffect(){
    if (!isRainingOn) {
        this->schedule(schedule_selector(HelloWorldScene::raining), 0.1f);
    }
    if(sptRain != nullptr){
        sptRain->removeFromParent();
        lblRainTimer->removeFromParent();
    }
    log("sptRain created");
    sptRain = Sprite::create("UI/shop/rain.png");
    this->addChild(sptRain, 10);
    sptRain->setPosition(btnScreenCapture->getPosition() + Point(0, -190));
    long now = TimeManager::getInstance()->getCurrentTime();
    int endTime = UserDefault::getInstance()->getIntegerForKey("odin_end_time", -1);
    lblRainTimer = Label::createWithTTF(GameManager::getInstance()->getTimeLeftInString(endTime - now), "GodoB.ttf", 20);
    this->addChild(lblRainTimer, 10);
    lblRainTimer->setPosition(sptRain->getPosition() + Point(0, -lblRainTimer->getContentSize().height/2 - sptRain->getContentSize().height/2));
    lblRainTimer->enableOutline(Color4B(57, 60, 51, 255));
    
    isRainingOn = true;
    updateTapReward();
    updateCurrencyLabels();
    updateTotalSanhoEffect();
}
void HelloWorldScene::stopRainingEffect(){
    if (isRainingOn) {
        this->unschedule(schedule_selector(HelloWorldScene::raining));
        
        if(sptRain != nullptr){
            log("sptRain destroyed");
            sptRain->removeFromParent();
            sptRain = nullptr;
            lblRainTimer->removeFromParent();
        }
    }
    isRainingOn = false;
    updateTapReward();
    updateCurrencyLabels();
    updateTotalSanhoEffect();
}
void HelloWorldScene::raining(float dt){
    for(int i = 0; i < 10; i++){
        Sprite* emit = Sprite::createWithSpriteFrameName("sun.png");
        int moveDistance = 100;
        int yVary = 300;
        emit->setPosition(Point(rand()%(2668 + moveDistance), 750 + rand()%yVary));
        emit->setScale(0.02f, 0.6f);
        emit->setRotation(10 + 180);
        emit->setOpacity(rand()%150 + 50);
        emit->setAnchorPoint(Point(0.5, 1));
        emit->runAction(Sequence::create(MoveBy::create(0.4f, Point(-moveDistance, - 750)), CallFuncN::create(CC_CALLBACK_1(HelloWorldScene::onRainDropHitGround, this)), NULL));
        svField->addChild(emit);
    }
}
void HelloWorldScene::onRainDropHitGround(Ref* ref){
    Sprite* spt = (Sprite*)ref;
    
    Sprite* emit = Sprite::createWithSpriteFrameName("sun.png");
    emit->setPosition(spt->getPosition());
    emit->setScale(0.3f, 0.1f);
    emit->setOpacity(rand()%150 + 50);
    emit->runAction(FadeOut::create(0.5f));
    emit->runAction(Sequence::create(ScaleTo::create(0.5f, 0.6f, 0.2f), CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, emit)), NULL));
    svField->addChild(emit, -900);
    
    spt->runAction(Sequence::create(ScaleTo::create(0.07f, 0.02f), CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, spt)), NULL));
}
void HelloWorldScene::startOdin(int dayCount){
    int endTime = UserDefault::getInstance()->getIntegerForKey("odin_end_time", -1);
    if(endTime > TimeManager::getInstance()->getCurrentTime()){
        UserDefault::getInstance()->setIntegerForKey("odin_end_time", endTime + 60*60*24*dayCount - 1);
    }else{
        UserDefault::getInstance()->setIntegerForKey("odin_end_time", (int)TimeManager::getInstance()->getCurrentTime() + 60*60*24*dayCount - 1);
    }
    createOdinEffect();
}
void HelloWorldScene::createOdinEffect(){
    if(sptShield != nullptr){
        sptShield->removeFromParent();
        lblOdinTimer->removeFromParent();
    }
    log("sptShield created");
    sptShield = Sprite::create("UI/shop/odin.png");
    this->addChild(sptShield, 10);
    sptShield->setPosition(btnScreenCapture->getPosition() + Point(0, -80));
    sptShield->runAction(RepeatForever::create(Sequence::create(ScaleTo::create(0.1f, 0.7f), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::onOdinHeartHit, this)), EaseBackOut::create(ScaleTo::create(0.1f, 1)), DelayTime::create(1), NULL)));
    long now = TimeManager::getInstance()->getCurrentTime();
    int endTime = UserDefault::getInstance()->getIntegerForKey("odin_end_time", -1);
    lblOdinTimer = Label::createWithTTF(GameManager::getInstance()->getTimeLeftInString(endTime - now), "GodoB.ttf", 20);
    this->addChild(lblOdinTimer, 10);
    lblOdinTimer->setPosition(sptShield->getPosition() + Point(0, -lblOdinTimer->getContentSize().height/2 - sptShield->getContentSize().height/2));
    lblOdinTimer->enableOutline(Color4B(57, 60, 51, 255));
}
void HelloWorldScene::destroyOdinEffect(){
    if(sptShield != nullptr){
        sptShield->removeFromParent();
        lblOdinTimer->removeFromParent();
    }
}
void HelloWorldScene::onOdinHeartHit(){
    if(sptShield != nullptr){
        Sprite* spt = Sprite::create("UI/shop/odin.png");
        this->addChild(spt, 11);
        spt->setPosition(sptShield->getPosition());
//        spt->setScale(0.7f);
        float time = 0.3f;
        spt->runAction(FadeOut::create(time));
        spt->runAction(Sequence::create(ScaleTo::create(time, 1.5f), CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, spt)), NULL));
    }
}
void HelloWorldScene::onShopItemClick(Ref* ref){
    Button* btn = (Button*)ref;
    int button = btn->getTag();
    
    if(button == SHOP_FREE_RAIN){
        GameManager::getInstance()->showVideo(VIDEO_FREE_RAIN);
        int interval = 60*60;
        UserDefault::getInstance()->setIntegerForKey(StringUtils::format("shop_timer_end_%d", button).c_str(), (int)TimeManager::getInstance()->getCurrentTime() + interval);
        int total = UserDefault::getInstance()->getIntegerForKey("key_shop_video_use_count", 0);
        total++;
        UserDefault::getInstance()->setIntegerForKey("key_shop_video_use_count", total);
        if(total%10 == 0){
            NativeInterface::NativeInterface::trackEvent("ad", "상점 광고 보기", Value(total).asString().c_str(), "", "video", total);
        }
    }else if(button == SHOP_FREE_GEM){
        GameManager::getInstance()->showVideo(VIDEO_FREE_GEM);
        int interval = 60*60;
        UserDefault::getInstance()->setIntegerForKey(StringUtils::format("shop_timer_end_%d", button).c_str(), (int)TimeManager::getInstance()->getCurrentTime() + interval);
        int total = UserDefault::getInstance()->getIntegerForKey("key_shop_video_use_count", 0);
        total++;
        UserDefault::getInstance()->setIntegerForKey("key_shop_video_use_count", total);
        if(total%10 == 0){
            NativeInterface::NativeInterface::trackEvent("ad", "상점 광고 보기", Value(total).asString().c_str(), "", "video", total);
        }
    }else if(button == SHOP_FREE_RELIC){
        GameManager::getInstance()->showVideo(VIDEO_FREE_RELIC);
        int interval = 60*60;
        UserDefault::getInstance()->setIntegerForKey(StringUtils::format("shop_timer_end_%d", button).c_str(), (int)TimeManager::getInstance()->getCurrentTime() + interval);
        int total = UserDefault::getInstance()->getIntegerForKey("key_shop_video_use_count", 0);
        total++;
        UserDefault::getInstance()->setIntegerForKey("key_shop_video_use_count", total);
        if(total%10 == 0){
            NativeInterface::NativeInterface::trackEvent("ad", "상점 광고 보기", Value(total).asString().c_str(), "", "video", total);
        }
    }else if(button == SHOP_RAIN){
        int price = 100;
        if (GameManager::getInstance()->getGem() >= price) {
            GameManager::getInstance()->addGem(-price);
            startRaining(15);
            NativeInterface::NativeInterface::trackEvent("shop", "축복의 소나기", "", "", "iap", 1);
        }else{
            GameManager::getInstance()->showDisposableMessage("not enough gem", this);
            GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
        }
    }else if(button == SHOP_ODIN_1){
        int price = 100;
        if (GameManager::getInstance()->getGem() >= price) {
            GameManager::getInstance()->addGem(-price);
            startOdin(1);
            NativeInterface::NativeInterface::trackEvent("shop", "오딘의 보호", "", "", "iap", 1);
        }else{
            GameManager::getInstance()->showDisposableMessage("not enough gem", this);
            GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
        }
    }else if(button == SHOP_GEM_SPECIAL_600){
//        GameManager::getInstance()->rewardIAP(IAP_GEM_SPECIAL_600);
        sdkbox::IAP::purchase("specialgem_600");
    }else if(button == SHOP_GEM_SPECIAL_1000){
//        GameManager::getInstance()->rewardIAP(IAP_GEM_SPECIAL_1000);
        sdkbox::IAP::purchase("specialgem_1000");
    }else if(button == SHOP_GEM_1){
//        GameManager::getInstance()->rewardIAP(IAP_GEM_0);
        sdkbox::IAP::purchase("tsgem_600");
    }else if(button == SHOP_GEM_2){
//        GameManager::getInstance()->rewardIAP(IAP_GEM_1);
        sdkbox::IAP::purchase("tsgem_1300");
    }else if(button == SHOP_GEM_3){
//        GameManager::getInstance()->rewardIAP(IAP_GEM_2);
        sdkbox::IAP::purchase("tsgem_7000");
    }else if(button == SHOP_GEM_4){
//        GameManager::getInstance()->rewardIAP(IAP_GEM_3);
        sdkbox::IAP::purchase("tsgem_15000");
    }else if(button == SHOP_PUPPY){
//        GameManager::getInstance()->rewardIAP(IAP_PUPPY);
        sdkbox::IAP::purchase("puppy_pkg");
    }else if(button == SHOP_BIRD){
//        GameManager::getInstance()->rewardIAP(IAP_BLUE_BIRD);
        sdkbox::IAP::purchase("bluebird_pkg");
    }else if(button == SHOP_ODIN_7){
//        GameManager::getInstance()->rewardIAP(IAP_STRONG_ODIN);
        sdkbox::IAP::purchase("tsprotect_1");
    }
    updateCurrencyLabels();
    updateShopItems();
}
void HelloWorldScene::updateBackgroundColor(){
    int r = sliderR->getPercent()*255.0f/100;
    int g = sliderG->getPercent()*255.0f/100;
    int b = sliderB->getPercent()*255.0f/100;
    lblR->setString(Value(r).asString());
    lblG->setString(Value(g).asString());
    lblB->setString(Value(b).asString());
    sptR->setOpacity(r);
    sptG->setOpacity(g);
    sptB->setOpacity(b);
    
    int r_subtractive = sliderR_color_subtractive->getPercent()*255.0f/100;
    int g_subtractive = sliderG_color_subtractive->getPercent()*255.0f/100;
    int b_subtractive = sliderB_color_subtractive->getPercent()*255.0f/100;
    lblR_color_subtractive->setString(Value(r_subtractive).asString());
    lblG_color_subtractive->setString(Value(g_subtractive).asString());
    lblB_color_subtractive->setString(Value(b_subtractive).asString());
//    sptBackground->setColor(Color3B(r_subtractive, g_subtractive, b_subtractive));
}
void HelloWorldScene::updateQuest(float dt){
    int lastDay = UserDefault::getInstance()->getIntegerForKey(KEY_LAST_PLAY_DAY, 0);
    int day = (int)(TimeManager::getInstance()->getCurrentTime()/(86400));
    if (lastDay != day) {
        for (int i = 0; i < 7; i++) {
            setQuestProgress(i, 0);
            setQuestRewardReceived(i, false);
        }
        UserDefault::getInstance()->setIntegerForKey(KEY_LAST_PLAY_DAY, day);
    }
    
    bool isThereQuestDoneAndNotReceived = false;
    for (int i = 0; i < 7; i++) {
        if (getQuestProgress(i) >= getQuestMaxCount(i) && !isQuestRewardReceived(i)) {
            isThereQuestDoneAndNotReceived = true;
            break;
        }
    }
    
    showRedBean(btnQuest, isThereQuestDoneAndNotReceived);
    
    int completeCount = 0;
    if (questLayer != nullptr) {
        btnQuest->loadTextureNormal("daily_quest_on.png");
        for (int i = 0; i < 7; i++) {
            Text* lbl = (Text*)questLayer->getChildByName("scrollView")->getChildByName(StringUtils::format("lblQuest_%d", i));
            GameManager::getInstance()->setLocalizedString(lbl, StringUtils::format("%s (%d/%d)", GameManager::getInstance()->getText(StringUtils::format("daily quest %d", i).c_str()).c_str(), getQuestProgress(i), getQuestMaxCount(i)));
            Button* btn = (Button*)lbl->getChildByName("btnGet");
            lbl = (Text*)btn->getChildByName("lblCount");
            Text* lblRecevied = (Text*)btn->getChildByName("lblGet");
            Sprite* sptIcon = (Sprite*)btn->getChildByName("sptIcon");
            if (getQuestProgress(i) >= getQuestMaxCount(i) || (i == 6 && completeCount >= 6)) { // test
                completeCount++;
                if (isQuestRewardReceived(i)) { // test
                    btn->setEnabled(false);
                    sptIcon->setColor(Color3B(200, 200, 200));
                    sptIcon->setVisible(false);
                    lbl->setColor(Color3B(200, 200, 200));
                    lbl->setVisible(false);
                    lblRecevied->setVisible(true);
                    GameManager::getInstance()->setLocalizedString(lblRecevied, "received");
                }else{
                    btn->setEnabled(true);
                    sptIcon->setColor(Color3B::WHITE);
                    sptIcon->setVisible(true);
                    lbl->setColor(Color3B::WHITE);
                    lbl->setVisible(true);
                    lblRecevied->setVisible(false);
                }
            }else{
                btn->setEnabled(false);
                sptIcon->setColor(Color3B(200, 200, 200));
                sptIcon->setVisible(true);
                lbl->setColor(Color3B(200, 200, 200));
                lbl->setVisible(true);
                lblRecevied->setVisible(false);
            }
        }
    }else{
        if (!isThereQuestDoneAndNotReceived) {
            btnQuest->loadTextureNormal("daily_quest_off.png");
        }else if(isThereQuestDoneAndNotReceived) {
            btnQuest->loadTextureNormal("daily_quest_alert.png");
        }
    }
}
bool HelloWorldScene::isQuestRewardReceived(int index){
    return UserDefault::getInstance()->getBoolForKey(StringUtils::format("questRewardReceived_%d", index).c_str());
}
void HelloWorldScene::setQuestRewardReceived(int index, bool received){
    UserDefault::getInstance()->setBoolForKey(StringUtils::format("questRewardReceived_%d", index).c_str(), received);
}
void HelloWorldScene::onTutorialClick(){
    if (tutorialWaitTime > 0) {
        return;
    }
    if (tutorialStep == 4 || tutorialStep == 5) {
        tutorialNode->removeFromParent();
        tutorialNode = nullptr;
        isTutorialOn = false;
        return;
    }
    tutorialNode->getChildByName("btnBack")->removeFromParent();
    tutorialNode->getChildByName("imgGrayBar")->setVisible(false);
    tutorialNode->getChildByName("lblOrder")->setVisible(true);
}
void HelloWorldScene::removeAndCreateTutorialNode(){
    if (tutorialNode != nullptr) {
        tutorialNode->removeFromParent();
    }
    tutorialNode = CSLoader::createNode("TutorialNode.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    this->addChild(tutorialNode, 10);
    Text* lblOrder = (Text*)tutorialNode->getChildByName("lblOrder");
    lblOrder->setVisible(false);
    lblOrder->setString(GameManager::getInstance()->getText(StringUtils::format("tutorial order %d", tutorialStep).c_str()));
    Text* lblDescription = (Text*)tutorialNode->getChildByName("imgGrayBar")->getChildByName("lblDescription");
    lblDescription->setString(GameManager::getInstance()->getText(StringUtils::format("tutorial %d", tutorialStep).c_str()));
    Button* btn = (Button*)tutorialNode->getChildByName("btnBack");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onTutorialClick, this));
    tutorialNode->getChildByName("imgFinger")->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.3f, Point(10, 0)), MoveBy::create(0.3f, Point(-10, 0)), nullptr)));
    tutorialNode->getChildByName("imgFinger")->setVisible(false);
    
    GameManager::getInstance()->refreshOrientationNeeded = true;
}
void HelloWorldScene::moveUpdate(float dt){
    for(auto animal: charList){
//        if(animal->getParent() == nullptr){
        if(!animal->freezed){
            animal->moveAuto(dt);
        }
        
//        }
    }
}
void HelloWorldScene::gameUpdate(float dt){
    bool isProductOnLeft = false;
    bool isProductOnRight = false;
    bool isRelicOnLeft = false;
    bool isRelicOnRight = false;
    for(auto animal: charList){
        if (animal->getPositionX() > -svField->getInnerContainerPosition().x - 90 && animal->getPositionX() < -svField->getInnerContainerPosition().x + size.width + 90) {
            if(animal->getParent() == nullptr){
                svField->addChild(animal);
                animal->stopActionByTag(55);
            }
        }
        else{
            if(animal->getParent() != nullptr){
                animal->removeFromParentAndCleanup(false);
            }
        
            if (animal->getPositionX() < -svField->getInnerContainerPosition().x - 50){ // left
                if(animal->talkBox != nullptr && animal->talkBox->getChildByName("sptBackground")->getTag() == TALK_BOX_LIFE){
                    isProductOnLeft = true;
                }else if(animal->talkBox != nullptr && animal->talkBox->getChildByName("sptBackground")->getTag() == TALK_BOX_RELIC){
                    isRelicOnLeft = true;
                }
            }else if (animal->getPositionX() > -svField->getInnerContainerPosition().x + size.width + 50){ // right
                if(animal->talkBox != nullptr && animal->talkBox->getChildByName("sptBackground")->getTag() == TALK_BOX_LIFE){
                    isProductOnRight = true;
                }else if(animal->talkBox != nullptr && animal->talkBox->getChildByName("sptBackground")->getTag() == TALK_BOX_RELIC){
                    isRelicOnRight = true;
                }
            }
        }
    }
    sptAnimalRelicAlertL->setVisible(isRelicOnLeft);
    sptAnimalRelicAlertR->setVisible(isRelicOnRight);
    sptAnimalProductAlertL->setVisible(isProductOnLeft);
    sptAnimalProductAlertR->setVisible(isProductOnRight);
    
    if (isTutorialOn) {
        if (isReadyToProceedTutorial) {
            tutorialStep++;
            tutorialWaitTime = 1;
            removeAndCreateTutorialNode();
            isReadyToProceedTutorial = false;
            if(tutorialStep == 1){
                ImageView* item = (ImageView*)svSkill->getInnerContainer()->getChildByName(StringUtils::format("sanhoItem%d", 0).c_str());
                Button* btn = (Button*)item->getChildByName("btnUpgrade");
                Node* sptFinger = tutorialNode->getChildByName("imgFinger");
                sptFinger->removeFromParentAndCleanup(false);
                btn->addChild(sptFinger);
                sptFinger->setVisible(true);
                sptFinger->setPosition(Point(-sptFinger->getContentSize().width/2, btn->getContentSize().height/2));
            }else if(tutorialStep == 2){
                ImageView* item = (ImageView*)svSkill->getInnerContainer()->getChildByName(StringUtils::format("sanhoItem%d", 0).c_str());
                Button* btn = (Button*)item->getChildByName("btnUpgrade");
                btn->removeChildByName("imgFinger");
                
                btn = (Button*)itemCharacter->getChildByName("itemAnimal_0")->getChildByName("btnUpgrade");
                Node* sptFinger = tutorialNode->getChildByName("imgFinger");
                sptFinger->removeFromParentAndCleanup(false);
                btn->addChild(sptFinger);
                sptFinger->setVisible(true);
                sptFinger->setPosition(Point(-sptFinger->getContentSize().width/2, btn->getContentSize().height/2));
                onTabChange(TAB_CHARACTER);
            }else if (tutorialStep == 3) {
                UserDefault::getInstance()->setBoolForKey("tutorial_done", true);
                UserDefault::getInstance()->flush();
                isTutorialOn = false;
                Button* btn = (Button*)itemCharacter->getChildByName("itemAnimal_0")->getChildByName("btnUpgrade");
                btn->removeChildByName("imgFinger");
                tutorialNode->removeFromParent();
                tutorialNode = nullptr;
                
                isTutorialOn = true;
                tutorialStep = 5;
                removeAndCreateTutorialNode();
                return;
            }else if (tutorialStep > 3){
                closePopup();
            }
        }else{
            if (tutorialNode != nullptr && tutorialNode->getChildByName("lblOrder")->isVisible()) {
                if (tutorialStep == 0) {
                    if (tapCounter >= 200) {
                        isReadyToProceedTutorial = true;
                    }
                }else if (tutorialStep == 1) {
                    if (isSanhoUnlocked(0)) {
                        isReadyToProceedTutorial = true;
                    }
                }else if (tutorialStep == 2) {
                    if (isCreatingAnimal || charList.size() > 0) {
                        isReadyToProceedTutorial = true;
                    }
                }
            }
        }
        tutorialWaitTime -= dt;
        if (tutorialNode != nullptr) {
            Text* lblOrder = (Text*)tutorialNode->getChildByName("lblOrder");
            std::string str = GameManager::getInstance()->getText(StringUtils::format("tutorial order %d", tutorialStep).c_str());
            if (tutorialStep == 0) {
                lblOrder->setString(StringUtils::format("%s\n%d/%d", str.c_str(), tapCounter, 200));
            }else if (tutorialStep == 1) {
                lblOrder->setString(StringUtils::format("%s\n%s/1.712k", str.c_str(), theLife->getExpression().c_str()));
            }else if (tutorialStep == 2) {
                lblOrder->setString(StringUtils::format("%s\n%s/5.0k", str.c_str(), theLife->getExpression().c_str()));
            }
        }
    }
    
    updateSkyTimeElapse += dt;
    if (updateSkyTimeElapse > 1) {
        updateSkyTimeElapse = 0;
        updateSky();
    }
    if (GameManager::getInstance()->refreshOrientationNeeded) {
        updateOrientation();
        GameManager::getInstance()->refreshOrientationNeeded = false;
    }
    updateBackgroundColor();
    if (autoTapTimeElapse > 0) {
        autoTapTimeElapse -= dt;
        autoOneTapTimeElapse += dt;
        if (autoOneTapTimeElapse >= 0.1f) {
            autoOneTapTimeElapse -= 0.1f;
            isTouchedForLife = true;
        }
    }
    if (buffTimeElapse > 0) {
        buffTimeElapse -= dt;
    }
    skillTimeCheckEllapse += dt;
    if (skillTimeCheckInterval < skillTimeCheckEllapse) {
        skillTimeCheckEllapse = 0;
        CheckSkillTime();
    }
    
    if (isTouchedForLife) {
        showLabelFromPool(svField,treeBack->getPosition() + Point(0, treeBack->getContentSize().height/2), tapReward->getExpression());
        theLife->addNum(tapReward);
        isTouchedForLife = false;
        
//        updateSky(); // test
//        testHour++; // test
    }
    
    if (requestBigTap) {
        requestBigTap = false;
        int level = UserDefault::getInstance()->getIntegerForKey(KEY_AUTO_TAP_LEVEL, 0);
        int multiply = (5 + level)*1000;
        BigNum* num = new BigNum();
        num->setNum(tapReward);
        num->multiply(multiply);
        theLife->addNum(num);
        delete num;
    }
    
    oneSecTimeElapse -= dt;
    if (oneSecTimeElapse < 0) {
        if (!firstLaunchScreenMoved) {
            firstLaunchScreenMoved = true;
//            if (GameManager::getInstance()->isPortrait) {
//                svField->getInnerContainer()->runAction(MoveTo::create(0.2f, Point(-1334 + size.width/2, 0)));
//            }else{
//                svField->getInnerContainer()->runAction(MoveTo::create(0.2f, Point(-1674 + size.width/2, 0)));
//            }
        }
        
        oneSecTimeElapse = 1.0f;
        if(isSanhoUnlocked(0)){
            theLife->addNum(totalSanhoEffectPerSec);
            showLabelFromPool(lblLife->getParent(), lblLife->getPosition() + Point(lblLife->getContentSize().width/2, 30), StringUtils::format("+%s", totalSanhoEffectPerSec->getExpression().c_str()), 60);
        }
        
        if (currentTab == TAB_SKILL) {
            updateSanhoButtonsEnable();
        }else if(currentTab == TAB_CHARACTER){
            updateAnimalTapDescription();
        }else if(currentTab == TAB_RELIC){
            updateRelic();
        }else if(currentTab == TAB_SHOP){
            updateShopItems();
        }else if(currentTab == TAB_ACHIEVEMENT){
            int sec = UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_PLAY_TIME, 0);
            sec += oneSecTimeElapse;
            UserDefault::getInstance()->setIntegerForKey(KEY_TOTAL_PLAY_TIME, sec);
            updateAchievementItems();
        }
        if (sptShield != nullptr) {
            long now = TimeManager::getInstance()->getCurrentTime();
            int endTime = UserDefault::getInstance()->getIntegerForKey("odin_end_time", -1);
            if (endTime > now) {
                GameManager::getInstance()->setTimeLeft(lblOdinTimer, (int)(endTime - now));
            }else{
                destroyOdinEffect();
            }
        }
        if (sptRain != nullptr) {
            long now = TimeManager::getInstance()->getCurrentTime();
            int endTime = UserDefault::getInstance()->getIntegerForKey("raining_end_time", -1);
            if (endTime > now) {
                GameManager::getInstance()->setTimeLeft(lblRainTimer, (int)(endTime - now));
            }else{
                stopRainingEffect();
            }
        }
        lblLife->setString(theLife->getExpression());
    }
    oneHalfTimeElapse -= dt;
    if (oneHalfTimeElapse < 0) {
        oneHalfTimeElapse = 1.3f;
        Button* btn = (Button*)itemTree->getChildByName("btnUpgrade");
        btn->setEnabled(theLife->IsBiggerThanThis(upgradeTabPrice) >= 0);
        
        
        
        UserDefault::getInstance()->setStringForKey(KEY_LIFE, theLife->getFullNumberStringByDotSplit());
//        
//        long now = TimeManager::getInstance()->getCurrentTime();
//        int endTime = UserDefault::getInstance()->getIntegerForKey("raining_end_time", -1);
//        if (isRainingOn && endTime < now) {
//            stopRainingEffect();
//        }
        
        
        
        updateMainMenuRedBean();
        
//        UserDefault::getInstance()->flush();
//        theLife->setNumFromFullNumberByDotSplitString(UserDefault::getInstance()->getStringForKey(KEY_LIFE, "0"));
    }
    
    sevenSecTimeElapse += dt;
    if(sevenSecTimeElapse > 7){
        sevenSecTimeElapse -= 7;
        int sec = UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_PLAY_TIME, 0);
        sec += 7;
        UserDefault::getInstance()->setIntegerForKey(KEY_TOTAL_PLAY_TIME, sec);
    }
    zorderTimeLeft -= dt;
//    if (zorderTimeLeft < 0) {
        zorderTimeLeft = 0.5f;
    float scaleY;
    
    for(auto ch : charList){
    
        ch->setLocalZOrder(-ch->getPosition().y);
        
        scaleY = 1 - (ch->getPositionY()/550)*0.5f;
        
        ch->anim->setScale(ch->anim->getScaleX()<0?-scaleY:scaleY, scaleY);
        
    }
    
//    }
    updateTalkBox(dt);
    
    // defense
    int lastCheckHour = UserDefault::getInstance()->getIntegerForKey(KEY_LAST_ATTACK_CHECK_TIME, -1);
    int hour = (int)(TimeManager::getInstance()->getCurrentTime()/3600);
    if (firstPopup == nullptr) {
        
        if( !isTutorialOn){
            if (lastCheckHour < 0) {
                lastCheckHour = hour;
                UserDefault::getInstance()->setIntegerForKey(KEY_LAST_ATTACK_CHECK_TIME, hour);
            }else{
                int now = TimeManager::getInstance()->getCurrentTime()%(60*60*24);
//                lblTimeTest->setString(StringUtils::format("last H: %d, %d, now: %s",lastCheckHour, hour, GameManager::getInstance()->getTimeLeftInString(now).c_str()));
                if (hour - lastCheckHour == 1) {// just defensed
                    log("just defeneded");
                    showAttackResult(doAttackIsSuccess(true), true);
                }else{  // defensed more than once
                    //            UserDefault::getInstance()->setIntegerForKey(KEY_LAST_ATTACK_CHECK_TIME, hour - 1); // test
                }
            }
        }
        
        // test
//        if(false){
//            lblHunterLastTime->setString(StringUtils::format("next attack hour: %d", lastCheckHour));
//            lblHunterNextTime->setString(StringUtils::format("current hour: %d", hour));
//            int secs = TimeManager::getInstance()->getCurrentTime()%3600;
//            lblHunterTimeLeft->setString(StringUtils::format("30 min time left: %d", 60*30 - secs));
//        }// test ends
    }
    if(btnDefense == nullptr && TimeManager::getInstance()->getCurrentTime()%3600 > 60*30 && lastCheckHour < hour + 1 && !isTutorialOn){// test
        btnDefense = Button::create("UI/icon/attack.png");
        this->addChild(btnDefense, 5);
        btnDefense->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::showAttackReady, this));
        btnDefense->setPosition(Point(size.width - btnDefense->getContentSize().width*2/3, size.height - btnDefense->getContentSize().height*2/3));
        btnDefense->runAction(RepeatForever::create(Sequence::create(EaseInOut::create(ScaleTo::create(0.15f, 0.7f), 2), EaseInOut::create(ScaleTo::create(0.15f, 1.0f), 2), EaseInOut::create(ScaleTo::create(0.15f, 0.8f), 2), EaseInOut::create(ScaleTo::create(0.15f, 1.0f), 2), DelayTime::create(0.15f), NULL)));
    }else if(btnDefense != nullptr && lastCheckHour == hour + 1){
        btnDefense->removeFromParent();
        btnDefense = nullptr;
    }
    
    if (firstPopup == nullptr) {
        if (absentRewardRequested ){//&& isPopupAvailable()) { // test
            absentRewardRequested = false;
            long now = TimeManager::getInstance()->getCurrentTime();
            int lastTime = UserDefault::getInstance()->getIntegerForKey(KEY_LAST_PAUSED_TIME, -1);
            if (lastTime < 0 || now - lastTime < 60*3) {
                UserDefault::getInstance()->setIntegerForKey(KEY_LAST_PAUSED_TIME, (int)now);
                return;
            }
            //        // test
            //        now = 1507726282;
            //        lastTime = 1507705313;
            absentReward->resetNum();
            
            UserDefault::getInstance()->setIntegerForKey(KEY_LAST_PAUSED_TIME, (int)now);
            absentReward->addNum(totalSanhoEffectPerSec);
            int rate = (int)(now - lastTime);
            if (rate > 60*60*24) {
                rate = 60*60*24;
            }
            absentReward->multiply(rate);
            log("absent reward now: %d, last: %d, sanho: %s, reward: %s", (int)now, lastTime, totalSanhoEffectPerSec->getExpression().c_str(), absentReward->getExpression().c_str());
            if (totalSanhoEffectPerSec->IsBiggerThanThis(new BigNum()) <= 0) {
                return;
            }
            theLife->addNum(absentReward);
            
            
            
            std::string text = StringUtils::format(GameManager::getInstance()->getText("absent reward").c_str(), absentReward->getExpression().c_str());
//            GameManager::getInstance()->showDisposableMessage(StringUtils::format("Dev test: %s", text.c_str()).c_str(), this);
            
//            Node* node = CSLoader::createNode("messageBox.csb");
//            setAsPopup(node);
//            GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
//            node->setPosition(size.width/2 - node->getContentSize().width/2, 0);
//            this->addChild(node, 10);
//            Node* frame = node->getChildByName("frame");
//            Text* lbl = (Text*)frame->getChildByName("lblTitle");
//
//            lbl->setString(text);
//            ddiyong(node);
//            
//            absentReward->resetNum();
//            
//            Button* btn = (Button*)node->getChildByName("btnBlock");
//            btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
//            btn = (Button*)frame->getChildByName("btnOk");
//            btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
        }
    }
    
    if (checkAbsentAttackRequested) {
        checkAbsentAttack();
    }
    // gameupdate done
}
void HelloWorldScene::updateMainMenuRedBean(){
    bool isSomethingThere = false;
    for (int i = 0; i < 13; i++) {
        long now = TimeManager::getInstance()->getCurrentTime();
        int endTime = UserDefault::getInstance()->getIntegerForKey(StringUtils::format("shop_timer_end_%d", i).c_str(), -1);
        long timeLeft = endTime - now;
        if (timeLeft <= 0) {
            isSomethingThere = true;
        }
    }
    showRedBean(btnList.at(5), isSomethingThere);
    showRedBean(btnList.at(6), isAchievementRedBeanEnabled());
    ((ImageView*)btnList.at(5)->getChildByName("redBean"))->loadTexture(selectedTapIndex == TAB_SHOP? "notice_point_2.png":"notice_point_1.png");
    ((ImageView*)btnList.at(6)->getChildByName("redBean"))->loadTexture(selectedTapIndex == TAB_ACHIEVEMENT? "notice_point_2.png":"notice_point_1.png");
    
    bool isBigger = theLife->IsBiggerThanThis(nextAnimalPrice) > 0;
    showRedBean(itemCharacter->getChildByName("itemAnimal_0")->getChildByName("btnUpgrade"), isBigger);
    
    int relicPoint = getRelicPoint();
    int pointPrice = gerRareAnimalPointPrice();
    showRedBean(itemCharacter->getChildByName("itemAnimal_1")->getChildByName("btnUpgrade"), relicPoint >= pointPrice);
    showRedBean(btnList.at(1), isBigger || pointPrice <= relicPoint);
    
    int relicPickPointPrice = getRelicPrice();
    showRedBean(btnList.at(3), relicPoint >= relicPickPointPrice);
    showRedBean(itemRelic->getChildByName("btnUpgrade"), relicPoint >= relicPickPointPrice);
    ((ImageView*)btnList.at(1)->getChildByName("redBean"))->loadTexture(selectedTapIndex == TAB_CHARACTER? "notice_point_2.png":"notice_point_1.png");
    ((ImageView*)btnList.at(3)->getChildByName("redBean"))->loadTexture(selectedTapIndex == TAB_RELIC? "notice_point_2.png":"notice_point_1.png");
}
void HelloWorldScene::closePopup(){
    if (isTutorialOn && tutorialNode != nullptr) {
        isTutorialOn = false;
        tutorialNode->removeFromParent();
        tutorialNode = nullptr;
    }
    if (_animalZoomerTouchListener != nullptr) {
        _eventDispatcher->removeEventListener(_animalZoomerTouchListener);
        _animalZoomerTouchListener = nullptr;
    }
//    btnBlocker->setVisible(false);
    if(secondPopup != nullptr){
        updateAnimalPickTab();
        this->unschedule(schedule_selector(HelloWorldScene::updateAttackReady));
        
        secondPopup->removeFromParent();
        secondPopup = nullptr;
    }else if (firstPopup != nullptr) {
        firstPopup->removeFromParent();
        firstPopup = nullptr;
    }
}
bool HelloWorldScene::isPopupAvailable(){
    if (firstPopup == nullptr) {
        return true;
    }else if(secondPopup == nullptr){
        return true;
    }else{
        return false;
    }
}
bool HelloWorldScene::setAsPopup(Node* node){
    if (firstPopup == nullptr) {
        firstPopup = node;
    }else if(secondPopup == nullptr){
        secondPopup = node;
    }else{
        return false;
    }
    return true;
}
int HelloWorldScene::getAttackerPower(int level){
    return 100 + level*100;
}
bool HelloWorldScene::doAttackIsSuccess(bool debuf){
    bool result = false;
    int attackerLevel = UserDefault::getInstance()->getIntegerForKey(KEY_ATTACKER_LEVEL, 0);
    int attackerPower = getAttackerPower(attackerLevel);
    int survivalPower = getTotalSurvivalPower();
    result = ((debuf?0.8f:1)*survivalPower - attackerPower) >= 0;
    log("attacker: %d, survival: %d(%d)", attackerPower, survivalPower, (int)(debuf?0.8f:1)*survivalPower);
    if (sptShield != nullptr) {
        return true;
    }
    return result;
}
void HelloWorldScene::updateTalkBox(float dt){
    _talkBoxTimeElapse -= dt;
    if(_talkBoxTimeElapse < 0){
        _talkBoxTimeElapse = 7; // test
        int count = (int)charList.size()/10 + 1;
        if(charList.size() > 0){
            for (int i = 0; i < count; i++) {
                int index = rand()%count;
                //            log("index: %d, size: %d", index, (int)charList.size());
                Character* animal = charList.at(index);
                if (animal->relicOnAndNotRecieved) {
                    continue;
                }
                int talkIndex = rand()%3;
//                if (relicGiveTimer < 0) {
//                    talkIndex = TALK_BOX_RELIC;
//                    relicGiveTimer = 120;
//                }
                std::string text = "";
                if(talkIndex == TALK_BOX_LIFE){
                    
                }else if(talkIndex == TALK_BOX_GEM){
                    text = GameManager::getInstance()->getText(StringUtils::format("ask share %d", rand()%40).c_str());
                }else if(talkIndex == TALK_BOX_TALK){
                    text = GameManager::getInstance()->getText(StringUtils::format("talk common%d", rand()%11).c_str());
                }
                animal->showTalkBox(talkIndex, 5, text);
            }
        }
        for (auto animal : charList){
            if (animal->relicRequested) {
                animal->relicRequested = false;
                
                animal->showTalkBox(TALK_BOX_RELIC, 60, "");
            }
        }
    }
}
void HelloWorldScene::CheckSkillTime(){
    long now = TimeManager::getInstance()->getCurrentTime();
    long usedTime = 0;
    long persistTime = 0;
    long coolingTime = 0;
    long remainTime = 0;
    
    Button* btn;
    Text* lblTime;
    std::string key;
    bool isThereTimeControl = false;
    for (int i = 0; i < btnSkillList.size(); i++) {
        if (i == 0) {
            usedTime = UserDefault::getInstance()->getDoubleForKey(KEY_AUTO_TAP_USED_TIME, 0);
        }else if (i == 1) {
            usedTime = UserDefault::getInstance()->getDoubleForKey(KEY_BIG_TAP_USED_TIME, 0);
        }else if (i == 2) {
            usedTime = UserDefault::getInstance()->getDoubleForKey(KEY_BUFF_USED_TIME, 0);
        }else if (i == 3) {
            usedTime = UserDefault::getInstance()->getDoubleForKey(KEY_RESET_USED_TIME, 0);
        }
        
        persistTime = getSkillPersistTime(SKILL_AUTO_TAP + i);
        coolingTime = getSkillCoolTime(SKILL_AUTO_TAP + i);
        remainTime = 0;
        btn = btnSkillList.at(i);
        lblTime = (Text*)btn->getChildByName("lblTime");
        lblTime->setVisible(false);
        btn->setEnabled(false);
        Point circlePos = Point(60, 60);
        
        ProgressTimer* PersistTimer = timerPersistingList.at(i);
        PersistTimer->setVisible(false);
        ProgressTimer* coolingTimer = timerCoolingList.at(i);
        coolingTimer->setVisible(false);
        if (usedTime + persistTime > now) { // persisting
            btn->setOpacity(100);
            remainTime = (usedTime + persistTime) - now;
            if (i == 0) {
                autoTapTimeElapse = remainTime;
                isThereTimeControl = true;
            }else if (i == 1) {
                
            }else if (i == 2) {
                buffTimeElapse = remainTime;
                isThereTimeControl = true;
            }else{
                
            }
            
            if (!PersistTimer->isVisible()) {
                PersistTimer->setVisible(true);
                PersistTimer->setPercentage(remainTime*100.0f/persistTime);
                PersistTimer->runAction(ProgressTo::create(remainTime, 0));
            }
        }else if (usedTime + persistTime + coolingTime > now) { // cooling
            lblTime->setVisible(true);
            if (i == 0) {
                stopAurora();
            }else if (i == 1) {
                
            }else if (i == 2) {
                if(rainbowNode != nullptr && !donottouchrainbow){
                    rainbowNode->removeFromParent();
                    rainbowNode=nullptr;
                    updateTapReward();
                    btnSkillList.at(2)->getChildByName("sptEffect")->setVisible(false);
                }
            }else{
                
            }
            
            btn->setOpacity(200);
            remainTime = (usedTime + persistTime + coolingTime) - now;
            
            if (!coolingTimer->isVisible()) {
                coolingTimer->setVisible(true);
                coolingTimer->setPercentage(remainTime*100.0f/coolingTime);
                coolingTimer->runAction(ProgressTo::create(remainTime, 0));
//                timer->runAction(RepeatForever::create(RotateBy::create(1, 100)));
            }
        }else{
            btn->setOpacity(255);
            lblTime->setVisible(false);
            timerPersistingList.at(i)->stopAllActions();
            timerCoolingList.at(i)->stopAllActions();
            timerPersistingList.at(i)->setVisible(false);
            timerCoolingList.at(i)->setVisible(false);
            btn->setEnabled(true);
            
            if (i == 0) {
                
            }else if (i == 1) {
                usedTime = UserDefault::getInstance()->getDoubleForKey(KEY_BIG_TAP_USED_TIME, 0);
            }else if (i == 2) {
                usedTime = UserDefault::getInstance()->getDoubleForKey(KEY_BUFF_USED_TIME, 0);
            }else{
                
            }
        }
        
        GameManager::getInstance()->setTimeLeft(lblTime, remainTime);
//        lblTime->setString(__String::createWithFormat("%02d:%02d:%02d", (int)(remainTime/(60*60)), (int)(remainTime/60), (int)remainTime%60)->getCString());
    }
    if (!isThereTimeControl) {
//        forceTime = -1;
        updateSky();
    }
}
void HelloWorldScene::stopAurora(){
    btnSkillList.at(0)->getChildByName("sptEffect")->setVisible(false);
    this->unschedule(schedule_selector(HelloWorldScene::updatingAurora));
    for(int i = 0;i<auroraList.size();i++){
        Sprite* spt = auroraList.at(i);
        spt->removeFromParent();
    }
    auroraList.clear();
}
void HelloWorldScene::updateOrientation(){
    size = Director::getInstance()->getVisibleSize();
    if (prologueLayer != nullptr) {
        loadedLayer->setPositionY(-size.height*2);
        svField->setPositionY(size.height*2);
        sunMoonBatch->setPositionY(size.height*2);
        Node* scrollView = prologueLayer->getChildByName("scrollView");
        prologueLayer->setPosition(size/2);//GameManager::getInstance()->isPortrait?Point::ZERO:Point(size.width/2 - prologueLayer->getContentSize().width/2, 0));
        scrollView->getChildByName("drawNode")->setPosition(-scrollView->getPosition()-prologueLayer->getPosition());
        return;
    }
    float bigger = size.width > size.height?size.width:size.height;
    float smaller = size.width < size.height?size.width:size.height;
    float svX = 0;
    float svY = 0;
    float fieldY = 0;
    float screenEndX = size.width;
    float skillY = 664;
    float skillGapX = 24;
    Node* btnAppear = loadedLayer->getChildByName("btnAppear");
    Node* btnHidden = loadedLayer->getChildByName("btnHidden");
//    log("isPortrait? %d", GameManager::getInstance()->isPortrait);
    if (GameManager::getInstance()->isPortrait) {
        size = Size(smaller, bigger);
        float startX = 0;
        float endX = size.width;
        GameManager::getInstance()->arrange(startX, endX, btnList);
//        btnAppear->setPositionX(size.width + 100);
//        btnHidden->setPositionX(size.width + 100);
        if (btnAppear->isVisible()) {
            //            svX = size.width;
            svY = -420;
            fieldY = 584-164;
//            skillY -= 420;
        }else{
            
            fieldY = 584;
        }
        btnAppear->setPositionX(250);
        btnHidden->setPositionX(250);
        sptAnimalRelicAlertL->setPosition(Point(0, size.height*3/4));
        sptAnimalRelicAlertR->setPosition(Point(size.width, size.height*3/4));
        sptAnimalProductAlertL->setPosition(Point(0, size.height*3/4));
        sptAnimalProductAlertR->setPosition(Point(size.width, size.height*3/4));
    }else{
        size = Size(bigger, smaller);
//        svField->setPosition(Point(size.width/2, 0));
        
        float startX = size.width - 750;//svSkill->getPosition().x + svSkill->getContentSize().width;
        float endX = size.width;
        GameManager::getInstance()->arrange(startX, endX, btnList);
        svX = size.width - 750;
        skillY = 86;
//        Node* btnAppear = loadedLayer->getChildByName("btnAppear");
//        btnAppear->setVisible(true);// test
//        Node* btnHidden = loadedLayer->getChildByName("btnHidden");
        if (btnAppear->isVisible()) {
//            svX = size.width;
            svY = -420;
            
        }else{
            
        }
        btnAppear->setPositionX(384 + 750);
        btnHidden->setPositionX(384 + 750);
//        for (auto btn : btnSkillList) {
//            btn->setVisible(!btnAppear->isVisible());
//        }
//        btnHidden->setPositionX(svX);
//        btnAppear->setPositionX(svX);
        sptAnimalRelicAlertL->setPosition(Point(0, size.height*2/4));
        sptAnimalRelicAlertR->setPosition(Point(size.width, size.height*2/4));
        sptAnimalProductAlertL->setPosition(Point(0, size.height*2/4));
        sptAnimalProductAlertR->setPosition(Point(size.width, size.height*2/4));
    }
    if (questLayer != nullptr) {
        questLayer->setPosition(Point(0, size.height));
    }
    float dur = 0.3f;
    svField->stopAllActions();
    svField->runAction(MoveTo::create(dur, Point(size.width/2, fieldY)));
//    svField->setPosition(Point(size.width/2, svY));
    
    for (int i = 0; i < btnList.size(); i++) {
        Button* btn = btnList.at(i);
        if(i==btnList.size() - 1){
            btn->setPositionX(svX + 696.00f);
        }else{
            btn->setPositionX(svX + btn->getContentSize().width*(i+0.5f));
        }
        
    }
    if(rainbowNode != nullptr){
        rainbowNode->setPosition(Point(740, 400+ svField->getPositionY()));
    }
    
    loadedLayer->getChildByName("pnlStatus")->setPositionX(svX + 12);
    itemTree->setPositionX(svX + 12);
    itemMap->setPositionX(svX + 12);
    itemCharacter->setPositionX(svX + 12);
    itemAchievement->setPositionX(svX + 12);
    itemRelic->setPositionX(svX + 12);
    pnlUIBG->setVisible(GameManager::getInstance()->isPortrait);
    uiTop->setPositionX(svX);
    uiBackground->setPositionX(svX);
    svSkill->setPositionX(svX);
    svCharacter->setPositionX(svX);
    svCollection->setPositionX(svX);
    svRelic->setPositionX(svX);
    svMap->setPositionX(svX);
    svShop->setPositionX(svX);

    loadedLayer->runAction(MoveTo::create(dur, Point(0, svY)));
    for(auto btn : btnList){
        btn->runAction(MoveTo::create(dur, Point(btn->getPositionX(), -svY)));
//        btn->setPositionY(600);
//        btn->runAction(MoveTo::create(dur, Point(btn->getPositionX(), svY == 0?0:600)));
    }
    if(svY < 0){
        if (!GameManager::getInstance()->isPortrait) {
            skillY -= svY;
        }
        
//        svField->runAction(MoveTo::create(dur, Point(svField->getPositionX(), -svY)));
    }

    
//    loadedLayer->getChildByName("pnlStatus")->runAction(MoveTo::create(dur, Point(svX + 12, svY)));
//    itemTree->runAction(MoveTo::create(dur, Point(svX + 12, svY)));
//    itemMap->runAction(MoveTo::create(dur, Point(svX + 12, svY)));
//    itemCharacter->runAction(MoveTo::create(dur, Point(svX + 12, svY)));
//    itemRelic->runAction(MoveTo::create(dur, Point(svX + 12, svY)));
//    pnlUIBG->setVisible(GameManager::getInstance()->isPortrait);
//    uiTop->runAction(MoveTo::create(dur, Point(svX, svY)));
//    uiBackground->runAction(MoveTo::create(dur, Point(svX, svY)));
//    svSkill->runAction(MoveTo::create(dur, Point(svX, svY)));
//    svCharacter->runAction(MoveTo::create(dur, Point(svX, svY)));
//    svCollection->runAction(MoveTo::create(dur, Point(svX, svY)));
//    svRelic->runAction(MoveTo::create(dur, Point(svX, svY)));
//    svMap->runAction(MoveTo::create(dur, Point(svX, svY)));
//    svShop->runAction(MoveTo::create(dur, Point(svX, svY)));
    
    int padding = 15;
    btnScreenCapture->setPosition(Point(padding + btnScreenCapture->getContentSize().width/2, size.height - btnScreenCapture->getContentSize().height/2 - padding));
    btnQuest->setPosition(Point(padding*2 + btnScreenCapture->getContentSize().width + btnQuest->getContentSize().width/2, size.height - btnQuest->getContentSize().height/2 - padding));
    btnScreenCaptureAll->setPosition(Point(padding*2 + btnScreenCapture->getContentSize().width + btnQuest->getContentSize().width/2, size.height - btnQuest->getContentSize().height/2 - padding));
    btnSetting->setPosition(Point(padding*3 + btnScreenCapture->getContentSize().width + btnQuest->getContentSize().width + btnSetting->getContentSize().width/2, size.height - btnSetting->getContentSize().height/2 - padding));
    btnAchievement->setPosition(Point(padding*4 + btnScreenCapture->getContentSize().width + btnQuest->getContentSize().width + btnSetting->getContentSize().width + btnAchievement->getContentSize().width/2, size.height - btnSetting->getContentSize().height/2 - padding));
    if(sptShield != nullptr){
        sptShield->setPosition(btnScreenCapture->getPosition() + Point(0, -80));
        lblOdinTimer->setPosition(sptShield->getPosition() + Point(0, -lblOdinTimer->getContentSize().height/2 - sptShield->getContentSize().height/2));
    }
    if(sptRain != nullptr){
        sptRain->setPosition(btnScreenCapture->getPosition() + Point(0, -190));
        lblRainTimer->setPosition(sptRain->getPosition() + Point(0, -lblRainTimer->getContentSize().height/2 - sptRain->getContentSize().height/2));
    }
    
    for (int i = 0; i < btnSkillList.size(); i++) {
        if (GameManager::getInstance()->isPortrait) {
            btnSkillList.at(btnSkillList.size() - i -1)->runAction(MoveTo::create(dur, Point(screenEndX - skillGapX - btnSkillList.at(i)->getContentSize().width/2 - (skillGapX + btnSkillList.at(i)->getContentSize().width)*i, skillY)));
        }else{
            btnSkillList.at(i)->runAction(MoveTo::create(dur, Point(skillGapX + btnSkillList.at(i)->getContentSize().width/2 + (skillGapX + btnSkillList.at(i)->getContentSize().width)*i, skillY)));
        }
    }
    svField->setContentSize(Size(size.width, svField->getContentSize().height));
    
    for (int i = 0; i < tabObjects.size(); i++) {
        tabObjects.at(i)->setVisible(tabObjects.at(i)->getTag() == i);
    }
    
    Size sizeToInspect = svField->getInnerContainer()->getContentSize();
    log("w: %f, h: %f", sizeToInspect.width, sizeToInspect.height);
//    pnlField->setContentSize(Size(1333.96f, 749.30));
    loadedLayer->setContentSize(size);
    ui::Helper::doLayout(loadedLayer);
//    Director::getInstance()->updateWinSize();
    
//    Node* sun = loadedLayer->getChildByName("sunMoon");
    sunMoon->setPosition(Point(size.width/2, size.height - 517));
    for (auto emit : shineEmitList) {
//        emit->setPositionY(size.height - 517);
        emit->setPositionY(size.height);
    }
    updateSky();
//    GameManager::getInstance()->scrollToItem(svField, treeFoot);
    
    if (currentTab == TAB_SKILL) {
        onTreeClick();
    }else if (currentTab == TAB_CHARACTER) {
        onAnimalClick();
    }else if (currentTab == TAB_COLLECTION) {
        onCollectionClick();
    }else if (currentTab == TAB_RELIC) {
        onRelicClick();
    }else if (currentTab == TAB_MAP) {
        onMapClick();
    }else if (currentTab == TAB_SHOP) {
        onShopClick();
    }else if (currentTab == TAB_ACHIEVEMENT) {
        onAchievementClick();
    }
    updateMainButtons();
    
    if(secondPopup != nullptr){
        secondPopup->setPosition(size.width/2, size.height/2);
    }
    if(firstPopup != nullptr){
        firstPopup->setPosition(size.width/2, size.height/2);
    }
    
    btnRecord->setPosition(Point(btnRecord->getContentSize().width*2/3, size.height - btnRecord->getContentSize().height*2/3));
    if (btnDefense != nullptr) {
        btnDefense->setPosition(Point(size.width - btnDefense->getContentSize().width*2/3, size.height - btnDefense->getContentSize().height*2/3));
    }
    
    svField->getInnerContainer()->stopAllActions();
    if (GameManager::getInstance()->isPortrait) {
        svField->getInnerContainer()->runAction(MoveTo::create(0, Point(-1334 + size.width/2, 0)));
    }else{
        svField->getInnerContainer()->runAction(MoveTo::create(0, Point(-1674 + size.width/2, 0)));
    }
    
    if (tutorialNode != nullptr) {
        tutorialNode->getChildByName("imgGrayBar")->setPosition(GameManager::getInstance()->isPortrait?Point(size.width/2, size.height/2):Point(size.width/2, size.height/2));
        tutorialNode->getChildByName("lblOrder")->setPosition(GameManager::getInstance()->isPortrait?Point(size.width/2, size.height*2/3 + 100):Point((size.width - 750)/2, size.height/2));
        ((Text*)tutorialNode->getChildByName("imgGrayBar")->getChildByName("lblDescription"))->setContentSize(Size(GameManager::getInstance()->isPortrait?750:size.width-750,500));
        ((Text*)tutorialNode->getChildByName("lblOrder"))->setContentSize(Size(GameManager::getInstance()->isPortrait?750:size.width-750,500));
    }
}

void HelloWorldScene::updateTapReward(){
    BigNum* num = new BigNum();
    BigNum* bigNum = getTapReward();
    
    num->addNum(bigNum);
    tapReward->setNum(num);
    delete num;
    delete bigNum;
}
void HelloWorldScene::updateIdleReward(){
    
}
int HelloWorldScene::getSkillPersistTime(int skillType){
    int level = 0;
    if (skillType == SKILL_AUTO_TAP) {
        level = UserDefault::getInstance()->getIntegerForKey(KEY_AUTO_TAP_LEVEL, 0);
    }else if (skillType == SKILL_BIG_TAP) {
        level = UserDefault::getInstance()->getIntegerForKey(KEY_BIG_TAP_LEVEL, 0);
    }else if (skillType == SKILL_TAP_BUFF) {
        level = UserDefault::getInstance()->getIntegerForKey(KEY_BUFF_LEVEL, 0);
    }
    return getSkillPersistTime(skillType, level);
}
int HelloWorldScene::getSkillPersistTime(int skillType, int level){
    if (skillType == SKILL_AUTO_TAP) {
        int time = 60*(3 + level);
        for (int i = 0; i < relicEffects.size(); i++) {
            if (isRelicInUse(i)) {
                std::string str = relicEffects.at(i).at(7);
                if(str.size() > 0){
                    time += Value(str).asInt();
                }
            }
        }
//        return 5;// test 
        return time;
    }else if (skillType == SKILL_TAP_BUFF) {
//        return 5;
        return 60*3;
    }
    return 0;
}
int HelloWorldScene::getSkillCoolTime(int skillType){
    int originalTime = 60;
    if (skillType == SKILL_AUTO_TAP) {
        originalTime = 60*30;
    }else if (skillType == SKILL_BIG_TAP) {
        originalTime = 60*60;
    }else if (skillType == SKILL_TAP_BUFF) {
        originalTime = 60*120;
    }else if (skillType == SKILL_RESET_SKILLS) {
        originalTime = 60*15;
    }
    
    for (int i = 0; i < relicEffects.size(); i++) {
        if (isRelicInUse(i)) {
            std::string str = relicEffects.at(i).at(10);
            if(str.size() > 0){
                originalTime -= Value(str).asInt();
            }
        }
    }
    
    int coolTimeReduceLevel = 0;
    return originalTime*(1.0f - coolTimeReduceLevel*0.05f);
}
void HelloWorldScene::onAutoTapSkillClick(){
    btnSkillList.at(0)->setEnabled(false);
    int level = UserDefault::getInstance()->getIntegerForKey(KEY_AUTO_TAP_LEVEL, 0);
    autoTapTimeElapse = getSkillPersistTime(SKILL_AUTO_TAP, level);
    long now = TimeManager::getInstance()->getCurrentTime();
    UserDefault::getInstance()->setDoubleForKey(KEY_AUTO_TAP_USED_TIME, now);

    int counter = getQuestProgress(5);
    if (counter < getQuestMaxCount(5)) {
        counter++;
        setQuestProgress(5, counter);
        if (questLayer != nullptr) {
            updateQuest(0);
        }
    }
    
    GameManager::getInstance()->showDisposableMessage(StringUtils::format("%s: %s", GameManager::getInstance()->getText("skill name 0").c_str(), StringUtils::format(GameManager::getInstance()->getText("skill description 0").c_str(), (int)(autoTapTimeElapse/60)).c_str()).c_str(), this);
    
    // effect
    showAurora();
    
//    forceTime = 24;
    updateSky();
    
    int total = UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_SKILL_USE, 0);
    total++;
    UserDefault::getInstance()->setIntegerForKey(KEY_TOTAL_SKILL_USE, total);
}
void HelloWorldScene::onBigTapSkillClick(){
    btnSkillList.at(1)->setEnabled(false);
    requestBigTap = true;
    int level = UserDefault::getInstance()->getIntegerForKey(KEY_BIG_TAP_LEVEL, 0);
    long now = TimeManager::getInstance()->getCurrentTime();
    UserDefault::getInstance()->setDoubleForKey(KEY_BIG_TAP_USED_TIME, now);

    int counter = getQuestProgress(5);
    if (counter < getQuestMaxCount(5)) {
        counter++;
        setQuestProgress(5, counter);
        if (questLayer != nullptr) {
            updateQuest(0);
        }
    }
    
    BigNum* num = new BigNum();
    num->setNum(tapReward);
    // skill 2 buff
    float relicEffect = 1;
    for (int i = 0; i < relicEffects.size(); i++) {
        if (isRelicInUse(i)) {
            std::string str = relicEffects.at(i).at(8);
            if(str.size() > 0){
                relicEffect += Value(str).asInt()*0.01f;
            }
        }
    }
    num->multiply((int)(3000*relicEffect));
    theLife->addNum(num);
    delete num;
    
    GameManager::getInstance()->showDisposableMessage(StringUtils::format("%s: %s", GameManager::getInstance()->getText("skill name 1").c_str(), StringUtils::format(GameManager::getInstance()->getText("skill description 1").c_str(), (int)(3000*relicEffect)).c_str()).c_str(), this);
    
    showWind();
    
    showLifeEffect();
    
    int total = UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_SKILL_USE, 0);
    total++;
    UserDefault::getInstance()->setIntegerForKey(KEY_TOTAL_SKILL_USE, total);
}
void HelloWorldScene::showParticle(Node* target, int count){
    for (int i = 0; i < count; i++) {
        Sprite* spt = Sprite::createWithSpriteFrameName("eff_ptc.png");
        target->getParent()->getParent()->addChild(spt);// treeBack is SpriteBatch that has the sprite eff_ptc.png

        spt->setPosition(target->getPosition() + Point((rand()%(int)target->getBoundingBox().size.width) - target->getBoundingBox().size.width/2, (rand()%(int)target->getContentSize().height) - target->getContentSize().height/2));
        spt->setOpacity(0);
        spt->runAction(FadeIn::create(0.16f + (rand()%20)*0.01f));
        float dur = 0.5f + (rand()%20)*0.01f;
        spt->setOpacity(rand()%150 + 100);
        spt->runAction(MoveBy::create(dur, Point(rand()%30 - 15, 30 + rand()%20)));
        spt->runAction(Sequence::create(DelayTime::create(dur*2/3), FadeOut::create(dur/3), CallFuncN::create(CC_CALLBACK_1(Sprite::removeFromParentAndCleanup, spt)), nullptr));
    }
}
void HelloWorldScene::showLifeEffect(){
    showParticle(lblLife, 20);
}
void HelloWorldScene::onBuffSkillClick(){
//    isBuffOn = true;
    btnSkillList.at(2)->setEnabled(false);
    
    int tapBuffLevel = UserDefault::getInstance()->getIntegerForKey(KEY_TAP_BUFF_LEVEL, 0);
    for (int i = 0; i < relicEffects.size(); i++) {
        if (isRelicInUse(i)) {
            std::string str = relicEffects.at(i).at(9);
            if(str.size() > 0){
                tapBuffLevel += Value(str).asInt();
            }
        }
    }
    tapBuffAmount = 3 + tapBuffLevel;
    
    
    int level = UserDefault::getInstance()->getIntegerForKey(KEY_BUFF_LEVEL, 0);
    buffTimeElapse = getSkillPersistTime(SKILL_TAP_BUFF, level);
    long now = TimeManager::getInstance()->getCurrentTime();
    UserDefault::getInstance()->setDoubleForKey(KEY_BUFF_USED_TIME, now);
    
    // do some effect
    showRainbow();
    int counter = getQuestProgress(5);
    if (counter < getQuestMaxCount(5)) {
        counter++;
        setQuestProgress(5, counter);
        if (questLayer != nullptr) {
            updateQuest(0);
        }
    }
    
    GameManager::getInstance()->showDisposableMessage(StringUtils::format("%s: %s", GameManager::getInstance()->getText("skill name 2").c_str(), StringUtils::format(GameManager::getInstance()->getText("skill description 2").c_str(), (int)(buffTimeElapse/60), tapBuffAmount).c_str()).c_str(), this);
    
//    forceTime = 12;
    updateTapReward();
    updateSky();
    
    int total = UserDefault::getInstance()->getIntegerForKey(KEY_TOTAL_SKILL_USE, 0);
    total++;
    UserDefault::getInstance()->setIntegerForKey(KEY_TOTAL_SKILL_USE, total);
}
void HelloWorldScene::resetSkills(){
    stopAurora();
    
    if(rainbowNode != nullptr && !donottouchrainbow){
        rainbowNode->removeFromParent();
        rainbowNode=nullptr;
        updateTapReward();
        btnSkillList.at(2)->getChildByName("sptEffect")->setVisible(false);
    }
    
    long now = TimeManager::getInstance()->getCurrentTime();
    
//    UserDefault::getInstance()->setDoubleForKey(KEY_AUTO_TAP_USED_TIME, 0); // test
    UserDefault::getInstance()->setDoubleForKey(KEY_AUTO_TAP_USED_TIME, now - getSkillCoolTime(SKILL_AUTO_TAP) - getSkillCoolTime(SKILL_AUTO_TAP));
    
//    UserDefault::getInstance()->setDoubleForKey(KEY_BIG_TAP_USED_TIME, 0); // test
    UserDefault::getInstance()->setDoubleForKey(KEY_BIG_TAP_USED_TIME, now - getSkillCoolTime(SKILL_BIG_TAP) - getSkillCoolTime(SKILL_BIG_TAP));
    
//    UserDefault::getInstance()->setDoubleForKey(KEY_BUFF_USED_TIME, 0); // test
    UserDefault::getInstance()->setDoubleForKey(KEY_BUFF_USED_TIME, now - getSkillCoolTime(SKILL_TAP_BUFF) - getSkillCoolTime(SKILL_TAP_BUFF));
    
    UserDefault::getInstance()->setDoubleForKey(KEY_RESET_USED_TIME, now);
    
    int total = UserDefault::getInstance()->getIntegerForKey("key_reset_skill_use_count", 0);
    total++;
    UserDefault::getInstance()->setIntegerForKey("key_reset_skill_use_count", total);
    if(total%10 == 0){
        NativeInterface::NativeInterface::trackEvent("ad", "스킬 초기화", Value(total).asString().c_str(), "", "video", total);
    }
}
void HelloWorldScene::onResetClick(){
    
//    GameManager::getInstance()->videoAdsIndex = VIDEO_RESET_SKILLS;
//    GameManager::getInstance()->showVideoDone();
    
    firstPopup = CSLoader::createNode("dialog.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    firstPopup->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(firstPopup, 10);
    Node* frame = firstPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString("ask reset skill", lbl);
    ddiyong(firstPopup);
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    btn = (Button*)frame->getChildByName("btnOk");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onResetConfirmed, this));
    btn->setTitleText(GameManager::getInstance()->getText("yes"));
    
    btn = (Button*)frame->getChildByName("btnCancel");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn->setTitleText(GameManager::getInstance()->getText("no"));

}
void HelloWorldScene::onResetConfirmed(){
    closePopup();
    GameManager::getInstance()->showVideo(VIDEO_RESET_SKILLS);
}
void HelloWorldScene::onVideoDone(){
    this->scheduleOnce(schedule_selector(HelloWorldScene::rewardVideoLater), 0.05f);
}
void HelloWorldScene::rewardVideoLater(float dt){
    log("reward video");
    if (GameManager::getInstance()->videoAdsIndex == VIDEO_RESET_SKILLS) {
        resetSkills();
    }else if (GameManager::getInstance()->videoAdsIndex == VIDEO_FREE_RAIN) {
        startRaining(3);
    }else if (GameManager::getInstance()->videoAdsIndex == VIDEO_FREE_GEM) {
        GameManager::getInstance()->addGem(5);
        updateCurrencyLabels();
    }else if (GameManager::getInstance()->videoAdsIndex == VIDEO_FREE_RELIC) {
        addRelicPoint(5);
        updateCurrencyLabels();
    }else if(GameManager::getInstance()->videoAdsIndex == VIDEO_500_TAP){
        BigNum* num = new BigNum();
        num->setNum(tapReward);
        num->multiply((int)(500));
        theLife->addNum(num);
        updateCurrencyLabels();
        delete num;
    }
    showRewardPopup( GameManager::getInstance()->videoAdsIndex);
}
void HelloWorldScene::showRewardPopup(int rewardType){
    std::string iconName;
    std::string msg;
    Size iconSize = Size(92, 92);
    if (rewardType == VIDEO_RESET_SKILLS) {
        iconName = "skill3.png";
        msg = GameManager::getInstance()->getText("cooltime reset");
    }else if (rewardType == VIDEO_FREE_RAIN) {
        iconName = "UI/shop/blessing_shoure.png";
        msg = StringUtils::format(GameManager::getInstance()->getText("double life for min").c_str(), 3);
    }else if (rewardType == VIDEO_FREE_GEM) {
        iconName = "UI/shop/gem_0.png";
        msg = StringUtils::format(GameManager::getInstance()->getText("get gem amount").c_str(), 5);
    }else if (rewardType == VIDEO_FREE_RELIC) {
        iconName = "icon_relic.png";
        iconSize = Size(42, 42);
        msg = StringUtils::format(GameManager::getInstance()->getText("get relic amount").c_str(), 5);
    }else if(rewardType == VIDEO_500_TAP){
        iconName = "icon_life.png";
        iconSize = Size(42, 32);
        msg = StringUtils::format(GameManager::getInstance()->getText("skill description 1").c_str(), 500);
    }
    firstPopup = CSLoader::createNode("rewardPopup.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    firstPopup->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(firstPopup, 10);
    Node* frame = firstPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    lbl->setString(msg);
    
    ddiyong(firstPopup);
    
    ImageView* sptIcon = (ImageView*)frame->getChildByName("sptIcon");
    sptIcon->loadTexture(iconName);
    sptIcon->setContentSize(iconSize);
    
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnOk");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    lbl = (Text*)btn->getChildByName("lblText");
    GameManager::getInstance()->setLocalizedString(lbl, "ok");
}
void HelloWorldScene::onTreeClick(){
    showScrollView(TAB_SKILL);
    updateMainTreeItem();
}
void HelloWorldScene::onAnimalClick(){
    showScrollView(TAB_CHARACTER);
    updateVisibleItemsOnAnimalTap();
    updateAnimalTapDescription();
}
void HelloWorldScene::onCollectionClick(){
    showScrollView(TAB_COLLECTION);
    updateCollectionItems();
}
void HelloWorldScene::updateCollectionItems(){
    std::string name;
    int animalCount = (int)animalNames.size();
    for (int i = 0; i < animalCount; i++) {
        int unitCount = getTotalAnimalCount(i);
        Label* lbl = (Label*)svCollection->getInnerContainer()->getChildByTag(i)->getChildByName("lblLevel");
        lbl->setString(StringUtils::format("Lv.%d", getAnimalLevel(i)));
        if (unitCount <= 0) {
            lbl->setVisible(false);
        }
        
        Sprite* sptIcon = (Sprite*)svCollection->getInnerContainer()->getChildByTag(i)->getChildByName("sptIcon");
        
        
        if(animalAtlas.at(i).size() > 0){
            name = animalAtlas.at(i).substr(0, animalAtlas.at(i).size() - 6);
        }
        
        if (getAccumulatedAnimalCount(i) > 0) {
            sptIcon->setTexture(StringUtils::format("res/ui-icons/icon_%s.png", name.c_str()).c_str());
            sptIcon->setOpacity(255);
            sptIcon->setColor(Color3B::WHITE);
        }
        
        ImageView* sptProgress = (ImageView*)svCollection->getInnerContainer()->getChildByTag(i)->getChildByName("sptProgress");
        sptProgress->setContentSize(Size(110.0f*getAnimalExp(i)/getAnimalCountForLevelUp(getAnimalLevel(i)), 20));
        
        lbl = (Label*)svCollection->getInnerContainer()->getChildByTag(i)->getChildByName("lblExp");
        lbl->setString(StringUtils::format("%d/%d", getAnimalExp(i), getAnimalCountForLevelUp(getAnimalLevel(i))));
    }
    
//    for (int i = 0; i < 5; i++) {
//        Widget* clone = (Widget*)svCollection->getInnerContainer()->getChildByName(__String::createWithFormat("item%d", i)->getCString());
//        Text* lblDescription = (Text*)clone->getChildByName("lblDescription");
//        GameManager::getInstance()->setLocalizedString(StringUtils::format("magic%d", i), lblDescription);
//        lblDescription = (Text*)clone->getChildByName("lblDescription_0");
//        GameManager::getInstance()->setLocalizedString(StringUtils::format("magicDescription%d", i), lblDescription);
//    }
}
void HelloWorldScene::onRelicClick(){
    showScrollView(TAB_RELIC);
    updateRelic();
}
void HelloWorldScene::onMapClick(){
    showScrollView(TAB_MAP);
    updateMap();
}
void HelloWorldScene::onShopClick(){
//    GameManager::getInstance()->showDisposableMessage("shop!!", this);
    showScrollView(TAB_SHOP);
    updateShopItems();
}
void HelloWorldScene::updateShopItems(){
    for (int i = 0; i < shopItemCount; i++) {
        ImageView* clone = (ImageView*)svShop->getInnerContainer()->getChildByName(__String::createWithFormat("item%d", i)->getCString());
        ImageView* sptIcon = (ImageView*)clone->getChildByName("sptIcon");
        
        Text* lblTitle = (Text*)clone->getChildByName("lblDescription");
        Text* lblDescription = (Text*)clone->getChildByName("lblDescription_0");
        
        Button* btn = (Button*)clone->getChildByName("btnUpgrade");
        
        Text* lblTimer = (Text*)sptIcon->getParent()->getChildByName("lblTimer");
        if (lblTimer != nullptr) {
            long now = TimeManager::getInstance()->getCurrentTime();
            int endTime = UserDefault::getInstance()->getIntegerForKey(StringUtils::format("shop_timer_end_%d", i).c_str(), -1);
            long timeLeft = endTime - now;
            lblTimer->setVisible(timeLeft > 0);
            btn->setEnabled(timeLeft <= 0);
            showRedBean(btn, btn->isEnabled());
            GameManager::getInstance()->setTimeLeft(lblTimer, timeLeft);
        }
        Text* lblPrice = (Text*)btn->getChildByName("lblPrice");
        ImageView* btnIcon = (ImageView*)btn->getChildByName("sptIcon");
        if ((i == SHOP_GEM_SPECIAL_600 && UserDefault::getInstance()->getBoolForKey("key_gem_special_600_purchased", false)) ||
            (i == SHOP_GEM_SPECIAL_1000 && UserDefault::getInstance()->getBoolForKey("key_gem_special_1000_purchased", false)) ||
            (i == SHOP_PUPPY && isRelicUnlocked(27))){
            btn->setEnabled(false);
            
            Text* lblButtonText = (Text*)btn->getChildByName("lblDescription");
            
            lblPrice->setVisible(false);
            btnIcon->setVisible(false);
            GameManager::getInstance()->setLocalizedString(lblButtonText, "purchase done");
            lblButtonText->setPosition(btn->getContentSize()/2);
        }
        
        if (i == 5) {
            lblPrice->setString(GameManager::getInstance()->strPrices[4]);
        }else if (i == 6) {
            lblPrice->setString(GameManager::getInstance()->strPrices[5]);
        }else if (i == 7) {
            lblPrice->setString(GameManager::getInstance()->strPrices[0]);
        }else if (i == 8) {
            lblPrice->setString(GameManager::getInstance()->strPrices[1]);
        }else if (i == 9) {
            lblPrice->setString(GameManager::getInstance()->strPrices[2]);
        }else if (i == 10) {
            lblPrice->setString(GameManager::getInstance()->strPrices[3]);
        }else if (i == 11) {
            lblPrice->setString(GameManager::getInstance()->strPrices[7]);
        }else if (i == 12) {
            lblPrice->setString(GameManager::getInstance()->strPrices[8]);
        }else if (i == 13) {
            lblPrice->setString(GameManager::getInstance()->strPrices[6]);
        }
        
        if (i == 5 || i == 6 ||i == 7 || i == 8 || i == 9 || i == 10 || i == 11 || i == 12 || i == 13) {
            GameManager::getInstance()->alignToCenter(btnIcon, lblPrice, 5, btn->getContentSize().width/2, 0);
        }
        
        
        if(lblDescription != nullptr){
            if (btn->isEnabled()) {
                clone->loadTexture("ui_list_bg.png");
                lblTitle->setTextColor(Color4B(57, 60, 51, 255));
                lblDescription->setTextColor(Color4B(105, 102, 83, 255));
                sptIcon->setOpacity(255);
            }else{
                clone->loadTexture("ui_list_bg_locked.png");
                lblTitle->setTextColor(Color4B(251, 249, 219, 255));
                lblDescription->setTextColor(Color4B(251, 249, 219, 255));
                sptIcon->setOpacity(150);
            }
        }
    }
}
void HelloWorldScene::showScrollView(int index){
    GameManager::getInstance()->playSoundEffect(SOUND_BTN_POS);
    currentTab = index;
    svSkill->setVisible(index == TAB_SKILL);
    svCharacter->setVisible(index == TAB_CHARACTER);
    svCollection->setVisible(index == TAB_COLLECTION);
    svRelic->setVisible(index == TAB_RELIC);
    svMap->setVisible(index == TAB_MAP);
    svShop->setVisible(index == TAB_SHOP);
    itemAchievement->setVisible(index == TAB_ACHIEVEMENT);
}
BigNum* HelloWorldScene::getTapReward(){
    return getTapReward(getMainTreeLevel());
}
BigNum* HelloWorldScene::getTapReward(int level){
    BigNum* num = new BigNum();
    num->addNum(10 + level, 0);

    float rate = 1;
    float buff = 0;
    if (buffTimeElapse > 0) {
        num->multiply(tapBuffAmount);
    }
    float relicRate = 0;
    for (int i = 0; i < relicEffects.size(); i++) {
        if (isRelicInUse(i)) {
            std::string str = relicEffects.at(i).at(5);
            if(str.size() > 0){
//                relicRate += Value(str).asInt()*0.01f;
                num->multiply(1 + Value(str).asInt()*0.01f);
            }
        }
    }
    
    if (isRainingOn) {
        num->multiply(2);
    }
    
//    num->multiply(animalEffect);
    applyAnimalEffect(num);
//    rate *= (1 + (buff + relicRate));
//    num->multiply(rate);
    
    int mainTreeBonus = getMainTreeBonusLevel();
    float mainTreeBonusRate = 1;
    for (int i = 0; i < mainTreeBonus; i++) {
//        mainTreeBonusRate *= 2;
        num->multiply(2);
    }
    
    int questTapBuffLevel = getQuestTapBuffLevel();
    for (int i = 0; i < questTapBuffLevel; i++) {
        num->multiply(2);
    }
    
//    num->multiply(mainTreeBonusRate);
    return num;
}
BigNum* HelloWorldScene::getUpgradeTapRewardPrice(int level){
    BigNum* num = new BigNum();
    num->addNum(200, 0);
    for (int i = 0; i < level; i++) {
        num->multiply(1.07f);
    }
    
    return num;
}
BigNum* HelloWorldScene::getUpgradeTapRewardPrice(){
    int level = getMainTreeLevel();
    upgradeTabPrice = getUpgradeTapRewardPrice(level);
    return upgradeTabPrice;
}
void HelloWorldScene::onRelicOnAnimalTouched(Character* node){
    GameManager::getInstance()->playSoundEffect(rand()%2==0?SOUND_ANIMAL_TAP_0:SOUND_ANIMAL_TAP_1);
    Label* lbl = Label::createWithTTF("+1", "GodoB.ttf", 25);
    svField->addChild(lbl, 10);
    float visibleTime = 0.5f;
    float fadingTime = 1;
    lbl->enableOutline(Color4B::BLACK);
    lbl->stopAllActions();
    lbl->setPosition(node->getPosition() + Point(0, node->anim->getBoundingBox().size.height/2));
    lbl->setOpacity(255);
    lbl->runAction(MoveBy::create(visibleTime+fadingTime, Point(0, 200)));
    lbl->runAction(Sequence::create(DelayTime::create(visibleTime), FadeOut::create(fadingTime), CallFunc::create(CC_CALLBACK_0(Label::removeFromParent, lbl)), NULL));
    node->relicOnAndNotRecieved = false;
    std::string strKey = StringUtils::format("relic_get_today_%d", node->ID);
    
    int todayGiveCount = UserDefault::getInstance()->getIntegerForKey(strKey.c_str(), 0);
    todayGiveCount++;
    UserDefault::getInstance()->setIntegerForKey(strKey.c_str(), todayGiveCount);
//    showMessage(StringUtils::format("get! %s, %d, %d",strKey.c_str(), node->ID, todayGiveCount));
    
    Point startPos = node->getPosition() + node->getParent()->getPosition() + Point(0, node->getParent()->getParent()->getPositionY());
    
    moveIconToPosition(ICON_RELIC, startPos, getPosInWorld(loadedLayer->getChildByName("pnlStatus")->getChildByName("iconRelic")), 0);
    updateCurrencyLabels();
}
void HelloWorldScene::onLifeOnAnimalTouched(Character* node){
    log("life touched");
    GameManager::getInstance()->playSoundEffect(rand()%2==0?SOUND_ANIMAL_TAP_0:SOUND_ANIMAL_TAP_1);
//    showLabelFromPool(svField, , tapReward->getExpression());
    BigNum* num = new BigNum();
    num->setNum(tapReward);
    num->multiply(10);
    
    Label* lbl = Label::createWithTTF(num->getExpression(), "GodoB.ttf", 25);
    svField->addChild(lbl, 10);
    float visibleTime = 0.5f;
    float fadingTime = 1;
    lbl->enableOutline(Color4B::BLACK);
    lbl->stopAllActions();
    lbl->setPosition(node->getPosition() + Point(0, node->anim->getBoundingBox().size.height/2));
    lbl->setOpacity(255);
    lbl->runAction(MoveBy::create(visibleTime+fadingTime, Point(0, 200)));
    lbl->runAction(Sequence::create(DelayTime::create(visibleTime), FadeOut::create(fadingTime), CallFunc::create(CC_CALLBACK_0(Label::removeFromParent, lbl)), NULL));
    
    theLife->addNum(num);
    delete num;
}
void HelloWorldScene::onGemOnAnimalTouched(Character* node){
    GameManager::getInstance()->playSoundEffect(rand()%2==0?SOUND_ANIMAL_TAP_0:SOUND_ANIMAL_TAP_1);
    showAnimalShare();
    return;
    closePopup();
    firstPopup = CSLoader::createNode("popup.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    firstPopup->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(firstPopup, 10);
    Node* frame = firstPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    std::string text = StringUtils::format("talk gem%d", rand()%9);
    GameManager::getInstance()->setLocalizedString(text, lbl);
    ddiyong(firstPopup);
    
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    btn = (Button*)frame->getChildByName("btnOk");
    lbl = (Text*)btn->getChildByName("lblText");
    int index = rand()%2;
    
    if(index == 0){
        btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onJustGemAndClose, this));
        GameManager::getInstance()->setLocalizedString("just gem", lbl);
    }else{
        btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onShareAndGem, this));
        GameManager::getInstance()->setLocalizedString("share and gem", lbl);
    }
}
void HelloWorldScene::showAnimalShare(){
    closePopup();
    
    firstPopup = CSLoader::createNode("popup.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    firstPopup->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(firstPopup, 10);
    Node* frame = firstPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    std::string text = StringUtils::format("talk gem%d", rand()%9);
    GameManager::getInstance()->setLocalizedString(text, lbl);
    ddiyong(firstPopup);
    
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
}
void HelloWorldScene::onJustGemAndClose(){
    closePopup();
    GameManager::getInstance()->addGem(1);
    updateMainTreeItem();
}
void HelloWorldScene::onShareAndGem(){
    GameManager::getInstance()->addGem(3);
    capture(0);
    updateMainTreeItem();
    Button* btn = (Button*)firstPopup->getChildByName("btnOk");
    btn->setEnabled(false);
}
int HelloWorldScene::getMainTreeLevel(){
    return UserDefault::getInstance()->getIntegerForKey(KEY_TAP_LEVEL, 0);
}
void HelloWorldScene::updateMainTreeItem(){
    Button* btn = (Button*)itemTree->getChildByName("btnUpgrade");
    
    BigNum* nextNum = new BigNum();
    nextNum->addNum(tapReward);
    nextNum->multiply(0.1f);
    Text* lblDescription = (Text*)btn->getChildByName("lblDescription");
//    lblDescription->setString(StringUtils::format("+%s %s", nextNum->getExpression().c_str(), GameManager::getInstance()->getText("life").c_str()));
    lblDescription->setString(StringUtils::format("+%s", nextNum->getExpression().c_str()));
    if(btn->getChildByName("plusIcon") == nullptr){
        ImageView* sptIcon = ImageView::create("icon_lifetab.png");
        btn->addChild(sptIcon);
        sptIcon->setScale(0.8f);
        sptIcon->setPosition(lblDescription->getPosition());
        sptIcon->setName("plusIcon");
    }
    GameManager::getInstance()->alignToCenter(btn->getChildByName("plusIcon"), lblDescription, 5, btn->getContentSize().width*btn->getScaleX()/2, 0);
    
    Node* icon = btn->getChildByName("sptIcon");
    BigNum* price = getUpgradeTapRewardPrice();
    Text* lblPrice = (Text*)btn->getChildByName("lblPrice");
    lblPrice->setString(price->getExpression());
    GameManager::getInstance()->alignToCenter(icon, lblPrice, 0, btn->getContentSize().width/2, -4);
    
    
    
    lblDescription = (Text*)itemTree->getChildByName("lblDescription");
    lblDescription->setString(StringUtils::format("%s%d %s", GameManager::getInstance()->getText("level").c_str(), getMainTreeLevel() + 1, GameManager::getInstance()->getText("foot tree").c_str()).c_str());
    lblDescription = (Text*)itemTree->getChildByName("lblDescription_0");
    lblDescription->setString(StringUtils::format("%s/%s", tapReward->getExpression().c_str(), GameManager::getInstance()->getText("tap").c_str()));
    
    if (getMainTreeLevel()/25 > getMainTreeBonusLevel()) {
        itemTree->getChildByName("btnBonus")->setVisible(true);
        lblDescription->setString(StringUtils::format("%s +200%%", GameManager::getInstance()->getText("life per tap").c_str()).c_str());
    }else{
        itemTree->getChildByName("btnBonus")->setVisible(false);
    }
    GameManager::getInstance()->alignToCenter(btn->getChildByName("sptIcon"), btn->getChildByName("lblPrice"), 0, btn->getContentSize().width/2, -4);
    
    updateCurrencyLabels();
    
    btn->setEnabled(theLife->IsBiggerThanThis(price) >= 0);
    
//    updateMainButtons();
}
void HelloWorldScene::updateCurrencyLabels(){
    Text* lbl = (Text*)loadedLayer->getChildByName("pnlStatus")->getChildByName("lblTap");
    lbl->setString(tapReward->getExpression());
    
    lbl = (Text*)loadedLayer->getChildByName("pnlStatus")->getChildByName("lblTime");
    lbl->setString(totalSanhoEffectPerSec->getExpression());
    
    lbl = (Text*)loadedLayer->getChildByName("pnlStatus")->getChildByName("lblRelic");
    lbl->setString(Value(getRelicPoint()).asString());
    
    lbl = (Text*)loadedLayer->getChildByName("pnlStatus")->getChildByName("lblGem");
    lbl->setString(Value(GameManager::getInstance()->getGem()).asString());
}
void HelloWorldScene::updateMainButtons(){
    Button* btn;
    Text* lbl;
    for (int i = 0; i < btnList.size(); i++) {
        btn = btnList.at(i);
        lbl = (Text*)btn->getChildByName("lbl");
        ImageView* icon = (ImageView*)btn->getChildByName("icon");
        if (selectedTapIndex == i) {
            btn->loadTextureNormal("MainBtn_On_pixel.png");
            icon->setColor(Color3B(255, 255, 255));
            lbl->setColor(Color3B(255, 255, 255));
        }else{
            btn->loadTextureNormal("MainBtn_Off_pixel.png");
            icon->setColor(Color3B(148, 126, 65));
            lbl->setColor(Color3B(118, 97, 40));
        }
        if (i == 0) {
            GameManager::getInstance()->setLocalizedString("field", lbl);
        }else if(i == 1){
            GameManager::getInstance()->setLocalizedString("animal", lbl);
        }else if(i == 2){
            GameManager::getInstance()->setLocalizedString("collection", lbl);
        }else if(i == 3){
            GameManager::getInstance()->setLocalizedString("relic", lbl);
        }else if(i == 4){
            GameManager::getInstance()->setLocalizedString("map", lbl);
        }else if(i == 5){
            GameManager::getInstance()->setLocalizedString("shop", lbl);
        }else if(i == 6){
            GameManager::getInstance()->setLocalizedString("achievement", lbl);
        }
    }
//    itemMap->setVisible(selectedTapIndex == TAB_MAP);
    itemTree->setVisible(selectedTapIndex == TAB_SKILL);
    itemCharacter->setVisible(selectedTapIndex == TAB_CHARACTER);
    itemRelic->setVisible(selectedTapIndex == TAB_RELIC);
}
void HelloWorldScene::upgradeTapReward(int amount){
    if(addAchievementCurrent(ACHIEVE_MAIN_TREE_500, amount) >= 500){
        sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQGA");
    }
    if(addAchievementCurrent(ACHIEVE_MAIN_TREE_777, amount) >= 777){
        sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQGQ");
    }
    if(addAchievementCurrent(ACHIEVE_MAIN_TREE_1000, amount) >= 1000){
        sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQGg");
    }
    
    GameManager::getInstance()->playSoundEffect(SOUND_UPGRADE);
    int level = getMainTreeLevel();
    level += amount;
    UserDefault::getInstance()->setIntegerForKey(KEY_TAP_LEVEL, level);
    updateTapReward();
    BigNum* bigUpgradePrice = new BigNum();
    bigUpgradePrice->setNum(upgradeTabPrice);
    
    for (int i = 0; i < 4; i++) {
        level++;
        bigUpgradePrice->addNum(getUpgradeTapRewardPrice(level));
    }
    if(theLife->IsBiggerThanThis(bigUpgradePrice) > 0 || btnUpgrade10->getOpacity() > 0){
        showBigUpgradeButtons(10000);   // main tree tag
    }
    delete bigUpgradePrice;
//    oneSecTimeElapse = 0;
    handleTheLifeChanged();
}
void HelloWorldScene::updateSky(){
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    int hour = now->tm_hour;
    if(forceTime >= 0){
        hour = forceTime;
    }
//    hour += testHour; // test
//    testHour++; // test
//    hour = hour%24; // test
//    hour = 1; // test
    int min = now->tm_min;
    float totalMin = min + hour*60;
    int skyDayY = 1080;
    int skyNightY = 750;
    float y = 0;
    float rate;
    int isNight = false;
    int isDawnOrSunset = false;
    if (totalMin < 5*60) { // night
        y = skyNightY;
        isNight = true;
    }else if (totalMin < 8*60) {    // dawn
        rate = (totalMin - 60*5)/(60*3);
        y = skyNightY + (skyDayY - skyNightY)*rate;
        isDawnOrSunset = true;
    }else if (totalMin < 17*60) {    // day
        y = skyDayY;
    }else if (totalMin < 20*60) {    // sunset
        rate = (totalMin - 17*60)/(60*3);
        y = skyDayY - (skyDayY - skyNightY)*rate;
        isDawnOrSunset = true;
    }else { // night
        y = skyNightY;
        isNight = true;
    }
    bool isShineOn = false;
    for(auto emit: shineEmitList){
        if (emit->getOpacity() > 0) {
            isShineOn = true;
            break;
        }
    }
    bool isTwinkleOn = false;
    for(auto twinkle: twinkleList){
        if (twinkle->getOpacity() > 0) {
            isTwinkleOn = true;
            break;
        }
    }
    if (isNight) { // night
        if (isShineOn) {
            for(auto emit: shineEmitList){
                emit->stopAllActions();
                emit->runAction(FadeOut::create((rand()%100)*0.01f + 0.1f));
            }
        }
    }else{ // day
        if (!isShineOn) {
            for(auto emit: shineEmitList){
//                emit->setOpacity(rand()%255);
                emit->stopAllActions();
                emit->runAction(RepeatForever::create(Sequence::create(DelayTime::create((rand()%100)*0.01f + 0.1f), FadeTo::create((rand()%100)*0.01f + 0.1f, 100), DelayTime::create((rand()%100)*0.01f + 0.1f), FadeOut::create((rand()%100)*0.01f + 0.1f), NULL)));
            }
        }
    }
    
    if (isNight || isDawnOrSunset) {
        if (!isTwinkleOn) {
            for(auto twinkle: twinkleList){
                twinkle->stopAllActions();
                twinkle->runAction(RepeatForever::create(Sequence::create(DelayTime::create((rand()%100)*0.01f + 0.1f), FadeTo::create((rand()%100)*0.01f + 0.1f, 200), DelayTime::create((rand()%100)*0.01f + 0.1f), FadeTo::create((rand()%100)*0.01f + 0.1f, 10), NULL)));
            }
        }
    }else{
        if (isTwinkleOn) {
            for(auto twinkle: twinkleList){
                twinkle->stopAllActions();
                twinkle->runAction(FadeOut::create((rand()%100)*0.01f + 0.1f));
            }
        }
    }
    
    float svFieldY = 0;
    if (GameManager::getInstance()->isPortrait) {
        svFieldY = 584;
    }
    sky->setPositionY(y + svFieldY);
    
//    Node* sun = loadedLayer->getChildByName("sunMoon");
    float rotationRate = totalMin/(24*60);
    float rotation = 180 + 360*rotationRate;
    sunMoon->setRotation(rotation);
}
void HelloWorldScene::runTreeIdleAnimation(){
    treeFoot->stopAllActions();
    treeFront->stopAllActions();
    treeFoot->setScale(1.04f);
    GameManager::getInstance()->makeItSiluk(treeFoot, 3, 1.02f);
    treeFront->setScale(1.04f);
    GameManager::getInstance()->makeItSiluk(treeFront, 3, 1.05f);
    // test
    float duration = 2.3f;
    for (int i = 0; i < leafList.size(); i++) {
        Node* leaf = leafList.at(i);
        leaf->stopAllActions();
        duration = 3.3f + (rand()%4)*0.1f;
        leaf->runAction(RepeatForever::create(Sequence::create(EaseOut::create(RotateTo::create(duration, rand()%60 - 30), 2), EaseInOut::create(RotateTo::create(duration*2, 0), 2), NULL)));
    }
}
void HelloWorldScene::shakeTree(){
    treeFoot->stopAllActions();
    treeFront->stopAllActions();
    treeFoot->setScale(1.04f);
    GameManager::getInstance()->makeItSilukOnce(treeFoot, 0.1f, 1.04f);
    treeFront->setScale(1.04f);
    GameManager::getInstance()->makeItSilukOnce(treeFront, 0.1f, 1.04f);
    // test
    float duration = 0.3f;
    for (int i = 0; i < leafList.size(); i++) {
        Node* leaf = leafList.at(i);
        leaf->stopAllActions();
        duration = 0.3f + (rand()%4)*0.1f;
        leaf->runAction(Sequence::create(EaseOut::create(RotateTo::create(duration, rand()%60 - 30), 2), EaseInOut::create(RotateTo::create(duration*2, 0), 2), NULL));
    }
    int leafCount = 5;
    int maxLeafCount = 50;
    float dur = 1;
    int leafCounter = 0;
    for (int i = (int)leafParticleList.size(); i < maxLeafCount; i++) {
        leafCounter++;
        if (leafCounter > leafCount) {
            break;
        }
        Sprite* spt = Sprite::createWithSpriteFrameName("leaf0.png");
        spt->setScale(0.5f);
        spt->setRotation(rand()%360);
        spt->setPosition(treeFoot->getPosition() + Point(rand()%200 - 100, rand()%200 - 100));
        spt->runAction(RotateBy::create(dur, rand()%180));
        spt->runAction(JumpBy::create(dur, Point(rand()%60 - 30, -rand()%20), 40, 1));
        spt->runAction(Sequence::create(DelayTime::create(dur/2), FadeOut::create(dur/2), CallFuncN::create(CC_CALLBACK_1(HelloWorldScene::onLeafParticleMoveDone, this)), NULL));
        leafParticleList.pushBack(spt);
        treeBack->addChild(spt, 1);
    }
    this->runAction(Sequence::create(DelayTime::create(1), CallFunc::create(CC_CALLBACK_0(HelloWorldScene::runTreeIdleAnimation, this)), NULL));
}
void HelloWorldScene::onLeafParticleMoveDone(Ref* ref){
    Sprite* spt = (Sprite*)ref;
    leafParticleList.eraseObject(spt);
    spt->removeFromParent();
}
float HelloWorldScene::getTreeYByLevel(int level){
    if (level == 0) {
        return 454.20f;
    }else if (level == 1) {
        return 468.21f;
    }else if (level == 2) {
        return 481.21f;
    }else if (level == 3) {
        return 494.21f;
    }else if (level == 4) {
        return 506.21f;
    }else if (level == 5) {
        return 512.21f;
    }else if (level == 6) {
        return 525.21f;
    }
}
void HelloWorldScene::OnHideClick(){
    GameManager::getInstance()->playSoundEffect(SOUND_MENU_DOWN);
    loadedLayer->getChildByName("btnAppear")->setVisible(true);
    loadedLayer->getChildByName("btnHidden")->setVisible(false);
    updateOrientation();
}
void HelloWorldScene::OnAppearClick(){
    GameManager::getInstance()->playSoundEffect(SOUND_MENU_UP);
    loadedLayer->getChildByName("btnAppear")->setVisible(false);
    loadedLayer->getChildByName("btnHidden")->setVisible(true);
    updateOrientation();
}
                               
void HelloWorldScene::onMoveSelectedNode(Ref* ref){
    Node* btn = (Node*)ref;
    if (btn->getTag() == 8){
        if (false) {// maintree
            
            currentTreeLevel ++;
            if (currentTreeLevel > 6) {
                currentTreeLevel = 0;
            }
//            setMainTree(currentTreeLevel, 5);
            //        setMainTree(6, currentTreeLevel);
        }else if(false){ // meerkat
            currentTreeLevel ++;
            if (currentTreeLevel > 4) {
                currentTreeLevel = 0;
            }
            setMeerkat(currentTreeLevel);
        }else if(false){ // monkey
            currentTreeLevel ++;
            if (currentTreeLevel > 5) {
                currentTreeLevel = 0;
            }
            setMonkey(currentTreeLevel);
        }else if(false){   // duck
            currentTreeLevel ++;
            if (currentTreeLevel > 5) {
                currentTreeLevel = 0;
            }
            setDuck(currentTreeLevel);
        }else if(false){   // baobob
            currentTreeLevel ++;
            if (currentTreeLevel > 5) {
                currentTreeLevel = 0;
            }
            setBaobob(currentTreeLevel);
        }else if(false){   // star0
            currentTreeLevel ++;
            if (currentTreeLevel > 5) {
                currentTreeLevel = 0;
            }
            setStar0(currentTreeLevel);
        }else if(false){   // star1
            currentTreeLevel ++;
            if (currentTreeLevel > 5) {
                currentTreeLevel = 0;
            }
            setStar1(currentTreeLevel);
        }else if(true){   // star2
            currentTreeLevel ++;
            if (currentTreeLevel > 5) {
                currentTreeLevel = 0;
            }
            setStar2(currentTreeLevel);
        }else{
            currentTreeLevel ++;
            if (currentTreeLevel > 5) {
                currentTreeLevel = 0;
            }
            setLion(currentTreeLevel);
        }
        return;
    }else if(btn->getTag() == 9){
        currentSelectedIndex++;
        if (currentSelectedIndex >= decoList.size()) {
            currentSelectedIndex = 0;
        }
        Sprite* deco = decoList.at(currentSelectedIndex);
        float scale = deco->getScale();
        deco->runAction(Sequence::create(ScaleTo::create(0.3, scale*2), ScaleTo::create(0.3f, scale), NULL));
        return;
    }
    Node* node = selectedNode;
    float x = node->getPositionX();
    float y = node->getPositionY();
    if (btn->getTag() == 0){
        y += 1;
    }else if (btn->getTag() == 1){
        x += 1;
    }else if (btn->getTag() == 2){
        y -= 1;
    }else if (btn->getTag() == 3){
        x -= 1;
    }else if (btn->getTag() == 4){
        y += 10;
    }else if (btn->getTag() == 5){
        x += 10;
    }else if (btn->getTag() == 6){
        y -= 10;
    }else if (btn->getTag() == 7){
        x -= 10;
    }
    node->setPosition(x, y);
    log("[%d, %d]", (int)x, (int)y);
}
void HelloWorldScene::setMeerkat(int level){
//    for(auto money: meerkatList){
//        money->removeFromParentAndCleanup(true);
//    }
//    meerkatList.clear();
    int count = 1 + level/100;
    if (count > 5) {
        count = 5;
    }
    for (int i = (int)meerkatList.size(); i < count; i++) {
        Sprite* spt = Sprite::createWithSpriteFrameName(StringUtils::format("meerkat%d.png", i));
        int local = 0;
        if (i == 0) {
            spt->setPosition(Point(1963, 247));
            local = 0;
        }else if (i == 1) {
            spt->setPosition(Point(2032, 255));
            local = 2;
        }else if (i == 2) {
            spt->setPosition(Point(2063, 247));
            local = 3;
        }else if (i == 3) {
            spt->setPosition(Point(1999, 263));
            local = 0;
        }else if (i == 4) {
            spt->setPosition(Point(2093, 265));
            local = 2;
        }
        svField->addChild(spt, -spt->getPosition().y + local + spt->getContentSize().height/2);
        meerkatList.pushBack(spt);
        zoomInScrolllView(spt, 2);
    }
}
void HelloWorldScene::setLion(int level){
//    for(auto lion: lionList){
//        lion->removeFromParentAndCleanup(true);
//    }
//    lionList.clear();
    int count = 1 + level/100;
    if (count > 5) {
        count = 5;
    }
    for (int i = (int)lionList.size(); i < count; i++) {
        Sprite* spt = Sprite::createWithSpriteFrameName(StringUtils::format("lionRock%d.png", i));
        if (i == 0) {
            spt->setPosition(Point(2020, 504));
        }else if (i == 1) {
            spt->setPosition(Point(1898, 443));
        }else if (i == 2) {
            spt->setPosition(Point(2032, 402));
        }else if (i == 3) {
            spt->setPosition(Point(1965, 680));
        }else if (i == 4) {
            spt->setPosition(Point(1911, 637));
        }
        rockBgBatch->addChild(spt, 1);
//        this->addChild(spt, 1);
//        spt->setPosition(spt->getPosition() + Point(-1500, 0));
        lionList.pushBack(spt);
        zoomInScrolllView(spt, 2);
    }
}

void HelloWorldScene::setDuck(int level){
//    for(auto duck: duckList){
//        duck->removeFromParentAndCleanup(true);
//    }
//    duckList.clear();
    int count = 1 + level/100;
    if (count > 5) {
        count = 5;
    }
    for (int i = (int)duckList.size(); i < count; i++) {
        Sprite* spt = Sprite::createWithSpriteFrameName(StringUtils::format("duckRock%d.png", i));
        if (i == 0) {
            spt->setPosition(Point(968, 646));
        }else if (i == 1) {
            spt->setPosition(Point(895, 584));
        }else if (i == 2) {
            spt->setPosition(Point(1148, 543));
        }else if (i == 3) {
            spt->setPosition(Point(1099, 612));
        }else if (i == 4) {
            spt->setPosition(Point(883, 510));
        }
        rockBgBatch->addChild(spt, 1);
        duckList.pushBack(spt);
        zoomInScrolllView(spt, 2);
    }
}

void HelloWorldScene::setStar0(int level){
//    for(auto star: star0List){
//        star->removeFromParentAndCleanup(true);
//    }
//    star0List.clear();
    Sprite* spt;
    if (star0List.size() == 0) {
        spt = Sprite::createWithSpriteFrameName(StringUtils::format("star%d.png", 0));
        star0List.pushBack(spt);
        starBatch->addChild(spt);
        spt->setPosition(Point(2246, 536));
    }
    
    int count = 1 + level/100;
    if (count > 5) {
        count = 5;
    }
    for (int i = (int)star0List.size() - 1; i < count; i++) {
        spt = Sprite::createWithSpriteFrameName(StringUtils::format("star0_%d.png", i));
        if (i == 0) {
            spt->setPosition(Point(2191, 535));
        }else if (i == 1) {
            spt->setPosition(Point(2289, 590));
        }else if (i == 2) {
            spt->setPosition(Point(2235, 526));
            selectedNode = spt;
        }else if (i == 3) {
            spt->setPosition(Point(2285, 484));
        }else if (i == 4) {
            spt->setPosition(Point(2292, 548));
        }
        starBatch->addChild(spt, 1);
        star0List.pushBack(spt);
        zoomInScrolllView(spt, 2);
    }
}

void HelloWorldScene::setStar1(int level){
//    for(auto star: star1List){
//        star->removeFromParentAndCleanup(true);
//    }
//    star1List.clear();
    Sprite* spt;
    if (star1List.size() == 0) {
        spt = Sprite::createWithSpriteFrameName(StringUtils::format("star%d.png", 1));
        star1List.pushBack(spt);
        starBatch->addChild(spt);
        spt->setPosition(Point(398, 646));
    }
    int count = 1 + level/100;
    if (count > 5) {
        count = 5;
    }
    for (int i = (int)star1List.size() - 1; i < count; i++) {
        spt = Sprite::createWithSpriteFrameName(StringUtils::format("star1_%d.png", i));
        if (i == 0) {
            spt->setPosition(Point(455, 650));
        }else if (i == 1) {
            spt->setPosition(Point(465, 596));
        }else if (i == 2) {
            spt->setPosition(Point(380, 596));
        }else if (i == 3) {
            spt->setPosition(Point(332, 665));
        }else if (i == 4) {
            spt->setPosition(Point(372, 710));
        }
        starBatch->addChild(spt, 1);
        star1List.pushBack(spt);
        zoomInScrolllView(spt, 2);
    }
}
void HelloWorldScene::setStar2(int level){
//    for(auto star: star2List){
//        star->removeFromParentAndCleanup(true);
//    }
//    star2List.clear();
    Sprite* spt;
    if (star2List.size() == 0) {
        spt = Sprite::createWithSpriteFrameName(StringUtils::format("star%d.png", 2));
        star2List.pushBack(spt);
        starBatch->addChild(spt);
        spt->setPosition(Point(1792, 666));
    }
    int count = 1 + level/100;
    if (count > 5) {
        count = 5;
    }
    for (int i = (int)star2List.size() - 1; i < count; i++) {
        spt = Sprite::createWithSpriteFrameName(StringUtils::format("star2_%d.png", i));
        if (i == 0) {
            spt->setPosition(Point(1710, 626));
        }else if (i == 1) {
            spt->setPosition(Point(1753, 680));
        }else if (i == 2) {
            spt->setPosition(Point(1767, 629));
        }else if (i == 3) {
            spt->setPosition(Point(1820, 655));
        }else if (i == 4) {
            spt->setPosition(Point(1854, 686));
        }
        starBatch->addChild(spt, 1);
        star2List.pushBack(spt);
        zoomInScrolllView(spt, 2);
    }
}

void HelloWorldScene::setBaobob(int level){
//    for(auto baobob: baobobList){
//        baobob->removeFromParentAndCleanup(true);
//    }
//    baobobList.clear();
    Sprite* spt;
    int count = 1 + level/100;
    if (count > 5) {
        count = 5;
    }
    for (int i = (int)baobobList.size(); i < count; i++) {
        if (i == 0) {
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("baobob%d.png", i));
            treeBatch->addChild(spt, 1);
            baobobList.pushBack(spt);
            spt->setPosition(Point(984, 470));
        }else if (i == 1) {
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("baobob%d_0.png", i));
            treeBatch->addChild(spt, 1);
            baobobList.pushBack(spt);
            spt->setPosition(Point(910, 436));
            
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("baobob%d_1.png", i));
            treeBatch->addChild(spt, 1);
            baobobList.pushBack(spt);
            spt->setPosition(Point(1669, 442));
        }else if (i == 2) {
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("baobob%d_0.png", i));
            treeBatch->addChild(spt, 1);
            baobobList.pushBack(spt);
            
            spt->setPosition(Point(622, 475));
            
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("baobob%d_1.png", i));
            treeBatch->addChild(spt, 1);
            baobobList.pushBack(spt);
            spt->setPosition(Point(1712, 485));
        }else if (i == 3) {
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("baobob%d_0.png", i));
            treeBatch->addChild(spt, 1);
            baobobList.pushBack(spt);
            spt->setPosition(Point(169, 453));
            
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("baobob%d_1.png", i));
            treeBatch->addChild(spt, 1);
            baobobList.pushBack(spt);
            
            spt->setPosition(Point(2465, 446));
        }else if (i == 4) {
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("baobob%d_0.png", i));
            treeBatch->addChild(spt, 1);
            baobobList.pushBack(spt);
            spt->setPosition(Point(78, 487));
            
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("baobob%d_1.png", i));
            treeBatch->addChild(spt, 1);
            baobobList.pushBack(spt);
            spt->setPosition(Point(2624, 472));
        }
        zoomInScrolllView(spt, 2);
        
    }
}

void HelloWorldScene::setMonkey(int level){
//    for(auto monkey: monkeyList){
//        monkey->removeFromParentAndCleanup(true);
//    }
//    monkeyList.clear();
    Sprite* spt;
    
    if (monkeyList.size() == 0) {
        Sprite* sptRuby = Sprite::createWithSpriteFrameName("ruby.png");
        svField->addChild(sptRuby);
        monkeyList.pushBack(sptRuby);
    }
    int height = svField->getContentSize().height;
    int zorder = 0;
    int count = 1 + level/100;
    if (count > 5) {
        count = 5;
    }
    for (int i = (int)monkeyList.size() - 1; i < count; i++) {
        if (i == 0) {
            if (level == 1) {
                spt = Sprite::createWithSpriteFrameName("1.png");
                spt->setPosition(Point(742, height - 584));
            }else{
                spt = Sprite::createWithSpriteFrameName("2_1.png");
                spt->setPosition(Point(760, height - 616));
            }
            monkeyList.at(0)->setPosition(Point(736, height - 420));
            zorder = -spt->getPosition().y + spt->getContentSize().height/2;
        }else if (i == 1) {
            if (level == 2) {
                spt = Sprite::createWithSpriteFrameName("2_2.png");
                spt->setPosition(Point(728, height - 482));
                monkeyList.at(0)->setPosition(Point(736, height - 322));
            }else{
                spt = Sprite::createWithSpriteFrameName("3_2.png");
                spt->setPosition(Point(722, height - 426));
                monkeyList.at(0)->setPosition(Point(736, height - 202));
            }
        }else if (i == 2) {
            continue;
        }else if (i == 3) {
            spt = Sprite::createWithSpriteFrameName("4_2.png");
            spt->setPosition(Point(788, height - 480));
        }else if (i == 4) {
            spt = Sprite::createWithSpriteFrameName("5_2.png");
            spt->setPosition(Point(672, height - 398));
        }
        svField->addChild(spt, zorder);
        spt->setLocalZOrder(zorder);
        monkeyList.pushBack(spt);
        zoomInScrolllView(spt, 2);
    }
    monkeyList.at(0)->setLocalZOrder(zorder);
}

void HelloWorldScene::setGrass(){
    for(auto deco : grassList){
        deco->removeFromParentAndCleanup(true);
    }
    grassList.clear();
    int height = svField->getContentSize().height - 1;
    if (isSanhoUnlocked(0)) {
        grassList.pushBack(addToField("grass_e_1.png", Point(1320, height - 376)));
    }
    if (getSanhoLevel(0) > 0) {
        grassList.pushBack(addToField("grass_e_2.png", Point(1176, height - 374)));
    }
    if (getSanhoLevel(0) > 1) {
        grassList.pushBack(addToField("grass_e_3.png", Point(1482, height - 374)));
    }
    if (getSanhoLevel(0) > 2) {
        grassList.pushBack(addToField("grass_e_4.png", Point(1586, height - 372)));
    }
    if (getSanhoLevel(0) > 3) {
        grassList.pushBack(addToField("grass_e_5.png", Point(1066, height - 372)));
    }
    if (isSanhoUnlocked(1)) {
        grassList.pushBack(addToField("grass_e_5.png", Point(950, height - 372)));
    }
    if (getSanhoLevel(1) > 0) {
        grassList.pushBack(addToField("grass_d_3.png", Point(976, height - 442)));
    }
    if (getSanhoLevel(1) > 1) {
        grassList.pushBack(addToField("grass_d_4.png", Point(1708, height - 442)));
        grassList.pushBack(addToField("grass_d_4.png", Point(1754, height - 442)));
    }
    if (getSanhoLevel(1) > 2) {
        grassList.pushBack(addToField("grass_e_6.png", Point(1720, height - 372)));
    }
    if (getSanhoLevel(1) > 3) {
        grassList.pushBack(addToField("bush_d_2.png", Point(1714, height - 384)));
    }
    if (isSanhoUnlocked(2)) {
        grassList.pushBack(addToField("grass_e_6.png", Point(808, height - 372)));
    }
    if (getSanhoLevel(2) > 0) {
        grassList.pushBack(addToField("grass_d_2.png", Point(874, height - 440)));
        grassList.pushBack(addToField("grass_d_1.png", Point(918, height - 442)));
        grassList.pushBack(addToField("grass_c_5.png", Point(930, height - 490)));
    }
    if (getSanhoLevel(2) > 1) {
        grassList.pushBack(addToField("grass_c_5.png", Point(1792, height - 490)));
        grassList.pushBack(addToField("grass_c_6.png", Point(1716, height - 496)));
    }
    if (getSanhoLevel(2) > 2) {
        grassList.pushBack(addToField("grass_d_5.png", Point(1848, height - 442)));
        grassList.pushBack(addToField("grass_d_6.png", Point(1892, height - 442)));
        grassList.pushBack(addToField("grass_d_4.png", Point(1938, height - 442)));
    }
    if (getSanhoLevel(2) > 3) {
        grassList.pushBack(addToField("grass_e_2.png", Point(1906, height - 374)));
    }
    if (isSanhoUnlocked(3)) {
        grassList.pushBack(addToField("grass_d_4.png", Point(594, height - 372)));
    }
    if (getSanhoLevel(3) > 0) {
        grassList.pushBack(addToField("grass_d_3.png", Point(808, height - 442)));
        grassList.pushBack(addToField("grass_d_4.png", Point(750, height - 442)));
    }
    if (getSanhoLevel(3) > 1) {
        grassList.pushBack(addToField("grass_c_6.png", Point(854, height - 496)));
        grassList.pushBack(addToField("grass_c_4.png", Point(788, height - 488)));
    }
    if (getSanhoLevel(3) > 2) {
        grassList.pushBack(addToField("bush_a_2.png", Point(678, height - 602)));
    }
    if (getSanhoLevel(3) > 3) {
        grassList.pushBack(addToField("bush_a_1.png", Point(814, height - 608)));
    }
    if (isSanhoUnlocked(4)) {
        grassList.pushBack(addToField("grass_b_5.png", Point(1048, height - 572)));
    }
    if (getSanhoLevel(4) > 0) {
        grassList.pushBack(addToField("grass_b_4.png", Point(1152, height - 576)));
    }
    if (getSanhoLevel(4) > 1) {
        grassList.pushBack(addToField("grass_b_2.png", Point(1318, height - 572)));
    }
    if (getSanhoLevel(4) > 2) {
        grassList.pushBack(addToField("grass_b_5.png", Point(1444, height - 572)));
    }
    if (getSanhoLevel(4) > 3) {
        grassList.pushBack(addToField("grass_b_3.png", Point(1594, height - 572)));
    }
    if (isSanhoUnlocked(5)) {
        grassList.pushBack(addToField("grass_e_3.png", Point(2046, height - 374)));
    }
    if (getSanhoLevel(5) > 0) {
        grassList.pushBack(addToField("grass_d_1.png", Point(2040, height - 442)));
        grassList.pushBack(addToField("grass_d_2.png", Point(1994, height - 440)));
    }
    if (getSanhoLevel(5) > 1) {
        grassList.pushBack(addToField("grass_c_6.png", Point(1894, height - 496)));
        grassList.pushBack(addToField("grass_c_3.png", Point(1972, height - 492)));
    }
    if (getSanhoLevel(5) > 2) {
        grassList.pushBack(addToField("grass_b_2.png", Point(1696, height - 572)));
    }
    if (getSanhoLevel(5) > 3) {
        grassList.pushBack(addToField("grass_b_1.png", Point(1776, height - 572)));
    }
    if (isSanhoUnlocked(6)) {
        grassList.pushBack(addToField("grass_e_5.png", Point(424, height - 374)));
    }
    if (getSanhoLevel(6) > 0) {
        grassList.pushBack(addToField("grass_d_3.png", Point(534, height - 442)));
        grassList.pushBack(addToField("grass_d_2.png", Point(466, height - 440)));
    }
    if (getSanhoLevel(6) > 1) {
        grassList.pushBack(addToField("grass_c_1.png", Point(656, height - 494)));
        grassList.pushBack(addToField("grass_c_2.png", Point(588, height - 496)));
    }
    if (getSanhoLevel(6) > 2) {
        grassList.pushBack(addToField("bush_c_1.png", Point(642, height - 442)));
    }
    if (getSanhoLevel(6) > 3) {
        grassList.pushBack(addToField("grass_b_6.png", Point(932, height - 574)));
    }
    if (isSanhoUnlocked(7)) {
        grassList.pushBack(addToField("grass_e_2.png", Point(2240, height - 374)));
    }
    if (getSanhoLevel(7) > 0) {
        grassList.pushBack(addToField("grass_d_3.png", Point(2138, height - 442)));
    }
    if (getSanhoLevel(7) > 1) {
        grassList.pushBack(addToField("grass_d_2.png", Point(2204, height - 440)));
    }
    if (getSanhoLevel(7) > 2) {
        grassList.pushBack(addToField("grass_c_4.png", Point(2148, height - 488)));
    }
    if (getSanhoLevel(7) > 3) {
        grassList.pushBack(addToField("grass_b_5.png", Point(1922, height - 572)));
    }
    if (isSanhoUnlocked(8)) {
        grassList.pushBack(addToField("grass_e_2.png", Point(306, height - 374)));
    }
    if (getSanhoLevel(8) > 0) {
        grassList.pushBack(addToField("grass_d_1.png", Point(424, height - 442)));
        grassList.pushBack(addToField("grass_d_2.png", Point(356, height - 442)));
    }
    if (getSanhoLevel(8) > 1) {
        grassList.pushBack(addToField("grass_c_1.png", Point(456, height - 494)));
        grassList.pushBack(addToField("grass_c_3.png", Point(534, height - 492)));
    }
    if (getSanhoLevel(8) > 2) {
        grassList.pushBack(addToField("grass_d_2.png", Point(290, height - 440)));
        grassList.pushBack(addToField("grass_c_3.png", Point(388, height - 432)));
    }
    if (getSanhoLevel(8) > 3) {
        grassList.pushBack(addToField("grass_b_1.png", Point(594, height - 572)));
    }
    if (isSanhoUnlocked(9)) {
        grassList.pushBack(addToField("grass_b_6.png", Point(2014, height - 572)));
        grassList.pushBack(addToField("grass_b_4.png", Point(2094, height - 574)));
    }
    if (getSanhoLevel(9) > 0) {
        grassList.pushBack(addToField("grass_c_6.png", Point(2214, height - 496)));
        grassList.pushBack(addToField("grass_c_5.png", Point(2288, height - 490)));
    }
    if (getSanhoLevel(9) > 1) {
        grassList.pushBack(addToField("grass_d_1.png", Point(2248, height - 442)));
        grassList.pushBack(addToField("grass_d_1.png", Point(2302, height - 442)));
    }
    if (getSanhoLevel(9) > 2) {
        grassList.pushBack(addToField("grass_d_5.png", Point(2046, height - 442)));
        grassList.pushBack(addToField("grass_d_6.png", Point(2362, height - 442)));
    }
    if (getSanhoLevel(9) > 3) {
        grassList.pushBack(addToField("bush_d_3.png", Point(2428, height - 386)));
    }
    if (isSanhoUnlocked(10)) {
        grassList.pushBack(addToField("grass_e_4.png", Point(148, height - 372)));
    }
    if (getSanhoLevel(10) > 0) {
        grassList.pushBack(addToField("grass_d_1.png", Point(246, height - 442)));
        grassList.pushBack(addToField("grass_c_2.png", Point(338, height - 496)));
    }
    if (getSanhoLevel(10) > 1) {
        grassList.pushBack(addToField("grass_b_3.png", Point(494, height - 572)));
    }
    if (getSanhoLevel(10) > 2) {
        grassList.pushBack(addToField("grass_b_2.png", Point(392, height - 572)));
    }
    if (getSanhoLevel(10) > 3) {
        grassList.pushBack(addToField("grass_b_1.png", Point(296, height - 572)));
        grassList.pushBack(addToField("grass_c_1.png", Point(282, height - 494)));
        grassList.pushBack(addToField("bush_d_1.png", Point(22, height - 385)));
    }
    if (isSanhoUnlocked(11)) {
        grassList.pushBack(addToField("bush_d_4.png", Point(2508, height - 392)));
    }
    if (getSanhoLevel(11) > 0) {
        grassList.pushBack(addToField("grass_d_4.png", Point(2452, height - 442)));
        grassList.pushBack(addToField("grass_d_4.png", Point(2508, height - 442)));
    }
    if (getSanhoLevel(11) > 1) {
        grassList.pushBack(addToField("grass_c_4.png", Point(2456, height - 488)));
    }
    if (getSanhoLevel(11) > 2) {
        grassList.pushBack(addToField("grass_b_6.png", Point(2208, height - 572)));
        grassList.pushBack(addToField("grass_b_5.png", Point(2318, height - 572)));
    }
    if (getSanhoLevel(11) > 3) {
        grassList.pushBack(addToField("bush_b_2.png", Point(2408, height - 534)));
    }
    if (isSanhoUnlocked(12)) {
        grassList.pushBack(addToField("grass_b_2.png", Point(108, height - 572)));
        grassList.pushBack(addToField("grass_b_2.png", Point(208, height - 572)));
    }
    if (getSanhoLevel(12) > 0) {
        grassList.pushBack(addToField("bush_b_1.png", Point(118, height - 500)));
    }
    if (getSanhoLevel(12) > 1) {
        grassList.pushBack(addToField("grass_a_1.png", Point(162, height - 702)));
    }
    if (getSanhoLevel(12) > 2) {
        grassList.pushBack(addToField("grass_a_2.png", Point(356, height - 696)));
        grassList.pushBack(addToField("grass_a_3.png", Point(488, height - 702)));
    }
    if (getSanhoLevel(12) > 3) {
        grassList.pushBack(addToField("grass_a_2.png", Point(1122, height - 694)));
        grassList.pushBack(addToField("grass_a_4.png", Point(1232, height - 692)));
        grassList.pushBack(addToField("grass_a_6.png", Point(1372, height - 700)));
    }
    if (isSanhoUnlocked(13)) {
        grassList.pushBack(addToField("grass_a_2.png", Point(1458, height - 696)));
        grassList.pushBack(addToField("grass_a_5.png", Point(1594, height - 696)));
    }
    if (getSanhoLevel(13) > 0) {
        grassList.pushBack(addToField("grass_a_4.png", Point(1734, height - 694)));
        grassList.pushBack(addToField("grass_a_6.png", Point(1870, height - 700)));
        grassList.pushBack(addToField("grass_a_5.png", Point(1964, height - 696)));
    }
    if (getSanhoLevel(13) > 1) {
        grassList.pushBack(addToField("grass_a_2.png", Point(2134, height - 702)));
        grassList.pushBack(addToField("grass_a_3.png", Point(2256, height - 702)));
        grassList.pushBack(addToField("grass_a_2.png", Point(2388, height - 694)));
    }
    if (getSanhoLevel(13) > 2) {
        grassList.pushBack(addToField("grass_b_4.png", Point(2448, height - 574)));
        grassList.pushBack(addToField("grass_b_6.png", Point(2568, height - 572)));
    }
    if (getSanhoLevel(13) > 3) {
        grassList.pushBack(addToField("bush_b_3.png", Point(2590, height - 500)));
    }
    
}
Sprite* HelloWorldScene::addToField(const char* sptName, cocos2d::Point pos){
    Sprite* spt = Sprite::createWithSpriteFrameName(sptName);
    spt->setPosition(Point(pos.x, pos.y - spt->getContentSize().height/2 - 1));
//    spt->setPosition(pos);
    spt->setAnchorPoint(Point(0.5, 0));
    svField->addChild(spt);
    spt->setLocalZOrder(-spt->getPosition().y);
//    DrawNode* draw = DrawNode::create();
//    draw->drawRect(Point(0, 0), spt->getContentSize(), Color4F::BLUE);
//    spt->addChild(draw);
    return spt;
}

void HelloWorldScene::updateSanhoImages(){
    updateMainTree();
    setGrass();
    if (isSanhoUnlocked(4)) {
        setMonkey(getSanhoLevel(4));
    }
    if (isSanhoUnlocked(5)) {
        setLion(getSanhoLevel(5));
    }
    if (isSanhoUnlocked(7)) {
        setStar0(getSanhoLevel(7));
    }
    if (isSanhoUnlocked(9)) {
        setDuck(getSanhoLevel(9));
    }
    if (isSanhoUnlocked(10)) {
        setBaobob(getSanhoLevel(10));
    }
    if (isSanhoUnlocked(11)) {
        setStar1(getSanhoLevel(11));
    }
    if (isSanhoUnlocked(12)) {
        setMeerkat(getSanhoLevel(12));
    }
    if (isSanhoUnlocked(13)) {
        setStar2(getSanhoLevel(13));
    }
}
void HelloWorldScene::unlockSanho(int index){
    GameManager::getInstance()->playSoundEffect(SOUND_CREATE_DECO);
    UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_SANHO_UNLOCKED_FORAMT, index).c_str(), true);
    updateSanhoImages();
    updateTotalSanhoEffect();
    
    int lastUnlockedIndex = UserDefault::getInstance()->getIntegerForKey("last_sanho_unlocked", 0);
    if(lastUnlockedIndex < index){
        UserDefault::getInstance()->setIntegerForKey("last_sanho_unlocked", index);
        UserDefault::getInstance()->setIntegerForKey(StringUtils::format("achievement_current_%d", ACHIEVE_SANHO_GOLD_TREE_UNLOCK).c_str(), index);
        if(index == 6){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQEg");
        }
        UserDefault::getInstance()->setIntegerForKey(StringUtils::format("achievement_current_%d", ACHIEVE_SANHO_WATER_FALL_UNLOCK).c_str(), index);
        if(index == 8){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQEw");
        }
        UserDefault::getInstance()->setIntegerForKey(StringUtils::format("achievement_current_%d", ACHIEVE_SANHO_MONKEY_STAR_UNLOCK).c_str(), index);
        if(index == 13){
            sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQFA");
        }
        
        int total = UserDefault::getInstance()->getIntegerForKey("key_sanho_unlock_event", 0);
        total++;
        UserDefault::getInstance()->setIntegerForKey("key_sanho_unlock_event", total);
        NativeInterface::NativeInterface::trackEvent("decoration", GameManager::getInstance()->getText(StringUtils::format("plant%d", index + 1).c_str()).c_str(), "", "", "unlock", 1);
    }
}
bool HelloWorldScene::isSanhoUnlocked(int index){
    return UserDefault::getInstance()->getBoolForKey(StringUtils::format(KEY_SANHO_UNLOCKED_FORAMT, index).c_str(), false);
}
void HelloWorldScene::upgradeSanho(int index, int amount){
    GameManager::getInstance()->playSoundEffect(SOUND_UPGRADE);
    int level = getSanhoLevel(index);
    level += amount;
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(KEY_SANHO_LEVEL_FORAMT, index).c_str(), level);
    updateTotalSanhoEffect();
}
int HelloWorldScene::getSanhoLevel(int index){
    return UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_SANHO_LEVEL_FORAMT, index).c_str(), 0);
}
BigNum* HelloWorldScene::getUpgradeSanhoPrice(int index, int amount){
    int level = getSanhoLevel(index);
    BigNum* num = getUpgardeSanhoPriceForLevel(index, level);
    for(int i = 0;i < amount;i++){
        num->multiply(1.07f);
    }
    return num;
}
BigNum* HelloWorldScene::getUpgardeSanhoPriceForLevel(int index, int level){
    BigNum* num = getUpgradeSanhoFirstPrice(index);
    for (int i = 0; i < level; i++ ) {
        num->multiply(1.07f);
    }
    return num;
}
BigNum* HelloWorldScene::getUpgradeSanhoFirstPrice(int index){
    BigNum* num = new BigNum();
    if (index == 0) {
        num->addNum(1600, 0);
    }else if (index == 1) {
        num->addNum(86400, 0);
    }else if (index == 2) {
        num->addNum(22, 2);
        num->addNum(118, 1);
        num->addNum(400, 0);
    }else if (index == 3) {
        num->addNum(16, 3);
        num->addNum(588, 2);
        num->addNum(800, 1);
    }else if (index == 4) {
        num->addNum(28, 4);
        num->addNum(665, 3);
        num->addNum(446, 2);
        num->addNum(400, 1);
    }else if (index == 5) {
        num->addNum(98, 5);
        num->addNum(332, 4);
        num->addNum(481, 3);
        num->addNum(152, 2);
    }else if (index == 6) {
        num->addNum(604, 6);
        num->addNum(93, 5);
        num->addNum(324, 4);
        num->addNum(197, 3);
        num->addNum(888, 2);
    }else if (index == 7) {
        num->addNum(6, 8);
        num->addNum(165, 7);
        num->addNum(376, 6);
        num->addNum(466, 5);
        num->addNum(763, 4);
        num->addNum(640, 3);
    }else if (index == 8) {
        num->addNum(98, 9);
        num->addNum(646, 8);
        num->addNum(023, 7);
        num->addNum(468, 6);
        num->addNum(218, 5);
        num->addNum(300, 4);
    }else if (index == 9) {
        num->addNum(2, 11);
        num->addNum(363, 10);
        num->addNum(361, 9);
        num->addNum(430, 8);
        num->addNum(215, 7);
        num->addNum(570, 6);
    }else if (index == 10) {
        num->addNum(81, 12);
        num->addNum(677, 11);
        num->addNum(771, 10);
        num->addNum(29, 9);
        num->addNum(494, 8);
        num->addNum(400, 7);
    }else if (index == 11) {
        num->addNum(3, 14);
        num->addNum(947, 13);
        num->addNum(813, 12);
        num->addNum(384, 11);
        num->addNum(939, 10);
        num->addNum(580, 9);
    }else if (index == 12) {
        num->addNum(259, 15);
        num->addNum(987, 14);
        num->addNum(198, 13);
        num->addNum(278, 12);
        num->addNum(581, 11);
    }else if (index == 13) {
        num->addNum(22, 17);
        num->addNum(813, 16);
        num->addNum(876, 15);
        num->addNum(648, 14);
        num->addNum(945, 13);
        num->addNum(500, 12);
    }
    return num;
}
void HelloWorldScene::onBonusSanhoClick(Ref* ref){
    Button* btn = (Button*)ref;
    int index = btn->getTag();
    addSanhoBonusLevel(index);
    updateSanhoItem(index);
    updateTotalSanhoEffect();
    GameManager::getInstance()->showDisposableMessage(StringUtils::format("(%s) %s +200%%", GameManager::getInstance()->getText(StringUtils::format("plant%d", index + 1).c_str()).c_str(), GameManager::getInstance()->getText("life per sec").c_str()).c_str(), this);
    updateCurrencyLabels();
    log("bonus: %d", index);
}
void HelloWorldScene::onUpgradeSanhoClick(Ref* ref){
    Button* btn = (Button*)ref;
    int index = btn->getTag();
    BigNum* upgradePrice = getUpgradeSanhoPrice(index, 1);
    if (theLife->IsBiggerThanThis(upgradePrice) >= 0) {
        theLife->subtractNum(upgradePrice);
        
        if (isSanhoUnlocked(index)) {
            upgradeSanho(index, 1);
        }else{
            unlockSanho(index);
            
            std::string text = StringUtils::format("%s +7", GameManager::getInstance()->getText("max animal possess count increased").c_str());
            GameManager::getInstance()->showDisposableMessage(text.c_str(), this);
        }
//        
//        if (!isCaptureMode) {
//            isTouchedForLife = true;
//        }
        
        updateSanhoItem(index);
        updateSanhoItem(index+1);
        showBigUpgradeButtons(index);
        handleTheLifeChanged();
        updateSanhoImages();
        shakeSanho();
        
        int growCounter = getQuestProgress(2);
        if (growCounter < getQuestMaxCount(2)) {
            growCounter++;
            setQuestProgress(2, growCounter);
            if (questLayer != nullptr) {
                updateQuest(0);
            }
        }
    }else{
        log("not enough money");
        GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NEGA);
    }
    delete upgradePrice;
}
int HelloWorldScene::getSanhoBonusLevel(int sanhoIndex){
    return UserDefault::getInstance()->getIntegerForKey(StringUtils::format("sanho_bonus_level_%d", sanhoIndex).c_str(), 0);
}
void HelloWorldScene::addSanhoBonusLevel(int sanhoIndex){
    int level = getSanhoBonusLevel(sanhoIndex);
    level++;
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format("sanho_bonus_level_%d", sanhoIndex).c_str(), level);
}
int HelloWorldScene::getMainTreeBonusLevel(){
    return UserDefault::getInstance()->getIntegerForKey("maintree_bonus_level", 0);
}
void HelloWorldScene::addMainTreeBonusLevel(){
    int level = getMainTreeBonusLevel();
    level++;
    UserDefault::getInstance()->setIntegerForKey("maintree_bonus_level", level);
}
void HelloWorldScene::updateSanhoItem(int index){
    int treeLevel = getMainTreeLevel();
    int level = getSanhoLevel(index);
    BigNum* num = getSanhoEffect(index);
    ImageView* item = (ImageView*)svSkill->getInnerContainer()->getChildByName(StringUtils::format("sanhoItem%d", index).c_str());
    if (item == nullptr) {
        return;
    }
    Button* btn = (Button*)item->getChildByName("btnUpgrade");
    Text* lblDescriptionInBtn = (Text*)btn->getChildByName("lblDescription");
    Text* lblPrice = (Text*)btn->getChildByName("lblPrice");
    BigNum* nextNum = new BigNum();
    nextNum->addNum(num);
    nextNum->multiply(0.1f);
    Node* icon = btn->getChildByName("sptIcon");
    if (isSanhoUnlocked(index)) {
//        lblDescriptionInBtn->setString(StringUtils::format("+%s %s", nextNum->getExpression().c_str(), GameManager::getInstance()->getText("life").c_str()));
        lblDescriptionInBtn->setString(StringUtils::format("+%s", nextNum->getExpression().c_str()));
        icon->setOpacity(255);
        if(btn->getChildByName("plusIcon") == nullptr){
            ImageView* sptIcon = ImageView::create("icon_time.png");
            btn->addChild(sptIcon);
            sptIcon->setScale(0.6f);
            sptIcon->setPosition(lblDescriptionInBtn->getPosition());
            sptIcon->setName("plusIcon");
        }
        GameManager::getInstance()->alignToCenter(btn->getChildByName("plusIcon"), lblDescriptionInBtn, 5, btn->getContentSize().width*btn->getScaleX()/2, 0);
    }else{
        GameManager::getInstance()->setLocalizedString(lblDescriptionInBtn, "unlock");
        icon->setOpacity(100);
    }
    
    
    Text* lblDescription = (Text*)item->getChildByName("lblDescription");
    Text* lblDescription_0 = (Text*)item->getChildByName("lblDescription_0");
    lblDescription_0->setString(StringUtils::format("%s %s/%s", num->getExpression().c_str(), GameManager::getInstance()->getText("life").c_str(), GameManager::getInstance()->getText("sec").c_str()).c_str());
    
    if (level/25 > getSanhoBonusLevel(index)) {
        item->getChildByName("btnBonus")->setVisible(true);
        lblDescription_0->setString(StringUtils::format("(%s) %s +200%%", GameManager::getInstance()->getText(StringUtils::format("plant%d", index + 1).c_str()).c_str(), GameManager::getInstance()->getText("life per sec").c_str()).c_str());
    }else{
        item->getChildByName("btnBonus")->setVisible(false);
    }
    
    bool condition0 = false;
    bool condition1 = false;
    int previousLevel;
    if(index == 0){
        condition0 = true;
        condition1 = true;
    }else{
        previousLevel = getSanhoLevel(index - 1);
        condition0 = previousLevel >= 100 - 1;
        condition1 = treeLevel >= (index + 1)*25 - 1;
    }
    ImageView* sptCheck0 = (ImageView*)item->getChildByName("sptCheck0");
    ImageView* sptCheck1 = (ImageView*)item->getChildByName("sptCheck1");
    Text* lblCondition0 = (Text*)item->getChildByName("lblCondition0");
    Text* lblCondition1 = (Text*)item->getChildByName("lblCondition1");
    Node* sptIcon = item->getChildByName("sptIcon");
    if(level == 0 || (level >= 0 && isSanhoUnlocked(index))){
        item->loadTexture("ui_list_bg.png");
        sptIcon->setOpacity(255);
        if(sptCheck0 != nullptr){
            sptCheck0->removeFromParentAndCleanup(true);
        }
        if(sptCheck1 != nullptr){
            sptCheck1->removeFromParentAndCleanup(true);
        }
        if(lblCondition0 != nullptr){
            lblCondition0->removeFromParentAndCleanup(true);
        }
        if(lblCondition1 != nullptr){
            lblCondition1->removeFromParentAndCleanup(true);
        }
        item->getChildByName("timeIcon")->setVisible(true);
        item->getChildByName("lblDescription_0")->setVisible(true);
        lblDescription->setTextColor(Color4B(94, 93, 73, 255));
//        lblDescription_0->setTextColor(Color4B(140, 137, 103, 255));
        btn->loadTextureNormal("btnUpgrade.png");
        btn->loadTexturePressed("btnUpgradeDown.png");
        lblDescription->setString(StringUtils::format("%s%d %s", GameManager::getInstance()->getText("level").c_str(), level + 1, GameManager::getInstance()->getText(StringUtils::format("plant%d", index + 1).c_str()).c_str()).c_str());
    }else{
        item->getChildByName("timeIcon")->setVisible(false);
        item->getChildByName("lblDescription_0")->setVisible(false);
        item->loadTexture("ui_list_bg_locked.png");
        sptIcon->setOpacity(100);
        sptCheck0->loadTexture(condition0?"sptChecked.png":"sptUnchecked.png");
        sptCheck1->loadTexture(condition1?"sptChecked.png":"sptUnchecked.png");
        lblCondition0->setString(StringUtils::format("%s Lv.%d", GameManager::getInstance()->getText(StringUtils::format("plant%d", index).c_str()).c_str(), 100).c_str());
        lblCondition1->setString(StringUtils::format("%s Lv.%d", GameManager::getInstance()->getText("tree of life").c_str(), (index + 1)*25).c_str());
        lblDescription->setTextColor(Color4B(250, 248, 214, 255));
//        lblDescription_0->setTextColor(Color4B(219, 210, 174, 255));
        lblDescription->setString(StringUtils::format("%s", GameManager::getInstance()->getText(StringUtils::format("plant%d", index + 1).c_str()).c_str()).c_str());
    }
    if((condition0 && condition1) || isSanhoUnlocked(index)){
        Node* lock = item->getChildByName("sptLock");
        if(lock != nullptr){
            lock->removeFromParentAndCleanup(true);
        }
        lblDescriptionInBtn->setTextColor(Color4B(255, 241, 127, 255));
        if(!isSanhoUnlocked(index)){
            btn->loadTextureNormal("btnCreate.png");
            btn->loadTexturePressed("btnCreateDown.png");
        }
    }else{
        lblDescriptionInBtn->setTextColor(Color4B(255, 122, 57, 255));
        btn->loadTextureNormal("btnDisabled.png");
        btn->loadTexturePressed("btnDisabled.png");
    }
    
    BigNum* price = getUpgradeSanhoPrice(index, 1);
    btn->setEnabled(theLife->IsBiggerThanThis(price) >= 0);
    lblPrice->setString(price->getExpression());
    GameManager::getInstance()->alignToCenter(icon, lblPrice, 0, btn->getContentSize().width/2, -4);
    delete price;
    delete nextNum;
    delete num;
}
void HelloWorldScene::updateSanhoButtonsEnable(){
    for (int i = 0; i < SANHO_COUNT; i++) {
        updateSanhoItem(i);
        
        Node* item = svSkill->getInnerContainer()->getChildByName(StringUtils::format("sanhoItem%d", i).c_str());
        Button* btn = (Button*)item->getChildByName("btnUpgrade");
        if (!btn->isEnabled()) {
            
            btn->loadTextureNormal("btnDisabled.png");
            btn->loadTexturePressed("btnDisabled.png");
            //                break;
        }else{
            btn->loadTextureNormal("btnCreate.png");
            btn->loadTexturePressed("btnCreateDown.png");
        }
    }
}
BigNum* HelloWorldScene::getSanhoFirstEffect(int index){
    BigNum* num = new BigNum();
    if (index == 0) {
        num->addNum(100, 0);
    }else if (index == 1) {
        num->addNum(1300, 0);
    }else if (index == 2) {
        num->addNum(26, 1);
    }else if (index == 3) {
        num->addNum(860, 1);
    }else if (index == 4) {
        num->addNum(51, 2);
    }else if (index == 5) {
        num->addNum(1, 3);
        num->addNum(400, 2);
    }else if (index == 6) {
        num->addNum(140, 3);
    }else if (index == 7) {
        num->addNum(6, 4);
        num->addNum(900, 3);
    }else if (index == 8) {
        num->addNum(630, 4);
    }else if (index == 9) {
        num->addNum(57, 5);
    }else if (index == 10) {
        num->addNum(4, 6);
        num->addNum(900, 5);
    }else if (index == 11) {
        num->addNum(412, 6);
    }else if (index == 12) {
        num->addNum(69, 7);
    }else if (index == 13) {
        num->addNum(11, 8);
    }
    return num;
}
void HelloWorldScene::applyAnimalEffect(BigNum* num){
    int totalSpawnedAnimalCount = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
    int area = 0;
    int index = 0;
//    animalEffect = 1;
    for (int i = 0; i < totalSpawnedAnimalCount; i++) {
        area = getArea(i);
        if (area == AREA_INVENTORY_AFRICA || area == AREA_AFRICA) {
            index = getAnimalIndexForID(i);
//            animalEffect *= Value(animalTimeEffect.at(index)).asInt()*0.01f;
            num->multiply(Value(animalTimeEffect.at(index)).asInt()*0.01f);
        }else{
//            log("not africa");
        }
    }
//    animalEffect *= 0.01f;
}
BigNum* HelloWorldScene::getSanhoEffect(int index){
    BigNum* num = getSanhoFirstEffect(index);
//    num->multiply(animalEffect + 1);
//    for (int i = 0; i < animalEffect; i++) {
//        num->multiply(2);
//    }
//    num->multiply(1 + ((int)(animalEffect*10)%10)*0.1f);
//    num->multiply(animalEffect);
    applyAnimalEffect(num);
    
    int level = getSanhoLevel(index);
    BigNum* numToAdd = new BigNum();
    numToAdd->addNum(num);
    numToAdd->multiply(0.1f);
    for (int i = 0; i < level; i++) {
//        num->multiply(1.07f);
        num->addNum(numToAdd);
    }
//    num->multiply(getSanhoBonusLevel(index)+1);// bonus sanho
    int sanhoBonus = getSanhoBonusLevel(index);
    float sanhoBonusRate = 1;
    for (int i = 0; i < sanhoBonus; i++) {
        num->multiply(2);
//        sanhoBonusRate *= 2;
    }
    delete numToAdd;
//    num->multiply(sanhoBonusRate);
    return num;
}
void HelloWorldScene::updateSanhoItems(){
    for (int i = 0; i < SANHO_COUNT; i++) {
        updateSanhoItem(i);
    }
}
void HelloWorldScene::updateTotalSanhoEffect(){
    totalSanhoEffectPerSec->resetNum();
    for (int i = 0; i < SANHO_COUNT; i++) {
        if (isSanhoUnlocked(i)) {
            BigNum* bigNum = getSanhoEffect(i);
            totalSanhoEffectPerSec->addNum(bigNum);
            delete bigNum;
        }else{
            break;
        }
    }
    float relicRate = 0;
    for (int i = 0; i < relicEffects.size(); i++) {
        if (isRelicInUse(i)) {
            std::string str = relicEffects.at(i).at(6);
            if(str.size() > 0){
                relicRate += Value(str).asInt()*0.01f;
            }
        }
    }
    
    
    totalSanhoEffectPerSec->multiply(relicRate + 1);
    
    if (isRainingOn) {
        totalSanhoEffectPerSec->multiply(2);
    }
    
    Text* lbl = (Text*)loadedLayer->getChildByName("pnlStatus")->getChildByName("lblTime");
    lbl->setString(totalSanhoEffectPerSec->getExpression());
    log("update sanho: %s", totalSanhoEffectPerSec->getExpression().c_str());
}
void HelloWorldScene::showBigUpgradeButtons(int index){
    Node* item = nullptr;
    btnUpgrade10->retain();
    btnUpgrade100->retain();
    btnUpgrade10->removeFromParentAndCleanup(false);
    btnUpgrade100->removeFromParentAndCleanup(false);
    if(index == 10000){
        item = itemTree;
        loadedLayer->addChild(btnUpgrade10);
        loadedLayer->addChild(btnUpgrade100);
    }else if (index >= 0) {   // sanho
        item = svSkill->getInnerContainer()->getChildByName(StringUtils::format("sanhoItem%d", index).c_str());
        svSkill->getInnerContainer()->addChild(btnUpgrade10);
        svSkill->getInnerContainer()->addChild(btnUpgrade100);
    }
    btnUpgrade10->release();
    btnUpgrade100->release();
    
    
    float dur = 0.2f;
    if (btnUpgrade10->getTag() == index && btnUpgrade10->getOpacity() == 255) {
        btnUpgrade10->stopActionByTag(77);
        Sequence* seq = Sequence::create(DelayTime::create(3), FadeOut::create(dur/2), MoveTo::create(0, Point(-5000, 0)), NULL);
        seq->setTag(77);
        btnUpgrade10->runAction(seq);
        
        btnUpgrade100->stopActionByTag(77);
        seq = Sequence::create(DelayTime::create(3), FadeOut::create(dur/2), MoveTo::create(0, Point(-5000, 0)), NULL);
        seq->setTag(77);
        btnUpgrade100->runAction(seq);
        updateBigUpgradeButtons();
        return;
    }
    btnUpgrade10->setTag(index);
    btnUpgrade100->setTag(index);
    updateBigUpgradeButtons();
    
    btnUpgrade10->setVisible(true);
    btnUpgrade10->stopAllActions();
    btnUpgrade10->setPosition(item->getPosition() + Point(568, -btnUpgrade10->getContentSize().height/2));
    btnUpgrade10->runAction(ScaleTo::create(dur, 0.8f));
    btnUpgrade10->runAction(FadeIn::create(dur/2));
    btnUpgrade10->runAction(MoveBy::create(dur, Point(-45, 0)));
    Sequence* seq =Sequence::create(DelayTime::create(3), FadeOut::create(dur/2), NULL);
    seq->setTag(77);
    btnUpgrade10->runAction(seq);
    
    btnUpgrade100->setVisible(true);
    btnUpgrade100->stopAllActions();
    btnUpgrade100->setPosition(item->getPosition() + Point(410 + 45, -btnUpgrade100->getContentSize().height/2));
    btnUpgrade100->runAction(ScaleTo::create(dur, 0.8f));
    btnUpgrade100->runAction(FadeIn::create(dur/2));
    btnUpgrade100->runAction(MoveBy::create(dur, Point(-45, 0)));
    seq = Sequence::create(DelayTime::create(3), FadeOut::create(dur/2), NULL);
    seq->setTag(77);
    btnUpgrade100->runAction(seq);
}
void HelloWorldScene::onUpgrade10Click(){
    int index = btnUpgrade10->getTag();
    if(index == 10000){
        BigNum* price = getUpgradeTapRewardPrice(getMainTreeLevel() + 10);
        if (theLife->IsBiggerThanThis(price) >= 0) {
            theLife->subtractNum(price);
            upgradeTapReward(10);
            addQuestProgress(2, 10);
            showBigUpgradeButtons(index);
            handleTheLifeChanged();
        }
        delete price;
    }else if (index >= 0) {   // sanho
        // price
        BigNum* price = getUpgradeSanhoPrice(index, 10);
        if (theLife->IsBiggerThanThis(price) >= 0) {
            theLife->subtractNum(price);
            
            addQuestProgress(2, 10);
            
            upgradeSanho(index, 10);
//            isTouchedForLife = true;
            updateSanhoItem(index);
            updateSanhoItem(index+1);
            showBigUpgradeButtons(index);
            handleTheLifeChanged();
        }else{
            // not enough life
        }
        delete price;
    }
    updateSanhoImages();
}
void HelloWorldScene::addQuestProgress(int index, int count){
    int growCounter = getQuestProgress(index);
    if (growCounter < getQuestMaxCount(index)) {
        growCounter += count;
        if (getQuestMaxCount(index) < growCounter) {
            growCounter = getQuestMaxCount(index);
        }
        setQuestProgress(index, growCounter);
        if (questLayer != nullptr) {
            updateQuest(0);
        }
    }
}
void HelloWorldScene::onUpgrade100Click(){
    int index = btnUpgrade10->getTag();
    if(index == 10000){
        BigNum* price = getUpgradeTapRewardPrice(getMainTreeLevel() + 100);
        if (theLife->IsBiggerThanThis(price) >= 0) {
            upgradeTapReward(100);
            theLife->subtractNum(price);
            addQuestProgress(2, 100);
            showBigUpgradeButtons(index);
            handleTheLifeChanged();
        }
        delete price;
    }else if (index >= 0) {   // sanho
        // price
        BigNum* price = getUpgradeSanhoPrice(index, 100);
        if (theLife->IsBiggerThanThis(price) >= 0) {
            theLife->subtractNum(price);
            upgradeSanho(index, 100);
            addQuestProgress(2, 100);
//            isTouchedForLife = true;
            updateSanhoItem(index);
            updateSanhoItem(index+1);
            showBigUpgradeButtons(index);
            handleTheLifeChanged();
        }else{
            // not enough life
        }
        delete price;
    }
    updateSanhoImages();
}
void HelloWorldScene::updateBigUpgradeButtons(){
    int index = btnUpgrade10->getTag();
    if(index == 10000){
        // 10
        Text* lblDescription = (Text*)btnUpgrade10->getChildByName("lblDescription");
        Text* lblPrice = (Text*)btnUpgrade10->getChildByName("lblPrice");
        BigNum* price = getUpgradeTapRewardPrice(getMainTreeLevel() + 10);
        btnUpgrade10->setEnabled(theLife->IsBiggerThanThis(price) >= 0);
        lblPrice->setString(price->getExpression());
        GameManager::getInstance()->alignToCenter(btnUpgrade10->getChildByName("sptIcon"), lblPrice, 0, btnUpgrade10->getContentSize().width/2, -4);
        btnUpgrade10->setEnabled(theLife->IsBiggerThanThis(price) >= 0);
        // effect
        BigNum* effectNum = getTapReward(getMainTreeLevel() + 10);
        effectNum->subtractNum(getTapReward());
        lblDescription->setString(StringUtils::format("+%s", effectNum->getExpression().c_str()));
        
        // 100
        lblDescription = (Text*)btnUpgrade100->getChildByName("lblDescription");
        lblPrice = (Text*)btnUpgrade100->getChildByName("lblPrice");
        delete price;
        price = getUpgradeTapRewardPrice(getMainTreeLevel() + 100);
        lblPrice->setString(price->getExpression());
        GameManager::getInstance()->alignToCenter(btnUpgrade100->getChildByName("sptIcon"), lblPrice, 0, btnUpgrade100->getContentSize().width/2, -4);
        btnUpgrade100->setEnabled(theLife->IsBiggerThanThis(price) >= 0);
        // effect
        effectNum = getTapReward(getMainTreeLevel() + 100);
        effectNum->subtractNum(getTapReward());
        lblDescription->setString(StringUtils::format("+%s", effectNum->getExpression().c_str()));
        delete price;
    }else if (index >= 0) {   // sanho
        int level = getSanhoLevel(index);
        
        Text* lblDescription = (Text*)btnUpgrade10->getChildByName("lblDescription");
        Text* lblPrice = (Text*)btnUpgrade10->getChildByName("lblPrice");
        // price
        BigNum* price = getUpgradeSanhoPrice(index, 10);
        lblPrice->setString(price->getExpression());
        GameManager::getInstance()->alignToCenter(btnUpgrade10->getChildByName("sptIcon"), lblPrice, 0, btnUpgrade10->getContentSize().width/2, -4);
        btnUpgrade10->setEnabled(theLife->IsBiggerThanThis(price) >= 0);
        
        // effect 10
        BigNum* effectNum = getSanhoFirstEffect(index);
        BigNum* effectToAdd = new BigNum();
        effectToAdd->addNum(effectNum);
        effectToAdd->multiply(0.1f);
        for (int i = 0; i < level + 10; i++) {
            effectNum->addNum(effectToAdd);
        }
        lblDescription->setString(StringUtils::format("+%s", effectNum->getExpression().c_str()));
        
        
        // effect 100
        lblDescription = (Text*)btnUpgrade100->getChildByName("lblDescription");
        lblPrice = (Text*)btnUpgrade100->getChildByName("lblPrice");
        for (int i = 0; i < 90; i++) {
            effectNum->addNum(effectToAdd);
        }
        lblDescription->setString(StringUtils::format("+%s", effectNum->getExpression().c_str()));
        price = getUpgradeSanhoPrice(index, 100);
        btnUpgrade100->setEnabled(theLife->IsBiggerThanThis(price) >= 0);
        lblPrice->setString(price->getExpression());
        GameManager::getInstance()->alignToCenter(btnUpgrade100->getChildByName("sptIcon"), lblPrice, 0, btnUpgrade100->getContentSize().width/2, -4);
        delete price;
    }
}

void HelloWorldScene::handleTheLifeChanged(){
    updateMainTreeItem();
    updateSanhoButtonsEnable();
//    oneSecTimeElapse = -1;
}
int HelloWorldScene::gochaRelic(){
    int random = rand()%1000;
    int index = 0;
    int rate = 0;
    int totalRate = 0;
    bool picked = false;
    while(true){
        rate = atof(relicEffects.at(index).at(4).c_str())*10;
        totalRate += rate;
        if(totalRate >= random){
            bool haveIt = UserDefault::getInstance()->getBoolForKey(StringUtils::format(KEY_RELIC_HAVE_FORMAT, index).c_str(), false);
            if(haveIt){
//                index = 0;
//                totalRate = 0;
//                random = rand()%1000;
//                continue;
                break;
            }else{
                UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_RELIC_HAVE_FORMAT, index).c_str(), true);
                picked = true;
                break;
            }
        }
        index++;
        if (index >= relicEffects.size()) {
//            index = 0;
            break;
        }
    }
    if (!picked) {
        index = getNextAvailableRelicIndex();
        UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_RELIC_HAVE_FORMAT, index).c_str(), true);
    }
    return index;
}
int HelloWorldScene::getNextAvailableRelicIndex(){
    int index = 0;
    while(true){
        bool haveIt = UserDefault::getInstance()->getBoolForKey(StringUtils::format(KEY_RELIC_HAVE_FORMAT, index).c_str(), false);
        if(haveIt){
            
        }else{
            return index;
        }
        index++;
        if (index >= relicEffects.size()) {
            return -1;
        }
    }
    return -1;
}
bool HelloWorldScene::isThereRelicLeftToPick(){
    int index = (int)relicEffects.size() - 1;
    while(index >= 0){
        if(!isRelicUnlocked(index)){
            return true;
        }
        index--;
    }
    return false;
}
void HelloWorldScene::loadAnimalPriceCsv(){
    std::string fileName = "animalPrice.csv";
    std::string csvStr = FileUtils::getInstance()->getStringFromFile(fileName);
    ValueVector rows = GameManager::getInstance()->split(csvStr, "\r\n");
    std::string str;
    for (int i = 1; i < (int)rows.size(); i ++) {
        ValueMap userParams;
        std::string strRow = rows.at(i).asString();
        ValueVector params = GameManager::getInstance()->split(rows.at(i).asString(), ",");
        if(params.at(0).asString().size() == 0){
            break;
        }
        for (int j = 0; j < (int)params.size(); j ++) {
            if (j == 2) {
                animalPrices.push_back(params.at(j).asString());
            }else if(j == 6){
                animalRarePrices.push_back(params.at(j).asString());
            }else if(j == 7){
                relicPrices.push_back(params.at(j).asString());
            }
        }
    }
}
void HelloWorldScene::loadRelicCSV(){
    std::string fileName = "relic.csv";
    std::string csvStr = FileUtils::getInstance()->getStringFromFile(fileName);
    ValueVector rows = GameManager::getInstance()->split(csvStr, "\r\n");
    relicEffects.clear();
    std::string str;
    for (int i = 2; i < (int)rows.size(); i ++) {
        ValueMap userParams;
        std::string strRow = rows.at(i).asString();
        ValueVector params = GameManager::getInstance()->split(rows.at(i).asString(), ",");
        if(params.at(0).asString().size() == 0){
            break;
        }
        log("%s.", params.at(0).asString().c_str());
        std::vector<std::string> list;
        for (int j = 0; j < (int)params.size(); j ++) {
            list.push_back(params.at(j).asString());
            log("  %s", params.at(j).asString().c_str());
        }
        relicEffects.push_back(list);
    }
}
void HelloWorldScene::loadAnimalCSV(){
    std::string fileName = "animals.csv";
    std::string csvStr = FileUtils::getInstance()->getStringFromFile(fileName);
    ValueVector rows = GameManager::getInstance()->split(csvStr, "\r\n");
    
    std::string str;
    for (int i = 3; i < (int)rows.size(); i ++) {
        ValueMap userParams;
        std::string strRow = rows.at(i).asString();
        log("%d. %s", i, strRow.c_str());
        ValueVector params = GameManager::getInstance()->split(rows.at(i).asString(), ",");
        for (int j = 0; j < (int)params.size(); j ++) {
            if (j == 0) {
                animalIndices.push_back(params.at(j).asString());
            }else if(j == 1){
                animalRank.push_back(params.at(j).asString());
            }else if(j == 3){
                animalNames.push_back(params.at(j).asString());
            }else if(j == 5){
                animalZoologicalNames.push_back(params.at(j).asString());
            }else if(j == 7){
                animalLand.push_back(params.at(j).asString());
            }else if(j == 8){
                animalTouchEffect.push_back(params.at(j).asString());
            }else if(j == 9){
                animalTimeEffect.push_back(params.at(j).asString());
            }else if(j == 11){
                animalRelicPoint.push_back(params.at(j).asString());
            }else if(j == 12){
                animalUnlockConditionTree.push_back(params.at(j).asString());
            }else if(j == 13){
                animalUnlockConditionAnimalCount.push_back(params.at(j).asString());
            }else if(j == 15){
                animalScale.push_back(params.at(j).asString());
            }else if(j == 16){
                animalAtlas.push_back(params.at(j).asString());
            }else if(j == 18){
                animalJson.push_back(params.at(j).asString());
            }else if(j == 19){
                animalSound1.push_back(params.at(j).asString());
            }else if(j == 20){
                animalSound2.push_back(params.at(j).asString());
            }
        }
    }
}
void HelloWorldScene::updateVisibleItemsOnAnimalTap(){
    return;
    int itemGapY = 6;
    int itemHeight = 104;
    std::vector<int> visibleLockedItems;
    std::vector<int> unlockedItems;
    ImageView* item;
    int showingLockedCount = 3;
    for (int i = 0; i < animalNames.size(); i++) {
        item = (ImageView*) svCharacter->getInnerContainer()->getChildByName(StringUtils::format("item%d", i).c_str());
        item->setVisible(true);
        if (isAnimalUnlocked(i, true)) {
            unlockedItems.push_back(i);
        }else if(showingLockedCount > 0){
            showingLockedCount--;
            visibleLockedItems.push_back(i);
        }else{
            item->setVisible(false);
        }
    }
    
    svCharacter->setInnerContainerSize(Size(750, (unlockedItems.size() + visibleLockedItems.size())*(itemGapY + itemHeight)));
    bool isUnlocked;
    Node* sptIcon;
    Button* btn;
    ImageView* iconInBtn;
    Text* lblDescription;
    Text* lblPrice;
    Text* lblCount;
    updateNextAnimalPrice();
    int index;
    for (int i = 0; i < unlockedItems.size() + visibleLockedItems.size(); i++) {
        if (i <unlockedItems.size()) {
            index = unlockedItems.at(i);
        }else{
            index = visibleLockedItems.at(i - unlockedItems.size());
        }
        item = (ImageView*) svCharacter->getInnerContainer()->getChildByName(StringUtils::format("item%d", index).c_str());
        item->setPosition(Point(12, svCharacter->getInnerContainerSize().height - i*(itemGapY + itemHeight)));
        
        sptIcon = item->getChildByName("sptIcon");
        isUnlocked = isAnimalUnlocked(index, false);
        sptIcon->setOpacity(isUnlocked?255:100);
        
        btn = (Button*)item->getChildByName("btnUpgrade");
        iconInBtn = (ImageView*)btn->getChildByName("sptIcon");
        lblDescription = (Text*)btn->getChildByName("lblDescription");
        lblPrice = (Text*)btn->getChildByName("lblPrice");
        lblCount = (Text*)item->getChildByName("lblCount");
        lblCount->setVisible(false);
        lblCount->setPositionY(75);
        lblCount->setLocalZOrder(1);
//        btn->setEnabled(isUnlocked);
        if (isUnlocked) {
            lblPrice->setPosition(Point(btn->getContentSize().width/2, 41));
            lblPrice->setString(nextAnimalPrice->getExpression());
            lblDescription->setVisible(true);;
            iconInBtn->setPositionY(31);
            GameManager::getInstance()->alignToCenter(iconInBtn, lblPrice, 0, btn->getContentSize().width/2, -4);
            lblPrice->setPositionY(41);
            
            int count = getAnimalCountInArea(selectedArea, index);//UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ANIMAL_COUNT_FORMAT, index).c_str(), 0);
//            int inven = UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ANIMAL_COUNT_INVENTORY_FORMAT, index).c_str(), 0);
            int totalAnimalCount = getTotalAnimalCount(index);
            lblCount->setString(StringUtils::format("%d/%d", count, totalAnimalCount));
            lblCount->setVisible(count > 0);
            iconInBtn->loadTexture("icon_leaf_white.png");
        }else{
            lblPrice->setPosition(Point(btn->getContentSize().width/2, 41));
            int forceUnlockPrice = 997;
            lblPrice->setString(Value(forceUnlockPrice).asString());
            GameManager::getInstance()->setLocalizedString("unlock under condition", lblDescription);
//            lblDescription->setVisible(false);;
            iconInBtn->loadTexture("icon_dia_white.png");
//            iconInBtn->setPosition(lblDescription->getPosition());
        }
    }
}
void HelloWorldScene::updateVisibleItemsOnSanhoTap(){
    int itemGapY = 6;
    int itemHeight = 104;
    std::vector<int> visibleLockedItems;
    std::vector<int> unlockedItems;
    ImageView* item;
    int showingLockedCount = 3;
    for (int i = 0; i < SANHO_COUNT; i++) {
        item = (ImageView*) svSkill->getInnerContainer()->getChildByName(StringUtils::format("sanhoItem%d", i).c_str());
        item->setVisible(true);
        if (isSanhoUnlocked(i)) {
            unlockedItems.push_back(i);
        }else if(showingLockedCount > 0){
            showingLockedCount--;
            visibleLockedItems.push_back(i);
        }else{
            item->setVisible(false);
        }
    }
    
    svSkill->setInnerContainerSize(Size(750, (unlockedItems.size() + visibleLockedItems.size())*(itemGapY + itemHeight)));
    bool isUnlocked;
    Node* sptIcon;
    Button* btn;
    ImageView* iconInBtn;
    Text* lblDescription;
    Text* lblPrice;
    Text* lblCount;
    int index;
    for (int i = 0; i < unlockedItems.size() + visibleLockedItems.size(); i++) {
        if (i <unlockedItems.size()) {
            index = unlockedItems.at(i);
        }else{
            index = visibleLockedItems.at(i - unlockedItems.size());
        }
        item = (ImageView*) svSkill->getInnerContainer()->getChildByName(StringUtils::format("sanhoItem%d", index).c_str());
        item->setPosition(Point(12, svSkill->getInnerContainerSize().height - i*(itemGapY + itemHeight)));
        
        sptIcon = item->getChildByName("sptIcon");
        isUnlocked = isSanhoUnlocked(index);
        sptIcon->setOpacity(isUnlocked?255:100);
        
    }
}
int HelloWorldScene::getTotalSurvivalPower(){
    int totalSurvivalPower = 0;
    for (int i = 0; i < animalNames.size(); i++) {
        if (getAccumulatedAnimalCount(i) > 0 && !isHunted(i)) {
            totalSurvivalPower += getSurvivalPower(i);
        }
    }
    return totalSurvivalPower;
}
void HelloWorldScene::updateAnimalTapDescription(){
    int totalAnimalCount = getTotalAnimalCountThatIHave();// getTotalAnimalCountInArea(selectedArea);
    Text* lblAnimalCount = (Text*)itemCharacter->getChildByName("lblAnimalCount");
    lblAnimalCount->setString(StringUtils::format("%s %d/%d", GameManager::getInstance()->getText("total animal").c_str(), totalAnimalCount, getMaxAnimalPossessCount()));
    int totalSurvivalPower = getTotalSurvivalPower();
    Text* lblSurvivalPower = (Text*)itemCharacter->getChildByName("lblSurvivalPower");
    lblSurvivalPower->setString(StringUtils::format("%s %d", GameManager::getInstance()->getText("total survival power").c_str(), totalSurvivalPower));
    
    Text* lblPercent = (Text*)itemCharacter->getChildByName("lblPercent");
    lblPercent->setString(StringUtils::format("%d/%d", totalAnimalCount, getMaxAnimalPossessCount()));
    LoadingBar* lbProgress = (LoadingBar*)itemCharacter->getChildByName("lbProgress");
    lbProgress->setPercent(totalAnimalCount*100.0f/getMaxAnimalPossessCount());
    updateAnimalPickTab();
    return;
    
    
    Text* lblDescription, *lblDescription_0, *lblDescriptionInBtn;
    Button* btn;
    ImageView* sptIcon = (ImageView*)itemCharacter->getChildByName("sptIcon");
    sptIcon->loadTexture(getAreaIconName(selectedArea));
    lblDescription = (Text*)itemCharacter->getChildByName("lblAnimalCount");
    totalAnimalCount = getTotalAnimalCountInArea(selectedArea);
    lblDescription->setString(StringUtils::format("%d/%d", totalAnimalCount, getMaxAnimalPossessCount()));
    GameManager::getInstance()->setLocalizedString("manage", (Text*)itemCharacter->getChildByName("btnManageAnimal")->getChildByName("lblDescription"));
    lbProgress = (LoadingBar*)itemCharacter->getChildByName("lbProgress");
    lbProgress->setPercent(totalAnimalCount*100.0f/getMaxAnimalPossessCount());
    lblDescription = (Text*)itemCharacter->getChildByName("lblPercent");
    lblDescription->setString(StringUtils::format("%0.1f%%", lbProgress->getPercent()));
    
    Node* rightBox =itemCharacter->getChildByName("itemAnimal_0");
    lblDescription = (Text*)rightBox->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString("rare species", lblDescription);
    lblDescription = (Text*)rightBox->getChildByName("lblDescription_0");
    GameManager::getInstance()->setLocalizedString("rare description", lblDescription);
    lblDescription = (Text*)rightBox->getChildByName("btnUpgrade")->getChildByName("lblDescription");
    GameManager::getInstance()->setLocalizedString("pick rare", lblDescription);
    
    for (int i = 0; i < animalNames.size(); i++) {
        ImageView* item = (ImageView*)svCharacter->getInnerContainer()->getChildByName(StringUtils::format("item%d", i).c_str());
//        item->getChildByName("timeIcon")->setVisible(false);
        item->getChildByName("lblDescription_0")->setVisible(false);
        ImageView* sptCheck0 = (ImageView*)item->getChildByName("sptCheck0");
        ImageView* sptCheck1 = (ImageView*)item->getChildByName("sptCheck1");
        Text* lblCondition0 = (Text*)item->getChildByName("lblCondition0");
        Text* lblCondition1 = (Text*)item->getChildByName("lblCondition1");
        sptIcon = (ImageView*)item->getChildByName("sptIcon");
        btn = (Button*)item->getChildByName("btnUpgrade");
        
        bool condition0 = isAnimalCondition0Meet(i);
        bool condition1 = isAnimalCondition1Meet(i);
        
        sptCheck0->loadTexture(condition0?"sptChecked.png":"sptUnchecked.png");
        sptCheck1->loadTexture(condition1?"sptChecked.png":"sptUnchecked.png");
        lblCondition0->setString(getAnimalCondition0(i).c_str());
        lblCondition1->setString(getAnimalCondition1(i).c_str());
        if(lblCondition1->getString().size() <= 0){
            sptCheck1->setVisible(false);
        }
        lblDescription = (Text*)item->getChildByName("lblDescription");
        lblDescription->setString(GameManager::getInstance()->getText(animalNames.at(i).c_str()));
        lblDescription_0 = (Text*)item->getChildByName("lblDescription_0");
        int animalCount = getAnimalCountInArea(selectedArea, i);
        lblDescription_0->setString(StringUtils::format("%d/%d", animalCount, getTotalAnimalCount(i)));
        item->getChildByName("animalCountBg")->setVisible(animalCount > 0);
        bool forceUnlocked = !(condition0 && condition1) && getTotalAnimalCount(i) > 0;
        if(getTotalAnimalCount(i) > 0 || (condition0 && condition1) || forceUnlocked){
            item->loadTexture("ui_list_bg.png");
            sptIcon->setOpacity(255);
            /*if(sptCheck0 != nullptr){
                sptCheck0->removeFromParentAndCleanup(true);
            }
            if(sptCheck1 != nullptr){
                sptCheck1->removeFromParentAndCleanup(true);
            }
            if(lblCondition0 != nullptr){
                lblCondition0->removeFromParentAndCleanup(true);
            }
            if(lblCondition1 != nullptr){
                lblCondition1->removeFromParentAndCleanup(true);
            }*/
//            item->getChildByName("timeIcon")->setVisible(true);
//            item->getChildByName("lblDescription_0")->setVisible(true);
            lblDescription->setTextColor(Color4B(94, 93, 73, 255));
            lblCondition0->setTextColor(Color4B(140, 137, 103, 255));
            lblCondition1->setTextColor(Color4B(140, 137, 103, 255));
            btn->loadTextureNormal("btnUpgrade.png");
            btn->loadTexturePressed("btnUpgradeDown.png");
//            lblDescription->setString(StringUtils::format("%s%d %s", GameManager::getInstance()->getText("level").c_str(), level + 1, GameManager::getInstance()->getText(StringUtils::format("plant%d", index + 1).c_str()).c_str()).c_str());
        }else{
//            item->getChildByName("timeIcon")->setVisible(false);
//            item->getChildByName("lblDescription_0")->setVisible(false);
            item->loadTexture("ui_list_bg_locked.png");
            sptIcon->setOpacity(100);
//            sptCheck0->loadTexture(condition0?"sptChecked.png":"sptUnchecked.png");
//            sptCheck1->loadTexture(condition1?"sptChecked.png":"sptUnchecked.png");
//            lblCondition0->setString(StringUtils::format("%s Lv.%d", GameManager::getInstance()->getText(StringUtils::format("plant%d", index).c_str()).c_str(), 100).c_str());
//            lblCondition1->setString(StringUtils::format("%s Lv.%d", GameManager::getInstance()->getText("tree of life").c_str(), (index + 1)*25).c_str());
            lblDescription->setTextColor(Color4B(250, 248, 214, 255));
            lblCondition0->setTextColor(Color4B(219, 210, 174, 255));
            lblCondition1->setTextColor(Color4B(219, 210, 174, 255));
//            lblDescription->setString(StringUtils::format("%s", GameManager::getInstance()->getText(StringUtils::format("plant%d", index + 1).c_str()).c_str()).c_str());
        }
        lblDescriptionInBtn = (Text*)btn->getChildByName("lblDescription");
        if((condition0 && condition1) || forceUnlocked){
            Node* lock = item->getChildByName("sptLock");
            if(lock != nullptr){
                lock->removeFromParentAndCleanup(true);
            }
            lblDescriptionInBtn->setTextColor(Color4B(255, 241, 127, 255));
            if(theLife->IsBiggerThanThis(nextAnimalPrice) >= 0 ){
                btn->loadTextureNormal("btnCreate.png");
                btn->loadTexturePressed("btnCreateDown.png");
            }else{
                btn->loadTextureNormal("btnDisabled.png");
                btn->loadTexturePressed("btnDisabled.png");
            }
            GameManager::getInstance()->setLocalizedString("create", lblDescriptionInBtn);
        }else{
            lblDescriptionInBtn->setTextColor(Color4B(255, 122, 57, 255));
            btn->loadTextureNormal("btnDisabled.png");
            btn->loadTexturePressed("btnDisabled.png");
            GameManager::getInstance()->setLocalizedString("unlock under condition", lblDescriptionInBtn);
        }
        
        // bought count
    }
//    
//    bool isUnlocked;
//    Button* btn;
//    for (int i = 0; i < animalNames.size(); i++) {
//        Node* item = svCharacter->getInnerContainer()->getChildByName(StringUtils::format("item%d", i).c_str());
//        isUnlocked = isAnimalUnlocked(i, false);
//        btn = (Button*)item->getChildByName("btnUpgrade");
//        if(theLife->IsBiggerThanThis(nextAnimalPrice) >= 0 && isUnlocked){
//            if (isUnlocked){
//                btn->loadTextureNormal("btnUpgrade.png");
//                btn->loadTexturePressed("btnUpgrade.png");
//            }else{
//                btn->loadTextureNormal("btnUnlock.png");
//                btn->loadTexturePressed("btnUnlock.png");
//            }
//        }else{
//            btn->loadTextureNormal("btnDisabled.png");
//            btn->loadTexturePressed("btnDisabled.png");
//        }
//    }
//    
}
bool HelloWorldScene::isAnimalCondition0Meet(int index){
    int mainTreeLevel = 0;
    std::string str = animalUnlockConditionTree.at(index);
    if(str.size() > 0){
        mainTreeLevel = Value(str).asInt();
    }
    int level = getMainTreeLevel();
    return mainTreeLevel == 0 || level >= mainTreeLevel;
}
bool HelloWorldScene::isAnimalCondition1Meet(int index){
    int needAnimalCount = -1;
    std::string str = animalUnlockConditionAnimalCount.at(index);
    if(str.size() > 0){
        needAnimalCount = Value(str).asInt();
    }
    if(needAnimalCount == -1){
        if(index == 5){
            return isSanhoUnlocked(2);
        }else if(index == 11){
            return isSanhoUnlocked(5);
        }else if(index == 13){
            return getSanhoLevel(2) >= 199;
        }else if(index == 18){
            return isSanhoUnlocked(7);
        }else if(index == 27){
            return isSanhoUnlocked(8);
        }
    }else{
        int animalCount = getTotalAnimalCountInArea(selectedArea);
        return needAnimalCount <= animalCount;
    }
    return true;
}
std::string HelloWorldScene::getAnimalCondition0(int index){
    int mainTreeLevel = 0;
    std::string str = animalUnlockConditionTree.at(index);
    if(str.size() > 0){
        mainTreeLevel = Value(str).asInt();
    }
    return StringUtils::format("%s %s %d", GameManager::getInstance()->getText("tree of life").c_str(), GameManager::getInstance()->getText("level").c_str(), mainTreeLevel);
}
std::string HelloWorldScene::getAnimalCondition1(int index){
    int needAnimalCount = -1;
    std::string str = animalUnlockConditionAnimalCount.at(index);
    if(str.size() > 0){
        needAnimalCount = Value(str).asInt();
    }
    if(needAnimalCount == -1){
        if(index == 5){
            return StringUtils::format("%s %s", GameManager::getInstance()->getText(StringUtils::format("plant%d", 3).c_str()).c_str(), GameManager::getInstance()->getText("needed").c_str());
        }else if(index == 11){
            return StringUtils::format("%s %s", GameManager::getInstance()->getText(StringUtils::format("plant%d", 6).c_str()).c_str(), GameManager::getInstance()->getText("needed").c_str());
        }else if(index == 13){
            return StringUtils::format("%s %s %d", GameManager::getInstance()->getText(StringUtils::format("plant%d", 3).c_str()).c_str(), GameManager::getInstance()->getText("level").c_str(), 199);
        }else if(index == 18){
            return StringUtils::format("%s %s", GameManager::getInstance()->getText(StringUtils::format("plant%d", 8).c_str()).c_str(), GameManager::getInstance()->getText("needed").c_str());
        }else if(index == 27){
            return StringUtils::format("%s %s", GameManager::getInstance()->getText(StringUtils::format("plant%d", 9).c_str()).c_str(), GameManager::getInstance()->getText("needed").c_str());
        }
    }else{
        return StringUtils::format(GameManager::getInstance()->getText("need animal format").c_str(), needAnimalCount);
    }
    return "";
}

bool HelloWorldScene::isAnimalUnlocked(int index, bool update){
    if (!update) {
        return UserDefault::getInstance()->getBoolForKey(StringUtils::format(KEY_UNLOCKED_ANIMAL_FORMAT, getAnimalIndex(index)).c_str());
    }
    if (animalRank.at(index).compare("normal") == 0) {
        int level = getMainTreeLevel();
        bool condition0 = level >= atoi(animalUnlockConditionTree.at(index).c_str());
        bool condition1 = true;
        if (animalUnlockConditionAnimalCount.size() > 0) {
            int unlockedCount = 0;
            for (int i = 0; i < animalNames.size(); i++) {
                if (isAnimalUnlocked(i, false)) {
                    unlockedCount++;
                }
            }
            condition1 = unlockedCount >= atoi(animalUnlockConditionAnimalCount.at(index).c_str());
        }
        bool condition2 = true;
        //check condition 2
        
        bool isUnlocked = condition0 && condition1 && condition2;
        UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_UNLOCKED_ANIMAL_FORMAT, getAnimalIndex(index)).c_str(), isUnlocked);
        return isUnlocked;
    }else{
        return false;
    }
}
//void HelloWorldScene::updateAnimalEffect(){
//    float tapRate = 1;
//    float timeRate = 1;
//    for (int i = 0; i < animalNames.size(); i++) {
//        int count = getAnimalCountInArea(selectedArea, i);
////        int inven = UserDefault::getInstance()->getIntegerForKey(StringUtils::format(KEY_ANIMAL_COUNT_INVENTORY_FORMAT, i).c_str(), 0);
//        for (int j = 0; j < count; j++) {
//            tapRate += Value(animalTouchEffect.at(i)).asInt()*0.01f;
//            timeRate += Value(animalTimeEffect.at(i)).asInt()*0.01f;
//        }
//    }
//    tapEffectFromAnimal = tapRate;
//    timeEffectFromAnimal = timeRate;
//    updateTapReward();
//    updateTotalSanhoEffect();
//}
void HelloWorldScene::shakeSanho(){
    float duration = 0.1f;
    for (int i = 0; i < shakingList.size(); i++) {
        Node* leaf = shakingList.at(i);
        leaf->stopAllActions();
        duration = 0.3f + (rand()%4)*0.1f;
        leaf->runAction(Sequence::create(EaseOut::create(RotateTo::create(duration, rand()%60 - 30), 2), EaseInOut::create(RotateTo::create(duration*2, 0), 2), NULL));
    }
    for (int i = 0; i < silukList.size(); i++) {
        Node* leaf = silukList.at(i);
        GameManager::getInstance()->makeItSilukOnce(leaf, duration, leaf->getScale());
    }
}
bool HelloWorldScene::isRelicUnlocked(int index){
    return UserDefault::getInstance()->getBoolForKey(StringUtils::format(KEY_RELIC_HAVE_FORMAT, index).c_str(), false);
}
void HelloWorldScene::unlockRelic(int index){
    UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_RELIC_HAVE_FORMAT, index).c_str(), true);
    resetRelic();
}
long HelloWorldScene::getRelicReadyStartedTime(int index){
    return (long)Value(UserDefault::getInstance()->getStringForKey(StringUtils::format(KEY_RELIC_READY_START_TIME_FORMAT, index).c_str())).asDouble();
}
bool HelloWorldScene::isRelicInUse(int index){
    return UserDefault::getInstance()->getBoolForKey(StringUtils::format(KEY_RELIC_IN_USE_FORMAT, index).c_str(), false);
}
void HelloWorldScene::unequipRelic(int index){
    UserDefault::getInstance()->setStringForKey(StringUtils::format(KEY_RELIC_READY_START_TIME_FORMAT, index).c_str(), Value((double)TimeManager::getInstance()->getCurrentTime()).asString());
    UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_RELIC_IN_USE_FORMAT, index).c_str(), false);

    updateMainTreeItem();
    updateTapReward();
    updateTotalSanhoEffect();
}
void HelloWorldScene::equipRelic(int index){
    UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_RELIC_IN_USE_FORMAT, index).c_str(), true);
    updateMainTreeItem();
    updateTapReward();
    updateTotalSanhoEffect();
}
long HelloWorldScene::getRelicReadyTime(){
    return 120;
}

std::string HelloWorldScene::getRelicEffect0(int index){
    int rateTap = Value(relicEffects.at(index).at(5)).asInt();
    int rateAuto = Value(relicEffects.at(index).at(6)).asInt();;
//    int effectIndex = 0;
//    for (int j = 5; j < 16;j++){
//        std::string str = relicEffects.at(index).at(j);
//        if(str.size() > 0){
//            rate = Value(str).asInt();
//            break;
//        }
//    }
    
    return StringUtils::format(GameManager::getInstance()->getText("relic effect format").c_str(), rateTap, rateAuto).c_str();
}
std::string HelloWorldScene::getRelicEffect1(int index){
    int rate = 0;
    int effectIndex = -1;
    for (int j = 7; j < 11;j++){
        std::string str = relicEffects.at(index).at(j);
        if(str.size() > 0){
            str = relicEffects.at(index).at(j);
            effectIndex = j - 5;
            rate = Value(str).asInt();
            break;
        }
    }
    if (effectIndex < 0) {
        return "";
    }
    return StringUtils::format(GameManager::getInstance()->getText(StringUtils::format("relic effect format%d", effectIndex).c_str()).c_str(), rate);
}
void HelloWorldScene::updateRelic(){
    int count = 0;
    ImageView* item, *sptIcon;
    Button* btn;
    Text* lblDescription, *lblDescription_0, *lblPrice;
    long startedTime;
    for (int i = 0; i < relicEffects.size(); i++) {
        if (!isRelicUnlocked(i)) {
            continue;
        }
        item = (ImageView*)svRelic->getChildByName(StringUtils::format("item%d", count).c_str());
        lblDescription = (Text*)item->getChildByName("lblDescription");
        GameManager::getInstance()->setLocalizedString(StringUtils::format("relic_%d", i).c_str(), lblDescription);
        lblDescription_0 = (Text*)item->getChildByName("lblDescription_0");
        lblDescription_0->setString( StringUtils::format("%s\n%s", getRelicEffect0(i).c_str(), getRelicEffect1(i).c_str()));
        btn = (Button*)item->getChildByName("btnUpgrade");
        startedTime = getRelicReadyStartedTime(i);
        bool isGettingReady = startedTime > 0;
        if (isGettingReady) {
            lblDescription->setTextColor(Color4B(250, 248, 214, 255));
            lblDescription_0->setTextColor(Color4B(206, 202, 169, 255));
            item->loadTexture("ui_list_bg_locked.png");
        }else{
            lblDescription->setTextColor(Color4B(94, 93, 73, 255));
            lblDescription_0->setTextColor(Color4B(142, 137, 103, 255));
            item->loadTexture("ui_list_bg.png");
    
        }

        lblDescription = (Text*)btn->getChildByName("lblDescription");;
        lblPrice = (Text*)btn->getChildByName("lblPrice");;
        sptIcon = (ImageView*)btn->getChildByName("sptIcon");
        sptIcon->setVisible(false);
        lblPrice->setVisible(true);
        if (isRelicInUse(i)) {
            lblDescription->setPositionY(58);
            GameManager::getInstance()->setLocalizedString("equipped", lblDescription);
            GameManager::getInstance()->setLocalizedString("unequip", lblPrice);
            lblPrice->setPositionX(btn->getContentSize().width/2);
            btn->loadTextureNormal("btnSelected");
            btn->loadTexturePressed("btnSelected");
        }else{
            lblPrice->setVisible(false);
            if (isGettingReady) {
                GameManager::getInstance()->setLocalizedString("getting ready", lblDescription);
                sptIcon->setVisible(true);
                lblDescription->setPositionY(58);
                long timeLeft = startedTime + getRelicReadyTime() - TimeManager::getInstance()->getCurrentTime();
                if(timeLeft < 0){
                    UserDefault::getInstance()->setStringForKey(StringUtils::format(KEY_RELIC_READY_START_TIME_FORMAT, i).c_str(), "-1");
                    i--;
                    continue;
                }
                lblPrice->setString(GameManager::getInstance()->getTimeLeftInString(timeLeft));
                lblPrice->setVisible(true);
                GameManager::getInstance()->alignToCenter(sptIcon, lblPrice, 0, btn->getContentSize().width/2, -4);
            }else{
                lblDescription->setPositionY(btn->getContentSize().height/2);
                GameManager::getInstance()->setLocalizedString("equip", lblDescription);
            }
            btn->loadTextureNormal("btnUpgrade");
            btn->loadTexturePressed("btnUpgrade");
        }
        count++;
    }
    std::vector<int> relicListInUse;
    for (int i = 0; i < relicEffects.size(); i++) {
        if (isRelicInUse(i)) {
            relicListInUse.push_back(i);
        }
    }
    if(itemRelic->getChildByName("btnUpgrade")->getChildByName("sptIcon") != nullptr){
        itemRelic->getChildByName("btnUpgrade")->getChildByName("sptIcon")->removeFromParentAndCleanup(true);
        itemRelic->getChildByName("btnUpgrade")->getChildByName("lblPrice")->removeFromParentAndCleanup(true);
    }
    
    lblDescription = (Text*)itemRelic->getChildByName("btnUpgrade")->getChildByName("lblDescription");
    lblDescription->setPositionY(45);
    GameManager::getInstance()->setLocalizedString("discover relic", lblDescription);
    GameManager::getInstance()->setLocalizedString("manage", ((Text*)itemRelic->getChildByName("btnManage")->getChildByName("lblDescription")));
    for (int i = 0; i < 5; i++) {
        item = (ImageView*)itemRelic->getChildByName(StringUtils::format("slot%d", i));
        sptIcon = (ImageView*)item->getChildByName("sptLock");
        if(isRelicSlotUnlocked(i)){
            if(item->getChildByName("sptIcon") != nullptr){
                item->getChildByName("sptIcon")->removeFromParentAndCleanup(true);
                item->getChildByName("lblPrice")->removeFromParentAndCleanup(true);
            }
            if (relicListInUse.size() > i) {
                sptIcon->loadTexture(StringUtils::format("res/ui-icons/%s.png", relicEffects.at(relicListInUse.at(i)).at(2).c_str()));
//                sptIcon->ignoreContentAdaptWithSize(true);
                sptIcon->setContentSize(Size(92, 92));
//                sptIcon->setScale(1.03f);
//                Sprite* spt = Sprite::create(StringUtils::format("res/ui-icons/%s.png", relicEffects.at(relicListInUse.at(i)).at(2).c_str()));
//                spt->setPosition(sptIcon->getPosition());
//                sptIcon->getParent()->addChild(spt);
//                spt->setName("relic");
                sptIcon->setScale(0.88f);
                sptIcon->setPositionY(50);
//                sptIcon->setSizeType(Widget::SizeType::ABSOLUTE);
                addStarToIcon(sptIcon, relicListInUse.at(i));
                sptIcon->setVisible(true);
            }else{
                sptIcon->setVisible(false);
            }
        }else{
            
        }
        
    }
    relicListInUse.clear();
}
void HelloWorldScene::addStarToIcon(Node* icon, int index){
    Sprite* spt;
    while(true){
        spt = (Sprite*)icon->getChildByName("star");
        if (spt == nullptr) {
            break;
        }
        spt->removeFromParent();
    }
    int starCount = Value(relicEffects.at(index).at(3)).asInt();
    spt = Sprite::create("sptStar.png");
    int startX = (icon->getContentSize().width - spt->getContentSize().width*starCount)/2 + spt->getContentSize().width/2;
    int starY = 2;
    for (int i = 0; i < starCount; i++) {
        if(i != 0){
            spt = Sprite::create("sptStar.png");
        }
        spt->setName("star");
        icon->addChild(spt, 100);
        spt->setPosition(Point(startX + spt->getContentSize().width*i, starY));
    }
}
void HelloWorldScene::resetRelic(){
    svRelic->removeAllChildren();
    int count = 0;
    ImageView* sptIcon;

    for (int i = 0; i < relicEffects.size(); i++) {
        if (!isRelicUnlocked(i)) {
            continue;
        }
        count++;
    }
    svRelic->setInnerContainerSize(Size(750, (count)*(itemGapY + itemHeight)));
    count = 0;
    for (int i = 0; i < relicEffects.size(); i++) {
        if (!isRelicUnlocked(i)) {
            continue;
        }
        Widget* clone = itemTemplete->clone();
        svRelic->getInnerContainer()->addChild(clone);
        clone->setPosition(Point(12, svRelic->getInnerContainerSize().height - count*(itemGapY + itemHeight)));
        clone->setName(__String::createWithFormat("item%d", count)->getCString());
        sptIcon = (ImageView*)clone->getChildByName("sptIcon");
        sptIcon->loadTexture(StringUtils::format("res/ui-icons/%s.png", relicEffects.at(i).at(2).c_str()));
        Button* btn = (Button*)clone->getChildByName("btnUpgrade");
        btn->addClickEventListener(CC_CALLBACK_1(HelloWorldScene::onRelicItemClick, this));
        btn->setTag(i);
        clone->removeChildByName("sptCheck0");
        clone->removeChildByName("sptCheck1");
        clone->removeChildByName("lblCondition0");
        clone->removeChildByName("lblCondition1");
        clone->removeChildByName("sptLock");
        sptIcon = (ImageView*)clone->getChildByName("sptIcon");
        addStarToIcon(sptIcon, i);
        sptIcon->setPositionY(sptIcon->getPositionY() + 5);
        
        count++;
    }
    
    updateRelic();
}
bool HelloWorldScene::isRelicSlotUnlocked(int slot){
    if(slot < 3){
        return true;
    }
    return UserDefault::getInstance()->getBoolForKey(StringUtils::format(KEY_RELIC_SLOT_UNLOCKED_FORMAT, slot).c_str(), false);
}
void HelloWorldScene::unlockRelicSlot(int slot){
    UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_RELIC_SLOT_UNLOCKED_FORMAT, slot).c_str(), true);
}
void HelloWorldScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
    if(GameManager::getInstance()->isInIntro){
        return;
    }
    if (keyCode == EventKeyboard::KeyCode::KEY_BACK)
    {
        if(secondPopup != nullptr){
            closePopup();
        }else if(firstPopup != nullptr){
            closePopup();
        }else{
            showExitPopup();
        }
    }
    log("Key with keycode %d released", (int)keyCode);
}
void HelloWorldScene::showExitPopup(){
    closePopup();
    firstPopup = CSLoader::createNode("dialog.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    firstPopup->setPosition(size.width/2 - firstPopup->getContentSize().width/2, 0);
    this->addChild(firstPopup, 10);
    Node* frame = firstPopup->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    GameManager::getInstance()->setLocalizedString("exit game", lbl);
    ddiyong(firstPopup);
    Button* btn = (Button*)firstPopup->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    
    btn = (Button*)frame->getChildByName("btnOk");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::onExitClick, this));
    btn->setTitleText(GameManager::getInstance()->getText("yes"));
    
    btn = (Button*)frame->getChildByName("btnCancel");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn->setTitleText(GameManager::getInstance()->getText("no"));
}
void HelloWorldScene::onExitClick(){
    GameManager::getInstance()->exitGame();
}
void HelloWorldScene::onRecordClick(){
    NativeInterface::NativeInterface::showVideoRecording();
}
void HelloWorldScene::showMessage(std::string msg){
    messageToShow = msg;
    this->scheduleOnce(schedule_selector(HelloWorldScene::showMessageLater), 0.1f)   ;
}
void HelloWorldScene::showMessageLater(float dt){
    GameManager::getInstance()->showDisposableMessage(messageToShow.c_str(), this);
}
bool HelloWorldScene::isHunted(int index){
    return UserDefault::getInstance()->getBoolForKey(StringUtils::format(KEY_HUNTED_FORMAT, index).c_str(), false);
}
void HelloWorldScene::setHunted(int index, bool hunted){
    UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_HUNTED_FORMAT, index).c_str(), hunted);
}
void HelloWorldScene::setAbsentRewardTime(){
    long now = TimeManager::getInstance()->getCurrentTime();
    UserDefault::getInstance()->setIntegerForKey(KEY_LAST_PAUSED_TIME, (int)now);
}
void HelloWorldScene::giveBackgroundReward(){
    absentRewardRequested = true;
}
void HelloWorldScene::checkAbsentAttack(){
    checkAbsentAttackRequested = false;
    int lastCheckHour = UserDefault::getInstance()->getIntegerForKey(KEY_LAST_ATTACK_CHECK_TIME, -1);
    int hour = (int)(TimeManager::getInstance()->getCurrentTime()/3600);
    if (lastCheckHour < 0) {
        UserDefault::getInstance()->setIntegerForKey(KEY_LAST_ATTACK_CHECK_TIME, hour);
        lastCheckHour = hour;
    }
    int accumulatedRelic = 0;
    std::string removedAnimalName = "";
    int huntedAnimalCount = 0;
    int level = UserDefault::getInstance()->getIntegerForKey(KEY_ATTACKER_LEVEL, 0);
    int attackCount = hour - lastCheckHour;
    if (attackCount <= 0) {
        log("attack count: %d", attackCount);
        return;
    }
    updateNotification();
    int successCount = 0;
    int failedCount = 0;
    int originalLevel = level;
    if(attackCount > 10){
        attackCount = 10;
    }
    for (int i = 0; i < attackCount; i++) {
        // hunt
        bool isSuccess = doAttackIsSuccess(true);
        if(isSuccess){
            if (accumulatedRelic < 100) {
                accumulatedRelic++;
            }
            successCount++;
        }else if (!isSuccess) {
            if (rand()%100 < 5 && charList.size() > 3) {
                huntedAnimalCount++;
                huntAnimal();
            }else{
                removedAnimalName = GameManager::getInstance()->getText("none");
            }
            failedCount++;
        }
        
        if (isSuccess) {
            level++;
            
        }else{
            level-=2;
        }
        if (sptShield == nullptr) {
            UserDefault::getInstance()->setIntegerForKey(KEY_ATTACKER_LEVEL, level);
        }
    }
    
    closePopup();
    
    Node* node = CSLoader::createNode("defenseMultiple.csb");
    GameManager::getInstance()->playSoundEffect(SOUND_POPUP_NORMAL);
    setAsPopup(node);
    
    node->setPosition(size.width/2, size.height/2);
    this->addChild(node, 10);
    Node* frame = node->getChildByName("frame");
    Text* lbl = (Text*)frame->getChildByName("lblTitle");
    lbl->setString(GameManager::getInstance()->getText("defense result"));
    ddiyong(node);
    
    Button* btn = (Button*)node->getChildByName("btnBlock");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)frame->getChildByName("btnCloseBig");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    btn = (Button*)node->getChildByName("btnOk");
    btn->addClickEventListener(CC_CALLBACK_0(HelloWorldScene::closePopup, this));
    lbl = (Text*)btn->getChildByName("lblText");
    GameManager::getInstance()->setLocalizedString(lbl, "ok");
    
    
    Text* lblValue = (Text*)frame->getChildByName("lblResultValue1");
    lblValue->setString(Value(successCount).asString());
    lblValue = (Text*)frame->getChildByName("lblResultValue2");
    lblValue->setString(Value(failedCount).asString());
    
    ImageView* sptImage = (ImageView*)node->getChildByName("sptImage");
    sptImage->loadTexture("attack_ready.png");
    
    ((Text*)frame->getChildByName("lblResultValue3"))->setString(Value(accumulatedRelic).asString());
    addRelicPoint(accumulatedRelic);
    
    lblValue = (Text*)frame->getChildByName("lblResultValue4");
    lblValue->setString(Value(huntedAnimalCount).asString());
    
//    ((Text*)frame->getChildByName("lblResultTitle0"))->setString(GameManager::getInstance()->getText("hunter"));
    ((Text*)frame->getChildByName("lblResultTitle1"))->setString(GameManager::getInstance()->getText("defense success"));
    ((Text*)frame->getChildByName("lblResultTitle2"))->setString(GameManager::getInstance()->getText("defense failed"));
    ((Text*)frame->getChildByName("lblResultTitle3"))->setString(GameManager::getInstance()->getText("receive"));
    ((Text*)frame->getChildByName("lblResultTitle4"))->setString(GameManager::getInstance()->getText("animal lost"));
    
    int hunterLevelChange = UserDefault::getInstance()->getIntegerForKey(KEY_ATTACKER_LEVEL, 0);
    ((Text*)frame->getChildByName("lblResultTitle5"))->setString(StringUtils::format(GameManager::getInstance()->getText(originalLevel < level?"hunter level rose":"hunter level reduce").c_str(), hunterLevelChange + 1));
    
    UserDefault::getInstance()->setIntegerForKey(KEY_LAST_ATTACK_CHECK_TIME, hour);
    
    int best = UserDefault::getInstance()->getIntegerForKey(KEY_BEST_HUNTER_LEVEL, 0);
    if (best < hunterLevelChange) {
        UserDefault::getInstance()->setIntegerForKey(KEY_BEST_HUNTER_LEVEL, hunterLevelChange);
    }
}
void HelloWorldScene::huntAnimal(){
    int index = 0;
    int totalSpawnedAnimalCount = UserDefault::getInstance()->getIntegerForKey(KEY_NEXT_ID, 0);
    while(true){
        index = rand()%totalSpawnedAnimalCount;
        int area = getArea(index);
        if (area == AREA_AFRICA || area == AREA_INVENTORY_AFRICA) {
            break;
        }
    }
    
    int indexToRemove = index;
//    UserDefault::getInstance()->setBoolForKey(StringUtils::format(KEY_HUNTED_FORMAT, indexToRemove).c_str(), true);
    //                setHunted(indexToRemove, true);
    subtractAccumulatedAnimalCount(indexToRemove);
    setAnimalArea(indexToRemove, AREA_HUNTED);
    for(auto animal: charList){
        if(animal->ID == indexToRemove){
//            removedAnimalName = StringUtils::format("%s %d", animalNames.at(ch->getTag()).c_str(), 1);
//            log("hunted animal: %s", removedAnimalName.c_str());
            charList.eraseObject(animal);
            animal->removeFromParent();
        }
    }
    
}
/*
 1. 방어 실패 시 동물은 최소 3개까지만 남기고 사라짐. 보유량 3기 미만일 시 사라지지 않음.
 2. 비접속시 이벤트로 획득하는 유물 포인트는 100개까지만 획득한 후 더이상 획득하지 않음. 비접속상태로 100개 이상 연속 획득한 경우 이 후 성공 발생 후에도 더이상 유물 포인트를 획득하지 않음.
 3. 일반종은 생명력 뽑기로 획득, 일반종이 사라지는 경우 생명력 뽑기 필요량이 이전 단계로 감소
 4. 희귀종과 일반종의 사라질 확률은 동등
 5. 중복 획득으로 도감 경험치가 상승해있는 경우 필드의 동물은 재 획득까지 사라지지만 도감 경험치는 1만 감소.
*/

//3. 갤럭시노트4에서 전체화면 캡쳐 팝업이 나타나지 않음 > 다른 기종 확인 필요
//4. 밀렵꾼 이벤트 발생 시 푸시 및 밀렵꾼 이벤트 종료 팝업등이 정상적으로 작동되지 않음 (밀렵꾼 이벤트 조건 및 활성화, 푸시에 대한 전반적인 점검이 필요합니다.





//7. 밀렵꾼 이벤트
//(1) 30분 쿨타임 종료 후 '꺼져버렷' 실행 > 보상 획득 > 동물 탄생 진행 > 즉시 밀렵꾼 이벤트 재발생
//(2) 쿨타임 진행 중에도 간헐적으로 이벤트가 발생
//(3) 이벤트가 발생 했음에도 푸시가 오지 않음
//(4) 부재중에 이벤트 발생 시간이 되어도 푸시가 오지 않음

//12. 동물 관리에서 '가이아로' 선택 시 크래쉬 발생

//14. 이벤트 오브젝트(어비스리움의 보물상자)를 통한 광고보기를 추가할 예정입니다. (객체 기획서 - 광고 시트 참고) >> 해당 오브젝트 이미지는 작업 후 전달드리겠습니다.

//0. 업적 메뉴를 추가해주세요.
//- 아이콘 : 전달드린 압축 파일 참조
//- UI 구성 : 전달드린 PPT문서 참조
//- 내용 : 객체기획서 참조

// 0. 애드몹의 전면배너는 모든 메뉴 진입 시 20%확률로 나타나도록 설정 부탁 드립니다.

// 동물 관리 클릭했더니 사냥꾼 팝업이 나왔다!



//3. 상점 보석 가격 변경 및 상품 추가 (객체기획서 - 상점 참조)


//1. 동물의 햇살 포인트 생산 관련
//(1). 동물 탄생 후 첫 햇살 포인트를 생산 후 두번째 생산을 하지 않습니다.
//(2). 게임을 완전히 종료한 후 재시작하는 경우 다시 생산을 합니다.
//(3). 게임 종료 후 재시작이라는 조건에만 생산하고 방치시에는 생산하지 않습니다.
//
//2. 인트로에서 스킵한 후 바로 인게임으로 진행되고 있으나 인트로 스킵 후 타이틀화면 다시 터치 후 게임으로 들어갈 수 있도록 뎁스가 조정되면 좋을 것 같습니다. (필수 사항 아님)
//
//3. 업적 메뉴
//> 청록마코 앵무 획득 시 유니크 획득 업적이 완료됨 (한정획득 업적은 완료되지 않음)
//> 화면공유 및 누적탭등 일부 업적 완료 후 빨간 유도점이 표시되지 않음
//
//4. 동물에 위치한 유도점이 해당 부분에 내용이 없음에도 표시됨
//
//5. 아직 밀렵꾼 이벤트가 일정한 시간마다 나타나지 않습니다.

//박대표님 그리고 두가지가 더 있습니다. 생산량 표시 폰트가 하얀색이라 가독이 잘 되지 않습니다. 원래 색상이 나을 것 같습니다. 그리고 다른 하나는 이미지로 전달드리겠습니다.
//초당 생산 효과


//4. 튜토리얼 때 강제 진행 (다른 행동을 할 수 없도록)
//6. 구글 클라우드 저장 기능
//7. 구글 게임 서비스 업적 아이콘 제작
//9. 동물 추가 총 8종 (아무르호랑이, 백호, 눈표범, 래서판다, 미국너구리, 자이언트판다, 아기판다, 꽃사슴)
//10. 튜토리얼 추가
//11. 영문 번역 본 제작 및 적용
//12. event 제작

//5. check - 광고 본 후 획득한 보상을 팝업으로 알림
//13. check - 햇살포인트 아이콘 말풍선 가운데로
//1. check - 터치 때 터치 위치에 작은 파티클 이펙트 넣기
//2. check - 화면 바깥에서 동물의 햇살, 생명력 생산이 있을 시 가장자리에 아이콘 표시
//3. check - 인트로 첫 시작때 스킵 할 수 없도록 (최초 실행 시 인트로)
//8. check - 푸시 아이콘 제작 및 적용



//첫 런칭 event
//개요 : 미션 달성 후 보상 받기
//기간 : 첫 업데이트 이 후
//미션 내용 및 보상 :
//1. 1만탭 달성 : 사막여우
//2. 광고보기 10회 : 버버리사자
//3. 출석 1회 : 아무르호랑이
//4. 출석 3회 : 햇살 50개
//5. 출석 7회 : 래서판다(유니크)
//6. 동물 탄생 10회 : 100 보석
//7. 동물 탄생 20회 : 500 보석
//8. 동물 탄생 30회 : 백호(희귀종)
//9. 모든 미션 달성 : 아기 판다(한정종)

