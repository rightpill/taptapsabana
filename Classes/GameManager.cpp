
#include "GameManager.h"
#include "HttpsManager.h"

#include "SimpleAudioEngine.h"

#include "TimeManager.h"
#include <fstream>
#include <iostream>
#include "expression_parser_cpp/parser.h"
#include "PluginSdkboxPlay/PluginSdkboxPlay.h"
#include "NativeInterface.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

GameManager* GameManager::m_mySingleton = NULL;

GameManager* GameManager::getInstance()
{
    if(NULL == m_mySingleton)
    {
        m_mySingleton = new GameManager();
    }
    
    return m_mySingleton;
}
GameManager::GameManager()
{
    jewelKind = 7;
    
    settingLayer = NULL;
  	LoadingScene = NULL;
    isInMiddleOfGame = false;
    isGoogleSigned = false;
    gameStarted = false;
    isUsingController = false;
	StageMode = 0;
    appAlreadyLaunched = false;
//    pushedLayer = NULL;
    initComplete = false;
    isPlayServiceLogIn = false;

    strPrices[0] = "4.99";
    strPrices[1] = "9.99";
    strPrices[2] = "49.99";
    strPrices[3] = "99.99";
    strPrices[4] = "2.99";
    strPrices[5] = "4.99";
    strPrices[6] = "2.99";
    strPrices[7] = "4.99";
    strPrices[8] = "4.99";
    
    pauseLayer = NULL;
   
    isGuestPlay = false;

    optionLayer = NULL;
    gameOverLayer = NULL;
    gameOverScene = NULL;
    world = NULL;
    achievementLayer = NULL;
    titleLayer = NULL;
    
    paidUser = false;
    settingLayer = NULL;
    
    isVideoRewardAttack = false;
    isVideoRewardEnergy = false;
    isVideoRewardCoins = false;
//    originallyDisabledMenusWhenLayerPushed = NULL;
    myPhoto = NULL;
    
    
    market = MARKET_PLAYSTORE_PAID;
//    priceArray = __Dictionary::create();
//    priceArray->retain();
    log("GameManager almost created");

    log("GameManager created");
}
void GameManager::onPlayServiceLogin(bool isLogin){
    if(settingLayer != NULL){
//        ((SettingLayer*)settingLayer)->onPlayServiceLogin(isLogin);
    }
}
void GameManager::initGameManager(){
	musicVolumn = UserDefault::getInstance()->getFloatForKey(KEY_MUSIC_VOLUMN, 1);
    setMusicVolumn(musicVolumn);
    soundVolumn = UserDefault::getInstance()->getFloatForKey(KEY_SOUND_VOLUMN, 1);
    setSoundVolumn(soundVolumn);
    bool enabled = UserDefault::getInstance()->getBoolForKey(KEY_PUSH_ALARM, false);
    
    //NativeInterface::NativeInterface::enablePush(enabled);
    NativeInterface::NativeInterface::resetPushNotification();
    initAchievement();
//    loadNickNames();
}
int GameManager::getGemForCoin(int coinCount){
    if(coinCount/100 == coinCount/100.0f){
        return coinCount/100;
    }else{
        return coinCount/100 + 1;
    }
}
void GameManager::addYellowTurningBackground(Node* node, int backWidth){
    Sprite* spt;
    for(int i = 0;i<2;i++){
        spt = Sprite::create("whiteSquare.png");
        spt->setScale(backWidth/spt->getContentSize().width);
        spt->setRotation(i*45);
        node->addChild(spt, -1);
        spt->setOpacity(100);
        spt->setColor(Color3B(255, 236, 79));
        BlendFunc bf = {GL_ONE, GL_ONE_MINUS_SRC_ALPHA};
        spt->setBlendFunc(bf);
        spt->getTexture()->setAntiAliasTexParameters();
        spt->setPosition(node->getContentSize().width/2, node->getContentSize().height/2);
        
        spt->runAction(RepeatForever::create(RotateBy::create(3, 360)));
    }
}
void GameManager::SetStringWithAutoLineBreak(TextBMFont* lbl, std::string str, float width){

	int next = 0;
	int spaceIndex = 0;
	for (int i = 1; i < str.length(); i++){
		lbl->setString(str.substr(next, i - next));
		if (lbl->getContentSize().width*lbl->getScaleX() > width){
			spaceIndex = i - 1;
			for (int j = spaceIndex; j > next; j--){
				if (str.substr(j, 1).compare(" ") == 0){
					spaceIndex = j;
					break;
				}
			}
			str.replace(spaceIndex,1, "\n");
			next = i;
		}
	}
	lbl->setString(str);
}
void GameManager::addCoin(int howMuch){
    int coin = getCoin();
    coin += howMuch;
    setCoin(coin);
}
void GameManager::setCoin(int howMuch){
    UserDefault::getInstance()->setIntegerForKey(KEY_COIN_LEFT, howMuch);
    UserDefault::getInstance()->flush();
}
int GameManager::getCoin(){
    return UserDefault::getInstance()->getIntegerForKey(KEY_COIN_LEFT, 0);
}
int GameManager::getGem(){
    return UserDefault::getInstance()->getIntegerForKey(KEY_GEM_LEFT, 30);
}
void GameManager::rewardTapjoy(int howMuch){
    addGem(howMuch);
//    ((Title*)titleLayer)->reservedStrForDisposableMessage = __String::createWithFormat(getText("gem rewarded").c_str(), howMuch)->getCString();
//    ((Title*)titleLayer)->titleJob = TITLE_JOB_SHOW_DISPOSABLE_MESSAGE;
}
void GameManager::addGem(int howMuch){
    int gem = getGem();
    gem += howMuch;
    setGem(gem);
}
void GameManager::setGem(int howMuch){
    UserDefault::getInstance()->setIntegerForKey(KEY_GEM_LEFT, howMuch);
    UserDefault::getInstance()->flush();
}
void GameManager::arrange(float startX, float endX, Vector<Button*> nodes){
    int count = (int)nodes.size();
    float itemWidth = nodes.at(0)->getContentSize().width;
    int index = 0;
    float positioningWidth = endX - startX - itemWidth;
    float gapX = positioningWidth / (count - 1);
    for (auto item : nodes){
        item->setPosition(Point(startX + itemWidth/2 + gapX*index, item->getPosition().y));
        index++;
    }
}

void GameManager::scrollToItem(ScrollView* scrollView, Node* node){
    scrollToItem(scrollView, node->getPosition(), Point::ZERO);
}
void GameManager::scrollToItem(ScrollView* scrollView, Point pos, Point offset){
    float currentY = scrollView->getInnerContainerPosition().y;
    float targetY = pos.y + offset.y;
    float realTargetY = -targetY + scrollView->getContentSize().height/2;
    if (realTargetY > 0) {
        realTargetY = 0;
    }else if(realTargetY < -scrollView->getInnerContainer()->getContentSize().height + scrollView->getContentSize().height){
        realTargetY = - scrollView->getInnerContainer()->getContentSize().height + scrollView->getContentSize().height;
    }
    float speed = 300;
    float dur = 0.05f*(currentY - realTargetY)/speed;
    if (dur < 0) {
        dur *= -1;
    }
    
    float currentX = scrollView->getInnerContainerPosition().x;
    float targetX = pos.x + offset.x;
    float realTargetX = -targetX + scrollView->getContentSize().width/2;
    if (realTargetX > 0) {
        realTargetX = 0;
    }else if(realTargetX < -scrollView->getInnerContainer()->getContentSize().width + scrollView->getContentSize().width){
        realTargetX = - scrollView->getInnerContainer()->getContentSize().width + scrollView->getContentSize().width;
    }
    dur = 0.1f*(currentX - realTargetX)/speed;
    if (dur < 0) {
        dur *= -1;
    }
    scrollView->getInnerContainer()->runAction(EaseOut::create(MoveTo::create(dur, Point(realTargetX, realTargetY)), 2));
}

void GameManager::runAnimation(Node* node, const char* name, bool repeat)
{
    node->stopActionByTag(ACTION_TAG_ANIMATION);
    Animation* animation = AnimationCache::getInstance()->getAnimation(name);
    Animate* animate = Animate::create(animation);
    
    if(repeat)
    {
        RepeatForever* forever =RepeatForever::create(animate);
        forever->setTag(ACTION_TAG_ANIMATION);
        node->runAction(forever);
    }
    else
    {
        animate->setTag(ACTION_TAG_ANIMATION);
        node->runAction(animate);
    }
}
void GameManager::showDisposableMessage(const char* msg, Node* parent){
    float localScale = 1;
    Label* disposableLabel = GameManager::getInstance()->getLocalizedLabel();
    parent->addChild(disposableLabel, 2002);
    disposableLabel->enableOutline(Color4B::BLACK, 8);
    //    disposableLabel->enableShadow();
    disposableLabel->setTextColor(Color4B::WHITE);
    disposableLabel->setSystemFontSize(20);
    disposableLabel->setAlignment(TextHAlignment::CENTER);
    setFontSize(disposableLabel, 40);
    float stayingTime = 0.5f;
    disposableLabel->setString(GameManager::getInstance()->getText(msg));
    float labelWidth = disposableLabel->getBoundingBox().size.width;
    Size size = getWorld()->size;
    
    disposableLabel->setPosition(size.width/2 - parent->getPositionX(), size.height*1/3);
    disposableLabel->runAction(Sequence::create(MoveBy::create(0.5, Point(0, 10)), DelayTime::create(stayingTime), FadeOut::create(1), CallFuncN::create(CC_CALLBACK_1(Label::removeFromParentAndCleanup, disposableLabel)), NULL));
    disposableLabel->setScale(localScale);
    
//    if (labelWidth > size.width) {
        disposableLabel->setWidth(size.width/disposableLabel->getScale());
//    }
    
    Sprite* sptDisposableBack = Sprite::create("whiteBigCircle.png");
    parent->addChild(sptDisposableBack, 2001);
    
    sptDisposableBack->setColor(Color3B::BLACK);
    sptDisposableBack->setOpacity(100);
    sptDisposableBack->setScale(0.8*localScale, 0.2*localScale);
    sptDisposableBack->stopAllActions();
    sptDisposableBack->setPosition(disposableLabel->getPosition());
    sptDisposableBack->runAction(Sequence::create(MoveBy::create(0.5, Point(0, 10)), DelayTime::create(stayingTime), FadeOut::create(1), CallFuncN::create(CC_CALLBACK_1(Sprite::removeFromParentAndCleanup, sptDisposableBack)), NULL));
}
void GameManager::verifyReceipt(std::string reciept, std::string signature){
    HttpsManager::getInstance()->nnProveReceipt(reciept, signature);
}
void GameManager::callbackFromBilling(int result)
{
    log("Gemshop - NResult: %d", result);
    
    if (result == 1) {
        log("Load SKU Items");
        NativeInterface::NativeInterface::loadSKUItems(GameManager::callbackFromBilling);
    }else if(result == 2){
        log("Load SKU Items Complete");
    }else if(result == 3){
        
        //layer->showMessage("Item ID is different from the server!", "OK");
    }else  if (result == 4) {
        //        (GameManager::sharedGameManager()->gemShopLayer)->showMessageBoxLater("Thank you. You are rich, now!");
        log("GemShop: purchase success");
//        ((Title*)GameManager::getInstance()->titleLayer)->titleJob = TITLE_JOB_HIDE_INDICATOR;
    }else if(result == 5){
        
        log("GemShop: purchase already owned");
    }else if(result == 6){
        log("GemShop: purchase failed.");
        
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        
#endif
    }
    log("callback complete");
}
void GameManager::rewardIAP(int index){
    if(world->addAchievementCurrent(ACHIEVE_PURCHASE, 1) >= 1){
        sdkbox::PluginSdkboxPlay::unlockAchievement("CgkIp4rw5u8HEAIQCQ");
    }
    
    if (index == IAP_GEM_0) {
        addGem(getIAPGemRewardCount(index));
        world->updateCurrencyLabels();
    }else if (index == IAP_GEM_1) {
        addGem(getIAPGemRewardCount(index));
        world->updateCurrencyLabels();
    }else if (index == IAP_GEM_2) {
        addGem(getIAPGemRewardCount(index));
        world->updateCurrencyLabels();
    }else if (index == IAP_GEM_3) {
        addGem(getIAPGemRewardCount(index));
        world->updateCurrencyLabels();
    }else if (index == IAP_GEM_SPECIAL_600)  {
        addGem(getIAPGemRewardCount(index));
        world->updateCurrencyLabels();
        UserDefault::getInstance()->setBoolForKey("key_gem_special_600_purchased", true);
    }else if (index == IAP_GEM_SPECIAL_1000) {
        addGem(getIAPGemRewardCount(index));
        world->updateCurrencyLabels();
        UserDefault::getInstance()->setBoolForKey("key_gem_special_1000_purchased", true);
    }else if (index == IAP_BLUE_BIRD) {
        world->startRaining(15);
        world->createAnimal(46);
    }else if (index == IAP_PUPPY) {
        world->startRaining(15);
        world->onTabChange(TAB_RELIC);
        world->onGochaRelicClick(nullptr);
        
        Node* frame = world->firstPopup->getChildByName("frame");
        Button* btn = (Button*)frame->getChildByName("btnGem");
        btn->setVisible(false);
        btn = (Button*)frame->getChildByName("btnRelic");
        btn->setVisible(false);
        cocostudio::timeline::ActionTimeline* action = CSLoader::createTimeline("relicGet.csb");
        world->firstPopup->runAction(action);
        action->gotoFrameAndPlay(5, false);
        world->scheduleOnce(schedule_selector(HelloWorldScene::onPuppyPickLater), 65.0f*1/50);
        
    }else if (index == IAP_STRONG_ODIN) {
        world->startOdin(7);
    }
    world->updateCurrencyLabels();
}
int GameManager::getIAPGemRewardCount(int index){
    int gemCount = -1;
    if (index == IAP_GEM_0) {
        gemCount = 600;
    }else if (index == IAP_GEM_1) {
        gemCount = 1300;
    }else if (index == IAP_GEM_2) {
        gemCount = 7000;
    }else if (index == IAP_GEM_3) {
        gemCount = 15000;
    }else if (index == IAP_GEM_SPECIAL_600) {
        gemCount = 600;
    }else if (index == IAP_GEM_SPECIAL_1000) {
        gemCount = 1000;
    }
    return gemCount;
}
void GameManager::setLanguageType(LanguageType lang){
    UserDefault::getInstance()->setIntegerForKey(KEY_SELECTED_LANGUAGE, (int)lang);
}
cocos2d::LanguageType GameManager::getLanguageType(){
    int languageType = UserDefault::getInstance()->getIntegerForKey(KEY_SELECTED_LANGUAGE, -1);
    cocos2d::LanguageType type = cocos2d::LanguageType::ENGLISH;
    if (languageType > -1) {
        type = (LanguageType)languageType;
    }else{
        type = Application::getInstance()->getCurrentLanguage();
    }
    return type;
}
int GameManager::getEnemyIndex(int stage, int ranking){
    return (stage*ranking + (ranking + 1)*7)%MAX_HERO_COUNT;
}
std::string GameManager::getText(const char* textId){
    if(languageTable[textId].getType() == Value::Type::NONE) {
        return textId;
    }
    ValueMap row = languageTable[textId].asValueMap();
    cocos2d::LanguageType type = getLanguageType();
    return getText(textId, type);
}
std::string GameManager::getText(const char* textId, cocos2d::LanguageType lType){
    ValueMap row = languageTable[textId].asValueMap();
    cocos2d::LanguageType type = lType;
    std::string str;
    if (type == LanguageType::KOREAN) {
        str = row.at("korean").asString();
    }else if (type == LanguageType::JAPANESE) {
        str = row.at("japanese").asString();
//    }else if (type == LanguageType::CHINESE_SIMPLIFIED) {
//        str = row.at("Chinese_simplified").asString();
//    }else if (type == LanguageType::CHINESE_TRADITIONAL) {
//        str = row.at("Chinese_traditional").asString();
    }else{
        str = row.at("english").asString();
    }
    
    while(str.find("\\n") != std::string::npos){
        str.replace(str.find("\\n"), 2, "\n");
    }
    //int index = (int)str.find("\\n");
    //if (index >= 0) {
    //    str.replace(index, 2, "\n");
    //}
    if (str.find_first_of("\"", 0) != std::string::npos) {  // if quote exist
        return str.substr(1, str.size() - 2);                                                                                                                                                                                                                                                                                                           
    }else{
        return str;
    }
}

void GameManager::loadNickNames(){
    std::string fileName = "nickname.csv";
    std::string csvStr = FileUtils::getInstance()->getStringFromFile(fileName);
    ValueVector rows = this->split(csvStr, "\r\n");
    
    std::string name;
    for (int i = 1; i < (int)rows.size(); i ++) {
        name = rows.at(i).asString();
        nickNames.push_back(name.substr(1, name.size() - 2));
    }
    log("end load nick names");
}
std::string GameManager::getNickName(int stageIndex, int ranking){
    int index = stageIndex*777 + ranking*77 + ranking;
    index = index%(int)nickNames.size();
    return nickNames.at(index);
}
void GameManager::loadLanguageSheet(){
    std::string fileName = "fonts/tslanguagesheet.csv";
    log("*fileName: %s", fileName.c_str());
    std::string csvStr = FileUtils::getInstance()->getStringFromFile(fileName);
    ValueVector rows = this->split(csvStr, "\r\n");
    ValueVector keys = this->split(rows.at(0).asString(), ",");
    for (int i = 1; i < (int)rows.size(); i ++) {
        ValueMap userParams;
        std::string strRow = rows.at(i).asString();
        log("%d. %s", i, strRow.c_str());
        ValueVector params = this->split(rows.at(i).asString(), ",");
        for (int j = 1; j < (int)params.size(); j ++) {
            std::string value =  params.at(j).asString();
            std::string theKey = keys.at(j).asString();
            userParams[theKey] = value;
        }
        std::string rowKey = params.at(0).asString();
        languageTable[rowKey] = userParams;
    }
    log("end language");
}
void GameManager::loadCSV(ValueVector& table, std::string fileName){
    std::string csvStr = FileUtils::getInstance()->getStringFromFile(fileName);
    ValueVector balls = this->split(csvStr, "\r\n");
    ValueVector keys = this->split(balls.at(0).asString(), ",");
    
    for (int i = 1; i < (int)balls.size(); i ++) {
        ValueMap userParams;
        ValueVector params = this->split(balls.at(i).asString(), ",");
        for (int j = 0; j < (int)params.size(); j ++) {
            userParams[keys.at(j).asString()] = params.at(j).asString();
            log("keys.at(j).asString(): %s", keys.at(j).asString().c_str());
        }
        
        table.push_back(Value(userParams));
    }
}
void GameManager::loadCSVFile(){
    loadLanguageSheet();
    
    /*
    for (int i = 0; i < (int)users.size(); i ++) {
        ValueMap user = users.at(i).asValueMap();
        std::string userId = user.at("index").asString();
        std::string name = user.at("name").asString();
        std::string place = user.at("place").asString();
        log("%s, %s, %s, %d", userId.c_str(), name.c_str(), place.c_str(), (int)users.size());
    }
    */
}
ValueVector GameManager::split(const std::string &str, const std::string &delim){
    ValueVector res;
    size_t current = 0, found, quoteStart, quoteEnd;
    while((found = str.find_first_of(delim, current)) != std::string::npos){
        if (delim.compare(",") == 0) {  // if csv
            if ((quoteStart = str.find_first_of("\"", current)) != std::string::npos) {  // if quote exist
                if (quoteStart < found) {
                    if((quoteEnd = str.find_first_of("\"", quoteStart+1)) != std::string::npos) {  // if quote pair
                        found = str.find_first_of(delim, quoteEnd);
                        if (found == std::string::npos) {
                            break;
                        }
                        /*current = quoteEnd + 1;    // set it to the next to the first quate
                         if ((found = str.find_first_of("\"", current)) != std::string::npos) {  // set found to the next quate
                         found = found - 1;
                         if (str.find_first_of(delim, found) == std::string::npos) { // break if this it the last item
                         break;
                         }
                         }*/
                    }
                }
            }
        }
        
        res.push_back(Value(std::string(str, current, found - current)));
        current = found + delim.size();
    }
    res.push_back(Value(std::string(str, current, str.size() - current)));
    return res;
}
std::string GameManager::getValueFromCSV(int table, int row, int column){
    
}
void GameManager::setTimeLeft(Label* lbl, long time){
    lbl->setString(getTimeLeftInString(time));
}
void GameManager::setTimeLeft(Text* lbl, long time){
    lbl->setString(getTimeLeftInString(time));
}
void GameManager::setTimeLeft(TextBMFont* lbl, long time){
    lbl->setString(getTimeLeftInString(time));
}
std::string GameManager::getTimeLeftInString(long time){
    int oneDay = 60*60*24;
    int oneHour = 60*60;
    int oneMin = 60;
    if (time >= oneDay*2) {
        return __String::createWithFormat("%dD", (int)(time/oneDay))->getCString();
    }else if(time >= oneDay) {
        int hour = (int)(time%oneDay)/oneHour;
        if (hour == 0) {
            return __String::createWithFormat("1D")->getCString();
        }else{
            return __String::createWithFormat("1D %dH", hour)->getCString();
        }
    }else {
        int sec = (int)((time%oneHour)%oneMin);
        return __String::createWithFormat("%02d:%02d:%02d", (int)(time/oneHour), (int)((time%oneHour)/oneMin), sec)->getCString();
    }
}
std::string GameManager::getTimeLeftInStringHMS(long time){
    int oneDay = 60*60*24;
    int oneHour = 60*60;
    int oneMin = 60;
    if (time >= oneDay*2) {
        return __String::createWithFormat("%dD", (int)(time/oneDay))->getCString();
    }else if(time >= oneDay) {
        int hour = (int)(time%oneDay)/oneHour;
        if (hour == 0) {
            return __String::createWithFormat("1D")->getCString();
        }else{
            return __String::createWithFormat("1D %dH", hour)->getCString();
        }
    }else {
        int sec = (int)((time%oneHour)%oneMin);
        if (sec%2==0) {
            return __String::createWithFormat("%02d:%02d:%02d", (int)(time/oneHour), (int)((time%oneHour)/oneMin), sec)->getCString();
        }else{
            return __String::createWithFormat("%02d %02d %02d", (int)(time/oneHour), (int)((time%oneHour)/oneMin), sec)->getCString();
        }
    }
}
int GameManager::GetUpgradePrice(Point pos){
    return 100;
}
void GameManager::setFontName(Label* lbl, const char* name, float fontSize){
    if(FileUtils::getInstance()->isFileExist(name))
    {
        TTFConfig config = lbl->getTTFConfig();
        config.fontFilePath = name;
        config.fontSize = fontSize;
        lbl->setTTFConfig(config);
    }
    else{
        lbl->setSystemFontName(name);
    }
}
const char* GameManager::getFont(int font){
    if(font == FONT_DEFAULT){
        if(Application::getInstance()->getCurrentLanguage() == LanguageType::KOREAN){
            return "BM-HANNA.ttf";
        }else{
            return "bitdust1.ttf";
        }
    }else if(font == FONT_ARIAL){
        return "Arial";
    }else if(font == FONT_BITDUST_ONE){
        return "bitdust1.ttf";
    }else if(font == FONT_BITDUST_TWO){
        return "bitdust2.ttf";
    }else if(font == FONT_CJK){
        return "NotoSansCJKjp-Bold.otf";
    }else if(font == FONT_DOHYUN){
        return "BMDOHYEON_ttf.ttf";
    }
    return "BM-HANNA.ttf";
}
std::string GameManager::GetSlotKey(int index){
	char buf[7];
	sprintf(buf, "Slot%d", index);
	return buf;
}

int GameManager::getPotionCount(){
    return UserDefault::getInstance()->getIntegerForKey("Potion_Count", 3);
}
void GameManager::setPotionCount(int potionCount){
    UserDefault::getInstance()->setIntegerForKey("Potion_Count", potionCount);
}
void GameManager::exitGame(){
    Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

const char* GameManager::getShortenedKoreanString(std::string str, int length){
    const char* nick = "";
    if (str.length() > length) {
        log("original: %s", str.c_str());
        int normalIndex = -1;
        for (int k = 0; k < length; k++) {
            if (str[k] >= 0 && (int)str[k] < 128) {
                normalIndex = k;
            }
        }
        
        log("normal: %d", normalIndex);
        if (normalIndex < 0 || normalIndex == length) {
            nick = __String::createWithFormat("%s...",str.substr(0, length).c_str())->getCString();
            log("all korean: normalIndex: %d", normalIndex);
        }else{
            int last = normalIndex;
            for (int i = last; i < length - 3; i += 3) {
                last += 3;
            }
            log("ugly/ last: %d, normalIndex: %d", last, normalIndex);
            nick = __String::createWithFormat("%s...",str.substr(0, (last + 1)).c_str())->getCString();
        }
    }
    return nick;
}

double GameManager::getAngle(Point pos1, Point pos2){
    float xGap = pos1.x - pos2.x;
    float yGap = pos1.y - pos2.y;
    return atan2(yGap, xGap)*180/3.14;
}
void GameManager::makeItSilukOccationally(Node* node, float interval, float animationDur){
    float scaleBit;
    if(node->getContentSize().width > node->getContentSize().height){
        scaleBit = node->getScaleY();
    }else{
        scaleBit = node->getScaleX();
    }
    node->runAction(RepeatForever::create(Sequence::create(EaseBackInOut::create(ScaleTo::create(animationDur, scaleBit*1.2)),
                                                           ScaleTo::create(animationDur/3, scaleBit),
                                                           DelayTime::create(interval),
                                                           ScaleTo::create(0, scaleBit*0.9), NULL)));
//    node->runAction(Sequence::create(DelayTime::create(startDelay),
//                                     RepeatForever::create(Sequence::create(EaseElasticOut::create(ScaleTo::create(animationDur*0.5, scaleBit), 2),
//                                                                            DelayTime::create(interval),
//                                                                            ScaleTo::create(0, scaleBit*0.9), NULL)), NULL));
}
void GameManager::makeItSiluk(Node* node, float interval, float scale){
    float originalScaleX = node->getScaleX();
    float originalScaleY = node->getScaleY();
    
    float duration = interval;
    float scaleBit;
    if(node->getContentSize().width > node->getContentSize().height){
        scaleBit = node->getContentSize().height*0.05*scale;
    }else{
        scaleBit = node->getContentSize().width*0.05*scale;
    }
    node->runAction(RepeatForever::create(Sequence::create(ScaleTo::create(duration, (1+scaleBit/node->getContentSize().width)*originalScaleX, (1-scaleBit/node->getContentSize().width)*originalScaleY), DelayTime::create(interval*0.1f), ScaleTo::create(duration, (1-scaleBit/node->getContentSize().width)*originalScaleX, (1+scaleBit/node->getContentSize().width)*originalScaleY), DelayTime::create(interval*0.1f), NULL)));
}
void GameManager::makeItSilukOnce(Node* node, float duration, float scale){
    float originalScaleX = node->getScaleX();
    float originalScaleY = node->getScaleY();
    
    float scaleBit;
    if(node->getContentSize().width > node->getContentSize().height){
        scaleBit = node->getContentSize().height*0.05*scale;
    }else{
        scaleBit = node->getContentSize().width*0.05*scale;
    }
    node->runAction(Sequence::create(ScaleTo::create(duration/2, (1+scaleBit/node->getContentSize().width)*originalScaleX, (1-scaleBit/node->getContentSize().width)*originalScaleY), ScaleTo::create(duration, (1-scaleBit/node->getContentSize().width)*originalScaleX, (1+scaleBit/node->getContentSize().width)*originalScaleY), ScaleTo::create(duration, scale), NULL));
}
void GameManager::makeItSiluk(Node* node){
    makeItSiluk(node, 0.3f, 1);
}


void GameManager::setStageScene(Scene* scene)
{
    isStageSetOnce = true;
//    stageScene->retain();
}

void GameManager::unscheduleAllNotifications(){
    //GoogleBilling::Billing::cancelAllLocalNotification();
}
void GameManager::scheduleLocalNotification(const char* title, const char* msg, int time){
    //GoogleBilling::Billing::registerLocalNotification(time, msg, title);
}
void GameManager::scheduleLocalNotification(){
    /*GoogleBilling::Billing::cancelAllLocalNotification();
    if (!this->getNotificationOn()) {
        return;
    }
    
    log("schedule");
    
    if(Application::getInstance()->getCurrentLanguage() == LanguageType::KOREAN){
        GoogleBilling::Billing::registerLocalNotification(60*60*24*1, "공주가 기다려요! 어서 공주를 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*2, "공주가 기다려요! 대리님 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*3, "깃발이 가득 찼어요! 맘껏 대리의 전설을 즐겨볼까요?", "깃발이 가득 찼어요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*4, "공주가 기다려요!어서 공주를 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*5, "공주가 기다려요! 어서 공주를 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*6, "공주가 기다려요! 대리님 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*7, "깃발이 가득 찼어요! 맘껏 대리의 전설을 즐겨볼까요?", "깃발이 가득 찼어요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*8, "공주가 기다려요!어서 공주를 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*9, "공주가 기다려요! 어서 공주를 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*10, "공주가 기다려요! 대리님 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*11, "깃발이 가득 찼어요! 맘껏 대리의 전설을 즐겨볼까요?", "깃발이 가득 찼어요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*12, "공주가 기다려요!어서 공주를 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*13, "공주가 기다려요! 어서 공주를 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*14, "공주가 기다려요! 대리님 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*15, "깃발이 가득 찼어요! 맘껏 대리의 전설을 즐겨볼까요?", "깃발이 가득 찼어요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*16, "공주가 기다려요!어서 공주를 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*17, "공주가 기다려요! 어서 공주를 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*18, "공주가 기다려요! 대리님 구해주세요!", "공주가 기다려요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*19, "깃발이 가득 찼어요! 맘껏 대리의 전설을 즐겨볼까요?", "깃발이 가득 찼어요!");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*20, "공주가 기다려요!어서 공주를 구해주세요!", "공주가 기다려요!");
    }else{
        GoogleBilling::Billing::registerLocalNotification(60*60*24*1, "Come and save the princess!", "The princess is waiting for you");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*3, "Come and save the princess!", "The princess is waiting for you");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*5, "Come and save the princess!", "The princess is waiting for you");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*7, "Come and save the princess!", "The princess is waiting for you");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*9, "Come and save the princess!", "The princess is waiting for you");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*11, "Come and save the princess!", "The princess is waiting for you");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*13, "Come and save the princess!", "The princess is waiting for you");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*15, "Come and save the princess!", "The princess is waiting for you");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*17, "Come and save the princess!", "The princess is waiting for you");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*19, "Come and save the princess!", "The princess is waiting for you");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*21, "Come and save the princess!", "The princess is waiting for you");
        GoogleBilling::Billing::registerLocalNotification(60*60*24*30, "Come and save the princess!", "The princess is waiting for you");
    }*/
}
void GameManager::setMusicVolumn(float vol)
{
    float value = 0;
    float percent = vol * 100;
    if(percent > 30){
        value += 0.1;
        value += (percent - 30)*0.9f/70;
    }else{
        value = percent/300.0f;
    }
    musicVolumn = vol;
    CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(vol);
    UserDefault::getInstance()->setFloatForKey(KEY_MUSIC_VOLUMN, vol);
//    GameManager::getInstance()->saveCoin();
}
void GameManager::setSoundVolumn(float vol)
{
    soundVolumn = vol;
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(vol);
    UserDefault::getInstance()->setFloatForKey(KEY_SOUND_VOLUMN, vol);
//    GameManager::getInstance()->saveCoin();
}
void GameManager::setNotificationOn(bool onOff)
{
    UserDefault::getInstance()->setBoolForKey(KEY_PUSH_ALARM, onOff);
//    GameManager::getInstance()->saveCoin();
}
float GameManager::getMusicVolumn()
{
    float value = 0;
    float percent = SimpleAudioEngine::getInstance()->getBackgroundMusicVolume() * 100;
    if(percent > 30){
        value += 0.1;
        value += (percent - 30)*0.9f/70;
    }else{
        value = percent/300.0f;
    }
    
    return value;
}
float GameManager::getSoundVolumn(){
    return SimpleAudioEngine::getInstance()->getEffectsVolume();
}
bool GameManager::getNotificationOn(){
    return UserDefault::getInstance()->getBoolForKey(KEY_PUSH_ALARM, true);
}

void GameManager::preLoadAllSoundEffect(){
    
    for (int i = 0; i < 9; i++) {
        int index = cocos2d::random()%9;
        char buf[20];
        sprintf(buf, "footStep%d.mp3", index);
        CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect(buf);
    }

    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("waterSplash.mp3");
}

void GameManager::fillLayerWithTile(Layer* layer, std::string sptName){
    Size size = Director::getInstance()->getWinSize();
    Sprite* sptTile = Sprite::create(sptName);
    float scale = 0.6875f;
    sptTile->setScale(scale);
    int currentWidth = 0;
    int currentHeight = 0;
    while(currentHeight < size.height){
        while(currentWidth < size.width){
            sptTile = Sprite::create(sptName);
            layer->addChild(sptTile, -99999);
            sptTile->setScale(scale);
            sptTile->setAnchorPoint(Point::ZERO);
            sptTile->setPosition(Point(currentWidth, currentHeight));
            
            currentWidth += sptTile->getBoundingBox().size.width;
        }
        currentWidth = 0;
        currentHeight += sptTile->getBoundingBox().size.height;
    }
}
void GameManager::playSoundEffect(int sound){
    if (false) { // music
        if (musicVolumn <= 0) {
            return;
        }
    }else{
        if (soundVolumn <= 0) {
            return;
        }
    }
    int index = 0;
    switch(sound)
	{
        case SOUND_SKILL_AURORA:
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/skill_aurora.mp3", false, 1, 0, 2);
            break;
        case SOUND_SKILL_WIND:
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/skill_wind.mp3", false, 1, 0, 2);
            break;
        case SOUND_SKILL_RAINBOW:
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/skill_rainbow.mp3", false, 1, 0, 2);
            break;
        case SOUND_CREATE_ANIMAL:
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/create_animal.mp3", false, 1, 0, 2);
            break;
        case SOUND_CREATE_DECO :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/create_deco.mp3", false, 1, 0, 2);
            break;
        case SOUND_CREATE_RELIC :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/create_relic.mp3", false, 1, 0, 2);
            break;
        case SOUND_POPUP_CREATE :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/popup_create.mp3", false, 1, 0, 2);
            break;
        case SOUND_POPUP_NEGA :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/popup_nega.mp3", false, 1, 0, 2);
            break;
        case SOUND_POPUP_NORMAL :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/popup_normal.mp3", false, 1, 0, 2);
            break;
        case SOUND_UPGRADE :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/btn_upgrade.mp3", false, 1, 0, 2);
            break;
        case SOUND_ANIMAL_TAP_0 :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/animal_tap_1.mp3", false, 1, 0, 2);
            break;
        case SOUND_ANIMAL_TAP_1 :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/animal_tap_2.mp3", false, 1, 0, 2);
            break;
        case SOUND_GRASS_0 :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/grass1.mp3", false, 1, 0, 0.1f);
            break;
        case SOUND_GRASS_1 :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/grass2.mp3", false, 1, 0, 0.1f);
            break;
        case SOUND_GRASS_2 :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/grass3.mp3", false, 1, 0, 0.1f);
            break;
        case SOUND_GRASS_3 :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/grass4.mp3", false, 1, 0, 0.1f);
            break;
        case SOUND_MENU_DOWN :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/btn_menu_down.mp3", false, 1, 0, 2);
            break;
        case SOUND_MENU_UP :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/btn_menu_up.mp3", false, 1, 0, 2);
            break;
        case SOUND_BTN_POS :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/btn_posi.mp3", false, 1, 0, 2);
            break;
        case SOUND_BTN_NEG :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/btn_nega.mp3", false, 1, 0, 2);
            break;
        case SOUND_SCENE_0 :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("scene1.mp3", false, 1, 0, 2);
            break;
        case SOUND_SCENE_1 :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/scene2.mp3", false, 1, 0, 2);
            break;
        case SOUND_SCENE_2 :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/scene3.mp3", false, 1, 0, 2);
            break;
        case SOUND_SCENE_3 :
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/scene4.mp3", false, 1, 0, 2);
            break;
        case SOUND_BGM_TITLE :
            if (musicVolumn > 0) {
                CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
                CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sound/BGM_title.mp3", true);
            }
            break;
        case SOUND_BGM_VILLAGE :
            if (musicVolumn > 0) {
                CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
                CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sound/bensound-memories2.mp3", true);
            }
            break;
        default:
            break;
	}
}
std::string GameManager::getVillageName(int stageIndex){
    std::string tag="";
    std::string theIndex = "";
    std::string stage="";
    if (stageIndex == 0) {
        stage = GameManager::getInstance()->getText("league0");
    }else if (stageIndex >= 1 && stageIndex <= 15) {  // region
        stage = GameManager::getInstance()->getText("league1");
        int fromStart = stageIndex - 1;
        if (fromStart/5 == 0) {
            tag = "III";
        }else if (fromStart/5 == 1) {
            tag = "II";
        }else if (fromStart/5 == 2) {
            tag = "I";
        }
        theIndex = Value(5 - fromStart%5).asString();
    }else if (stageIndex >= 16 && stageIndex <= 25) {  // world
        stage = GameManager::getInstance()->getText("league2");
        int fromStart = stageIndex - 16;
        if (fromStart/5 == 0) {
            tag = "B";
        }else if (fromStart/5 == 1) {
            tag = "A";
        }
        theIndex = Value(5 - fromStart%5).asString();
    }else if (stageIndex >= 26 && stageIndex <= 40) {  // bronze,silver,gold
        int fromStart = stageIndex - 26;
        stage = GameManager::getInstance()->getText(__String::createWithFormat("league%d", 3 + fromStart/5)->getCString());
        theIndex = Value(5 - fromStart%5).asString();
    }else if (stageIndex >= 41 && stageIndex <= 48) {  // platinum, summoners
        int fromStart = stageIndex - 41;
        stage = GameManager::getInstance()->getText(__String::createWithFormat("league%d", 6 + fromStart/4)->getCString());
        if (fromStart%4 < 3) {
            theIndex = 'C' - fromStart%4;
        }else{
            theIndex = "S";
        }
    }else if (stageIndex == 49){    // master
        stage = GameManager::getInstance()->getText("league8");
    }else if (stageIndex >= 50 && stageIndex <= 54){    // champion
        int fromStart = stageIndex - 50;
        stage = GameManager::getInstance()->getText("league9");
        theIndex = Value(5 - fromStart%5).asString();
    }else if (stageIndex >= 55 && stageIndex <= 69) {  // empire
        stage = GameManager::getInstance()->getText("league10");
        int fromStart = stageIndex - 55;
        if (fromStart/5 == 0) {
            tag = "III";
        }else if (fromStart/5 == 1) {
            tag = "II";
        }else if (fromStart/5 == 2) {
            tag = "I";
        }
        theIndex = Value(5 - fromStart%5).asString();
    }else if (stageIndex >= 70 && stageIndex <= 74){    // ambassador
        int fromStart = stageIndex - 70;
        stage = GameManager::getInstance()->getText("league11");
        theIndex = Value(5 - fromStart%5).asString();
    }else{
        int fromStart = stageIndex - 75;
        stage = GameManager::getInstance()->getText("league12");
        theIndex = Value(fromStart + 1).asString();
    }
    
    if (tag.size() > 0) {
        stage += " " + tag;
    }
    if (theIndex.size() > 0) {
        stage += " - " + theIndex;
    }
    
    return stage;
}

inline bool caseInsCharCompareN(char a, char b){
    return(toupper(a) == toupper(b));
}
bool GameManager::caseInsCompare(std::string& str1, std::string& str2){
    return ((str1.size() == str2.size()) && std::equal(str1.begin(), str1.end(), str2.begin(), caseInsCharCompareN));
}
int GameManager::getColleaguePurchasePrice(int index){
    /*if (index == 0) {
        return 100;
    }else if(index == 1){
        return 500;
    }else if(index == 2){
        return 1000;
    }else if(index == 3){
        return 3000;
    }else if(index == 4){
        return 5000;
    }else if(index == 5){
        return 7000;
    }else if(index == 6){
        return 9000;
    }else if(index == 7){
        return 11000;
    }else if(index == 8){
        return 13000;
    }else if(index == 9){
        return 15000;
    }
    return 0;*/
    return 1000*(2*index+1);
}
static void printProperties(Properties* properties, int indent)
{
    // Print the name and ID of the current namespace.
    const char* spacename = properties->getNamespace();
    const char* id = properties->getId();
    char chindent[64];
    int i=0;
    for(i=0; i<indent*2;i++)
        chindent[i] = ' ';
    chindent[i] = '\0';
    
//    log("%sNamespace: %s  ID: %s\n%s{", chindent, spacename, id, chindent);
    
    // Print all properties in this namespace.
    const char* name = properties->getNextProperty();
    const char* value = NULL;
    while (name != NULL)
    {
        value = properties->getString(name);
//        log("%s%s = %s", chindent, name, value);
        name = properties->getNextProperty();
    }
    
    Properties* space = properties->getNextNamespace();
    while (space != NULL)
    {
        printProperties(space, indent+1);
        space = properties->getNextNamespace();
    }
    
//    log("%s}\n",chindent);
}

void GameManager::setSpriteEffect(Sprite* spt, int effect){
    
    auto properties = Properties::createNonRefCounted("2d_effects.material#sample");
    
    // Print the properties of every namespace within this one.
    printProperties(properties, 0);
    
    Material *mat1 = Material::createWithProperties(properties);
    
    if (effect == SPRITE_EFFECT_BLUR) {
        spt->setGLProgramState(mat1->getTechniqueByName("blur")->getPassByIndex(0)->getGLProgramState());
    }else if(effect == SPRITE_EFFECT_OUTLINE) {
        spt->setGLProgramState(mat1->getTechniqueByName("outline")->getPassByIndex(0)->getGLProgramState());
    }else if(effect == SPRITE_EFFECT_NOISE) {
        spt->setGLProgramState(mat1->getTechniqueByName("noise")->getPassByIndex(0)->getGLProgramState());
    }else if(effect == SPRITE_EFFECT_EDGE) {
        spt->setGLProgramState(mat1->getTechniqueByName("edge_detect")->getPassByIndex(0)->getGLProgramState());
    }
    
    // properties is not a "Ref" object
    CC_SAFE_DELETE(properties);
}

float GameManager::getValueForFunction(std::string str){
    if (str.compare("") != 0)
    {
        Parser prs;
        try
        {
            char* result;
            result = prs.parse(str.c_str());
//            printf("\t%s\n", result);
            return Value(result).asFloat();;
        }
        catch (...)
        {
            printf("\tError: Unknown error occured in parser\n");
        }
    }
    return -1;
}
void GameManager::setLocalizedString(Label* lbl, std::string str){
    std::string text = getText(str.c_str());
    if (text.compare(lbl->getString()) != 0) {
        lbl->setString(text);
    }
}
void GameManager::setLocalizedString(Text* lbl, std::string str){
    setLocalizedString(str, lbl);
}
std::string GameManager::getEnemyName(int stage, int ranking){
    return getNickName(stage, ranking);
    //return "I Need name";
    int maxNameCount = 50;
    int index = (stage*3 + (ranking + 1)*7)%maxNameCount;
    if (index == 30) {
        return getText("dary");
    }else{
        return getText(__String::createWithFormat("enemy name %d", index)->getCString());
    }
}
void GameManager::setLocalizedString(std::string str, Text* lbl){
    std::string text = getText(str.c_str());
    if (text.compare(lbl->getString()) != 0) {
        lbl->setString(text);
        lbl->setFontName(getLocalizedFont());
    }
}
void GameManager::runSceneAnimation(Node* layer, std::string sceneName, bool loop){
    cocostudio::timeline::ActionTimeline* action = CSLoader::createTimeline(sceneName);
    layer->runAction(action);
    action->gotoFrameAndPlay(0, loop);
}
void GameManager::setStringWithLocalizedFont(std::string str, Text* lbl){
    lbl->setString(str);
    lbl->setFontName(getLocalizedFont());
}
const char* GameManager::getLocalizedFont(){
    if (GameManager::getInstance()->getLanguageType() == LanguageType::KOREAN) {
        return "GodoB.ttf";
    }else if (GameManager::getInstance()->getLanguageType() == LanguageType::JAPANESE/* ||
              GameManager::getInstance()->getLanguageType() == LanguageType::CHINESE_SIMPLIFIED ||
              GameManager::getInstance()->getLanguageType() == LanguageType::CHINESE_TRADITIONAL*/) {
        //        return "BMJUA_ttf.ttf";
        return "NotoSansCJKjp-Bold.otf";
    }else{
        //return "visitor1.ttf";
        return "GodoB.ttf";
        //return "NotoSansCJKjp-Bold.otf";
    }
}
void GameManager::showShining(Node* parent, int glowCount, const char* particleName, float scale, Point pos, int varDistance, Color3B color, Widget::TextureResType fileType){
    float speed = 3;
    for (int i = 0; i < glowCount; i++) {
        Sprite* sptParticle;
        if (fileType == Widget::TextureResType::PLIST) {
            sptParticle = Sprite::createWithSpriteFrameName(particleName);
        }else{
            sptParticle = Sprite::create(particleName);
        }
//        sptParticle->setColor(color);
//        sptParticle->setBlendFunc(cocos2d::BlendFunc::ADDITIVE);
        parent->addChild(sptParticle);
//        sptParticle->setScale(scale + (rand()%100 - 50)*scale/100);
        sptParticle->setPosition(pos + Point(rand()%varDistance-(varDistance/2), rand()%varDistance - varDistance/2));
        float dur = (0.3f + (rand()%varDistance)*0.1f)/speed;
        sptParticle->runAction(Sequence::create(MoveBy::create(dur, Point(rand()%(int)(varDistance*0.4f)-(varDistance*0.2), (varDistance*0.8) + rand()%(int)(varDistance*1.2f))), CallFuncN::create(CC_CALLBACK_1(Node::removeFromParentAndCleanup, sptParticle)), nullptr));
        sptParticle->runAction(Sequence::create(DelayTime::create(dur - 0.3f), FadeOut::create(0.3f), nullptr));
        sptParticle->runAction(RotateBy::create(dur, 360));
    }
}
cocos2d::Label* GameManager::getLocalizedLabel(const char* text, Color4B color){
    if (GameManager::getInstance()->getLanguageType() == LanguageType::KOREAN) {
        Label* lbl = Label::createWithTTF(text, getLocalizedFont(), 60);
        lbl->setTextColor(color);
        lbl->setScale(0.08);
        return lbl;
    }else{
        //        return Label::createWithBMFont("whitePixelFont.fnt", "Label");
        //        Label* lbl = Label::createWithTTF("label", "bitdust1.ttf", 80);
        Label* lbl = Label::createWithTTF(text, getLocalizedFont(), 60);
        lbl->setTextColor(color);
        lbl->setScale(0.08);
        return lbl;
    }
}
cocos2d::Label* GameManager::getLocalizedLabel(){
    return getLocalizedLabel("label", Color4B::WHITE);
}
Point GameManager::getGemCountPosition(){
    Size size = Director::getInstance()->getWinSize();
    return Point(size.width/2 + 190, size.height - 30);
}


Layer* GameManager::getShopLayer()
{
//    if (!shopLayer) {
//        shopLayer = ShopLayer::create();
//        shopLayer->retain();
//    }
    return NULL;//shopLayer;
}

HelloWorldScene* GameManager::getWorld()
{
    return world;
}
void GameManager::setWorld(HelloWorldScene* layer)
{
    world = layer;
}
void GameManager::SpriteMoveDone(Node* node){
	node->stopAllActions();
	node->removeFromParentAndCleanup(true);
}
void GameManager::showParticleExplosion(Node* prt, const char* sptName, Point pos, int distance, float scale, Widget::TextureResType type){
	Sprite* particle;
	float delay = 0;
	float time;
	float angle = 0;
	for (int i = 0; i < 20; i++){
		if (type == Widget::TextureResType::PLIST){
			particle = Sprite::createWithSpriteFrameName(sptName);
		}
		else{
			particle = Sprite::create(sptName);
		}
		prt->addChild(particle);
		particle->setPosition(pos);
		angle = cocos2d::random() % 360;
		time = 0.3f + (cocos2d::random() % 10)*0.02f;
		delay = (cocos2d::random() % 10)*0.01f;
		particle->runAction(FadeOut::create(delay + time));
		particle->runAction(Sequence::create(
			DelayTime::create(delay),
			EaseOut::create(MoveBy::create(time, Point(distance*cos(angle*3.14f / 180), distance*sin(angle*3.14f/180))), 3),
			CallFuncN::create(CC_CALLBACK_1(GameManager::SpriteMoveDone, this)), NULL));
	}
}
void GameManager::pushLayer(Layer* parent, Layer* layer){
//    disableLayer(parent);
//    parent->addChild(layer, 1000);
    animateFadeIn(layer, parent);
}

void GameManager::pushLayerWithoutDisable(Layer* parent, Layer* layer){
    //    parent->addChild(layer, 1000);
    animateFadeIn(layer, parent);
}
void GameManager::googleSignInOrOut(bool sign){
    if(sign){
        //        MessageBox("Signed in", "Google Play Service");
        log("** google Signed in");
    }else{
        //        MessageBox("Signed out", "Google Play Service");
        log("** google not signed in");
    }
    
//    if(titleLayer != NULL && ((TitleLayer*)titleLayer)->titleMain != NULL){
//        ((TitleLayer*)titleLayer)->removeSignInButton();
//    }
    isGoogleSigned = sign;
    
}
void GameManager::animateFadeIn(Node* layer, Node* parent){
    parent->addChild(layer, 1000);
    layer->setScale(0.1f);
    layer->runAction(Sequence::create(ScaleTo::create(0.2, 1.1), ScaleTo::create(0.05, 0.95),ScaleTo::create(0.05, 1), NULL));
    
    Size size = Director::getInstance()->getWinSize();
    Sprite* blackBack = Sprite::create("blackSquare.png");
    parent->addChild(blackBack, 999);
    blackBack->setTag(7899);
    blackBack->setScaleX(size.width);
    blackBack->setScaleY(size.height);
    blackBack->setPosition(Point(size.width/2, size.height/2));
}
void GameManager::animateFadeOut(Node* layer){
//    layer->setScale(0.1f);
    layer->runAction(Sequence::create(ScaleTo::create(0.06, 0.85),ScaleTo::create(0.1, 1.2), ScaleTo::create(0.1, 0.1), CallFuncN::create(CC_CALLBACK_1(GameManager::animationFadeOutDone, this)), NULL));
}

void GameManager::animationFadeInDone(Node* layer){
//    log("fade in done");
}
void GameManager::animationFadeOutDone(Node* layer){
    Layer* parent = (Layer*)layer->getParent();
    parent->removeChildByTag(7899);
    layer->removeFromParentAndCleanup(true);
    log("layer removed");
//    enableLayer(parent);
}
/*
void GameManager::disableLayer(Layer* layer){
    layer->setTouchEnabled(false);
//    log("layer retain count: %d", layer->retainCount());
    CCMenu* menu;
    Layer* childLayer;
//    log("disable layer children count: %d", layer->getChildren().size());
    for (int i = 0; i < layer->getChildren().size(); i++) {
        menu = dynamic_cast<CCMenu*>(layer->getChildren().at(i));
        if (menu) {
            if (!menu->isEnabled()) {
                Vector<Droppable*>disabledMenusArray = dynamic_cast<CCArray*>((CCArray*)layer->getUserData());
                if (disabledMenusArray == NULL) {
                    disabledMenusArray = CCArray::create();
                    disabledMenusArray->retain();
                    layer->setUserData((void*)disabledMenusArray);
                }
                disabledMenusArray.pushBack(menu);
            }else{
                menu->setEnabled(false);
            }
        }else{
            childLayer = dynamic_cast<Layer*>(layer->getChildren().at(i));
            if (childLayer != NULL) {
                disableLayer(childLayer);
            }
        }
    }
}

void GameManager::popLayer(Layer* layer){
    animateFadeOut(layer);
}

void GameManager::popLayerWithoutAnimation(Layer* layer){
    animationFadeOutDone(layer);
}

void GameManager::enableLayer(Layer* layer){
    layer->setTouchEnabled(true);
    CCMenu* menu;
    Layer* childLayer;
    Vector<Droppable*>disabledMenusArray = NULL;
    for (int i = 0; i < layer->getChildren().size(); i++) {
        menu = dynamic_cast<CCMenu*>(layer->getChildren().at(i));
        if (menu) {
            disabledMenusArray = dynamic_cast<CCArray*>((CCArray*)layer->getUserData());
            
            if (disabledMenusArray != NULL && disabledMenusArray->containsObject(menu)) {
                
            }else{
                menu->setEnabled(true);
            }
        }
        childLayer = dynamic_cast<Layer*>(layer->getChildren().at(i));
        if (childLayer != NULL) {
            enableLayer(childLayer);
        }
    }

    if (disabledMenusArray != NULL) {
        disabledMenusArray->release();
        layer->setUserData(NULL);
    }
}*/


long GameManager::strToLong(const char* str){
    std::stringstream strValue;
    strValue << str;
    
    long longValue;
    strValue >> longValue;
    
    return longValue;
}
Sprite* GameManager::getGrayScaleImage(const char* src, int devider){
    Image *img=  new Image();
    img->initWithImageFile(src);

//    II) From Sprite :
    
//    II.1) Sprite -> RenderTexture2D
    
//    II.2) RenderTexture2D -> Image (Image *testImage = RenderText2D->newImage();)
    
//    2.Then You can do what you need :
    
//        Image *img= ... // make Image from Sprite
        int x=3;
    if(img->hasAlpha())
        x=4;
    
    unsigned char *data = new unsigned char[img->getDataLen()*x];
    data = img->getData();
    // [0][0] => Left-Top Pixel !
    // But cocos2d Location Y-axis is Bottom(0) to Top(max)
    
//    unsigned char *newData = new unsigned char[img->getDataLen()*x];
    
    for(int i=0;i<img->getWidth();i++)
    {
        for(int j=0;j<img->getHeight();j++)
        {
            unsigned char *pixel = data + (i + j * img->getWidth()) * x;
            
            // You can see/change pixels' RGBA value(0-255) here !
            unsigned char r = *pixel;
            unsigned char g = *(pixel + 1);
            unsigned char b = *(pixel + 2);
//            unsigned char a = *(pixel + 3);
            
            *pixel = (r+g+b)/devider;
            *(pixel + 1) = (r+g+b)/devider;
            *(pixel + 2) = (r+g+b)/devider;
        }
    }
//    3.Then, convert it to texture_2D
    //Image -> Texture2d
    Texture2D* texture_2D= new Texture2D();
    texture_2D->initWithImage(img);
//    4.And Finally back to Sprite
    return Sprite::createWithTexture(texture_2D);
}

Sprite* GameManager::getGrayScaleSpriteWithSpriteFrameName(const char* src ){
//    Image *img=  new Image();
//    img->initWithImageFile(src);
    Sprite* sprite = Sprite::createWithSpriteFrameName(src);
    
    RenderTexture* r = RenderTexture::create(sprite->getTexture()->getPixelsWide(), sprite->getTexture()->getPixelsHigh());
    
    r->beginWithClear(1, 1, 1, 0);
    sprite->visit();
    r->end();
    
    Image *img = r->newImage();
    
    int x=3;
    if(img->hasAlpha())
        x=4;
    
    unsigned char *data = new unsigned char[img->getDataLen()*x];
    data = img->getData();
    for(int i=0;i<img->getWidth();i++)
    {
        for(int j=0;j<img->getHeight();j++)
        {
            unsigned char *pixel = data + (i + j * img->getWidth()) * x;
            
            // You can see/change pixels' RGBA value(0-255) here !
            unsigned char r = *pixel;
            unsigned char g = *(pixel + 1);
            unsigned char b = *(pixel + 2);
            //            unsigned char a = *(pixel + 3);
            
            *pixel = (r+g+b)/3;
            *(pixel + 1) = (r+g+b)/3;
            *(pixel + 2) = (r+g+b)/3;
        }
    }
    //    3.Then, convert it to texture_2D
    //Image -> Texture2d
    Texture2D* texture_2D= new Texture2D();
    texture_2D->initWithImage(img);
    //    4.And Finally back to Sprite
    return Sprite::createWithTexture(texture_2D);
}

Sprite* GameManager::getSpriteShapeSolidColorSprite(const char* src, Color3B color){
    Image *img=  new Image();
    img->initWithImageFile(src);

    int x=3;
    if(img->hasAlpha())
        x=4;
    
    unsigned char *data = new unsigned char[img->getDataLen()*x];
    data = img->getData();
    
    for(int i=0;i<img->getWidth();i++)
    {
        for(int j=0;j<img->getHeight();j++)
        {
            unsigned char *pixel = data + (i + j * img->getWidth()) * x;
            
            if(img->hasAlpha())
            {
                if(*(pixel + 3) == 0){
                    continue;
                }
            }
            *pixel = color.r;
            *(pixel + 1) = color.g;
            *(pixel + 2) = color.b;
        }
    }
    Texture2D* texture_2D= new Texture2D();
    texture_2D->initWithImage(img);

    return Sprite::createWithTexture(texture_2D);
}
void GameManager::SendAchievement(const char* text, int count, int goal){
    const char* theText;
    if (GameManager::getInstance()->market == MARKET_MAC) {
        theText = __String::createWithFormat("%s_Mac", text)->getCString();
    }else{
        theText = text;
    }
    //GoogleBilling::Billing::updateAchievement(text, count, goal);
}
void GameManager::showVideo(int index){
//    if (!HttpsManager::getInstance()->isConnected) {
//        
//        return;
//    }
    int videoCounter = getWorld()->getQuestProgress(0);
    if (videoCounter < getWorld()->getQuestMaxCount(0)) {
        videoCounter++;
        getWorld()->setQuestProgress(0, videoCounter);
    }
    videoAdsIndex = index;
    NativeInterface::NativeInterface::showUnityAdsVideo(); // test
//    showVideoDone();    // test 
    return;
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    int applovinPer = HttpsManager::getInstance()->appLovinPercent;
    int unityPer = HttpsManager::getInstance()->unityPercent;
    int total = applovinPer + unityPer;
    int randomValue = rand()%total;
    log("unity ads: %d, applovin: %d, randomValue: %d", unityPer, applovinPer, randomValue);
    if (randomValue <= unityPer) {
        log("unity ads play");
        NativeInterface::NativeInterface::showUnityAdsVideo();
    }else{
        log("applovin play");
//        NativeInterface::NativeInterface::showAppLovinVideo();
    }
    
}
void GameManager::showVideoDone(){
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    world->onVideoDone();
}

bool GameManager::checkDataSecure(){
    
    return true;
}
void GameManager::showVideoFailed(){
    
//    ((TitleLayer*)titleLayer)->showVideoFailed();
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
void GameManager::clickAdsFailed(){
    
}
void GameManager::clickAdsDone(){
    
}
/*
RenderTexture* GameManager::createAdditiveBorder( Sprite* label, int size, Color3B color, GLubyte opacity )
{
    RenderTexture* rt = RenderTexture::create(label->getTexture()->getContentSize().width + size * 2,
                                                  label->getTexture()->getContentSize().height+size * 2
                                                  );
    
    Point originalPos = label->getPosition();
    
    Color3B originalColor = label->getColor();
    
    GLubyte originalOpacity = label->getOpacity();
    
    bool originalVisibility = label->isVisible();
    
    label->setColor(color);
    
    label->setOpacity(opacity);
    
    label->setVisible(true);
    
    ccBlendFunc originalBlend = label->getBlendFunc();
    
    ccBlendFunc bf = {GL_SRC_ALPHA, GL_ONE}; // GL_SRC_ALPHA for glow effect
    label->setBlendFunc(bf);
    
    Point bottomLeft = Point(
                             label->getTexture()->getContentSize().width * label->getAnchorPoint().x + size,
                             label->getTexture()->getContentSize().height * label->getAnchorPoint().y + size);
    
    Point positionOffset= Point(
                                label->getTexture()->getContentSize().width  * label->getAnchorPoint().x - label->getTexture()->getContentSize().width / 2,
                                label->getTexture()->getContentSize().height * label->getAnchorPoint().y - label->getTexture()->getContentSize().height / 2);
    
    
    Point position = ccpSub(originalPos, positionOffset);
    
    //rt->getSprite()->getTexture()->setAntiAliasTexParameters();
    rt->begin();
    
    for (int i=0; i<360; i+= 15) // you should optimize that for your needs
    {
        label->setPosition(Point(bottomLeft.x + sin(CC_DEGREES_TO_RADIANS(i))*size, bottomLeft.y + cos(CC_DEGREES_TO_RADIANS(i))*size)
                           );
        label->visit();
    }
    rt->end();
    
    label->setPosition(originalPos);
    label->setColor(originalColor);
    label->setBlendFunc(originalBlend);
    label->setVisible(originalVisibility);
    label->setOpacity(originalOpacity);
    
    rt->setPosition(position);
    
    return rt;
}


RenderTexture* GameManager::createAdditive( Sprite* label, Color3B color, GLubyte opacity, int additiveCount)
{
    RenderTexture* rt = RenderTexture::create(label->getTexture()->getContentSize().width ,
                                                  label->getTexture()->getContentSize().height);
    
    Point originalPos = label->getPosition();
    
    Color3B originalColor = label->getColor();
    
    GLubyte originalOpacity = label->getOpacity();
    
    bool originalVisibility = label->isVisible();
    
    label->setColor(color);
    
    label->setOpacity(opacity);
    
    label->setVisible(true);
    
    ccBlendFunc originalBlend = label->getBlendFunc();
    
    ccBlendFunc bf = {GL_SRC_ALPHA, GL_ONE}; // GL_SRC_ALPHA for glow effect
    label->setBlendFunc(bf);
    
    Point bottomLeft = Point(
                             label->getTexture()->getContentSize().width * label->getAnchorPoint().x,
                             label->getTexture()->getContentSize().height * label->getAnchorPoint().y);
    
    Point positionOffset= Point(
                                label->getTexture()->getContentSize().width  * label->getAnchorPoint().x - label->getTexture()->getContentSize().width / 2,
                                label->getTexture()->getContentSize().height * label->getAnchorPoint().y - label->getTexture()->getContentSize().height / 2);
    
    
    Point position = originalPos - positionOffset;
    
    //rt->getSprite()->getTexture()->setAntiAliasTexParameters();
    rt->begin();
    
    label->setPosition(Point(bottomLeft.x, bottomLeft.y));
    for(int i = 0;i < additiveCount;i++){
        label->visit();
    }
    
    rt->end();
    
    label->setPosition(originalPos);
    label->setColor(originalColor);
    label->setBlendFunc(originalBlend);
    label->setVisible(originalVisibility);
    label->setOpacity(originalOpacity);
    
    rt->setPosition(position);
    
    return rt;
}
*/

void GameManager::initAchievement(){
    setAchievementGoal(ACHIEVEMENT_STAR_COLLECTER, 20);
    setAchievementGoal(ACHIEVEMENT_KEEPER, 1);
    setAchievementGoal(ACHIEVEMENT_FIRST_FRUIT, 1);
    setAchievementGoal(ACHIEVEMENT_RICH_MAN, 10000);
    setAchievementGoal(ACHIEVEMENT_KILLER, 5000);
    setAchievementGoal(ACHIEVEMENT_HOT_KETTLE, 20);
    setAchievementGoal(ACHIEVEMENT_WHOS_BOSS, 10);
    setAchievementGoal(ACHIEVEMENT_SOUL_LEGEND, 1);
}

//__String* GameManager::getInAppItemPrice(const char* name){
//    CCObject* obj = priceArray->objectForKey(name);
//    return (__String*)obj;
//}

//void GameManager::setInAppItemPrice(const char* name, const char* price){
//    GameManager::getInstance()->priceArray->setObject(__String::create(price), name);
//    log("cpp price: %s", ((__String*)GameManager::getInstance()->priceArray->objectForKey(name))->getCString());
//}
void GameManager::setAchievementGoal(int achievementIndex, int goalCount){
    char buf[50];
    sprintf(buf,"Achievement_goal%d" , achievementIndex);
    UserDefault::getInstance()->setIntegerForKey(buf, goalCount);
    saveCoin();
}
bool GameManager::setAchievement(int achievementIndex, int countToAdd){
    char buf[50];
	sprintf(buf, "Achievement_goal%d", achievementIndex);
    int goalCount = UserDefault::getInstance()->getIntegerForKey(buf, 0);

    char buff[50];
	sprintf(buff, "Achievement_current%d", achievementIndex);
    int original = UserDefault::getInstance()->getIntegerForKey(buff, 0);
    int currentCount = countToAdd;//
    
    if (currentCount > goalCount) {
        currentCount = goalCount;
    }
    UserDefault::getInstance()->setIntegerForKey(buff, currentCount);
    return goalCount <= currentCount && original < currentCount;
}
int GameManager::getAchievementGoalCount(int achievementIndex){
    char buf[50];
	sprintf(buf, "Achievement_goal%d", achievementIndex);
    return UserDefault::getInstance()->getIntegerForKey(buf, 0);
}
int GameManager::getAchievementCurrentCount(int achievementIndex){
    char buf[50];
	sprintf(buf, "Achievement_current%d", achievementIndex);
    return UserDefault::getInstance()->getIntegerForKey(buf, 0);
}
int GameManager::getIndexOf(std::vector<int> vec, int index){
    for (int i = 0; i < vec.size(); i++) {
        if(vec.at(i) == index){
            return i;
        }
    }
    return -1;
}

void GameManager::setLoadedData(const char* key, const char* value){
    if(std::strcmp(value, "true") == 0 || std::strcmp(value, "false") == 0){
		UserDefault::getInstance()->setBoolForKey(key, cocos2d::Value(value).asBool());
    }else{
		UserDefault::getInstance()->setIntegerForKey(key, cocos2d::Value(value).asInt());
    }
    
    // Money
    /*if(std::strcmp(key, "GemCount") == 0){
        GameManager::getInstance()->setGem(Value(value).asInt());
        return;
    }else if(std::strcmp(key, "CoinCount") == 0){
        GameManager::getInstance()->setCoin(Value(value).asInt());
        return;
    }*/
    // laset clear
    /*if(std::strcmp(key, KEY_LAST_CLEAR_STAGE) == 0){
        UserDefault::getInstance()->setIntegerForKey(KEY_LAST_CLEAR_STAGE, Value(value).asInt());
        return;
    }
    
    // Exp
    if(std::strcmp(key, "GExp") == 0){
        UserDefault::getInstance()->setIntegerForKey("GExp", Value(value).asInt());
        return;
    }
    
    // Stage
    for (int th = 0; th < totalThemeCount; th++) {
        for (int st = 0; st < 24; st++) {
            // star
            char buf[50];
            sprintf(buf, KEY_STAR_COUNT_FORMAT, th, st);
            if (std::strcmp(key, buf) == 0) {
                UserDefault::getInstance()->setIntegerForKey(buf, Value(value).asInt());
                return;
            }
            
            // mastery
            char masterBuf[50];
            sprintf( masterBuf, KEY_STAGE_MASTER_FORMAT, th*24 + st);
            if (std::strcmp(key, masterBuf) == 0) {
                UserDefault::getInstance()->setIntegerForKey(masterBuf, Value(value).asInt());
                return;
            }
        }
    }
    // Pet
    for (int i = 0; i < 100; i++) {
        char pbuf[50];
        sprintf( pbuf, KEY_PET_FORMAT, i);
        if (std::strcmp(key, pbuf)) {
            UserDefault::getInstance()->setIntegerForKey(pbuf, Value(value).asInt());
            return;
        }
        
        
        char plbuf[50];
        sprintf( plbuf, KEY_PET_LEVEL_FORMAT, i);
        if (std::strcmp(key, plbuf)) {
            UserDefault::getInstance()->setIntegerForKey(plbuf, Value(value).asInt());
            return;
        }
    }
    
    // Character
    for (int i = 0; i < TOTAL_CHARACTER_COUNT; i++) {
        char cbuf[25];
        sprintf(cbuf, KEY_CHARACTER_PURCHASED_FORMAT, i);
        if (std::strcmp(key, cbuf)) {
            UserDefault::getInstance()->setBoolForKey(cbuf, Value(value).asBool());
            return;
        }
    }
    
    // Weapon
    char wbuf[25];
    for (int i = 0; i < TOTAL_WEAPON_COUNT; i++) {
        sprintf(wbuf, KEY_WEAPON_PURCHASED_FORMAT, i);
        if (std::strcmp(key, wbuf) == 0) {
            UserDefault::getInstance()->setBoolForKey(wbuf, Value(value).asBool());
            return;
        }
        
        // Weapon Power
        if (std::strcmp(key, __String::createWithFormat(KEY_WEAPON_POWER_LEVEL, i)->getCString()) == 0) {
            UserDefault::getInstance()->setIntegerForKey(__String::createWithFormat(KEY_WEAPON_POWER_LEVEL, i)->getCString(), Value(value).asInt());
            return;
        }
        
        // Weapon Critical
        if (std::strcmp(key, __String::createWithFormat(KEY_WEAPON_CRITICAL_LEVEL, i)->getCString()) == 0) {
            UserDefault::getInstance()->setIntegerForKey(__String::createWithFormat(KEY_WEAPON_CRITICAL_LEVEL, i)->getCString(), Value(value).asInt());
            return;
        }
    }
    */
}
void GameManager::reset(){
   

}
void GameManager::loadGameData(){
    
}
void GameManager::saveGameData(){
    
}

const char* GameManager::getAchievementId(int achievementIndex){
	return "achievementID";
}
void GameManager::saveCoin(){

}

/*
 switch (this->getTag()) {
 case PET_NONE:
 
 case PET_BOOGI:
 
 case PET_BURI:
 
 case PET_CHUS:
 
 case PET_DORA_0:
 
 case PET_DORA_1:
 
 case PET_DORA_2:
 
 case PET_DORA_3:
 
 case PET_DORIS_0:
 
 case PET_DORIS_1:
 
 case PET_DORIS_2:
 
 case PET_DORIS_3:
 
 case PET_FRITH_0:
 
 case PET_FRITH_1:
 
 case PET_FRITH_2:
 
 case PET_FRITH_3:
 
 case PET_KIWI:
 
 case PET_KUMURI:
 
 case PET_LEXY:
 
 case PET_LINDIS_0:
 
 case PET_LINDIS_1:
 
 case PET_LINDIS_2:
 
 case PET_LINDIS_3:
 
 case PET_NEPTUN:
 
 case PET_OXTO:
 
 case PET_PLUTO:
 
 case PET_RIUS:
 
 case PET_ROLO_0:
 
 case PET_ROLO_1:
 
 case PET_ROLO_2:
 
 case PET_ROLO_3:
 
 case PET_RURIK_0:
 
 case PET_RURIK_1:
 
 case PET_RURIK_2:
 
 case PET_RURIK_3:
 
 case PET_TERRY:
 
 case PET_TORI:
 
 case PET_TORORI:
 
 case PET_UNO:
 
 case PET_VETH:
 
 case PET_WALOONG_0:
 
 
 case PET_WALOONG_1:
 
 case PET_WALOONG_2:
 
 case PET_WALOONG_3:
 
 default:
 
 break;
 }
 */
void GameManager::scrollTheLayer(ScrollView* scrollLayer, bool isLeft, bool isHorizontal, int howMuch){
    Point pos;
    if(isHorizontal){
        float x = scrollLayer->getInnerContainer()->getPosition().x + scrollLayer->getContentSize().width*(isLeft?1:-1)*howMuch;
        if(x > 0){
            x = 0;
        }else if(x < -(scrollLayer->getInnerContainerSize().width - scrollLayer->getContentSize().width)){
            x = -(scrollLayer->getInnerContainerSize().width - scrollLayer->getContentSize().width);
        }
        scrollLayer->getInnerContainer()->runAction(MoveTo::create(0.3, Point(x, scrollLayer->getInnerContainer()->getPosition().y)));
    }else{
        
    }
}
Layer* GameManager::getTitleLayer(){
    return titleLayer;
}
void GameManager::setFontSize(Label* lbl, float size){
    TTFConfig config = lbl->getTTFConfig();
    config.fontSize = size;
    lbl->setTTFConfig(config);
}
Color3B GameManager::getRandomColor(){
    int ranNum = cocos2d::random()%6;
    switch (ranNum) {
        case 0:
            return Color3B(255, cocos2d::random()%255, cocos2d::random()%255);
        case 1:
            return Color3B(cocos2d::random()%255, 255, cocos2d::random()%255);
        case 2:
            return Color3B(cocos2d::random()%255, cocos2d::random()%255, 255);
        case 3:
            return Color3B(255, 255, cocos2d::random()%255);
        case 4:
            return Color3B(cocos2d::random()%255, 255, 255);
        case 5:
            return Color3B(255, cocos2d::random()%255, 255);
        default:
            return Color3B::WHITE;
    }
}
cocos2d::Point GameManager::getRandomPointFromRect(cocos2d::Rect rect){
    return Point(rect.origin.x + rand()%((int)rect.size.width), rect.origin.y + rand()%((int)rect.size.height));
}
void GameManager::alignToCenter(Node* node0, Node* node1, float gap, float centerX, float offsetX){
    float totalWidth = node0->getContentSize().width*node0->getScaleX() + node1->getContentSize().width*node1->getScaleX() + gap;
//    float centerX = centerNode->getPosition().x;
    node0->setPositionX(centerX - totalWidth/2 + node0->getContentSize().width*node0->getScaleX()/2 + offsetX);
    node1->setPositionX(centerX - totalWidth/2 + node0->getContentSize().width*node0->getScaleX() + gap + node1->getContentSize().width*node1->getScaleX()/2 + offsetX);
}

void GameManager::alertToUser(std::string str){
    log("*what");
    log("%s", str.c_str());
    getWorld()->showMessage(str);
}
bool GameManager::isPaidGame(){
    return false; // test now
}
// admob
void GameManager::adViewDidReceiveAd(const std::string &name) {
    log("adViewDidReceiveAd: %s", name.c_str());
    if (!GameManager::getInstance()->isPaidGame()) {
        if(sdkbox::PluginAdMob::isAvailable("banner")){
            sdkbox::PluginAdMob::show("banner");
        }
    }
}
void GameManager::adViewDidFailToReceiveAdWithError(const std::string &name, const std::string &msg) {
    log("adViewDidFailToReceiveAdWithError: %s, msg: %s", name.c_str(), msg.c_str());
        sdkbox::PluginAdMob::cache(name);
}
void GameManager::adViewWillPresentScreen(const std::string &name) {log("adViewWillPresentScreen: %s", name.c_str());}
void GameManager::adViewDidDismissScreen(const std::string &name) {
    log("adViewDidDismissScreen: %s", name.c_str());
    
}
void GameManager::adViewWillDismissScreen(const std::string &name) {
    log("adViewWillDismissScreen: %s", name.c_str());
    sdkbox::PluginAdMob::cache(name);
}
void GameManager::adViewWillLeaveApplication(const std::string &name) {log("adViewWillLeaveApplication: %s", name.c_str());}
void GameManager::reward(const std::string &name, const std::string &currency, double amount) {
    log("name: %s, currency: %s, amount: %d", name.c_str(), currency.c_str(), (int)amount);
    showVideoDone();
}


// iap methods
void GameManager::onSuccess(sdkbox::Product const& product) {
    log("onSuccess");
    if(product.name.compare("tsprotect_1") == 0){
        rewardIAP(IAP_STRONG_ODIN);
        NativeInterface::NativeInterface::trackEvent("shop", "오딘의 강력한 보호", "", "", "iap", 1);
    }else if(product.name.compare("tsgem_600") == 0){
        rewardIAP(IAP_GEM_0);
        NativeInterface::NativeInterface::trackEvent("shop", "보석 600", "", "", "iap", 1);
    }else if(product.name.compare("tsgem_1300") == 0){
        rewardIAP(IAP_GEM_1);
        NativeInterface::NativeInterface::trackEvent("shop", "보석 1300", "", "", "iap", 1);
    }else if(product.name.compare("tsgem_7000") == 0){
        rewardIAP(IAP_GEM_2);
        NativeInterface::NativeInterface::trackEvent("shop", "보석 7000", "", "", "iap", 1);
    }else if(product.name.compare("tsgem_15000") == 0){
        rewardIAP(IAP_GEM_3);
        NativeInterface::NativeInterface::trackEvent("shop", "보석 15000", "", "", "iap", 1);
    }else if(product.name.compare("specialgem_600") == 0){
        rewardIAP(IAP_GEM_SPECIAL_600);
        NativeInterface::NativeInterface::trackEvent("shop", "특가 보석 600", "", "", "iap", 1);
    }else if(product.name.compare("specialgem_1000") == 0){
        rewardIAP(IAP_GEM_SPECIAL_1000);
        NativeInterface::NativeInterface::trackEvent("shop", "특가 보석 1000", "", "", "iap", 1);
    }else if(product.name.compare("bluebird_pkg") == 0){
        rewardIAP(IAP_BLUE_BIRD);
        NativeInterface::NativeInterface::trackEvent("shop", "행운의 파랑새", "", "", "iap", 1);
    }else if(product.name.compare("puppy_pkg") == 0){
        rewardIAP(IAP_PUPPY);
        NativeInterface::NativeInterface::trackEvent("shop", "행복의 강아지", "", "", "iap", 1);
    }
    saveCoin();
    
    //    setIAPSuccess(index);
    UserDefault::getInstance()->flush();
}
void GameManager::onFailure(sdkbox::Product const& p, const std::string &msg){
    log("onFailure: %s", msg.c_str());
    std::size_t found = msg.find("Already");
    if (found!=std::string::npos){
        //        MessageBox("Try to restore purchases", "Already Owned");
        log("Owned");
    }else{
        log("Not owned");
    }
}
void GameManager::onCanceled(sdkbox::Product const& p) {
    log("onCanceled");
    
}
void GameManager::onRestored(sdkbox::Product const& p) {
    log("onRestored");
    
    if(p.name.compare("removeads") == 0){
        log("onRestored removeads");
        //        setIAPSuccess(0);
    }
}
void GameManager::onProductRequestSuccess(std::vector<sdkbox::Product> const &products){
    log("onProductRequestSuccess");
    for(int i = 0;i < products.size();i++){
        sdkbox::Product product = products.at(i);
        log("product id: %s", product.id.c_str());
        log("product name: %s", product.name.c_str());
        log("product price: %s, value: %f", product.price.c_str(), product.priceValue);
        
        if(product.name.compare("tsgem_600") == 0){
            strPrices[0] = product.price;
        }else if(product.name.compare("tsgem_1300") == 0){
            strPrices[1] = product.price;
        }else if(product.name.compare("tsgem_7000") == 0){
            strPrices[2] = product.price;
        }else if(product.name.compare("tsgem_15000") == 0){
            strPrices[3] = product.price;
        }else if(product.name.compare("specialgem_600") == 0){
            strPrices[4] = product.price;
        }else if(product.name.compare("specialgem_1000") == 0){
            strPrices[5] = product.price;
        }else if(product.name.compare("tsprotect_1") == 0){
            strPrices[6] = product.price;
        }else if(product.name.compare("bluebird_pkg") == 0){
            strPrices[7] = product.price;
        }else if(product.name.compare("puppy_pkg") == 0){
            strPrices[8] = product.price;
        }
        //        else if(product.name.compare("starter_1") == 0){
        //            strPrices[5] = product.price;
        //            log("starter_1 ads price");
        //        }else if(product.name.compare("starter_2") == 0){
        //            strPrices[6] = product.price;
        //            log("starter_2 ads price");
        //        }else if(product.name.compare("starter_3") == 0){
        //            strPrices[7] = product.price;
        //            log("starter_3 ads price");
        //        }
    }
    log("onProductRequestSuccess complete" );
}

void GameManager::onProductRequestFailure(const std::string &msg) {
    log("onProductRequestFailure: %s", msg.c_str());
}
void GameManager::onRestoreComplete(bool ok, const std::string &msg){
    log("onRestoreComplete: %d, %s", (int)ok, msg.c_str());
}
void GameManager::onInitialized(bool ok){
    log("onInitialized: %d", (int)ok);
}
