#include "AppDelegate.h"
#include "HelloWorldScene.h"
#ifdef SDKBOX_ENABLED
#include "PluginFacebook/PluginFacebook.h"
#endif
#ifdef SDKBOX_ENABLED
#include "PluginAdMob/PluginAdMob.h"
#endif
#include "GameManager.h"
#ifdef SDKBOX_ENABLED
#include "PluginSdkboxPlay/PluginSdkboxPlay.h"
#include "PluginIAP/PluginIAP.h"
#endif
#include "IntroScene.h"
#include "LogoSplash.h"
USING_NS_CC;

static cocos2d::Size designResolutionSize = cocos2d::Size(750, 1334);
static cocos2d::Size smallResolutionSize = cocos2d::Size(750, 1334);
static cocos2d::Size mediumResolutionSize = cocos2d::Size(750, 1334);
static cocos2d::Size largeResolutionSize = cocos2d::Size(750, 1334);

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages, 
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {

    // iap verify receipt
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    sdkbox::init("85f1fa2732ea32d7d518c74806135be3", "2e7a1bb2be90b4f2", "apple");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    sdkbox::init("4c5802bbe20bd8176413e140cbaaaf8c", "a7745553ec52acd0", "googleplay");
#endif
    // iap verify receipt
    sdkbox::IAP::enableUserSideVerification(true);
    sdkbox::IAP::setDebug(false);
    
#ifdef SDKBOX_ENABLED
    sdkbox::PluginAdMob::init();
#endif
#ifdef SDKBOX_ENABLED
    sdkbox::PluginSdkboxPlay::init();
    sdkbox::PluginSdkboxPlay::signin();
    log("sdkboxplay done");
    sdkbox::IAP::setListener(GameManager::getInstance());
    
    sdkbox::IAP::init();
    
    sdkbox::PluginFacebook::init();
#endif
    
//#ifdef SDKBOX_ENABLED
    
//#endif
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        glview = GLViewImpl::createWithRect("TapTapSabana", cocos2d::Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
#else
        glview = GLViewImpl::create("TapTapSabana");
#endif
        director->setOpenGLView(glview);
    }
    
    FileUtils::getInstance()->addSearchPath("res");
    FileUtils::getInstance()->addSearchPath("UI");
    FileUtils::getInstance()->addSearchPath("fonts");
    FileUtils::getInstance()->addSearchPath("spine");
    FileUtils::getInstance()->addSearchPath("batchs");
    FileUtils::getInstance()->addSearchPath("sound");
    FileUtils::getInstance()->addSearchPath("animation");
    
    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);
    GameManager::getInstance()->loadCSVFile();
    GameManager::getInstance()->refreshOrientationNeeded = true;
    glview->setDesignResolutionSize(designResolutionSize.height, designResolutionSize.width, ResolutionPolicy::FIXED_HEIGHT);
    
    auto frameSize = glview->getFrameSize();
    // if the frame's height is larger than the height of medium size.
    if (frameSize.height > mediumResolutionSize.height)
    {
        director->setContentScaleFactor(MIN(largeResolutionSize.height/designResolutionSize.height, largeResolutionSize.width/designResolutionSize.width));
    }
    // if the frame's height is larger than the height of small size.
    else if (frameSize.height > smallResolutionSize.height)
    {
        director->setContentScaleFactor(MIN(mediumResolutionSize.height/designResolutionSize.height, mediumResolutionSize.width/designResolutionSize.width));
    }
    // if the frame's height is smaller than the height of medium size.
    else
    {
        director->setContentScaleFactor(MIN(smallResolutionSize.height/designResolutionSize.height, smallResolutionSize.width/designResolutionSize.width));
    }
    GameManager::getInstance()->initGameManager();

    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile("rockBg.plist");
    cache->addSpriteFramesWithFile("mainTree.plist");
    cache->addSpriteFramesWithFile("star.plist");
    cache->addSpriteFramesWithFile("tree.plist");
    cache->addSpriteFramesWithFile("frontRock.plist");
    cache->addSpriteFramesWithFile("meerkat.plist");
    cache->addSpriteFramesWithFile("monkey.plist");
    cache->addSpriteFramesWithFile("bush.plist");
    register_all_packages();

    applicationScreenSizeChanged(frameSize.width, frameSize.height);

    // create a scene. it's an autorelease object
    auto scene = IntroScene::createScene();
//    auto scene = LogoSplash::createScene();
    
    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();
    UserDefault::getInstance()->flush();
    
    GameManager::getInstance()->getWorld()->setAbsentRewardTime();
    GameManager::getInstance()->getWorld()->updateNotification();
    log("updateNotification: appdelegate");
    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    GameManager::getInstance()->getWorld()->giveBackgroundReward();
    GameManager::getInstance()->getWorld()->checkAbsentAttackRequested = true;
    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}

void AppDelegate::applicationScreenSizeChanged(int width, int height){
    log("screen size:%d, %d", width, height);
    GameManager::getInstance()->isPortrait = width < height;
    log("appdel isPortrait? %d", GameManager::getInstance()->isPortrait);
    if(GameManager::getInstance()->isInIntro && GameManager::getInstance()->intro != nullptr){
        GameManager::getInstance()->intro->updateOrientation();
    }
    if(GameManager::getInstance()->isInLogo && GameManager::getInstance()->logoSplash != nullptr){
        GameManager::getInstance()->logoSplash->updateOrientation();
        return;
    }
    
    GameManager::getInstance()->refreshOrientationNeeded = true;
    
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        glview = GLViewImpl::createWithRect("Zoobyssrium", cocos2d::Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
#else
        glview = GLViewImpl::create("Zoobyssrium");
#endif
        director->setOpenGLView(glview);
    }
    // Set the design resolution
    if (width < height) {
//        glview->setFrameSize(designResolutionSize.width, designResolutionSize.height);
        glview->setFrameSize(width, height);
        glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::FIXED_HEIGHT);
    }else{
//        glview->setFrameSize(designResolutionSize.height, designResolutionSize.width);
        glview->setFrameSize(width, height);
        glview->setDesignResolutionSize(designResolutionSize.height, designResolutionSize.width, ResolutionPolicy::FIXED_HEIGHT);
    }
    
    
    auto frameSize = glview->getFrameSize();
    // if the frame's height is larger than the height of medium size.
    if (frameSize.height > mediumResolutionSize.height)
    {
        director->setContentScaleFactor(MIN(largeResolutionSize.height/designResolutionSize.height, largeResolutionSize.width/designResolutionSize.width));
    }
    // if the frame's height is larger than the height of small size.
    else if (frameSize.height > smallResolutionSize.height)
    {
        director->setContentScaleFactor(MIN(mediumResolutionSize.height/designResolutionSize.height, mediumResolutionSize.width/designResolutionSize.width));
    }
    // if the frame's height is smaller than the height of medium size.
    else
    {
        director->setContentScaleFactor(MIN(smallResolutionSize.height/designResolutionSize.height, smallResolutionSize.width/designResolutionSize.width));
    }
    
//    setupResolutionPolicy(width, height);
    
    register_all_packages();
    
    if(GameManager::getInstance()->getWorld() != nullptr && GameManager::getInstance()->getWorld()->prologueLayer != nullptr){
        GameManager::getInstance()->getWorld()->updateOrientation();
    }
}

void AppDelegate::setupResolutionPolicy(float designW, float designH)
{
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    Size screenSize = glview->getFrameSize();
    
    float designRatio = designW / designH;
    float screenRatio = screenSize.height / screenSize.width;
    
    ResolutionPolicy resolutionPolicy = screenRatio > designRatio ?
    ResolutionPolicy::FIXED_HEIGHT : ResolutionPolicy::FIXED_WIDTH;
    
    glview->setDesignResolutionSize(designW, designH, resolutionPolicy);
}


