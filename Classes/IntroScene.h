#ifndef __INTROSCENE_SCENE_H__
#define __INTROSCENE_SCENE_H__

#include "cocos2d.h"
#include "BigNum.h"
#include "Character.h"
#include "ui/UIHelper.h"
#include "ui/UIButton.h"
#include "ui/UIListView.h"
#include "ui/UIText.h"
#include "ui/UITextField.h"
#include "ui/UIText.h"
#include "ui/UISlider.h"
#include "ui/UIImageView.h"
#include "ui/UILoadingBar.h"
#include "cocostudio/CocoStudio.h"
#include <editor-support/spine/spine-cocos2dx.h>
#include <editor-support/spine/SkeletonAnimation.h>
using namespace spine;
using namespace std;

USING_NS_CC;
using namespace cocos2d::ui;
class IntroScene : public cocos2d::Layer
{
    cocos2d::Size size;
    Vector<Character*> animalList;
    Node* backgroundNode;
    Vector<Sprite*> backgroundItems0;
    Vector<Sprite*> backgroundItems1;
    Vector<Sprite*> backgroundItems2;
    Vector<Sprite*> backgroundItems3;
    Vector<Sprite*> backgroundItems4;
    Vector<Sprite*> backgroundItems5;
    float animalCreatingTimeElapse = 0.3f;
    Character* theApe;
    Character* horse0;
    Character* horse1;
    void startHorseAni(float dt);
    void startHorseAni2(float dt);
    void stopHorse(float dt);
    void statApeAni(float dt);
    void showAnimals();
    void hideAnimals();
    Character* hunter;
    Character* lion;
    Sprite* bigRock;
    Sprite* skyAndSea;
    Label* lblTouchToContinue;
    Sprite* logo;
    void runBackground(Vector<Sprite*> list, float speed, float dt);
    bool isBackgroundRunning;
    float animalTurningTime = 2.0f;
    float animalPeaceTime = 4.5f;
    float hunterRuningTime = 2.5f;
    float animalAdditionalPeaceTime = 1.0f;
    int turningAnimalIndex = 0;
    Scene* worldScene;
    Layer* fieldLayer;
    void createExtraMountain();
public:
    void updateOrientation();
    void hideLogo();
    void showIntro();
    bool inInLogo = true;
    Layer* logoLayer;
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(IntroScene);
    void addListener();
    void onTouchesEnded(const std::vector<Touch*>& touches, Event* event);
    void stopBackgroundsRun();
    void startBackgroundsRun();
    void runningBackground(float dt);
    void startTurningAnimals();
    void turningAnimals(float dt);
    void startAnimalsRunning();
    void removeBackgroundItems(Vector<Sprite*> list);
    void onStartRockAnimation();
    void startLabelBlink();
    void stopBackgroundRun();
    void animalCreating(float dt);
    bool isIntroEnded = false;
    void skip();
    void hideHunter();
    Node* loadedLayer;
};

#endif // __INTROSCENE_SCENE_H__
