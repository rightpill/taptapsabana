//
//  LogoSplash.cpp
//  Tapsabana-mobile
//
//  Created by PACKSUNG PILL on 27/12/2017.
//

#include "LogoSplash.h"
#include "GameManager.h"
Scene* LogoSplash::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = LogoSplash::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool LogoSplash::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
//    GameManager::getInstance()->logoSplash = this;
//    GameManager::getInstance()->isInLogo = true;
    Size size = Director::getInstance()->getWinSize();
    
    LayerColor* layer = LayerColor::create(Color4B(255, 255, 255, 255));
    this->addChild(layer);
    this->setAnchorPoint(Point::ZERO);
    
    Sprite* sptPublisher = Sprite::create("purple9.png");
    this->addChild(sptPublisher);
    sptPublisher->setOpacity(0);
    sptPublisher->setPosition(size/2);
    sptPublisher->setScale(4);
    sptPublisher->runAction(Sequence::create(FadeIn::create(1), DelayTime::create(1), FadeOut::create(1), CallFunc::create(CC_CALLBACK_0(LogoSplash::goToNext, this)),NULL));
    
    log("logo: %s, font: %s", GameManager::getInstance()->getText("data warn").c_str(), GameManager::getInstance()->getLocalizedFont());
    Label* lblDataWarn = Label::createWithTTF(GameManager::getInstance()->getText("data warn"), GameManager::getInstance()->getLocalizedFont(), 30);
    this->addChild(lblDataWarn);
    lblDataWarn->setWidth(size.width - 20);
    lblDataWarn->setPosition(Point(size.width/2, lblDataWarn->getContentSize().height));
    lblDataWarn->setTextColor(Color4B::BLACK);
 
    return true;
}
void LogoSplash::goToNext(){
    log("goTonext");
    GameManager::getInstance()->isInLogo = false;
    auto scene = IntroScene::createScene();
    log("goTonext 1");
    Director::getInstance()->replaceScene(scene);
    log("goTonext 2");
}

void LogoSplash::updateOrientation(){
    Size size = Director::getInstance()->getWinSize();
    if(!GameManager::getInstance()->isPortrait){
        if(size.height > size.width){
//            size = Size(size.height, size.width);
        }
        this->setRotation(-90);
        this->setPositionX(size.height);
        
    }else{
        this->setRotation(0);
        this->setPositionX(0);
    }
}
