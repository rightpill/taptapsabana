//
//  IntroScene.cpp
//  Tapsabana
//
//  Created by PACKSUNG PILL on 3/16/17.
//
//

#include "IntroScene.h"

#include "HelloWorldScene.h"
#include "TimeManager.h"
#include "SimpleAudioEngine.h"

#include "GameManager.h"
USING_NS_CC;

Scene* IntroScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = IntroScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool IntroScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    GameManager::getInstance()->intro = this;
    GameManager::getInstance()->isInIntro = true;
    worldScene = HelloWorldScene::createScene();
    worldScene->retain();
    
    size = Director::getInstance()->getWinSize();
    
    logoLayer = LayerColor::create(Color4B(255, 255, 255, 255));
    this->addChild(logoLayer);
    logoLayer->setAnchorPoint(Point::ZERO);
    logoLayer->setRotation(-90);
    logoLayer->setPositionX(size.height);
    
    GameManager::getInstance()->isPortrait = size.height > size.width;
    if(size.height > size.width){
        size = Size(size.height, size.width);
    }
    this->setAnchorPoint(Point(0, 0));
    
    
    Sprite* sptPublisher = Sprite::create("purple9.png");
    logoLayer->addChild(sptPublisher);
    sptPublisher->setOpacity(0);
    sptPublisher->setPosition(Point(size.height/2, size.width/2));
    sptPublisher->setScale(4);
    sptPublisher->runAction(Sequence::create(FadeIn::create(1), DelayTime::create(1), FadeOut::create(1), DelayTime::create(1), CallFunc::create(CC_CALLBACK_0(IntroScene::hideLogo, this)),NULL));
    
    log("logo: %s, font: %s", GameManager::getInstance()->getText("data warn").c_str(), GameManager::getInstance()->getLocalizedFont());
    Label* lblDataWarn = Label::createWithTTF(GameManager::getInstance()->getText("data warn"), GameManager::getInstance()->getLocalizedFont(), 30);
    logoLayer->addChild(lblDataWarn);
    lblDataWarn->setWidth(size.height - 20);
    lblDataWarn->setPosition(Point(size.height/2, lblDataWarn->getContentSize().height));
    lblDataWarn->setTextColor(Color4B::BLACK);
    
    updateOrientation();
    
    addListener();
    return true;
}
void IntroScene::hideLogo(){
    logoLayer->setVisible(false);
    inInLogo = false;
    showIntro();
}
void IntroScene::showIntro(){
    bool isFirstTime = UserDefault::getInstance()->getBoolForKey("KEY_IS_FIRST_TIME", true);
    UserDefault::getInstance()->setBoolForKey("KEY_IS_FIRST_TIME", false);
    if(isFirstTime){
        Sprite* sky = Sprite::createWithSpriteFrameName("sky.png");
        sky->setAnchorPoint(Point(0, 0));
        sky->setPosition(Point(0, size.height/2 - 210));
        sky->setScaleX(1334);
        sky->setScaleY(1.9f);
        this->addChild(sky, -100);
        
        fieldLayer = Layer::create();
        this->addChild(fieldLayer, -99);
        
        
        CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("scene1.mp3", true);
        //
        //    Sprite* sunMoon = Sprite::createWithSpriteFrameName("sun.png");
        //    sunMoon->setPosition(Point(0, 320));
        //    sunMoon->setAnchorPoint(Point(0, -1));
        //    sunMoon->getTexture()->setAntiAliasTexParameters();
        //    Sprite* spt = Sprite::createWithSpriteFrameName("moon.png");
        //    sunMoon->addChild(spt);
        //    spt->setPosition(Point(sunMoon->getContentSize().width/2, -sunMoon->getContentSize().height*2.5f));
        //    spt->getTexture()->setAntiAliasTexParameters();
        //
        //
        Sprite* spt = Sprite::createWithSpriteFrameName("ground.png");
        fieldLayer->addChild(spt, -99);
        spt->setAnchorPoint(Point(0,0));
        spt->setScaleX(1334);
        spt->getTexture()->setAliasTexParameters();
        
        backgroundNode = Node::create();
        float x = 0;
        float y = size.height/2 - 14;
        int extraGapX = 700;
        int extraGapXVar = 700;
        int count = 1;
        while(true){
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("mountain%d.png", count));
            fieldLayer->addChild(spt);
            backgroundItems0.pushBack(spt);
            
            spt->setAnchorPoint(Point(0, 0));
            spt->setScale(2);
            spt->setPosition(Point(x, y));
            spt->setLocalZOrder(size.height - y);
            if(x > size.width){
                break;
            }
            x += spt->getContentSize().width + extraGapX + rand()%extraGapXVar;
            count++;
            if(count > 2){
                count = 1;
                break;
            }
        }
        
        
        y = size.height/2 - 14;
        count = 1;
        extraGapX = 200;
        extraGapXVar = 700;
        x = 0;
        while(true){
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("baobob%d_0.png", count));
            backgroundItems1.pushBack(spt);
            fieldLayer->addChild(spt);
            spt->setAnchorPoint(Point(0, 0));
            spt->setPosition(Point(x, y));
            spt->setLocalZOrder(size.height - y);
            if(x > size.width){
                break;
            }
            x += spt->getContentSize().width + extraGapX + rand()%extraGapXVar;
            count++;
            if(count > 3){
                count = 1;
            }
        }
        
        // 4th
        y = size.height/2 - 40;
        count = 1;
        extraGapX = 200;
        extraGapXVar = 200;
        x = 0;
        while(true){
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("bush_d_%d.png", count));
            backgroundItems2.pushBack(spt);
            fieldLayer->addChild(spt);
            spt->setAnchorPoint(Point(0, 0));
            spt->setPosition(Point(x, y));
            spt->setLocalZOrder(size.height - y);
            if(x > size.width){
                break;
            }
            x += spt->getContentSize().width + extraGapX + rand()%extraGapXVar;
            count++;
            if(count > 4){
                count = 1;
            }
        }
        
        y = size.height/2 - 140;
        count = 1;
        extraGapX = 10;
        extraGapXVar = 20;
        x = 0;
        while(true){
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("grass_c_%d.png", count));
            backgroundItems3.pushBack(spt);
            fieldLayer->addChild(spt);
            spt->setAnchorPoint(Point(0, 0));
            spt->setPosition(Point(x, y));
            spt->setLocalZOrder(size.height - y);
            if(x > size.width){
                break;
            }
            x += spt->getContentSize().width + extraGapX + rand()%extraGapXVar;
            count++;
            if(count > 6){
                count = 1;
            }
        }
        
        
        y = 140;
        count = 1;
        extraGapX = 10;
        extraGapXVar = 90;
        x = 0;
        while(true){
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("grass_b_%d.png", count));
            backgroundItems3.pushBack(spt);
            fieldLayer->addChild(spt);
            spt->setAnchorPoint(Point(0, 0));
            spt->setPosition(Point(x, y));
            spt->setLocalZOrder(size.height - y);
            if(x > size.width){
                break;
            }
            x += spt->getContentSize().width + extraGapX + rand()%extraGapXVar;
            count++;
            if(count > 6){
                count = 1;
            }
        }
        
        y = 10;
        count = 1;
        extraGapX = 10;
        extraGapXVar = 50;
        x = 0;
        while(true){
            spt = Sprite::createWithSpriteFrameName(StringUtils::format("grass_a_%d.png", count));
            backgroundItems3.pushBack(spt);
            fieldLayer->addChild(spt);
            spt->setAnchorPoint(Point(0, 0));
            spt->setPosition(Point(x, y));
            spt->setLocalZOrder(size.height - y);
            if(x > size.width){
                break;
            }
            x += spt->getContentSize().width + extraGapX + rand()%extraGapXVar;
            count++;
            if(count > 6){
                count = 1;
            }
        }
        
        loadedLayer = CSLoader::createNode("Intro.csb");
        loadedLayer->setContentSize(size);
        loadedLayer->setPosition(Point::ZERO);
        loadedLayer->setAnchorPoint(Point::ZERO);
        //    ui::Helper::doLayout(loadedLayer);
        this->addChild(loadedLayer, 10000);
        Node* dummyLion = loadedLayer->getChildByName("rockNode")->getChildByName("imgLion");
        lion = Character::create();
        lion->anim = SkeletonAnimation::createWithJsonFile("bigcat_1.json.txt", "bigcat_1.atlas", 1);
        lion->addChild(lion->anim);
        lion->anim->setAnimation(0, "idle", true);
        loadedLayer->getChildByName("rockNode")->addChild(lion);
        lion->setPosition(dummyLion->getPosition() + Point(0, -130));
        dummyLion->removeFromParent();
        
        // animals
        //    animalsNode = Node::create();
        Node* dummy;
        float xScale= 0.5f;
        float yScale= 0.5f;
        std::string aniName = "idle";
        this->scheduleOnce(schedule_selector(IntroScene::startHorseAni), 1.8f);
        this->scheduleOnce(schedule_selector(IntroScene::startHorseAni2), 5.1f);
        this->scheduleOnce(schedule_selector(IntroScene::stopHorse), 5.4f);
        for (int i = 0; i < 10; i++) {
            Character* ch = Character::create();
            animalList.pushBack(ch);
            fieldLayer->addChild(ch, 9999);
            aniName = "idle";
            dummy = loadedLayer->getChildByName(StringUtils::format("Image_%d", i));
            xScale= 0.5f;
            yScale= 0.5f;
            if (i == 0) {
                ch->setSpine("hors_1.json.txt", "hors_1.atlas");
                horse0 = ch;
                aniName = "alert";
            }else if (i == 1) {
                ch->setSpine("hors_1.json.txt", "hors_1.atlas");
                xScale = (-0.5f);
                horse1 = ch;
                aniName = "alert2";
            }else if (i == 2) {
                ch->setSpine("s_gazel_1.json.txt", "s_gazel_1.atlas");
                xScale = (-0.5f);
            }else if (i == 3) {
                ch->setSpine("ape_3.json.txt", "ape_3.atlas");
                aniName = "idle3";
            }else if (i == 4) {
                ch->setSpine("ape_3.json.txt", "ape_3.atlas");
                aniName = "idle3";
                theApe = ch;
                this->scheduleOnce(schedule_selector(IntroScene::statApeAni), 0.5f);
                xScale = (-0.5f);
            }else if (i == 5) {
                ch->setSpine("bull_1.json.txt", "bull_1.atlas");
                xScale = 0.5f;
                yScale = 0.5f;
                //            ch->setSpineAnimation(29);
            }else if (i == 6) {
                ch->setSpine("s_gazel_3.json.txt", "s_gazel_3.atlas");
                aniName = "idle";
            }else if (i == 7) {
                ch->setSpine("s_gazel_1.json.txt", "s_gazel_1.atlas");
                aniName = "rest";
            }else if (i == 8) {
                ch->setSpine("s_gazel_1.json.txt", "s_gazel_1.atlas");
                xScale = (-0.5f);
            }else if (i == 9) {
                ch->setSpine("s_gazel_1.json.txt", "s_gazel_1.atlas");
                aniName = "rest";
            }
            ch->runAnimation(aniName, true);
            ch->setPosition(dummy->getPosition());
            dummy->removeFromParent();
            ch->setLocalZOrder(size.height - ch->getPositionY());
            ch->anim->setScale(xScale, yScale);
        }
        //    this->addChild(animalsNode, 9999);
        
        hunter = Character::create();
        hunter->anim = SkeletonAnimation::createWithJsonFile("intro_guys.json.txt", "intro_guys.atlas", 1);
        hunter->addChild(hunter->anim);
        hunter->setScale(-1, 1);
        hunter->anim->setAnimation(0, "drivibg", true); // drivibg2
        
        //Sprite::createWithSpriteFrameName("duckRock0.png");
        float hunterX = size.width*3/4;
        float hunterY = 190;
        //    hunter->setPosition(Point(hunterX, hunterY));
        hunter->setPosition(Point(hunterX, -size.height));
        hunter->setLocalZOrder(size.height - hunter->getPositionY());
        this->addChild(hunter);
        
        //    skyAndSea = Sprite::createWithSpriteFrameName("sun.png");
        //    this->addChild(skyAndSea);
        
        //    lblTouchToContinue = Label::createWithTTF("Tap to continue", "fonts/arial.ttf", 30);
        //    this->addChild(lblTouchToContinue);
        
        //    logo = Sprite::createWithSpriteFrameName("treeFront.png");
        //    this->addChild(logo);
        //
        isBackgroundRunning = false;
        
        this->schedule(schedule_selector(IntroScene::runningBackground));
        
        
        
        
        // run animation
        animalPeaceTime = 4.5f;
        hunterRuningTime = 2.5f;
        animalAdditionalPeaceTime = 1.0f;
        animalTurningTime = 2.0f;
        float huntingTime = 5.5f;
        float rockMovingTime = 3.0f;
        for(auto animal : animalList){
            animal->runAction(Sequence::create(DelayTime::create(animalPeaceTime),
                                               //                                            MoveTo::create(0, Point(0, -size.height)),
                                               DelayTime::create(hunterRuningTime),
                                               //                                            MoveTo::create(0, Point(size.width/2 - 1334/2, 0)),
                                               DelayTime::create(animalAdditionalPeaceTime),
                                               CallFunc::create(CC_CALLBACK_0(IntroScene::startTurningAnimals, this)),
                                               DelayTime::create(animalTurningTime),
                                               //                                           MoveBy::create(0, Point(-size.width, 0)),
                                               //                                            CallFunc::create(CC_CALLBACK_0(IntroScene::startAnimalsRunning, this)),
                                               //                                            MoveBy::create(hunterRuningTime + rockMovingTime, Point(size.width, 0)),
                                               NULL));
        }
        this->runAction(Sequence::create(DelayTime::create(animalPeaceTime),
                                         CallFunc::create(CC_CALLBACK_0(IntroScene::hideAnimals, this)),
                                         DelayTime::create(hunterRuningTime),
                                         CallFunc::create(CC_CALLBACK_0(IntroScene::showAnimals, this)),
                                         DelayTime::create(animalAdditionalPeaceTime),
                                         CallFunc::create(CC_CALLBACK_0(IntroScene::startTurningAnimals, this)),
                                         DelayTime::create(animalTurningTime),
                                         CallFunc::create(CC_CALLBACK_0(IntroScene::startAnimalsRunning, this)),
                                         //                                       CallFunc::create(CC_CALLBACK_0(IntroScene::hideAnimals, this)),
                                         //                                       MoveTo::create(hunterRuningTime + rockMovingTime, Point(size.width, 0)),
                                         NULL));
        //    bigRock->runAction(Sequence::create(MoveTo::create(0, Point(-size.width/2, 0)),
        //                                        DelayTime::create(animalPeaceTime  + hunterRuningTime + animalAdditionalPeaceTime + animalTurningTime + hunterRuningTime),
        //                                        MoveTo::create(rockMovingTime, Point(size.width/2 + 300, size.height/2)), DelayTime::create(1), NULL));
        //    bigRock->runAction(Sequence::create(ScaleTo::create(0, 3),
        //                                        DelayTime::create(animalPeaceTime  + hunterRuningTime + animalAdditionalPeaceTime + animalTurningTime + hunterRuningTime),
        //                                        ScaleTo::create(rockMovingTime, 1), NULL));
        this->runAction(Sequence::create(DelayTime::create(animalPeaceTime  + hunterRuningTime + animalAdditionalPeaceTime + animalTurningTime + huntingTime),
                                         DelayTime::create(rockMovingTime), DelayTime::create(1), NULL));
        this->runAction(Sequence::create(DelayTime::create(animalPeaceTime  + hunterRuningTime + animalAdditionalPeaceTime + animalTurningTime + huntingTime),
                                         CallFunc::create(CC_CALLBACK_0(IntroScene::onStartRockAnimation, this)), DelayTime::create(1.9f), CallFunc::create(CC_CALLBACK_0(IntroScene::stopBackgroundRun, this)), DelayTime::create(1.5), CallFunc::create(CC_CALLBACK_0(IntroScene::startLabelBlink, this)), NULL));
        hunter->runAction(Sequence::create(MoveTo::create(0, Point(hunterX, -size.height)),
                                           DelayTime::create(animalPeaceTime),
                                           MoveTo::create(0, Point(hunterX, hunterY)),
                                           CallFunc::create(CC_CALLBACK_0(IntroScene::startBackgroundsRun, this)),
                                           DelayTime::create(hunterRuningTime),
                                           MoveTo::create(0, Point(0, -size.height)),
                                           CallFunc::create(CC_CALLBACK_0(IntroScene::stopBackgroundsRun, this)),
                                           DelayTime::create(animalAdditionalPeaceTime + animalTurningTime),
                                           MoveTo::create(0, Point(hunterX, hunterY)),
                                           CallFunc::create(CC_CALLBACK_0(IntroScene::startBackgroundsRun, this)),
                                           DelayTime::create(huntingTime+1.5f),
                                           CallFunc::create(CC_CALLBACK_0(IntroScene::hideHunter, this)),
                                           NULL));
        
        if(GameManager::getInstance()->isPortrait){
            size = Director::getInstance()->getWinSize();
            updateOrientation();
        }
    }else{
        GameManager::getInstance()->playSoundEffect(SOUND_BGM_TITLE);
        Sprite* sptBack = Sprite::create("title_background.png");
        this->addChild(sptBack);
        sptBack->setPosition(size/2);
        sptBack->setRotation(-90);
        
        std::string name = "title.json.txt";
        std::string atlas = "title.atlas";
        SkeletonAnimation* anim = SkeletonAnimation::createWithJsonFile(name, atlas, 1);
        addChild(anim);
        anim->setPosition(size/2);
        anim->setRotation(-90);
        anim->setAnimation(0, "animation", true);
        
        Sprite* sptLogo = Sprite::create("logotitle_b.png");
        this->addChild(sptLogo);
        sptLogo->setPosition(Point(228, size.height/2));
        sptLogo->setRotation(-90);
        sptLogo->setScale(0.5f);
        isIntroEnded = true;
        if (!GameManager::getInstance()->isPortrait){
            
            
        }
        size = Director::getInstance()->getWinSize();
        updateOrientation();
    }
}
void IntroScene::hideHunter(){
    hunter->setVisible(false);
}
void IntroScene::startHorseAni(float dt){
    horse0->runAnimation("alert", true);
    horse1->runAnimation("alert2", true);
}
void IntroScene::startHorseAni2(float dt){
    horse0->runAnimation("alert", false);
    horse1->runAnimation("alert2", false);
}
void IntroScene::stopHorse(float dt){
    horse0->anim->stopAllActions();
    horse1->anim->stopAllActions();
}
void IntroScene::statApeAni(float dt){
//    theApe->setSpine("ape_3.json.txt", "ape_3.atlas");
    theApe->runAnimation("idle3", true);
}
void IntroScene::showAnimals(){
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("scene3.mp3", true);
    for(auto animal : animalList){
        animal->setVisible(true);
    }
}
void IntroScene::hideAnimals(){
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("scene2.mp3", true);
    for(auto animal : animalList){
        animal->setVisible(false);
    }
}
void IntroScene::onStartRockAnimation(){
    cocostudio::timeline::ActionTimeline* action = CSLoader::createTimeline("Intro.csb");
    loadedLayer->runAction(action);
    action->gotoFrameAndPlay(0, false);
    fieldLayer->runAction(Sequence::create(DelayTime::create(65/45.0f), CallFunc::create(CC_CALLBACK_0(IntroScene::createExtraMountain, this)), MoveBy::create(0.1f, Point(0, -180)), NULL));
}
void IntroScene::createExtraMountain(){
    // special order
    Sprite* spt = Sprite::createWithSpriteFrameName("mountain0.png");
    fieldLayer->addChild(spt);
    //    backgroundItems0.pushBack(spt);
    spt->setAnchorPoint(Point(0, 0));
    spt->setScale(2);
    spt->setPosition(Point(-200, size.height/2 - 14));
    spt->setLocalZOrder(size.height - (size.height/2 - 14));
    
    float x = spt->getPosition().x + spt->getContentSize().width*spt->getScaleX() - 20;
    spt = Sprite::createWithSpriteFrameName("mountain1.png");
    fieldLayer->addChild(spt);
    //    backgroundItems0.pushBack(spt);
    spt->setAnchorPoint(Point(0, 0));
    spt->setScale(2);
    spt->setPosition(Point(x, size.height/2 - 14));
    spt->setLocalZOrder(size.height - (size.height/2 - 14));
    
    spt = Sprite::create("bush/bush_c_1.png");
    fieldLayer->addChild(spt);
    spt->setAnchorPoint(Point(0.5, 0));
    spt->setPosition(Point(300, size.height/2 - 54 - spt->getContentSize().height/2));
    spt->setLocalZOrder(size.height - (size.height/2 - 54 - spt->getContentSize().height/2));
}
void IntroScene::startLabelBlink(){
    isIntroEnded = true;
    loadedLayer->getChildByName("lblTouch")->runAction(RepeatForever::create(Sequence::create(FadeIn::create(0.3f), DelayTime::create(0.5f), FadeOut::create(0.3f), NULL)));
    GameManager::getInstance()->playSoundEffect(SOUND_BGM_TITLE);
}
void IntroScene::addListener(){
    auto listener = EventListenerTouchAllAtOnce::create();
    listener->onTouchesEnded = CC_CALLBACK_2(IntroScene::onTouchesEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}
void IntroScene::skip(){
    GameManager::getInstance()->isInIntro = false;
    //    Director::getInstance()->replaceScene(TransitionCrossFade::create(1, worldScene));
    Director::getInstance()->replaceScene(worldScene);
    
    GameManager::getInstance()->playSoundEffect(SOUND_BGM_VILLAGE);
    worldScene->release();
    removeBackgroundItems(backgroundItems0);
    removeBackgroundItems(backgroundItems1);
    removeBackgroundItems(backgroundItems2);
    removeBackgroundItems(backgroundItems3);
    removeBackgroundItems(backgroundItems4);
    removeBackgroundItems(backgroundItems5);
}
void IntroScene::onTouchesEnded(const std::vector<Touch*>& touches, Event* event){
    Point location = touches.at(0)->getLocationInView();
    location = Director::getInstance()->convertToGL(location);
    if(inInLogo){
        return;
    }
    
    if (isIntroEnded) {
        skip();
    }else{
//        for(auto animal: animalList){
//            animal->removeFromParent();
//        }
//        this->unscheduleAllCallbacks();
//        animalList.clear();
//        hunter->setVisible(false);
//        this->stopAllActions();
//        loadedLayer->stopAllActions();
//        fieldLayer->stopAllActions();
//        cocostudio::timeline::ActionTimeline* action = CSLoader::createTimeline("Intro.csb");
//        loadedLayer->runAction(action);
//        action->gotoFrameAndPause(150);
//        stopBackgroundRun();
//        startLabelBlink();
    }
}void IntroScene::stopBackgroundsRun(){
    isBackgroundRunning = false;
}
void IntroScene::startBackgroundsRun(){
    isBackgroundRunning = true;
}
void IntroScene::runningBackground(float dt){
    if(!isBackgroundRunning){
        return;
    }
    runBackground(backgroundItems0, 200, dt);
    runBackground(backgroundItems1, 400, dt);
    runBackground(backgroundItems2, 900, dt);
    runBackground(backgroundItems3, 1600, dt);
    runBackground(backgroundItems4, 2400, dt);
    runBackground(backgroundItems5, 3200, dt);
}
void IntroScene::runBackground(Vector<Sprite*> list, float speed, float dt){
    Sprite* spt;
    Sprite* sptToMove = nullptr;
    Sprite* sptInTheEnd = nullptr;
    
    for(int i = 0 ; i < list.size();i++){
        spt = list.at(i);
        spt->setPositionX(spt->getPositionX() + speed*dt);
        if(sptInTheEnd == nullptr){
            sptInTheEnd = spt;
        }else if(sptInTheEnd->getPositionX() < spt->getPositionX()){
            sptInTheEnd = spt;
        }
        if(spt->getPositionX() - spt->getContentSize().width*spt->getScaleX() > size.width){
            sptToMove = spt;
        }
    }
    if(sptToMove != nullptr){
        sptToMove->setPositionX(-sptToMove->getContentSize().width*spt->getScaleX());
    }
}
void IntroScene::stopBackgroundRun(){
    this->unschedule(schedule_selector(IntroScene::runningBackground));
    Sprite *spt;
    Vector<Sprite*> listToRemove;
    for(int i = 0 ; i < backgroundItems1.size();i++){
        spt = backgroundItems1.at(i);
        if(spt->getPositionX() < size.width/2)
            listToRemove.pushBack(spt);
    }
    for(int i = 0 ; i < listToRemove.size();i++){
        spt = listToRemove.at(i);
        backgroundItems1.eraseObject(spt);
        spt->removeFromParentAndCleanup(true);
    }
    this->schedule(schedule_selector(IntroScene::animalCreating));
    
    for(int i = 0 ; i < animalList.size();i++){
        Character* animal = animalList.at(i);
        animal->runAction(MoveBy::create(3, Point(-size.width*2.5f, 0)));
    }
}
void IntroScene::animalCreating(float dt){
    animalCreatingTimeElapse -= dt;
    if(animalCreatingTimeElapse > 0){
        return;
    }
    animalCreatingTimeElapse = 0.04f + (rand()%10)*0.01f;
    Character* spt;
    
    Character* ch = Character::create();
    animalList.pushBack(ch);
    fieldLayer->addChild(ch, 9999);
    ch->setSpine(rand()%15);
    ch->setPosition(Point(size.width, 150 + rand()%150));
    spTrackEntry* entry = ch->anim->setAnimation(0, "run", true);
    if (entry == nullptr) {
        ch->removeFromParent();
        return;
    }
    
//    ch->runAnimation(CHARACTER_ANI_RUN, 1);
    float scaleY = 1 - (ch->getPositionY()/550)*0.5f;
//    ch->anim->setScale(ch->anim->getScaleX()<0?-scaleY:scaleY, scaleY);
    ch->anim->setScale(1);
    
    ch->setLocalZOrder(size.height - ch->getPositionY());
    
    ch->runAction(MoveBy::create(4, Point(-size.width*2.5f, 0)));
    
    Vector<Character*> listToRemove;
    for(int i = 0 ; i < animalList.size();i++){
        spt = animalList.at(i);
        if (spt->getPositionX() + 100 + animalList.at(i)->getPositionX() < 0)
            listToRemove.pushBack(spt);
    }
    for(int i = 0 ; i < listToRemove.size();i++){
        spt = listToRemove.at(i);
        animalList.eraseObject(spt);
        spt->removeFromParentAndCleanup(true);
    }
}
void IntroScene::startTurningAnimals(){
//    int repeat = (int)animals.size();
    hunter->anim->setAnimation(0, "drivibg2", true); //
    this->schedule(schedule_selector(IntroScene::turningAnimals), 0.1f);
}
void IntroScene::turningAnimals(float dt){
    if(animalList.size() > turningAnimalIndex){
        animalList.at(turningAnimalIndex)->anim->stopAllActions();
        animalList.at(turningAnimalIndex)->anim->setScaleX(-0.5f);
        if(turningAnimalIndex == 0){
//            animalList.at(turningAnimalIndex)->anim->setAnimation(0, "idle", true);
            animalList.at(turningAnimalIndex)->anim->setScaleX(0.5f);
        }else if(turningAnimalIndex == 1){
//            animalList.at(turningAnimalIndex)->anim->setAnimation(0, "idle", true);
        }else if(turningAnimalIndex == 9){
            animalList.at(turningAnimalIndex)->anim->setAnimation(0, "alert", true);
        }else{
            animalList.at(turningAnimalIndex)->anim->setAnimation(0, "alert", true);
        }
        turningAnimalIndex++;
    }
    if(turningAnimalIndex > 9){
        this->unschedule(schedule_selector(IntroScene::turningAnimals));
    }
}
void IntroScene::startAnimalsRunning(){
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("scene4.mp3", true);
    for (auto ch: animalList) {
        ch->anim->setScaleX(0.5f);
        ch->setPositionX(ch->getPositionX() - size.width*2/3);
//        ch->runAnimation(CHARACTER_ANI_IDLE, 1);
        ch->anim->setAnimation(0, "run", true);
        ch->runAction(MoveBy::create(10, Point(size.width*2.0f, 0)));
    }
}
void IntroScene::removeBackgroundItems(Vector<Sprite*> list){
    Sprite *spt;
    for(int i = 0 ; i < list.size();i++){
        spt = list.at(i);
        spt->removeFromParentAndCleanup(true);
    }
}
void IntroScene::updateOrientation(){
    if(GameManager::getInstance()->isPortrait){
        if(size.height > size.width){
            size = Size(size.height, size.width);
        }
        this->setRotation(90);
        this->setPositionY(size.width);
        
    }else{
        this->setRotation(0);
        this->setPositionY(0);
    }    
}
