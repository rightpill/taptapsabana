#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "BigNum.h"
#include "Character.h"
#include "ui/UIHelper.h"
#include "ui/UIButton.h"
#include "ui/UIListView.h"
#include "ui/UIText.h"
#include "ui/UITextField.h"
#include "ui/UIText.h"
#include "ui/UISlider.h"
#include "ui/UIImageView.h"
#include "ui/UILoadingBar.h"
#include "cocostudio/CocoStudio.h"
#include <editor-support/spine/spine-cocos2dx.h>
#include <editor-support/spine/SkeletonAnimation.h>
using namespace spine;
using namespace std;
#define SKILL_AUTO_TAP 0
#define SKILL_BIG_TAP 1
#define SKILL_TAP_BUFF 2
#define SKILL_RESET_SKILLS 3

#define ICON_LIFE 0
#define ICON_RELIC 1
#define ICON_GEM 2
#define ICON_GEM_10 3
#define ICON_GEM_100 4

#define TAB_SKILL 0
#define TAB_CHARACTER 1
#define TAB_COLLECTION 2
#define TAB_RELIC 3
#define TAB_MAP 4
#define TAB_SHOP 5
#define TAB_ACHIEVEMENT 6

//#define MAX_ANIMAL_COUNT 40
#define AREA_KIND 6
#define AREA_AFRICA 0
#define AREA_INVENTORY_AFRICA -1
#define AREA_NATURE -3
#define AREA_HUNTED -4
#define AREA_NONE -2

#define ACHIEVE_RARE_ANIMAL_1 10
#define ACHIEVE_RARE_ANIMAL_10 11
#define ACHIEVE_LION 20
#define ACHIEVE_BIRD 30
#define ACHIEVE_4_STAR_RELIC 40
#define ACHIEVE_5_STAR_RELIC 50
#define ACHIEVE_UNIQUE_ANIMAL 60
#define ACHIEVE_LIMITED_ANIMAL 70
#define ACHIEVE_PURCHASE 80
#define ACHIEVE_TAP_10000 90
#define ACHIEVE_TAP_100000 91
#define ACHIEVE_TAP_1000000 92
#define ACHIEVE_TAP_10000000 93
#define ACHIEVE_ATTEND_3 100
#define ACHIEVE_ATTEND_7 101
#define ACHIEVE_ATTEND_14 102
#define ACHIEVE_ATTEND_30 103
#define ACHIEVE_SANHO_GOLD_TREE_UNLOCK 110
#define ACHIEVE_SANHO_WATER_FALL_UNLOCK 111
#define ACHIEVE_SANHO_MONKEY_STAR_UNLOCK 112
#define ACHIEVE_SHARE_1 120
#define ACHIEVE_SHARE_10 121
#define ACHIEVE_SHARE_30 122
#define ACHIEVE_MAIN_TREE_500 130
#define ACHIEVE_MAIN_TREE_777 131
#define ACHIEVE_MAIN_TREE_1000 132

#define SHOP_FREE_RAIN 0
#define SHOP_FREE_GEM 1
#define SHOP_FREE_RELIC 2
#define SHOP_RAIN 3
#define SHOP_ODIN_1 4
#define SHOP_GEM_SPECIAL_600 5
#define SHOP_GEM_SPECIAL_1000 6
#define SHOP_GEM_1 7
#define SHOP_GEM_2 8
#define SHOP_GEM_3 9
#define SHOP_GEM_4 10
#define SHOP_BIRD 11
#define SHOP_PUPPY 12
#define SHOP_ODIN_7 13

#define SANHO_COUNT 14
USING_NS_CC;
using namespace cocos2d::ui;
class HelloWorldScene : public cocos2d::Layer
{
    int forceTime = -1;
    float zorderTimeLeft=0;
    float skillTimeCheckInterval = 1.0f;
    float skillTimeCheckEllapse = 0;
    Sprite* sptR;
    Sprite* sptG;
    Sprite* sptB;
    void updateBackgroundColor();
    BigNum* upgradeTabPrice=nullptr;
    Slider* sliderR;
    void showRainbow();
    void showWind();
    void showLifeEffect();
    void showAurora();
    void createOdinEffect();
    void destroyOdinEffect();
    void onOdinHeartHit();
    Label* lblOdinTimer;
    Label* lblRainTimer;
    // test
    Label* lblHunterTimeLeft;
    Label* lblHunterLastTime;
    Label* lblHunterNextTime;
    Label* lblTimeTest;
    
    bool isRainingOn = false;
    void createRainingEffect();
    void stopRainingEffect();
    void raining(float dt);
    void onRainDropHitGround(Ref* ref);
    void stopAurora();
    bool donottouchrainbow = false;
    void startRainbowShin(float dt);
    Node* rainbowNode;
    Vector<Sprite*> auroraList;
    void updatingAurora(float dt);
    Slider* sliderG;
    Slider* sliderB;
    Sprite* sptShield = nullptr;
    Sprite* sptRain = nullptr;
    void addRedBean(Node* node, cocos2d::Point pos = cocos2d::Point::ZERO);
    void showRedBean(Node* node, bool visible);
    void huntAnimal();
    Node* uiTop;
    Node* uiBackground;
    Text* lblR;
    Text* lblG;
    Text* lblB;
    Node* treeBack;
    Node* treeFoot;
    void onLeafParticleMoveDone(Ref* ref);
    Vector<Sprite*> leafParticleList;
    
    bool isTutorialOn = false;
    int tutorialStep = 0;
    Node* tutorialNode = nullptr;
    void removeAndCreateTutorialNode();
    float tutorialWaitTime = 1;
    bool isReadyToProceedTutorial = false;
    void onTutorialClick();
    Node* treeFront;
    void addQuestTapBuffLevel();
    void moveIconToPosition(int iconType, cocos2d::Point startPos, cocos2d::Point endPos, float delay = 0);
    void onIconMoveDone(Ref* ref);
    int getQuestTapBuffLevel();
    int relicPickDiaPrice = 250;
//    int relicPickPointPrice = 25;
    int getRelicPrice();
    int rootFallIndex = -1;
    int rareAnimalPickDiaPrice = 500;
//    int rareAnimalPickPointPrice = 50;
    int gerRareAnimalPointPrice();
    int normalDiaPrice = 100;
    int normalPointPrice = 5;
    Vector<Node*> leafList;
    SpriteBatchNode* mainTreeBatch;
    SpriteBatchNode* starBatch;
    SpriteBatchNode* treeBatch;
    SpriteBatchNode* rockBgBatch;
    SpriteBatchNode* frontRockBatch;
//    SpriteBatchNode* meerkatBatch;
    SpriteBatchNode* sunMoonBatch;
    //    SpriteBatchNode* monkeyBatch;
    Vector<Sprite*> shineEmitList;
    Vector<Sprite*> twinkleList;
    Vector<Sprite*> meerkatList;
    Vector<Sprite*> monkeyList;
    Sprite* sky;
    Sprite* sunMoon;
    void updateMainTree();
    cocos2d::Point getHalfPos(cocos2d::Size theSize);
    void setMeerkat(int level);
    void setMonkey(int level);
    void setLion(int level);
    void setDuck(int level);
    void setBaobob(int level);
    void setStar0(int level);
    Vector<Sprite*> star0List;
    void setStar1(int level);
    Vector<Sprite*> star1List;
    void setStar2(int level);
    Vector<Sprite*> flowerList;
    Vector<Sprite*> mushroomList;
    Vector<Sprite*> blueLeafList;
    Vector<Sprite*> yellowLeafList;
    Vector<Sprite*> pinkBushList;
    Vector<Sprite*> star2List;
    Vector<Sprite*> duckList;
    Vector<Sprite*> baobobList;
    Vector<Sprite*> lionList;
    Vector<Sprite*> decoList;
    Vector<Sprite*> grassList;
    void shakeTree();
    void runTreeIdleAnimation();
    Slider* sliderR_color_subtractive;
    Slider* sliderG_color_subtractive;
    Slider* sliderB_color_subtractive;
    Text* lblR_color_subtractive;
    Text* lblG_color_subtractive;
    Text* lblB_color_subtractive;
    Node* selectedNode;
    void onMoveSelectedNode(Ref* ref);
    int currentTreeLevel = 0;
    int currentSelectedIndex = 0;
public:
//    std::string getBushName(int index);
//    cocos2d::Point getBushPoint(int index);
    
    void startRaining(int min);
    void startOdin(int dayCount);
    bool requestBigTap = false;
    Vector<Character*> charList;
    int testHour = 0;
    Vector<Button*> btnList;
    Vector<Button*> btnSkillList;
    Vector<ProgressTimer*> timerPersistingList;
    Vector<ProgressTimer*> timerCoolingList;
    Vector<Node*> tabObjects;
    static cocos2d::Scene* createScene();
    ScrollView* svField;
    Widget* pnlUIBG;
    Node* loadedLayer;
    ImageView* itemTree;
    ImageView* itemRelic;
    ImageView* itemCharacter;
    ImageView* itemAchievement;
    ScrollView* svSkill;
    ScrollView* svCollection;
    ScrollView* svCharacter;
    ScrollView* svRelic;
    ScrollView* svMap;
    ScrollView* svShop;
    ScrollView* svAchievement;
    void showRewardPopup(int rewardType);
    int addAchievementCurrent(int index, int amount);
    int getAchievementCurrent(int index);
    int getAchievementMax(int index, int level);
    int getAchievementReward(int index, int maxProgress);
    int getAchievementMax(int index);
    int getAchievementRewardReceivedForHowMuchProgress(int index);
    void setAchievementRewardReceivedForHowMuchProgress(int index, int amount);
    void onAchievementItemClick(Ref* ref);
//    bool isBuffOn = false;
    BigNum* theLife;
    BigNum* totalSanhoEffectPerSec;
    BigNum* tapReward;
    BigNum* nextAnimalPrice;
    
    cocos2d::Size size;
    Vector<Label*> labelPool;
    float getTreeYByLevel(int level);
    Text* lblLife;
    int labelPoolCount;
    int labelPoolIndex;
    void backToLabelPool(Ref* ref);
    virtual bool init();
    CREATE_FUNC(HelloWorldScene);
    
    bool isTouchedForLife=false;
    float autoTapTimeElapse = 0;
    float autoOneTapTimeElapse = 0;
    float buffTimeElapse = 0;
    int tapBuffAmount = 3;
    float sevenSecTimeElapse = 0;
    float oneSecTimeElapse = 0;
    float oneHalfTimeElapse = 1.5f;
    void gameUpdate(float dt);
    void moveUpdate(float dt);
    void updateTapReward();
    void updateIdleReward();
    void onMainMenuClick(Ref* ref);
    void onTabChange(int tab);
    void onUpgradeDecoClick(Ref* ref);
    void onUpgradeMainTreeClick();
    void onBonusMainTreeClick();
    void onManageCharacterClick();
    void onPickNormalAnimalClick();
    void onPickRareAnimalClick();
    void showAnimalDrawPopup(bool isRare);
    void onGetRareAnimalByPoint();
    void onGetRareAnimalByGem();
    void onGetNormalAnimalByPoint();
    void onGetNormalAnimalByGem();
    void onCreateCharacterClick(Ref* ref);
    int indexToCreateAnimal = -1;
    void createAnimalLater();
    void onMagicItemClick(Ref* ref);
    void updateCollectionItems();
    int getMagicItemPrice(int index);
    void onRelicItemClick(Ref* ref);
    void onGochaRelicClick(Ref* ref);
    void onGochaWithGemClick();
    void onGochaWithRelicClick();
    void onRelicManageClick();
    void onRelicDetailClick(Ref* ref);
    void onRelicPickLater(float dt);
    void onPuppyPickLater(float dt);
    void onVideoDone();
    void rewardVideoLater(float dt);
    int getNextAvailableRelicIndex();
    void showRelicDetail(int index);
    void onAnimalOnFieldClick(Ref* ref);
    std::string filePath;
    int captureCounter = 0;
    bool captureSuccess = false;
    int selectedArea;
    void afterCaptured(bool succeed, const std::string& outputFile);
    void capture(float dt);
    void captureLater();
    
    void askShare();
    void doShare();
    
    Node* prologueLayer = nullptr;
    
    float _talkBoxTimeElapse=0;
    void updateTalkBox(float dt);
    void onAnimalDetailClick(Ref* ref);
    void onAnimalDetailClickFromInventory(Ref* ref);
    void showAnimalInputText();
    void onAnimalInputTextClick();
    void onAnimalShareClick();
    void onAnimalPutInClick();
    void onAnimalPutOutClick();
    void onAnimalToNature();
    void updateNotification();
    void onAnimalManageAreaClick();
    void showAnimalDetail(int index);
    void showAnimalDetailFromInventory(int ID);
    void showAnimalGot(int index, bool showBackLight = false);
    void showAttackResult(bool isSuccess, bool debuf);
    void showAttackReady();
    void updateAttackReady(float dt);
    std::string getRelicEffect0(int index);
    std::string getRelicEffect1(int index);
    void onMoveToRegionClick(Ref* ref);
    void onShopItemClick(Ref* ref);
    void updateOrientation();
    void updateMainButtons();
    void onTreeClick();
    void onAnimalClick();
    int currentTab = TAB_SKILL;
    
    void onCollectionClick();
    void onRelicClick();
    void updateRelic();
    
    void resetRelic();
    long getRelicReadyStartedTime(int index);
    bool isRelicInUse(int index);
    long getRelicReadyTime();
    void unequipRelic(int index);
    void equipRelic(int index);
    Widget* itemTemplete;
    int itemGapY = 6;
    int itemHeight = 104;
    void onMapClick();
    void onShopClick();
    void onAchievementClick();
    void updateShopItems();
    void updateAchievementItems();
    bool isAchievementRedBeanEnabled();
    int achievementCount = 26;
    int shopItemCount = 14;
    void ddiyong(Node* node);
    void onAutoTapSkillClick();
    void onBigTapSkillClick();
    void onBuffSkillClick();
    void onResetClick();
    void onResetConfirmed();
    void resetSkills();
    void showScrollView(int index);
    
    void showLabelFromPool(Node* parent, cocos2d::Point pos, std::string text, int moveHeight = 200);
    
    void addListener();
    void onFieldTouch(Ref* pSender, Widget::TouchEventType type);
    void onSignatureTouch(Ref* pSender, Widget::TouchEventType type);
    void onMainMenuTouch(Ref* pSender, Widget::TouchEventType type);
    cocos2d::Point lastMainMenuPoint;
    PointArray* signatureArray = nullptr;
    cocos2d::Point lastSignaturePoint;
    void startGame();
    void onOkPrologue();
    
    void CheckSkillTime();
    int getSkillPersistTime(int skillType);
    int getSkillPersistTime(int skillType, int level);
    int getSkillCoolTime(int skillType);
    BigNum* getUpgradeTapRewardPrice();
    BigNum* getUpgradeTapRewardPrice(int level);
    BigNum* getTapReward();
    BigNum* getTapReward(int level);
    void updateMainTreeItem();
    float updateSkyTimeElapse = 60;
    void updateSky();
    int selectedTapIndex=0;
    void OnHideClick();
    void OnAppearClick();
    
    int grassLevel = 0;
    void setGrass();
    Sprite* addToField(const char* sptName, cocos2d::Point pos);

    void unlockSanho(int index);
    bool isSanhoUnlocked(int index);
    void upgradeSanho(int index, int amount);
    BigNum* getUpgradeSanhoPrice(int index, int amount);
    void updateSanhoImages();
    BigNum* getUpgardeSanhoPriceForLevel(int index, int level);
    BigNum* getUpgradeSanhoFirstPrice(int index);
    int getSanhoLevel(int index);
    void onUpgradeSanhoClick(Ref* ref);
    void onBonusSanhoClick(Ref* ref);
    BigNum* getSanhoFirstEffect(int index);
    BigNum* getSanhoEffect(int index);
    void updateSanhoItem(int index);
    void updateSanhoItems();
    void updateTotalSanhoEffect();
    void updateSanhoButtonsEnable();
    Button* btnUpgrade10;
    Button* btnUpgrade100;
    void updateBigUpgradeButtons();
    void onUpgrade10Click();
    void onUpgrade100Click();
    void showBigUpgradeButtons(int index);
    int getMainTreeLevel();
    void onJustGemAndClose();
    void onShareAndGem();
    bool isInitDone= false;
    void onLifeOnAnimalTouched(Character* node);
    void onRelicOnAnimalTouched(Character* node);
    void onGemOnAnimalTouched(Character* node);
    void onInputTextOk();
    void upgradeTapReward(int amount);
    void handleTheLifeChanged();
    Node* firstPopup=nullptr;
    Node* secondPopup=nullptr;
    void createAnimal(int index);
    void loadRelicCSV();
    std::vector<std::vector<std::string>> relicEffects;
    int gochaRelic();
    bool isThereRelicLeftToPick();
    void loadAnimalCSV();
    int getAnimalIndex(int index);
    bool isCaptureMode = false;
//    float animalEffect = 0;
    std::vector<std::string> animalIndices;
    std::vector<std::string> animalRank;
    std::vector<std::string> animalNames;
    std::vector<std::string> animalZoologicalNames;
    std::vector<std::string> animalLand;
    std::vector<std::string> animalTouchEffect;
    std::vector<std::string> animalTimeEffect;
    std::vector<std::string> animalRelicPoint;
    std::vector<std::string> animalUnlockConditionTree;
    std::vector<std::string> animalUnlockConditionAnimalCount;
    std::vector<std::string> animalScale;
    std::vector<std::string> animalAtlas;
    std::vector<std::string> animalJson;
    std::vector<std::string> animalSound1;
    std::vector<std::string> animalSound2;
    std::vector<std::string> animalPrices;
    std::vector<std::string> animalRarePrices;
    std::vector<std::string> relicPrices;
    void loadAnimalPriceCsv();
    
    ImageView* itemMap;
    void updateVisibleItemsOnAnimalTap();
    void updateVisibleItemsOnSanhoTap();
    bool isAnimalUnlocked(int index, bool update);
    void updateAnimalTapDescription();
    Node* lastUpdatedSanho=nullptr;
    
    void createAnimalDone();
    bool isCreatingAnimal = false;
    void applyAnimalEffect(BigNum* num);
    float tapEffectFromAnimal = 1;
    float timeEffectFromAnimal = 1;
    void updateMap();
    void onMapButtonClick(Ref* ref);
    int getTotalAnimalCount(int index);
    int getTotalAnimalCountThatIHave();
    int lastTotalAnimalCount = 0;
    int getTotalAnimalCountInArea(int areaIndex);
    int getAnimalCountInArea(int areaIndex, int animalIndex);
    void addAnimalCountInArea(int areaIndex, int animalIndex, int amount);
    std::string getAreaIconName(int area);
    bool isAreaUnlocked(int areaIndex);
    void unlockArea(int areaIndex);
    int getRelicPrice(int relicIndex);
    int getRelicPoint();
    void addRelicPoint(int amount);
    bool isRelicUnlocked(int index);
    void unlockRelic(int index);
    bool isRelicSlotUnlocked(int slot);
    void tryUnlockRelicSlot(int index);
    void unlockRelicSlot(int slot);
    void addStarToIcon(Node* icon, int index);
    void closePopup();
//    void closeFirstPopup();
//    void closeSecondPopup();
    void shakeSanho();
    Button* btnRecord;
    Button* btnDefense;
    Button* btnScreenCapture;
    Button* btnScreenCaptureAll;
    Button* btnQuest;
    Node* questLayer = nullptr;
    int getQuestProgress(int index);
    void setQuestProgress(int index, int count);
    int getQuestMaxCount(int index);
    void addQuestProgress(int index, int count);
    void updateQuest(float dt);
    void onRecevieQuestRewardClick(Ref* ref);
    bool isQuestRewardReceived(int index);
    void setQuestRewardReceived(int index, bool received);
    
    int tapCounter = 0;
    bool tapQuestDone = false;
    Button* btnSetting;
    Button* btnAchievement;
    void onAchievementServiceClick();
    void onScreenCaptureClick();
    void onScreenCaptureAllClick();
    void captureScreen();
    
    void onDailyQuestClick();
    void onSettingClick();
    void changeSettingState(int index, bool isOn, bool animate);
    void changeItToSettingOn(Ref* ref);
    void changeItToSettingOff(Ref* ref);
    void changeSettingButtonToOn(Ref* ref);
    void onMusicClick();
    void onSoundEffectClick();
    void onAlertClick();
    
    int getSurvivalPower(int index);
    int getAnimalLevel(int index);
    int getAnimalCountForLevelUp(int level);
    int getAnimalExp(int index);
    int getAccumulatedAnimalCount(int index);
    void addAccumulatedAnimalCount(int index);
    void subtractAccumulatedAnimalCount(int index);
    void onRecordClick();
    std::string messageToShow;
    void showMessage(std::string msg);
    void showMessageLater(float dt);
    Vector<Sprite*> shakingList;
    Vector<Sprite*> silukList;
    bool isAnimalCondition0Meet(int index);
    bool isAnimalCondition1Meet(int index);
    std::string getAnimalCondition0(int index);
    std::string getAnimalCondition1(int index);
    void showParticle(Node* target, int count);
    void onRelicSlotClick(Ref* ref);
    Character* animalCreateEffect;
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event) override;
    void showExitPopup();
    void onExitClick();
    void zoomInScrolllView(Node* target, float scale);
    void zoomInEnded();
    void showAnimalShare();
    int getSanhoBonusLevel(int sanhoIndex);
    void addSanhoBonusLevel(int sanhoIndex);
    int getMainTreeBonusLevel();
    void addMainTreeBonusLevel();
    float yMove;
    cocos2d::EventListenerKeyboard* _keyListener;
    void onExpandInventoryClick();
    int getInventorySlotCount();
    void addInventorySlotCount();
    bool doAttackIsSuccess(bool debuf);
    int getTotalSurvivalPower();
    bool isHunted(int index);
    void setHunted(int index, bool hunted);
    int getAttackerPower(int level);
    void startDefense();
    void updateAnimalPickTab();
//    Node* btnBlocker;
    cocos2d::Point touchBeganFieldPos;
    void updateNextAnimalPrice();
    void setAbsentRewardTime();
    void giveBackgroundReward();
    BigNum* absentReward;
    bool absentRewardRequested = true;
    bool setAsPopup(Node* node);
    bool isPopupAvailable();
    void checkAbsentAttack();
    bool checkAbsentAttackRequested = true;
    void updateCurrencyLabels();
    int unlockRelicSlotPrice = 999;
    bool firstLaunchScreenMoved = false;
    
    int getMaxAnimalPossessCount();
    void modifyShareLine();
    void takePicture();
    void onModifyLineDone();
    void onChangeShareModeClick();
    bool isShareAnimalPaused = false;
    void onShareAnimalClick();
    void onShareAnimalDone();
    bool isAnimalShare = false;
    
    
    EventListenerTouchAllAtOnce* _animalZoomerTouchListener = nullptr;
    void onTouchesBegan(const std::vector<Touch*>& touches, Event* event);
    void onTouchesMoved(const std::vector<Touch*>& touches, Event* event);
    void onTouchesEnded(const std::vector<Touch*>& touches, Event* event);
    void hideZoomerIcon();
    float zoomStartDistance = 0;
    float zoomEndDistance = 0;
    cocos2d::Point zoomStartPoint;
    cocos2d::Point zoomEndPoint;
    bool zoomStarted=false;
    void onAnimalShareTouch(Ref* pSender, Widget::TouchEventType type);
    bool zoomEnabled = false;
    bool moveEnabled = false;
    int getArea(int ID);
    int getAnimalIndexForID(int ID);
    void showAnimal(Ref* ref);
    void hideAnimal(Ref* ref);
    void onToNatureClick(Ref* ref);
    void onToNatureConfirmed();
    void onToNatureCanceled();
    cocos2d::Point moveStartPoint;
    void setAnimalArea(int ID, int theArea);
    int getProtectedAnimalCount();
    int getMaxProtectedAnimalCount();
    
    cocos2d::Point getPosInWorld(Node* node);
    int getAchievementIndex(int i);
    Character* present = nullptr;
    void onPresentClick();
    void onPresentAppearDone();
    void onPresentUpdate(float dt);
    void onPresentConfirmed();
    int presentAdsIndex = 0;
    
    void updateMainMenuRedBean();
    
    Sprite* sptAnimalProductAlertL;
    Sprite* sptAnimalProductAlertR;
    Sprite* sptAnimalRelicAlertL;
    Sprite* sptAnimalRelicAlertR;
    
    Button* btnSkipTutorial;
    void onSkipTutorialClick();
};

#endif // __HELLOWORLD_SCENE_H__
