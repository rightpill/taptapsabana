//
//  TimeManager.cpp
//  LegendDaryKakao
//
//  Created by SUNG PILL PACK on 14. 2. 14..
//
//

#include "TimeManager.h"
#include "GameManager.h"
#include "HttpsManager.h"
#include "json/document.h"

//#include "Kakao/Common/GameUserInfo.h"
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//#include "UrlJni.h"
//#endif

using namespace cocos2d;
using namespace cocos2d::network;
TimeManager* TimeManager::m_mySingleton = NULL;

TimeManager::TimeManager()
{
    timeEstablished = false;
    isTrying = false;
}

TimeManager* TimeManager::getInstance()
{
    if(NULL == m_mySingleton)
    {
        m_mySingleton = TimeManager::create();
        m_mySingleton->webTime = -1;
        m_mySingleton->retain();
//        m_mySingleton->titleLayer = (TitleScene*)GameManager::sharedGameManager()->getTitleLayer();
    }
    
    return m_mySingleton;
}
bool TimeManager::init()
{
    Layer::init();
    return true;
}

void TimeManager::onLoadGameUserInfoErrorComplete(char const *status, char const *error) {
//    CCMessageBox("네트워크 상태를 확인해주세요", "게임 사용자 정보를 불러올 수 없습니다.");
    //    log("onLoadGameUserInfoErrorComplete : %s, %s", status, error);
}
void TimeManager::onLoadGameUserInfoComplete() {
    
    timeEstablished = true;
//    webTime = GameUserInfo::getInstance()->serverTime;
    log("web time: %ld", webTime);
    //    time_t startTime;
    struct tm * timeinfo;
    time (&startLocalTime);
    timeinfo = localtime (&startLocalTime);
}
void TimeManager::getHttpTime()
{
    if (getLocalTime) {
        time_t currentLocalTime;
        struct tm * timeinfo;
        time (&currentLocalTime);
        timeinfo = localtime (&currentLocalTime);
        
        long theTime = currentLocalTime;
        webTime = currentLocalTime;
        timeEstablished = true;
        return;
    }
    isTrying = true;
    timeEstablished = false;
    HttpRequest* request = new HttpRequest();
    //    request->setUrl("http://www.timeapi.org/utc/now.json");
    request->setUrl("https://google.com/");
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback(this, httpresponse_selector(TimeManager::onHttpRequestCompleted));
    request->setTag("Http time from google.com");
    HttpClient::getInstance()->send(request);
    request->release();
}
int TimeManager::getMonthIndex( std::string name )
{
    std::map<std::string, int> months
    {
        { "Jan", 1 },
        { "Feb", 2 },
        { "Mar", 3 },
        { "Apr", 4 },
        { "May", 5 },
        { "Jun", 6 },
        { "Jul", 7 },
        { "Aug", 8 },
        { "Sep", 9 },
        { "Oct", 10 },
        { "Nov", 11 },
        { "Dec", 12 },
        { "jan", 1 },
        { "feb", 2 },
        { "mar", 3 },
        { "apr", 4 },
        { "may", 5 },
        { "jun", 6 },
        { "jul", 7 },
        { "aug", 8 },
        { "sep", 9 },
        { "oct", 10 },
        { "nov", 11 },
        { "dec", 12 }
    };
    
    const auto iter = months.find( name );
    
    if( iter != months.cend() )
        return iter->second;
    return -1;
}
void TimeManager::onHttpRequestCompleted(Node *sender, void *data)
{
    log("time complete request http");
    HttpResponse *response = (HttpResponse*)data;
    std::string str = std::string(response->getResponseHeader()->begin(), response->getResponseHeader()->end());
    int size = (int)response->getResponseHeader()->size();
    std::string resposeData = std::string(response->getResponseData()->begin(), response->getResponseData()->end());
    if (size > 110) {
        for (int i = 80; i < 100; i++) {
            //            str.append();
        }
    }
    if (!response->isSucceed())
    {
        log("%s", response->getErrorBuffer());
        getHttpTime();
        return;
    }
    //    response->getResponseHeader()
    
    int countryIndex = (int)resposeData.find("google.co.");
    std::string countryStr = resposeData.substr(countryIndex + 10, 2);
    countryCode = countryStr;
    if (countryIndex < 0) {
        countryCode = "kr";
    }
    log("%s", resposeData.c_str());
    
    
    
    int index = (int)str.find("Date");
    
    int indexOfGMT = (int)str.find("GMT");
    std::string subStr = str.substr(index, indexOfGMT - index);
    int smallIndex = 0;
    //    while ((smallIndex = (int)subStr.find_first_of(":")) > 0){
    //        subStr.erase(subStr.find_first_of(":"), 1);
    //    }
    while ((smallIndex = (int)subStr.find_first_of(",")) > 0){
        subStr.erase(subStr.find_first_of(","), 1);
    }
    log("%s", subStr.c_str());
    ValueVector vector = GameManager::getInstance()->split(subStr, " ");
    
    std::string dayOfWeek = vector.at(1).asString();
    std::string date = vector.at(2).asString();
    std::string month = vector.at(3).asString();
    std::string year = vector.at(4).asString();
    std::string timeStr = vector.at(5).asString();
    
    ValueVector timeVector = GameManager::getInstance()->split(timeStr, ":");
    std::string hour = timeVector.at(0).asString();
    std::string min = timeVector.at(1).asString();
    std::string sec = timeVector.at(2).asString();
    
    struct tm theTime;
    theTime.tm_year = Value(year).asInt()-1900;
    theTime.tm_mon = Value(getMonthIndex(month)).asInt();
    theTime.tm_mday = Value(date).asInt();
    theTime.tm_hour = Value(hour).asInt();
    theTime.tm_min = Value(min).asInt();
    theTime.tm_sec = Value(sec).asInt();
    time_t what = mktime(&theTime);
    long tempWebTime = what;
    long tick = tempWebTime;
    
    webTime = tempWebTime;
    log("** web time: %ld", tempWebTime);
    time_t now;
    now = webTime;
    receivedTime = *localtime(&now); // 지역표준시로 변환한다 (대한민국은 KST)
    //ts = gmtime(&now);  // 국제표준시 GMT로 변환한다
    //    struct tm etet = *localtime(&now);
    
    uint64_t yesterdayTimeInInt = tick - 60*60*24;
    time_t yesterdayNow;
    yesterdayNow = yesterdayTimeInInt;
    receivedYesterdayTime = *localtime(&yesterdayNow);
    
    struct tm * timeinfo;
    time (&startLocalTime);
    timeinfo = localtime (&startLocalTime);
    
    //    log("http time-> %d/%d/%d %d:%d:%d", receivedTime->tm_year,receivedTime->tm_mon, receivedTime->tm_mday, receivedTime->tm_hour, receivedTime->tm_min, receivedTime->tm_sec);
    timeEstablished = true;
    /*
     //    printf("Http Test, dump data: ");
     //char numberInChar[12];
     int startIndex = -1;
     int endIndex = -1;
     for (unsigned int i = 0; i < buffer->size(); i++)
     {
     printf("%c", (*buffer)[i]);
     if ((*buffer)[i] == ':') {
     startIndex = i+1;
     }else if((*buffer)[i] == '}'){
     endIndex = i;
     }
     }
     long tick = 0;
     int num = 0;
     long p = 0;
     if (endIndex - startIndex > 10) {
     startIndex = endIndex - 10;
     }
     for (int i = startIndex; i < endIndex; i++) {
     num = ((int)((*buffer)[i]) - 48);
     //log("num: %d", num);
     p = pow(10, endIndex - i -1);
     //log("p: %d", p);
     tick += num*(p);
     }
     timeEstablished = true;
     webTime = tick;
     log("web time: %ld", webTime);
     //    time_t startTime;
     struct tm * timeinfo;
     time (&startLocalTime);
     timeinfo = localtime (&startLocalTime);*/
}

long TimeManager::getCurrentTime(){
    time_t currentLocalTime;
    struct tm * timeinfo;
    time (&currentLocalTime);
    timeinfo = localtime (&currentLocalTime);
    if (getLocalTime) {
        return currentLocalTime + testDay*60*60*24;
    }
//    log("** get current time web: %ld, startLocalTime: %ld, currentLocalTime: %ld", webTime, startLocalTime, currentLocalTime);
    return webTime + (currentLocalTime - startLocalTime) + testDay*60*60*24;
//    return GameUserInfo::getInstance()->serverTime + (currentLocalTime - startLocalTime);
}

int TimeManager::getDay(){
    return receivedTime.tm_mday;
}
int TimeManager::getMonth(){
    return receivedTime.tm_mon;
}
int TimeManager::getYear(){
    return receivedTime.tm_year;
}

int TimeManager::getYesterdayDay(){
    return receivedYesterdayTime.tm_mday;
}
int TimeManager::getYesterdayMonth(){
    return receivedYesterdayTime.tm_mon;
}
int TimeManager::getYesterdayYear(){
    return receivedYesterdayTime.tm_year;
}


