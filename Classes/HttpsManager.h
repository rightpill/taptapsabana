//
//  HttpsManager.hpp
//  PuzzleRoyale
//
//  Created by PACKSUNG PILL on 10/7/16.
//
//

#ifndef HttpsManager_hpp
#define HttpsManager_hpp

#include <stdio.h>
#include <thread>
#include <iostream>
#include <time.h>
#include "cocos2d.h"
#include "ExtensionMacros.h"

#include "network/HttpRequest.h"
#include "network/HttpResponse.h"
#include "network/HttpClient.h"

#define NN_NONE -1
#define NN_INIT 0
#define NN_SAVE 10
#define NN_SAVE_FAIL 11
#define NN_SAVE_TIME 12
#define NN_SAVE_DEVICE 13
#define NN_LOAD 20
#define NN_LOAD_FAIL 21
#define NN_LOAD_TIME 22
#define NN_LOAD_LIMIT 23
#define NN_LOAD_NONE 24
#define NN_PROVE_RECEIPT 30
#define NN_PROVE_RECEIPT_SUCCESS 31
#define NN_PROVE_RECEIPT_FAIL 32
#define NN_POST_BOX_INIT 40
#define NN_POST_BOX_ITEM 41
#define NN_POST_BOX_RECEIVE 42
#define NN_CROSS 50
#define NN_CROSS_IMAGE 51
#define NN_COUPON 60
#define NN_COUPON_SUCCESS 61
#define NN_COUPON_USED 62
#define NN_COUPON_EXPIRE 63
#define NN_COUPON_FAIL 64


//#include "Title.h"

#define COMMAND_GET 0
#define COMMAND_SEND 1
class HttpsManager: public cocos2d::Layer
{
private:
    HttpsManager();
    
    static HttpsManager* m_mySingleton;
    static HttpsManager* m_mySingletonForStage;
    
public:
    time_t startLocalTime;
    long webTime;
    bool isTrying;
    std::string platform;
    static HttpsManager* getInstance();
    static HttpsManager* getInstanceForStage();
    
    virtual bool init();
    CREATE_FUNC(HttpsManager);
    //void getData(std::string url, int job);
    void getData(std::string url, std::string data, int job);
    void sendData(std::string url, std::string data, int job);
    std::vector<std::string> dataQueue;
    std::vector<std::string> urlQueue;
    std::vector<int> commandQueue;
    std::vector<int> jobQueue;
    bool lock=false;
    bool isInProcess=false;
    void onHttpRequestCompleted(cocos2d::Ref *sender, void *data);
    
    bool initDone=false;
    void nnInit();
    std::vector<std::string> postBoxItemss;
    std::vector<std::string> postBoxItemIDs;
    std::vector<std::string> postBoxItemsDescription;
    
    void startThread();
    void networkThread(float dt);
    std::vector<int> postBoxItemsExpire;
    bool isPostBoxGot=false;
    bool isPostBoxReceiveStarted=false;
    bool isPostBoxReceiveDone=false;
    bool isPostBoxReceiveSuccess=false;
    bool isNetworkFail=false;
    void nnGetPostBox();
    const char* secretKey;
    bool isCouponeDone=false;
    std::string couponItem;
    int couponResult=0;
    std::string couponID;
    void nnCoupon(std::string coupon);
    void nnReceivePostBoxItem(std::string itemID, int count);
    std::string postBoxID;
    bool saveComplete=false;
    int saveResult=0;
    void nnSave(std::string data);
    int loadResult=1;
    bool loadComplete=false;
    std::string loadedData;
    void nnLoad();
    bool proveResultCame=false;
    std::string receiptResult;
    bool isReceiptGood;
    void nnProveReceipt(std::string purchase, std::string signature);
    void nnProveReceipt(std::string receipt);
    bool promotionReceived=false;
    bool isWaitingForPostbox=false;
    bool isWaitingForCross=false;
    std::string promotionImageLink="";
    std::string promotionLink;
    void nnGetCrossPromotion();
    int whichJob;
    int appLovinPercent=0;
    int tapjoyPercent=0;
    int unityPercent=100;
    int crossPercent=50;
    bool useService=false;
    std::string cliendId;
    //std::string uuid="";
    std::string udid;
    std::string version;
    std::string data;
    std::string ts;
    std::string hash;
    std::string requestData;
    std::string gameURL;
    std::string function;
    std::string gameID;
    
    bool isNetworkConnected();
    bool isConnected=false;
};

#endif /* HttpsManager_hpp */
