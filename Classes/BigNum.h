//
//  BigNum.h
//  Zoobyssrium
//
//  Created by PACKSUNG PILL on 1/12/17.
//
//

#ifndef BigNum_hpp
#define BigNum_hpp
#include <string>
#include <vector>
#include <stdio.h>
using namespace std;
class  BigNum
{
private:
    std::string getZeroBaseNum(int num, bool removeNegative);
public:
    std::string splitChar = ".";
    BigNum();
    ~BigNum(){};
    void setNumFromFullNumberByDotSplitString(string str);
    void resetNum();
    std::string getNumForSave();
    int unitIndex=0;
    std::vector<int> numbers;
    void addNum(int amount, int unit);
    void addNum(BigNum* num);
    void subtractNum(int amount, int unit);
    void subtractNum(BigNum* num);
    void arrange();
    void multiply(float rate);
    int IsBiggerThanThis(BigNum* num);
    void RemoveUnusedHigherZero();
    std::string getUnitStr(int unit);
    std::string getExpression();
    std::string getFullNumberStringByDotSplit();
    void setNum(BigNum* num);
};

#endif /* BigNum_hpp */
