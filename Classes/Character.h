//
//  Animal.h
//  Zoobyssrium
//
//  Created by PACKSUNG PILL on 1/14/17.
//
//

#ifndef Animal_hpp
#define Animal_hpp

#include "cocos2d.h"
#include "ui/UIButton.h"
#include "BigNum.h"

#include <editor-support/spine/spine-cocos2dx.h>
#include <editor-support/spine/SkeletonAnimation.h>

#define ANIMAL_APE_0 0
#define ANIMAL_APE_1 1
#define ANIMAL_APE_2 2
#define ANIMAL_HIPO_0 3
#define ANIMAL_HIPO_1 4
#define ANIMAL_HIPO_2 5
#define ANIMAL_RHINO_0 6
#define ANIMAL_RHINO_1 7
#define ANIMAL_RHINO_2 8
#define ANIMAL_ELEPH_0 9
#define ANIMAL_ELEPH_1 10
#define ANIMAL_ELEPH_2 11
#define CHARACTER_ANI_IDLE 0
#define CHARACTER_ANI_IDLE2 1
#define CHARACTER_ANI_IDLE3 2
#define CHARACTER_ANI_WALK 3
#define CHARACTER_ANI_RUN 4
using namespace spine;

#define TALK_BOX_LIFE 0
#define TALK_BOX_GEM 1
#define TALK_BOX_TALK 2
#define TALK_BOX_RELIC 3

USING_NS_CC;
class Character : public cocos2d::ui::Widget
{
    
public:
    int charIndex;
    cocos2d::Size size;
    cocos2d::Rect moveRect0;
    cocos2d::Rect moveRect1;
    cocos2d::Rect moveRect2;
    cocos2d::Rect moveRect3;
    cocos2d::Rect forbiddenRect;
    cocos2d::Rect entireRect;
//    ui::Button* btnTalkBox = nullptr;
    Node* talkBox;
    void showTalkBox(int talkType, float persistTime, std::string text);
    void removeTalkBox(float dt);
    void onTalkTouched(Ref* ref);
    float moveSpeed = 10;
    float nextActionTimeLeft = 0;
    virtual bool init();
    CREATE_FUNC(Character);
    SkeletonAnimation* anim;
    void setSpineAnimation(int index);
    void setSpine(int index);
    void setSpine(std::string json, std::string atlas);
    float runAnimation(int ani, float timeScale);
    float runAnimation(std::string aniName);
    float runAnimation(std::string aniName, bool loop);
    void startMove();
    void updateTalkboxPosition(float dt);
//    float height;
    std::string name;
    void moveAuto(float dt);
    float getIdleTime(int index);
    cocos2d::Point getMovePoint();
    cocos2d::Point getCurrentPoint();
    void setCurrentPoint(cocos2d::Point pos);
    void setPosForFirstTime();
    void readyForFriend();
    void goBackToNormalLife();
    void stayForAWhile();
    int stayCount = 0;
    int lastAnimation = -1;
    float relicGiveTimer = 5;
    bool relicOnAndNotRecieved = false;
    bool relicRequested = false;
    void setID();
    void setArea(int area);
    void loadArea();
    int area = 0;
    int ID=0;
    bool isMoving = false;
    float moveTime = 0;
    cocos2d::Point moveBit;
    bool freezed = false;
};

#endif /* Animal_hpp */
