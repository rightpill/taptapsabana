//
//  LogoSplash.hpp
//  Tapsabana-mobile
//
//  Created by PACKSUNG PILL on 27/12/2017.
//

#ifndef LogoSplash_hpp
#define LogoSplash_hpp

#include "cocos2d.h"
#include "BigNum.h"
#include "Character.h"
#include "ui/UIHelper.h"
#include "ui/UIButton.h"
#include "ui/UIListView.h"
#include "ui/UIText.h"
#include "ui/UITextField.h"
#include "ui/UIText.h"
#include "ui/UISlider.h"
#include "ui/UIImageView.h"
#include "ui/UILoadingBar.h"
#include "cocostudio/CocoStudio.h"
#include <editor-support/spine/spine-cocos2dx.h>
#include <editor-support/spine/SkeletonAnimation.h>
using namespace spine;
using namespace std;

USING_NS_CC;
using namespace cocos2d::ui;
class LogoSplash : public cocos2d::Layer
{
   
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(LogoSplash);
    
    void goToNext();
    void updateOrientation();
};


#endif /* LogoSplash_hpp */
