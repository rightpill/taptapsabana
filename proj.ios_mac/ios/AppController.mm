/****************************************************************************
 Copyright (c) 2010 cocos2d-x.org

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "AppController.h"
#import "platform/ios/CCEAGLView-ios.h"
#import "cocos2d.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "GameManager.h"

#import <CommonCrypto/CommonHMAC.h>
//#import "ALSdk.h"
@implementation AppController

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;
static AppController *instance;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    

    cocos2d::Application *app = cocos2d::Application::getInstance();
    app->initGLContextAttrs();
    cocos2d::GLViewImpl::convertAttrs();

    // Override point for customization after application launch.

    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];

    // Init the CCEAGLView
    CCEAGLView *eaglView = [CCEAGLView viewWithFrame: [window bounds]
                                         pixelFormat: (NSString*)cocos2d::GLViewImpl::_pixelFormat
                                         depthFormat: cocos2d::GLViewImpl::_depthFormat
                                  preserveBackbuffer: NO
                                          sharegroup: nil
                                       multiSampling: NO
                                     numberOfSamples: 0 ];
    
    // Enable or disable multiple touches
    [eaglView setMultipleTouchEnabled:YES];

    // Use RootViewController manage CCEAGLView 
    _viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    _viewController.wantsFullScreenLayout = YES;
    _viewController.view = eaglView;

    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: _viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:_viewController];
    }

    [window makeKeyAndVisible];
    // keep screen on
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:true];

    // IMPORTANT: Setting the GLView should be done after creating the RootViewController
    cocos2d::GLView *glview = cocos2d::GLViewImpl::createWithEAGLView(eaglView);
    cocos2d::Director::getInstance()->setOpenGLView(glview);

    instance = self;

    // Initialize Unity Ads
    //[[UnityAds sharedInstance] setTestMode:YES];
    //[[UnityAds sharedInstance] setDebugMode:YES];
//    [[UnityAds sharedInstance] setDelegate:self];
    //puzzle royale AOS 130262
    // iOs 130257
    // platform 85204
//    [[UnityAds sharedInstance] startWithGameId:@"130257" andViewController:_viewController];
    [UnityAds initialize:@"130257" delegate:self];
    
    // AppLovin
    /*[ALSdk initializeSdk];
    // Preload call using a load delegate
    //[ALIncentivizedInterstitialAd preloadAndNotify: self];
    
    
    [ALInterstitialAd shared].adDisplayDelegate = self;
    //[GameCenter startGameCenter];
   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tjcConnectSuccess:)
                                                 name:TJC_CONNECT_SUCCESS
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tjcConnectFail:)
                                                 name:TJC_CONNECT_FAILED
                                               object:nil];
    */
    //Turn on Tapjoy debug mode
    /*[Tapjoy setDebugEnabled:YES]; //Do not set this for any version of the app released to an app store
    [Tapjoy setUserID:[[NSUUID UUID] UUIDString]];
    //Tapjoy connect call
    //nanoo iOS  cgcJd4LDSOCTl15rS8i-4AEByD0DrqoG3XgHtDQ1OQbY19dcLvrwHaqiNNtY
    //nanoo AOS  6Z66TlvNTW-ciE5Y4DW8kAECs8gLnYnceA8vIozcjIaPD3ZHugqu7XagieHI
    [Tapjoy connect:@"cgcJd4LDSOCTl15rS8i-4AEByD0DrqoG3XgHtDQ1OQbY19dcLvrwHaqiNNtY"];
    
    // push
    [Tapjoy setApplicationLaunchingOptions:launchOptions];  // push
    */
    // push end
    
    app->run();
    
    return true;
}
-(void)askAllowAlert{
    [self enablePush:NO];
    [self enablePush:YES];
}
-(void)enablePush:(BOOL)enable{
    if (enable) {
        // Registering for remote notifications
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
        {
            // iOS 8 Notifications
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            // iOS < 8 Notifications
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
        }
    }else{
        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    }
}
/*
// tapjoy push
// called when the remote notification is registered
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [Tapjoy setDeviceToken:deviceToken];
}
// tapjoy push
// called when the user get push message while playing the app
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo {
    [Tapjoy setReceiveRemoteNotification:userInfo];
}
// tapjoy push end

-(void)tjcConnectSuccess:(NSNotification*)notifyObj{NSLog(@"Tapjoy connect Succeeded");}
-(void)tjcConnectFail:(NSNotification*)notifyObj{
    NSLog(@"Tapjoy connect Failed");
}

- (void)requestDidSucceed:(TJPlacement*)placement
{
    NSLog(@"aTapjoy requestDidSucceed");
    [self destroyProgressionAlert];
    [placement showContentWithViewController:self.viewController];
}

- (void)contentIsReady:(TJPlacement*)placement
{
    NSLog(@"aTapjoy contentIsReady");
}

- (void)requestDidFail:(TJPlacement*)placement error:(NSError *)error
{
    [self destroyProgressionAlert];
    NSLog(@"aTapjoy requestDidFail");
}
- (void)contentDidAppear:(TJPlacement*)placement{
    NSLog(@"aTapjoy Content did appear for %@ placement", [placement placementName]);
    tapjoyPlacement = [TJPlacement placementWithName:@"PD iOS placement" delegate:self ];
}
- (void)contentDidDisappear:(TJPlacement*)placement{
    NSLog(@"aTapjoy Content did disappear for %@ placement", [placement placementName]);
    
    [self createProgressionAlertWithMessage];
    [Tapjoy getCurrencyBalanceWithCompletion:^(NSDictionary *parameters, NSError *error) {
        if (error){
            NSLog(@"aTapjoy getCurrentBalance error");
        }
        else
        {
            int amount = [parameters[@"amount"] intValue];
            NSLog(@"aTapjoy spend: %d", amount);
            [self destroyProgressionAlert];
            if (amount > 0) {
                GameManager::getInstance()->rewardTapjoy(amount);
                [Tapjoy spendCurrency:amount completion:^(NSDictionary *parameters, NSError *error) {
                    if (error){
                        NSLog(@"aTapjoy getCurrentBalance error");
                    }else{
                        NSLog(@"aTapjoy spend complete");
                    }
                }];
            }
        }
    }];
}

- (void)showTapjoyOfferwall{
    [self createProgressionAlertWithMessage];
    tapjoyPlacement = [TJPlacement placementWithName:@"PR iOS placement" delegate:self];
    [tapjoyPlacement requestContent];
}

- (void)placement:(TJPlacement*)placement didRequestPurchase:(TJActionRequest*)request productId:(NSString*)productId
{
    NSString *message = [NSString stringWithFormat: @"didRequestPurchase -- productId: %@, token: %@, requestId: %@", productId, request.token, request.requestId];
    [[[UIAlertView alloc] initWithTitle: @"Got Action Callback" message: message delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil] show];
    
    // Your app must call either completed or cancelled to complete the lifecycle of the request
    [request completed];
}


- (void)placement:(TJPlacement*)placement didRequestReward:(TJActionRequest*)request itemId:(NSString*)itemId quantity:(int)quantity
{
    NSString *message = [NSString stringWithFormat: @"didRequestReward -- itemId: %@, quantity: %d, token: %@, requestId: %@", itemId, quantity, request.token, request.requestId];
    [[[UIAlertView alloc] initWithTitle: @"Got Action Callback" message: message delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil] show];
    
    // Your app must call either completed or cancelled to complete the lifecycle of the request
    [request completed];
}


- (void)placement:(TJPlacement*)placement didRequestCurrency:(TJActionRequest*)request currency:(NSString*)currency amount:(int)amount
{
    NSString *message = [NSString stringWithFormat: @"didRequestCurrency -- currency: %@, amount: %d, token: %@, requestId: %@", currency, amount, request.token, request.requestId];
    [[[UIAlertView alloc] initWithTitle: @"Got Action Callback" message: message delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil] show];
    
    // Your app must call either completed or cancelled to complete the lifecycle of the request
    [request completed];
}


- (void)placement:(TJPlacement*)placement didRequestNavigation:(TJActionRequest*)request location:(NSString *)location
{
    NSString *message = [NSString stringWithFormat: @"didRequestNavigation -- location: %@, token: %@, requestId: %@", location, request.token, request.requestId];
    [[[UIAlertView alloc] initWithTitle: @"Got Action Callback" message: message delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil] show];
    
    // Your app must call either completed or cancelled to complete the lifecycle of the request
    [request completed];
}*/
// tapjoy end

- (void)createProgressionAlertWithMessage
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    progressAlert = [[[UIAlertView alloc] initWithTitle:@"" message:@"Please wait..." delegate:self cancelButtonTitle:nil otherButtonTitles:nil] autorelease];
    
    [progressAlert show];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    // Adjust the indicator so it is up a few pixels from the bottom of the alert
    indicator.center = CGPointMake(progressAlert.bounds.size.width / 2, progressAlert.bounds.size.height - 50);
    [indicator startAnimating];
    [progressAlert addSubview:indicator];
    [indicator release];
#endif
}

- (void)destroyProgressionAlert
{
    if(progressAlert != nil)
    {
        [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
        progressAlert = nil;
    }
}

- (const char*)getHash:(const char*)rawData secretKey:(const char*)scret {
    NSString* key = [NSString stringWithUTF8String:scret];
    NSString* data = [NSString stringWithUTF8String:rawData];
    
    const char *cKey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    
    NSLog(@"%@", hash);
    
    NSString* s = [AppController base64forData:hash];
    return [s UTF8String];
}
+ (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {  value |= (0xFF & input[j]);  }  }  NSInteger theIndex = (i / 3) * 4;  output[theIndex + 0] = table[(value >> 18) & 0x3F];
        output[theIndex + 1] = table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6) & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0) & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

// unity ads delegate
- (void)unityAdsReady:(NSString *)placementId {
    NSLog(@"UADS Ready");
    
    
}

- (void)unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message {
    NSLog(@"UADS UnityAds ERROR: %ld - %@",(long)error, message);
    
}

- (void)unityAdsDidStart:(NSString *)placementId {
    NSLog(@"UADS Start");
    [self destroyProgressionAlert];
}

- (void)unityAdsDidFinish:(NSString *)placementId withFinishState:(UnityAdsFinishState)state {
    NSLog(@"UADS FINISH");
    if(state == UnityAdsFinishState::kUnityAdsFinishStateSkipped){
        
    }else if(state == UnityAdsFinishState::kUnityAdsFinishStateCompleted){
        GameManager::getInstance()->showVideoDone();
    }else{
        log("video skipped");
    }
}
// unity ads ends
+ (AppController*)getInstance
{
    return instance;
}
- (void)showUnityAdsVideo
{
    // Set the zone before checking readiness or attempting to show.
    //[[UnityAds sharedInstance] setZone:@"rewardedVideoZone"];
    
    // Use the canShow method to check for zone readiness,
    //  then use the canShowAds method to check for ad readiness.
    
    if ([UnityAds isReady]) {
        [self createProgressionAlertWithMessage];
//        [UnityAds show:_viewController placementId:@"rewardedVideo"];
        [UnityAds show:_viewController];
    }
}
/*
- (void)showApplovinVideo
{
    if([ALInterstitialAd isReadyForDisplay]){
        [ALInterstitialAd show];
        [self createProgressionAlertWithMessage];
    }
    else{
        // No interstitial ad is currently available.  Perform failover logic...
    }
}
/
#pragma mark - Ad Load Delegate Unity
-(void)log:(NSString *)str
{
    log([str UTF8String]);
}
- (void)adService:(ALAdService *)adService didLoadAd:(ALAd *)ad
{
    [self log: @"Rewarded video loaded"];
}

- (void)adService:(ALAdService *)adService didFailToLoadAdWithError:(int)code
{
    [self log: [NSString stringWithFormat: @"Rewarded video failed to load with error code %d", code]];
}

#pragma mark - Ad Reward Delegate

- (void)rewardValidationRequestForAd:(ALAd *)ad didSucceedWithResponse:(nonnull NSDictionary *)response
{
 
    // i.e. - "Coins", "Gold", whatever you set in the dashboard.
    NSString *currencyName = [response objectForKey: @"currency"];
    
    // For example, "5" or "5.00" if you've specified an amount in the UI.
    NSString *amountGivenString = [response objectForKey: @"amount"];
    NSNumber *amountGiven = [NSNumber numberWithFloat: [amountGivenString floatValue]];
    
    // Do something with this information.
    // [MYCurrencyManagerClass updateUserCurrency: currencyName withChange: amountGiven];
    [self log: [NSString stringWithFormat: @"Rewarded %@ %@", amountGiven, currencyName]];
    
    // By default we'll show a UIAlertView informing your user of the currency & amount earned.
    // If you don't want this, you can turn it off in the Manage Apps UI.
}

- (void)rewardValidationRequestForAd:(ALAd *)ad didFailWithError:(NSInteger)responseCode
{
    if (responseCode == kALErrorCodeIncentivizedUserClosedVideo)
    {
        // Your user exited the video prematurely. It's up to you if you'd still like to grant
        // a reward in this case. Most developers choose not to. Note that this case can occur
        // after a reward was initially granted (since reward validation happens as soon as a
        // video is launched).
    }
    else if (responseCode == kALErrorCodeIncentivizedValidationNetworkTimeout || responseCode == kALErrorCodeIncentivizedUnknownServerError)
    {
        // Some server issue happened here. Don't grant a reward. By default we'll show the user
        // a UIAlertView telling them to try again later, but you can change this in the
        // Manage Apps UI.
    }
    else if (responseCode == kALErrorCodeIncentiviziedAdNotPreloaded)
    {
        // Indicates that the developer called for a rewarded video before one was available.
    }
}

- (void)rewardValidationRequestForAd:(ALAd *)ad didExceedQuotaWithResponse:(NSDictionary *)response
{
    // Your user has already earned the max amount you allowed for the day at this point, so
    // don't give them any more money. By default we'll show them a UIAlertView explaining this,
    // though you can change that from the Manage Apps UI.
}

- (void)rewardValidationRequestForAd:(ALAd *)ad wasRejectedWithResponse:(NSDictionary *)response
{
    // Your user couldn't be granted a reward for this view. This could happen if you've blacklisted
    // them, for example. Don't grant them any currency. By default we'll show them a UIAlertView explaining this,
    // though you can change that from the Manage Apps UI.
}

#pragma mark - Ad Display Delegate Unity

- (void)ad:(nonnull ALAd *)ad wasDisplayedIn:(nonnull UIView *)view
{
    [self log: @"Ad Displayed"];
    [self destroyProgressionAlert];
}

- (void)ad:(nonnull ALAd *)ad wasHiddenIn:(nonnull UIView *)view
{
    [self log: @"Ad Dismissed"];
    GameManager::getInstance()->showVideoDone();
}

- (void)ad:(nonnull ALAd *)ad wasClickedIn:(nonnull UIView *)view
{
    [self log: @"Ad Clicked"];
}
#pragma mark - Ad Video Playback Delegate AppLovin

- (void)videoPlaybackBeganInAd:(nonnull ALAd *)ad
{
    [self log: @"Video Started"];
}

- (void)videoPlaybackEndedInAd:(nonnull ALAd *)ad atPlaybackPercent:(nonnull NSNumber *)percentPlayed fullyWatched:(BOOL)wasFullyWatched
{
    log("AppLovin Video Ended: %d", wasFullyWatched);
    if (wasFullyWatched) {
        
    }
    
 }
 */
#pragma mark - Ad Reward Delegate End

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
     //We don't need to call this method any more. It will interupt user defined game pause&resume logic
    /* cocos2d::Director::getInstance()->pause(); */
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
     //We don't need to call this method any more. It will interupt user defined game pause&resume logic
    /* cocos2d::Director::getInstance()->resume(); */
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    cocos2d::Application::getInstance()->applicationDidEnterBackground();
//    [Tapjoy endSession];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::Application::getInstance()->applicationWillEnterForeground();
//    [Tapjoy startSession];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}
- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    
    NSLog(@"%@", shortcutItem.type);
    NSString* typeFree = @"free";
    NSString* typeRanking = @"ranking";
    if([typeRanking isEqualToString:shortcutItem.type]){
//        GameManager::getInstance()->startRankingBattle();
    }else if([typeFree isEqualToString:shortcutItem.type]){
//        GameManager::getInstance()->startFreeBattle();
    }
}

- (void)dealloc {
    [window release];
    [super dealloc];
}


@end
