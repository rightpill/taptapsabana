//
//  Ios_NativeInterface.m
//  AsteroidInvaders2
//
//  Created by EAA van Poeijer on 17-12-12.
//
//

// Copyright (c) 2012 SeventyFour
#import "NativeInterface.h"
//#import <Storekit/StoreKit.h>
#import "Ios_BillingManager.h"

#import "AppController.h"

//#import "cocos2d.h"
//#import "AppDelegate.h"
#import "GameManager.h"
//#import "Chartboost.h"
//#import <AdColony/AdColony.h>
//#import "CSingleton.h"
//#include "FacebookSDK/FacebookSDK.h"
//#include <FacebookSDK/FacebookSDK.h>
//#define NO_FACEBOOK_INTEGRATION


namespace NativeInterface
{
    bool NativeInterface::init(actionFinished callback)
    {
//        [Ios_BillingManager sharedManager].billingAvailableCallback = callback;
//        [[Ios_BillingManager sharedManager] initialize];
        return true;
    }
    void NativeInterface::loadSKUItems(actionFinished callback)
    {
//        CCLOG("iOS load SKU Items");
//        [Ios_BillingManager sharedManager].loadSKUItemsCallback = callback;
//        [[Ios_BillingManager sharedManager] requestProductData];
    }
    const char* NativeInterface::getSkuDescription(const char* item)
    {
//        return [[Ios_BillingManager sharedManager]getSkuDescription:item];
    }
    const char* NativeInterface::getSkuPrice(const char* item)
    {
//        return [[Ios_BillingManager sharedManager]getSkuPrice:item];
    }
    const char* NativeInterface::getSkuDetails(const char* item)
    {
//        return [[Ios_BillingManager sharedManager]getSkuDetails:item];
    }
    const char* NativeInterface::getSkuTitle(const char* item)
    {
//        return [[Ios_BillingManager sharedManager]getSkuTitle:item];
    }
    const char* NativeInterface::getSkuType(const char* item)
    {
//        return [[Ios_BillingManager sharedManager]getSkuType:item];
    }
    
    
    void NativeInterface::cancelAllLocalNotification(){
//        [[Ios_BillingManager sharedManager] cancelAllLocalNotification];
    }
    
    bool NativeInterface::isPurchased(const char *item)
    {
        // NOT IMPLEMENTED IS HANDLED DIFFERENT IN IOS
        
        return false;
    }
    void NativeInterface::purchase(const char* item, int code, actionFinished callback)
    {
        CCLOG("purchasing: %s, code: %d", item, code);
        // int code is used for restoring 1 = restore
//        [Ios_BillingManager sharedManager].purchaseCallback = callback;
//        [[Ios_BillingManager sharedManager] purchase:item code:code];
    }
    /*
    void NativeInterface::naverPurchase(const char* item, int code, actionFinished callback)
    {
        // int code is used for restoring 1 = restore
        //[Ios_BillingManager sharedManager].purchaseCallback = callback;
        //[[Ios_BillingManager sharedManager] purchase:item code:code];
    }
    void NativeInterface::ktPurchase(const char* item, actionFinished callback){}
    void NativeInterface::samsungPurchase(const char* item, actionFinished callback){}
    void NativeInterface::requestPaymentTStore(const char* item, actionFinished callback)
    {
        // int code is used for restoring 1 = restore
        //       //[Ios_BillingManager sharedManager].purchaseCallback = callback;
        //        //[[Ios_BillingManager sharedManager] purchase:item];
    }*/
    
    void NativeInterface::restorePurchases(const char* item, actionFinished callback)
    {
//        [Ios_BillingManager sharedManager].checkPurchasesCallback = callback;
//        [[Ios_BillingManager sharedManager] restorePurchases];
        
    }
    void NativeInterface::showVideoRecording(){}
    void NativeInterface::showAds()
    {
//        [[Ios_BillingManager sharedManager] showAds];
    }
    void NativeInterface::showTopAds()
    {
        //        //[[Ios_BillingManager sharedManager] showTopAds];
    }
    void NativeInterface::destroyAds()
    {
//        [[Ios_BillingManager sharedManager] hideAds];
    }
    void NativeInterface::showRectAds()
    {
        //        //[[Ios_BillingManager sharedManager] showRectAds];
    }
    void NativeInterface::destroyRectAds()
    {
        //        //[[Ios_BillingManager sharedManager] hideRectAds];
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    /*void NativeInterface::ShowGameCenterAchievement(){
//        [GameCenter showAchievements];
    }
    void NativeInterface::StartGameCenter(){
        [GameCenter startGameCenter];
    }
    void NativeInterface::SendAchievement(const char* text, float percent){
//        [GameCenter sendAchievementComplete:text percentage:percent];
    }
    
    void NativeInterface::showLeaderboard(const char* leaderboardID)
    {
        [GameCenter showLeaderboard:leaderboardID];
    }
    
     */
    void NativeInterface::copyToClipboard(std::string str)
    {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%s", str.c_str()];
    }
    void NativeInterface::postScore(int score, const char* leaderboardID)
    {
//        [GameCenter reportScore:score scoreType:leaderboardID];
    }
#endif
    void NativeInterface::trackEvent(const char* category, const char* name, const char* param1, const char* param2, const char* valueName, int value){
        
    }
    void NativeInterface::enablePush(bool enable){
        [[AppController getInstance] enablePush:enable];
    }
    void NativeInterface::showUnityAdsVideo(){
        [[AppController getInstance] showUnityAdsVideo];
    }
    void NativeInterface::exitGame(){}
    void NativeInterface::showTapjoyOfferwall(){
        [[AppController getInstance] showTapjoyOfferwall];
    }
    const char* NativeInterface::getHash(const char* data, const char* secretKey){
        return [[AppController getInstance] getHash:data secretKey:secretKey];
    }
    void NativeInterface::askAllowAlert(){
        [[AppController getInstance] askAllowAlert];
    }
    const char* NativeInterface::getUDID(){
        NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
        return [Identifier UTF8String];
        //CFUUIDRef udid = CFUUIDCreate(NULL);
        //NSString *udidString = (NSString *) CFUUIDCreateString(NULL, udid);
        //return [udidString UTF8String];
    }
    const char* NativeInterface::getUUID(){
        NSString *uuid = [[NSUUID UUID] UUIDString];
        return [uuid UTF8String];
        //NSUUID *uuid = [[NSUUID alloc]initWithUUIDString:@"20B0DDE7-6087-4607-842A-E97C72E4D522"];
        //NSLog(@"%@",uuid);
        //NSLog(@"%@",[uuid UUIDString]);
        //return [[uuid UUIDString] UTF8String];
    }
    /*void NativeInterface::showAdColonyVideo(){
        [AdColony playVideoAdForZone:@"vz2d6e46b8ab3f4888ab"
         withDelegate:[Ios_BillingManager sharedManager]
         withV4VCPrePopup:YES
         andV4VCPostPopup:YES];
    }*/
    void NativeInterface::addAdColonyRewards(){
        //        GameManager::getInstance()->showAdColonyVideoDone();
    }
    
    void NativeInterface::showChartboost(){
        /*Chartboost *cb = [Chartboost sharedChartboost];
         
         cb.appId = @"53b7a7c089b0bb7eabde7325";
         cb.appSignature = @"99b4dfb4a386236f2b20530b4822df5e86fffab2";
         
         [cb startSession];
         [cb showInterstitial];*/
    }
    void NativeInterface::shareLink(const char* title, const char* link){
        NSString *titleToShare = [NSString stringWithUTF8String:title];
        NSString *linkToShare = [NSString stringWithUTF8String:link];
        NSArray *itemsToShare = @[titleToShare, [NSURL URLWithString:linkToShare]];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
        activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //or whichever you don't need
        [[AppController getInstance].viewController presentViewController:activityVC animated:YES completion:nil];
    }
    void NativeInterface::sharePhoto(const char* fileName, const char* msg){
        NSString *textToShare = [NSString stringWithUTF8String:msg];
        UIImage *imageToShare = [UIImage imageNamed:[NSString stringWithUTF8String:fileName]];
//        NSArray *itemsToShare = @[textToShare, imageToShare, [NSURL URLWithString:@"http://onelink.to/platformdefense"]];
        NSArray *itemsToShare = @[textToShare, imageToShare];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
        activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //or whichever you don't need
        [[AppController getInstance].viewController presentViewController:activityVC animated:YES completion:nil];
    }
    void NativeInterface::showChartboostMoreApps(){
        /*NSLog(@"show chartboost more apps");
         Chartboost *cb = [Chartboost sharedChartboost];
         
         cb.appId = @"53b7a7c089b0bb7eabde7325";
         cb.appSignature = @"99b4dfb4a386236f2b20530b4822df5e86fffab2";
         
         [cb startSession];
         [cb showMoreApps];*/
    }
    
    //    void NativeInterface::consumePurchase(const char* item,actionFinished callback )
    //    {
    //        // Implementation is different
    //        // On ios this is done on server side;
    //        if (callback)
    //            (*callback)(NativeInterface::CONSUMEPURCHASE_YES);
    //
    //    }
    
    void NativeInterface::openUrl(const char* str)
    {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        NSURL *url = [NSURL URLWithString:[NSString stringWithUTF8String:str]];
        [[UIApplication sharedApplication] openURL:url];
#endif
        //[[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"GotFreeCoin"];
    }
    int NativeInterface::getIntegerForKey(const char* key)
    {
        NSNumber* num = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithUTF8String:key]];
        if (num) {
            return [num intValue];
        }
        return -1;
    }
    void NativeInterface::resetPushNotification(){
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
#endif
    }
    void NativeInterface::cancelNotifacation(const char* msg, int seconds)
    {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        UILocalNotification *noti = [[UILocalNotification alloc]init];
        noti.fireDate = [NSDate dateWithTimeIntervalSinceNow:seconds];
        noti.timeZone = [NSTimeZone systemTimeZone];
        noti.alertBody = [NSString stringWithUTF8String: msg];
        noti.alertAction = @"Play!";
        noti.applicationIconBadgeNumber = 1;
        noti.soundName = UILocalNotificationDefaultSoundName;
        noti.userInfo = [NSDictionary dictionaryWithObject:@"FromNotification" forKey:@"How"];
        [[UIApplication sharedApplication] cancelLocalNotification:noti];
        
#endif
    }
    void NativeInterface::Push_Notification(const char* msg, int seconds)
    {
        resetPushNotification();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        // UILocalNotification 객체 생성
        UILocalNotification *noti = [[UILocalNotification alloc]init];
        
        // 알람 발생 시각 설정.
        noti.fireDate = [NSDate dateWithTimeIntervalSinceNow:seconds];
        
        // timeZone 설정.
        noti.timeZone = [NSTimeZone systemTimeZone];
        
        // 알림 메시지 설정
        noti.alertBody = [NSString stringWithUTF8String: msg];
        
        // 알림 액션 설정
        noti.alertAction = @"Play!";
        
        // 아이콘 뱃지 넘버 설정. 임의로 1 입력
        noti.applicationIconBadgeNumber = 1;
        
        // 알림 사운드 설정. 자체 제작 사운드도 가능. (if nil = no sound)
        noti.soundName = UILocalNotificationDefaultSoundName;
        
        // 임의의 사용자 정보 설정. 알림 화면엔 나타나지 않음
        noti.userInfo = [NSDictionary dictionaryWithObject:@"FromNotification" forKey:@"How"];
        
        // UIApplication을 이용하여 알림을 등록.
        [[UIApplication sharedApplication] scheduleLocalNotification:noti];
        
#endif
    }
    
}
