#import <UIKit/UIKit.h>
#import <UnityAds/UnityAds.h>

//#import <Tapjoy/Tapjoy.h>
//#import <Tapjoy/TJPlacement.h>

@class RootViewController;

@interface AppController : NSObject <UIApplicationDelegate, UnityAdsDelegate/*,ALAdLoadDelegate, ALAdRewardDelegate, ALAdDisplayDelegate, ALAdVideoPlaybackDelegate, TJPlacementDelegate*/> {
    UIWindow *window;
    AppController* _appController;
//    TJPlacement* tapjoyPlacement;
    UIAlertView* progressAlert;

}
/*
- (void)requestDidSucceed:(TJPlacement*)placement;
- (void)requestDidFail:(TJPlacement*)placement error:(NSError*)error;
- (void)contentIsReady:(TJPlacement*)placement;
- (void)contentDidAppear:(TJPlacement*)placement;
- (void)contentDidDisappear:(TJPlacement*)placement;
- (void)placement:(TJPlacement*)placement didRequestReward:(TJActionRequest*)request itemId:(NSString*)itemId quantity:(int)quantity;
- (void)placement:(TJPlacement*)placement didRequestCurrency:(TJActionRequest*)request currency:(NSString*)currency amount:(int)amount;
- (void)placement:(TJPlacement*)placement didRequestNavigation:(TJActionRequest*)request location:(NSString *)location;
*/
-(void)enablePush:(BOOL)enable;
-(void)showUnityAdsVideo;
-(void)askAllowAlert;
-(void)showApplovinVideo;
-(void)showTapjoyOfferwall;
//- (void)log:(NSString *)message;
+(AppController*)getInstance;
+ (NSString*)base64forData:(NSData*)theData ;
- (const char*)getHash:(const char*)rawData secretKey:(const char*)scret ;
@property(nonatomic, readonly) RootViewController* viewController;

@end

