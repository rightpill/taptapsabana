<GameFile>
  <PropertyGroup Name="HorizontalLayer" Type="Layer" ID="bf17b13a-c718-4ad5-abe7-e0c3e6eeb4f7" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="14" Speed="0.5000">
        <Timeline ActionTag="415849446" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_1.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="2" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_2.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="4" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_3.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="6" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_4.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="8" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_5.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="10" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_6.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="12" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_7.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="14" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_8.png" Plist="" />
          </TextureFrame>
        </Timeline>
        <Timeline ActionTag="415849446" Property="BlendFunc">
          <BlendFuncFrame FrameIndex="0" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="2" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="4" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="6" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="8" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="10" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="12" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="14" Tween="False" Src="1" Dst="771" />
        </Timeline>
        <Timeline ActionTag="-1592287964" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_1.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="2" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_2.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="4" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_3.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="6" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_4.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="8" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_5.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="10" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_6.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="12" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_7.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="14" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_8.png" Plist="" />
          </TextureFrame>
        </Timeline>
        <Timeline ActionTag="-1592287964" Property="BlendFunc">
          <BlendFuncFrame FrameIndex="0" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="2" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="4" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="6" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="8" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="10" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="12" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="14" Tween="False" Src="1" Dst="771" />
        </Timeline>
        <Timeline ActionTag="57402723" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_1.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="2" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_2.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="4" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_3.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="6" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_4.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="8" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_5.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="10" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_6.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="12" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_7.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="14" Tween="False">
            <TextureFile Type="Normal" Path="eff_skibox_8.png" Plist="" />
          </TextureFrame>
        </Timeline>
        <Timeline ActionTag="57402723" Property="BlendFunc">
          <BlendFuncFrame FrameIndex="0" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="2" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="4" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="6" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="8" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="10" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="12" Tween="False" Src="1" Dst="771" />
          <BlendFuncFrame FrameIndex="14" Tween="False" Src="1" Dst="771" />
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="177" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="svField" ActionTag="2118426580" Tag="296" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" LeftEage="880" RightEage="880" TopEage="237" BottomEage="237" Scale9OriginX="-880" Scale9OriginY="-237" Scale9Width="1760" Scale9Height="474" ScrollDirectionType="Horizontal" ctype="ScrollViewObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="667.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="2668" Height="750" />
          </AbstractNodeData>
          <AbstractNodeData Name="pnlUIBG" ActionTag="-1508444841" Tag="289" IconVisible="False" RightMargin="584.0000" TopMargin="165.1848" TouchEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="750.0000" Y="584.8152" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.5622" Y="0.7798" />
            <SingleColor A="255" R="51" G="42" B="59" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnCommunity" ActionTag="919879192" VisibleForFrame="False" Tag="25" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="1082.4148" RightMargin="185.5852" TopMargin="30.9027" BottomMargin="651.0973" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="36" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="66.0000" Y="68.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1115.4148" Y="685.0973" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8361" Y="0.9135" />
            <PreSize X="0.0495" Y="0.0907" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Normal" Path="btnCommunity.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnAchievement" ActionTag="-1275141539" VisibleForFrame="False" Tag="26" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="1162.9078" RightMargin="114.0922" TopMargin="27.5333" BottomMargin="650.4667" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="57.0000" Y="72.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1191.4078" Y="686.4667" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8931" Y="0.9153" />
            <PreSize X="0.0427" Y="0.0960" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnSetting" ActionTag="1406317589" VisibleForFrame="False" Tag="27" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="1234.1936" RightMargin="43.8064" TopMargin="35.6180" BottomMargin="659.3820" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="26" Scale9Height="33" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="56.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1262.1936" Y="686.8820" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9462" Y="0.9158" />
            <PreSize X="0.0420" Y="0.0733" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Normal" Path="btnSetting.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="uiTop" ActionTag="-1570950622" Tag="179" IconVisible="False" RightMargin="584.0000" TopMargin="124.0000" BottomMargin="568.0000" Scale9Enable="True" LeftEage="235" RightEage="22" TopEage="19" BottomEage="19" Scale9OriginX="235" Scale9OriginY="19" Scale9Width="13" Scale9Height="20" ctype="ImageViewObjectData">
            <Size X="750.0000" Y="58.0000" />
            <AnchorPoint />
            <Position Y="568.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.7573" />
            <PreSize X="0.5622" Y="0.0773" />
            <FileData Type="Normal" Path="uibox_top.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="uiBackground" ActionTag="1424482780" Tag="180" IconVisible="False" RightMargin="584.0000" TopMargin="182.0000" BottomMargin="94.0000" Scale9Width="2" Scale9Height="2" ctype="ImageViewObjectData">
            <Size X="750.0000" Y="474.0000" />
            <AnchorPoint />
            <Position Y="94.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.1253" />
            <PreSize X="0.5622" Y="0.6320" />
            <FileData Type="Normal" Path="ui_bg_pixel.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnSkill3" ActionTag="-1780611351" Tag="433" IconVisible="False" LeftMargin="771.8400" RightMargin="474.1600" TopMargin="61.4400" BottomMargin="600.5600" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="58" Scale9Height="66" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="88.0000" Y="88.0000" />
            <Children>
              <AbstractNodeData Name="sptEffect" ActionTag="415849446" VisibleForFrame="False" Tag="434" IconVisible="False" LeftMargin="-3.6326" RightMargin="-4.3674" TopMargin="-3.6024" BottomMargin="-4.3976" ctype="SpriteObjectData">
                <Size X="96.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.3674" Y="43.6024" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5042" Y="0.4955" />
                <PreSize X="1.0909" Y="1.0909" />
                <FileData Type="Normal" Path="eff_skibox_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbTime" ActionTag="364905513" VisibleForFrame="False" Tag="435" IconVisible="False" LeftMargin="3.0351" RightMargin="1.6209" TopMargin="61.1077" BottomMargin="13.5255" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="83.3439" Y="13.3668" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.7071" Y="20.2089" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5080" Y="0.2296" />
                <PreSize X="0.9471" Y="0.1519" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblDescription" ActionTag="953521045" VisibleForFrame="False" Tag="436" IconVisible="False" LeftMargin="-6.5426" RightMargin="-10.4574" TopMargin="96.9597" BottomMargin="-40.9597" FontSize="28" LabelText="Auto tap" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="105.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="45.9574" Y="-24.9597" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5222" Y="-0.2836" />
                <PreSize X="1.1932" Y="0.3636" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblTime" ActionTag="-1668081255" VisibleForFrame="False" Tag="437" IconVisible="False" LeftMargin="25.6780" RightMargin="23.3220" TopMargin="53.7230" BottomMargin="9.2770" FontSize="20" LabelText="999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="39.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5069" ScaleY="0.4885" />
                <Position X="45.4471" Y="21.4895" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5164" Y="0.2442" />
                <PreSize X="0.4432" Y="0.2841" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="815.8400" Y="644.5600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6116" Y="0.8594" />
            <PreSize X="0.0660" Y="0.1173" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="skill3.png" Plist="" />
            <PressedFileData Type="Normal" Path="skill3.png" Plist="" />
            <NormalFileData Type="Normal" Path="skill3.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnSkill2" ActionTag="20884403" Tag="72" IconVisible="False" LeftMargin="661.8411" RightMargin="584.1589" TopMargin="61.4363" BottomMargin="600.5637" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="58" Scale9Height="66" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="88.0000" Y="88.0000" />
            <Children>
              <AbstractNodeData Name="sptEffect" ActionTag="-1592287964" VisibleForFrame="False" Tag="501" IconVisible="False" LeftMargin="-3.6326" RightMargin="-4.3674" TopMargin="-3.6024" BottomMargin="-4.3976" ctype="SpriteObjectData">
                <Size X="96.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.3674" Y="43.6024" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5042" Y="0.4955" />
                <PreSize X="1.0909" Y="1.0909" />
                <FileData Type="Normal" Path="eff_skibox_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbTime" ActionTag="1431311040" VisibleForFrame="False" Tag="81" IconVisible="False" LeftMargin="3.0351" RightMargin="1.6209" TopMargin="61.1077" BottomMargin="13.5255" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="83.3439" Y="13.3668" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.7071" Y="20.2089" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5080" Y="0.2296" />
                <PreSize X="0.9471" Y="0.1519" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblDescription" ActionTag="-1193074292" VisibleForFrame="False" Tag="75" IconVisible="False" LeftMargin="-6.5426" RightMargin="-10.4574" TopMargin="96.9597" BottomMargin="-40.9597" FontSize="28" LabelText="Auto tap" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="105.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="45.9574" Y="-24.9597" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5222" Y="-0.2836" />
                <PreSize X="1.1932" Y="0.3636" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblTime" ActionTag="-1887886833" Tag="80" IconVisible="False" LeftMargin="25.6780" RightMargin="23.3220" TopMargin="53.7230" BottomMargin="9.2770" FontSize="20" LabelText="999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="39.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5069" ScaleY="0.4885" />
                <Position X="45.4471" Y="21.4895" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5164" Y="0.2442" />
                <PreSize X="0.4432" Y="0.2841" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="705.8411" Y="644.5637" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5291" Y="0.8594" />
            <PreSize X="0.0660" Y="0.1173" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="skill2.png" Plist="" />
            <PressedFileData Type="Normal" Path="skill2.png" Plist="" />
            <NormalFileData Type="Normal" Path="skill2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnSkill1" ActionTag="-1808334064" Tag="71" IconVisible="False" LeftMargin="549.4454" RightMargin="696.5546" TopMargin="61.4363" BottomMargin="600.5637" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="58" Scale9Height="66" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="88.0000" Y="88.0000" />
            <Children>
              <AbstractNodeData Name="lbTime" ActionTag="-598535742" VisibleForFrame="False" Tag="348" IconVisible="False" LeftMargin="2.1996" RightMargin="2.4565" TopMargin="61.1078" BottomMargin="13.5254" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="83.3439" Y="13.3668" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="43.8716" Y="20.2088" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4985" Y="0.2296" />
                <PreSize X="0.9471" Y="0.1519" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblTime" ActionTag="532691115" VisibleForFrame="False" Tag="349" IconVisible="False" LeftMargin="24.8419" RightMargin="24.1581" TopMargin="53.7230" BottomMargin="9.2770" FontSize="20" LabelText="999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="39.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5069" ScaleY="0.4885" />
                <Position X="44.6110" Y="21.4895" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5069" Y="0.2442" />
                <PreSize X="0.4432" Y="0.2841" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="593.4454" Y="644.5637" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4449" Y="0.8594" />
            <PreSize X="0.0660" Y="0.1173" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="skill1.png" Plist="" />
            <PressedFileData Type="Normal" Path="skill1.png" Plist="" />
            <NormalFileData Type="Normal" Path="skill1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnSkill0" ActionTag="-1607082626" Tag="70" IconVisible="False" LeftMargin="433.5338" RightMargin="812.4662" TopMargin="61.4363" BottomMargin="600.5637" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="58" Scale9Height="66" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="88.0000" Y="88.0000" />
            <Children>
              <AbstractNodeData Name="sptEffect" ActionTag="57402723" VisibleForFrame="False" Tag="502" IconVisible="False" LeftMargin="-3.5768" RightMargin="-4.4232" TopMargin="-4.3117" BottomMargin="-3.6883" ctype="SpriteObjectData">
                <Size X="96.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.4232" Y="44.3117" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5048" Y="0.5035" />
                <PreSize X="1.0909" Y="1.0909" />
                <FileData Type="Normal" Path="eff_skibox_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbTime" ActionTag="381089011" VisibleForFrame="False" Tag="350" IconVisible="False" LeftMargin="3.4482" RightMargin="1.2078" TopMargin="61.1077" BottomMargin="13.5255" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="83.3439" Y="13.3668" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="45.1202" Y="20.2089" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5127" Y="0.2296" />
                <PreSize X="0.9471" Y="0.1519" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblTime" ActionTag="622560765" VisibleForFrame="False" Tag="351" IconVisible="False" LeftMargin="26.0911" RightMargin="22.9089" TopMargin="53.7230" BottomMargin="9.2770" FontSize="20" LabelText="999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="39.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5069" ScaleY="0.4885" />
                <Position X="45.8602" Y="21.4895" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5211" Y="0.2442" />
                <PreSize X="0.4432" Y="0.2841" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="477.5338" Y="644.5637" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3580" Y="0.8594" />
            <PreSize X="0.0660" Y="0.1173" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="skill0.png" Plist="" />
            <PressedFileData Type="Normal" Path="skill0.png" Plist="" />
            <NormalFileData Type="Normal" Path="skill0.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnSkill3_" ActionTag="-90436162" VisibleForFrame="False" Tag="69" IconVisible="False" LeftMargin="-183.6034" RightMargin="1386.6034" TopMargin="774.3574" BottomMargin="-145.3574" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="131.0000" Y="121.0000" />
            <Children>
              <AbstractNodeData Name="lbTime" ActionTag="1397042480" Tag="84" IconVisible="False" LeftMargin="-0.1792" RightMargin="0.3266" TopMargin="124.8296" BottomMargin="-31.3656" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="130.8526" Y="27.5360" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="65.2471" Y="-17.5976" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4981" Y="-0.1454" />
                <PreSize X="0.9989" Y="0.2276" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblDescription" ActionTag="-1988588888" Tag="76" IconVisible="False" LeftMargin="-8.2529" RightMargin="-7.7471" TopMargin="149.0103" BottomMargin="-60.0103" FontSize="28" LabelText="Reset Skills" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="147.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="65.2471" Y="-44.0103" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4981" Y="-0.3637" />
                <PreSize X="1.1221" Y="0.2645" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblTime" ActionTag="1288231564" Tag="77" IconVisible="False" LeftMargin="41.7471" RightMargin="42.2529" TopMargin="121.3673" BottomMargin="-32.3673" FontSize="28" LabelText="999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="47.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="65.2471" Y="-16.3673" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.4981" Y="-0.1353" />
                <PreSize X="0.3588" Y="0.2645" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-118.1034" Y="-84.8574" />
            <Scale ScaleX="0.5832" ScaleY="0.6985" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.0885" Y="-0.1131" />
            <PreSize X="0.0982" Y="0.1613" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="svCharacter" ActionTag="-1505349228" Tag="1" IconVisible="False" RightMargin="584.0000" TopMargin="347.0000" BottomMargin="94.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="750.0000" Y="309.0000" />
            <AnchorPoint />
            <Position Y="94.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.1253" />
            <PreSize X="0.5622" Y="0.4120" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="750" Height="474" />
          </AbstractNodeData>
          <AbstractNodeData Name="itemTree" ActionTag="957686530" IconVisible="False" LeftMargin="13.0000" RightMargin="595.0000" TopMargin="238.0000" BottomMargin="408.0000" Scale9Enable="True" LeftEage="4" RightEage="4" TopEage="4" BottomEage="4" Scale9OriginX="4" Scale9OriginY="4" Scale9Width="718" Scale9Height="96" ctype="ImageViewObjectData">
            <Size X="726.0000" Y="104.0000" />
            <Children>
              <AbstractNodeData Name="btnUpgrade" ActionTag="1110351044" Tag="203" IconVisible="False" LeftMargin="582.0000" RightMargin="6.0000" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="3" RightEage="3" TopEage="3" BottomEage="3" Scale9OriginX="3" Scale9OriginY="3" Scale9Width="132" Scale9Height="86" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="138.0000" Y="92.0000" />
                <Children>
                  <AbstractNodeData Name="lblDescription" ActionTag="-1793125679" Tag="204" IconVisible="False" LeftMargin="29.5000" RightMargin="29.5000" TopMargin="22.5000" BottomMargin="46.5000" FontSize="20" LabelText="생명창조" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="79.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="69.0000" Y="58.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6304" />
                    <PreSize X="0.5725" Y="0.2500" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptIcon" ActionTag="-149771249" Tag="205" IconVisible="False" LeftMargin="25.5851" RightMargin="90.4149" TopMargin="53.0000" BottomMargin="23.0000" LeftEage="7" RightEage="7" TopEage="5" BottomEage="5" Scale9OriginX="7" Scale9OriginY="5" Scale9Width="8" Scale9Height="6" ctype="ImageViewObjectData">
                    <Size X="22.0000" Y="16.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="36.5851" Y="31.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2651" Y="0.3370" />
                    <PreSize X="0.1594" Y="0.1739" />
                    <FileData Type="Normal" Path="icon_leaf_white.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblPrice" ActionTag="-519901934" Tag="206" IconVisible="False" LeftMargin="52.0000" RightMargin="52.0000" TopMargin="50.5000" BottomMargin="20.5000" FontSize="18" LabelText="8.7j" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="34.0000" Y="21.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="69.0000" Y="31.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.3370" />
                    <PreSize X="0.2464" Y="0.2283" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="651.0000" Y="52.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8967" Y="0.5000" />
                <PreSize X="0.1901" Y="0.8846" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="btnDisabled.png" Plist="" />
                <PressedFileData Type="Normal" Path="btnUpgradeDown.png" Plist="" />
                <NormalFileData Type="Normal" Path="btnUpgrade.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblDescription" ActionTag="1146632068" Tag="207" IconVisible="False" LeftMargin="110.8477" RightMargin="474.1523" TopMargin="12.7177" BottomMargin="66.2823" FontSize="22" LabelText="눈이 맑은 사슴" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="141.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="110.8477" Y="78.7823" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="94" G="93" B="73" />
                <PrePosition X="0.1527" Y="0.7575" />
                <PreSize X="0.1942" Y="0.2404" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="timeIcon" ActionTag="-656408497" Tag="165" IconVisible="False" LeftMargin="112.6200" RightMargin="569.3800" TopMargin="47.7700" BottomMargin="16.2300" LeftEage="11" RightEage="11" TopEage="9" BottomEage="9" Scale9OriginX="11" Scale9OriginY="9" Scale9Width="12" Scale9Height="12" ctype="ImageViewObjectData">
                <Size X="44.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="134.6200" Y="36.2300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1854" Y="0.3484" />
                <PreSize X="0.0606" Y="0.3846" />
                <FileData Type="Normal" Path="icon_lifetab.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptIconBg" ActionTag="-206481212" Tag="208" IconVisible="False" LeftMargin="6.0000" RightMargin="628.0000" TopMargin="6.0000" BottomMargin="6.0000" Scale9Enable="True" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="82" Scale9Height="82" ctype="ImageViewObjectData">
                <Size X="92.0000" Y="92.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.0000" Y="52.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0716" Y="0.5000" />
                <PreSize X="0.1267" Y="0.8846" />
                <FileData Type="Normal" Path="icon_frame.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptIcon" ActionTag="281049541" Tag="209" IconVisible="False" LeftMargin="12.5000" RightMargin="634.5000" TopMargin="13.0000" BottomMargin="13.0000" LeftEage="26" RightEage="26" TopEage="15" BottomEage="15" Scale9OriginX="20" Scale9OriginY="15" Scale9Width="6" Scale9Height="16" ctype="ImageViewObjectData">
                <Size X="79.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.0000" Y="52.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0716" Y="0.5000" />
                <PreSize X="0.1088" Y="0.7500" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblDescription_0" ActionTag="-510059578" Tag="210" IconVisible="False" LeftMargin="163.6623" RightMargin="313.3377" TopMargin="44.6956" BottomMargin="17.3044" FontSize="18" LabelText="사바나 사막의 꽃 눈이 맑은 사슴&#xA;그치요~?" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="249.0000" Y="42.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="163.6623" Y="38.3044" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="140" G="137" B="103" />
                <PrePosition X="0.2254" Y="0.3683" />
                <PreSize X="0.3430" Y="0.4038" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position X="13.0000" Y="512.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0097" Y="0.6827" />
            <PreSize X="0.5442" Y="0.1387" />
            <FileData Type="Normal" Path="ui_list_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="svSkill" ActionTag="799721750" IconVisible="False" RightMargin="584.0000" TopMargin="347.0000" BottomMargin="94.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="750.0000" Y="309.0000" />
            <Children>
              <AbstractNodeData Name="item0" ActionTag="-1364272165" Tag="85" IconVisible="False" LeftMargin="12.0000" RightMargin="12.0000" TopMargin="187.0000" BottomMargin="183.0000" Scale9Enable="True" LeftEage="4" RightEage="4" TopEage="4" BottomEage="4" Scale9OriginX="4" Scale9OriginY="4" Scale9Width="718" Scale9Height="96" ctype="ImageViewObjectData">
                <Size X="726.0000" Y="104.0000" />
                <Children>
                  <AbstractNodeData Name="btnUpgrade" ActionTag="1147235349" Tag="91" IconVisible="False" LeftMargin="582.0000" RightMargin="6.0000" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="3" RightEage="3" TopEage="3" BottomEage="3" Scale9OriginX="3" Scale9OriginY="3" Scale9Width="132" Scale9Height="86" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="138.0000" Y="92.0000" />
                    <Children>
                      <AbstractNodeData Name="lblDescription" ActionTag="1914220501" Tag="106" IconVisible="False" LeftMargin="29.5000" RightMargin="29.5000" TopMargin="22.5000" BottomMargin="46.5000" FontSize="20" LabelText="생명창조" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="79.0000" Y="23.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="69.0000" Y="58.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.6304" />
                        <PreSize X="0.5725" Y="0.2500" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="sptIcon" ActionTag="-1032514319" Tag="210" IconVisible="False" LeftMargin="25.0851" RightMargin="89.9149" TopMargin="51.5000" BottomMargin="21.5000" LeftEage="7" RightEage="7" TopEage="6" BottomEage="6" Scale9OriginX="7" Scale9OriginY="6" Scale9Width="9" Scale9Height="7" ctype="ImageViewObjectData">
                        <Size X="23.0000" Y="19.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="36.5851" Y="31.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2651" Y="0.3370" />
                        <PreSize X="0.1667" Y="0.2065" />
                        <FileData Type="Normal" Path="icon_dia_white.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblPrice" ActionTag="337020656" Tag="107" IconVisible="False" LeftMargin="52.0000" RightMargin="52.0000" TopMargin="51.0000" BottomMargin="20.0000" FontSize="18" LabelText="8.7j" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="34.0000" Y="21.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                        <Position X="69.0000" Y="41.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.4457" />
                        <PreSize X="0.2464" Y="0.2283" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="1.0000" />
                    <Position X="720.0000" Y="6.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9917" Y="0.0577" />
                    <PreSize X="0.1901" Y="0.8846" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="btnDisabled.png" Plist="" />
                    <PressedFileData Type="Normal" Path="btnCreateDown.png" Plist="" />
                    <NormalFileData Type="Normal" Path="btnCreate.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblDescription" ActionTag="-200021082" Tag="86" IconVisible="False" LeftMargin="110.8477" RightMargin="474.1523" TopMargin="12.7177" BottomMargin="66.2823" FontSize="22" LabelText="눈이 맑은 사슴" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="141.0000" Y="25.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="110.8477" Y="78.7823" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="94" G="93" B="73" />
                    <PrePosition X="0.1527" Y="0.7575" />
                    <PreSize X="0.1942" Y="0.2404" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblCount" ActionTag="-92927672" VisibleForFrame="False" Tag="214" IconVisible="False" LeftMargin="506.5000" RightMargin="140.5000" TopMargin="33.5000" BottomMargin="47.5000" FontSize="20" LabelText="생명창조" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="79.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="546.0000" Y="59.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7521" Y="0.5673" />
                    <PreSize X="0.1088" Y="0.2212" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptIconBg" ActionTag="702970741" Tag="158" IconVisible="False" LeftMargin="6.0000" RightMargin="628.0000" TopMargin="6.0000" BottomMargin="6.0000" Scale9Enable="True" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="82" Scale9Height="82" ctype="ImageViewObjectData">
                    <Size X="92.0000" Y="92.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="52.0000" Y="52.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0716" Y="0.5000" />
                    <PreSize X="0.1267" Y="0.8846" />
                    <FileData Type="Normal" Path="icon_frame.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptIcon" ActionTag="-321890710" Tag="89" IconVisible="False" LeftMargin="12.5000" RightMargin="634.5000" TopMargin="13.0000" BottomMargin="13.0000" LeftEage="26" RightEage="26" TopEage="15" BottomEage="15" Scale9OriginX="20" Scale9OriginY="15" Scale9Width="6" Scale9Height="16" ctype="ImageViewObjectData">
                    <Size X="79.0000" Y="78.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="52.0000" Y="52.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0716" Y="0.5000" />
                    <PreSize X="0.1088" Y="0.7500" />
                    <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblDescription_0" ActionTag="-736375906" Tag="254" IconVisible="False" LeftMargin="110.8477" RightMargin="366.1523" TopMargin="45.6956" BottomMargin="16.3044" FontSize="18" LabelText="사바나 사막의 꽃 눈이 맑은 사슴&#xA;그치요~?" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="249.0000" Y="42.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position X="110.8477" Y="58.3044" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="140" G="137" B="103" />
                    <PrePosition X="0.1527" Y="0.5606" />
                    <PreSize X="0.3430" Y="0.4038" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position X="12.0000" Y="287.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0160" Y="0.6055" />
                <PreSize X="0.9680" Y="0.2194" />
                <FileData Type="Normal" Path="ui_list_bg_locked.png.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sanhoItem" ActionTag="-1615073916" Tag="149" IconVisible="False" LeftMargin="12.8529" RightMargin="11.1471" TopMargin="74.9999" BottomMargin="295.0001" Scale9Enable="True" LeftEage="4" RightEage="4" TopEage="4" BottomEage="4" Scale9OriginX="4" Scale9OriginY="4" Scale9Width="718" Scale9Height="96" ctype="ImageViewObjectData">
                <Size X="726.0000" Y="104.0000" />
                <Children>
                  <AbstractNodeData Name="btnUpgrade" ActionTag="-1948686679" Tag="150" IconVisible="False" LeftMargin="582.0000" RightMargin="6.0000" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="3" RightEage="3" TopEage="3" BottomEage="3" Scale9OriginX="3" Scale9OriginY="3" Scale9Width="132" Scale9Height="86" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="138.0000" Y="92.0000" />
                    <Children>
                      <AbstractNodeData Name="lblDescription" ActionTag="1544208566" Tag="151" IconVisible="False" LeftMargin="29.5000" RightMargin="29.5000" TopMargin="22.5000" BottomMargin="46.5000" FontSize="20" LabelText="생명창조" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="79.0000" Y="23.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="69.0000" Y="58.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.6304" />
                        <PreSize X="0.5725" Y="0.2500" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="sptIcon" ActionTag="-1112782361" Tag="152" IconVisible="False" LeftMargin="25.5851" RightMargin="90.4149" TopMargin="53.0000" BottomMargin="23.0000" LeftEage="7" RightEage="7" TopEage="5" BottomEage="5" Scale9OriginX="7" Scale9OriginY="5" Scale9Width="8" Scale9Height="6" ctype="ImageViewObjectData">
                        <Size X="22.0000" Y="16.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="36.5851" Y="31.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2651" Y="0.3370" />
                        <PreSize X="0.1594" Y="0.1739" />
                        <FileData Type="Normal" Path="icon_leaf_white.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblPrice" ActionTag="184977171" Tag="153" IconVisible="False" LeftMargin="52.0000" RightMargin="52.0000" TopMargin="51.0000" BottomMargin="20.0000" FontSize="18" LabelText="8.7j" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="34.0000" Y="21.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                        <Position X="69.0000" Y="41.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.4457" />
                        <PreSize X="0.2464" Y="0.2283" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="1.0000" />
                    <Position X="720.0000" Y="6.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9917" Y="0.0577" />
                    <PreSize X="0.1901" Y="0.8846" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="btnDisabled.png" Plist="" />
                    <PressedFileData Type="Normal" Path="btnUpgradeDown.png" Plist="" />
                    <NormalFileData Type="Normal" Path="btnUpgrade.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblDescription" ActionTag="-846036126" Tag="154" IconVisible="False" LeftMargin="108.6400" RightMargin="476.3600" TopMargin="12.7177" BottomMargin="66.2823" FontSize="22" LabelText="눈이 맑은 사슴" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="141.0000" Y="25.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="108.6400" Y="78.7823" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="94" G="93" B="73" />
                    <PrePosition X="0.1496" Y="0.7575" />
                    <PreSize X="0.1942" Y="0.2404" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptIconBg" ActionTag="1584573596" Tag="159" IconVisible="False" LeftMargin="6.0000" RightMargin="628.0000" TopMargin="6.0000" BottomMargin="6.0000" Scale9Enable="True" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="82" Scale9Height="82" ctype="ImageViewObjectData">
                    <Size X="92.0000" Y="92.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="52.0000" Y="52.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0716" Y="0.5000" />
                    <PreSize X="0.1267" Y="0.8846" />
                    <FileData Type="Normal" Path="icon_frame.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptIcon" ActionTag="-1836748480" Tag="156" IconVisible="False" LeftMargin="12.5000" RightMargin="634.5000" TopMargin="13.0000" BottomMargin="13.0000" LeftEage="26" RightEage="26" TopEage="15" BottomEage="15" Scale9OriginX="20" Scale9OriginY="15" Scale9Width="6" Scale9Height="16" ctype="ImageViewObjectData">
                    <Size X="79.0000" Y="78.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="52.0000" Y="52.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0716" Y="0.5000" />
                    <PreSize X="0.1088" Y="0.7500" />
                    <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptLock" ActionTag="1186091234" Tag="164" IconVisible="False" LeftMargin="29.0000" RightMargin="651.0000" TopMargin="21.0000" BottomMargin="21.0000" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="16" Scale9Height="32" ctype="ImageViewObjectData">
                    <Size X="46.0000" Y="62.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="52.0000" Y="52.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0716" Y="0.5000" />
                    <PreSize X="0.0634" Y="0.5962" />
                    <FileData Type="Normal" Path="sptLock.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="timeIcon" ActionTag="-1402366925" Tag="166" IconVisible="False" LeftMargin="112.6232" RightMargin="569.3768" TopMargin="47.7662" BottomMargin="16.2338" LeftEage="11" RightEage="11" TopEage="9" BottomEage="9" Scale9OriginX="11" Scale9OriginY="9" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                    <Size X="44.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="134.6232" Y="36.2338" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1854" Y="0.3484" />
                    <PreSize X="0.0606" Y="0.3846" />
                    <FileData Type="Normal" Path="icon_time.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblDescription_0" ActionTag="1069001284" Tag="157" IconVisible="False" LeftMargin="164.6623" RightMargin="312.3377" TopMargin="46.9033" BottomMargin="15.0967" FontSize="18" LabelText="사바나 사막의 꽃 눈이 맑은 사슴&#xA;그치요~?" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="249.0000" Y="42.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="164.6623" Y="36.0967" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="140" G="137" B="103" />
                    <PrePosition X="0.2268" Y="0.3471" />
                    <PreSize X="0.3430" Y="0.4038" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btnUpgrade1" ActionTag="134340738" VisibleForFrame="False" Tag="158" IconVisible="False" LeftMargin="485.0000" RightMargin="147.0000" TopMargin="15.0000" BottomMargin="15.0000" FontSize="14" Scale9Enable="True" LeftEage="3" RightEage="3" TopEage="3" BottomEage="3" Scale9OriginX="3" Scale9OriginY="3" Scale9Width="4" Scale9Height="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="94.0000" Y="74.0000" />
                    <Children>
                      <AbstractNodeData Name="lblDescription" ActionTag="327768081" Tag="159" IconVisible="False" LeftMargin="7.5000" RightMargin="7.5000" TopMargin="4.5000" BottomMargin="46.5000" FontSize="20" LabelText="생명창조" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="79.0000" Y="23.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="47.0000" Y="58.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.7838" />
                        <PreSize X="0.8404" Y="0.3108" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_10" ActionTag="-872593627" Tag="160" IconVisible="False" LeftMargin="11.7014" RightMargin="40.2986" TopMargin="34.7673" BottomMargin="7.2327" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="16" Scale9Height="12" ctype="ImageViewObjectData">
                        <Size X="42.0000" Y="32.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="32.7014" Y="23.2327" />
                        <Scale ScaleX="0.7000" ScaleY="0.7000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3479" Y="0.3140" />
                        <PreSize X="0.4468" Y="0.4324" />
                        <FileData Type="Normal" Path="icon_life.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblPrice" ActionTag="552838174" Tag="161" IconVisible="False" LeftMargin="48.1163" RightMargin="11.8837" TopMargin="40.2673" BottomMargin="12.7327" FontSize="18" LabelText="8.7j" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="34.0000" Y="21.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="65.1163" Y="23.2327" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6927" Y="0.3140" />
                        <PreSize X="0.3617" Y="0.2838" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="532.0000" Y="52.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7328" Y="0.5000" />
                    <PreSize X="0.1295" Y="0.7115" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                    <PressedFileData Type="Normal" Path="btn1_bg.png" Plist="" />
                    <NormalFileData Type="Normal" Path="btn1_bg.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btnUpgrade2" ActionTag="1191369211" VisibleForFrame="False" Tag="162" IconVisible="False" LeftMargin="390.0000" RightMargin="242.0000" TopMargin="15.0000" BottomMargin="15.0000" FontSize="14" Scale9Enable="True" LeftEage="3" RightEage="3" TopEage="3" BottomEage="3" Scale9OriginX="3" Scale9OriginY="3" Scale9Width="4" Scale9Height="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="94.0000" Y="74.0000" />
                    <Children>
                      <AbstractNodeData Name="lblDescription" ActionTag="-1105487926" Tag="163" IconVisible="False" LeftMargin="7.5000" RightMargin="7.5000" TopMargin="4.5000" BottomMargin="46.5000" FontSize="20" LabelText="생명창조" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="79.0000" Y="23.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="47.0000" Y="58.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.7838" />
                        <PreSize X="0.8404" Y="0.3108" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_10" ActionTag="-1948668507" Tag="164" IconVisible="False" LeftMargin="11.7014" RightMargin="40.2986" TopMargin="34.7673" BottomMargin="7.2327" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="16" Scale9Height="12" ctype="ImageViewObjectData">
                        <Size X="42.0000" Y="32.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="32.7014" Y="23.2327" />
                        <Scale ScaleX="0.7000" ScaleY="0.7000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3479" Y="0.3140" />
                        <PreSize X="0.4468" Y="0.4324" />
                        <FileData Type="Normal" Path="icon_life.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblPrice" ActionTag="629848637" Tag="165" IconVisible="False" LeftMargin="48.1163" RightMargin="11.8837" TopMargin="40.2673" BottomMargin="12.7327" FontSize="18" LabelText="8.7j" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="34.0000" Y="21.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="65.1163" Y="23.2327" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6927" Y="0.3140" />
                        <PreSize X="0.3617" Y="0.2838" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="437.0000" Y="52.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6019" Y="0.5000" />
                    <PreSize X="0.1295" Y="0.7115" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                    <PressedFileData Type="Normal" Path="btn1_bg.png" Plist="" />
                    <NormalFileData Type="Normal" Path="btn1_bg.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptCheck0" ActionTag="1695057420" Tag="160" IconVisible="False" LeftMargin="113.3995" RightMargin="594.6005" TopMargin="48.3125" BottomMargin="38.6875" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="8" Scale9Height="7" ctype="ImageViewObjectData">
                    <Size X="18.0000" Y="17.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="122.3995" Y="47.1875" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1686" Y="0.4537" />
                    <PreSize X="0.0248" Y="0.1635" />
                    <FileData Type="Normal" Path="sptChecked.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblCondition0" ActionTag="1188107852" Tag="161" IconVisible="False" LeftMargin="135.2947" RightMargin="341.7053" TopMargin="46.4496" BottomMargin="36.5504" FontSize="18" LabelText="사바나 사막의 꽃 눈이 맑은 사슴" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="249.0000" Y="21.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="135.2947" Y="47.0504" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="217" G="213" B="175" />
                    <PrePosition X="0.1864" Y="0.4524" />
                    <PreSize X="0.3430" Y="0.2019" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptCheck1" ActionTag="-772752539" Tag="162" IconVisible="False" LeftMargin="113.3995" RightMargin="594.6005" TopMargin="74.0517" BottomMargin="13.9483" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="8" Scale9Height="6" ctype="ImageViewObjectData">
                    <Size X="18.0000" Y="16.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="122.3995" Y="21.9483" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1686" Y="0.2110" />
                    <PreSize X="0.0248" Y="0.1538" />
                    <FileData Type="Normal" Path="sptUnchecked.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblCondition1" ActionTag="-205685341" Tag="163" IconVisible="False" LeftMargin="135.2947" RightMargin="341.7053" TopMargin="71.6889" BottomMargin="11.3111" FontSize="18" LabelText="사바나 사막의 꽃 눈이 맑은 사슴" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="249.0000" Y="21.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="135.2947" Y="21.8111" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="217" G="213" B="175" />
                    <PrePosition X="0.1864" Y="0.2097" />
                    <PreSize X="0.3430" Y="0.2019" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position X="12.8529" Y="399.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0171" Y="0.8418" />
                <PreSize X="0.9680" Y="0.2194" />
                <FileData Type="Normal" Path="ui_list_bg.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position Y="94.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.1253" />
            <PreSize X="0.5622" Y="0.4120" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="750" Height="474" />
          </AbstractNodeData>
          <AbstractNodeData Name="itemMap" ActionTag="-567959992" Tag="288" IconVisible="False" LeftMargin="12.0000" RightMargin="596.0000" TopMargin="238.0000" BottomMargin="348.0000" Scale9Enable="True" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="8" Scale9Height="8" ctype="ImageViewObjectData">
            <Size X="726.0000" Y="164.0000" />
            <Children>
              <AbstractNodeData Name="sptWorld" ActionTag="-951493500" Tag="176" IconVisible="False" LeftMargin="13.8064" RightMargin="461.1936" TopMargin="5.6610" BottomMargin="5.3390" LeftEage="82" RightEage="82" TopEage="50" BottomEage="50" Scale9OriginX="82" Scale9OriginY="50" Scale9Width="87" Scale9Height="53" ctype="ImageViewObjectData">
                <Size X="251.0000" Y="153.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="139.3064" Y="81.8390" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1919" Y="0.4990" />
                <PreSize X="0.3457" Y="0.9329" />
                <FileData Type="Normal" Path="worldMap.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptSmallArea0" ActionTag="705265133" Tag="177" IconVisible="False" LeftMargin="351.4622" RightMargin="327.5378" TopMargin="4.1897" BottomMargin="112.8103" LeftEage="30" RightEage="30" TopEage="29" BottomEage="29" Scale9OriginX="30" Scale9OriginY="29" Scale9Width="30" Scale9Height="32" ctype="ImageViewObjectData">
                <Size X="47.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="374.9622" Y="136.3103" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5165" Y="0.8312" />
                <PreSize X="0.0647" Y="0.2866" />
                <FileData Type="Normal" Path="africaIcon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptSmallArea1" ActionTag="-130830041" Tag="182" IconVisible="False" LeftMargin="537.3468" RightMargin="141.6532" TopMargin="4.1897" BottomMargin="112.8103" LeftEage="30" RightEage="30" TopEage="29" BottomEage="29" Scale9OriginX="30" Scale9OriginY="29" Scale9Width="30" Scale9Height="32" ctype="ImageViewObjectData">
                <Size X="47.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="560.8468" Y="136.3103" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7725" Y="0.8312" />
                <PreSize X="0.0647" Y="0.2866" />
                <FileData Type="Normal" Path="asiaIcon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptSmallArea2" ActionTag="754801583" Tag="180" IconVisible="False" LeftMargin="351.3025" RightMargin="327.6975" TopMargin="59.0342" BottomMargin="57.9658" LeftEage="30" RightEage="30" TopEage="29" BottomEage="29" Scale9OriginX="30" Scale9OriginY="29" Scale9Width="30" Scale9Height="32" ctype="ImageViewObjectData">
                <Size X="47.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="374.8025" Y="81.4658" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5163" Y="0.4967" />
                <PreSize X="0.0647" Y="0.2866" />
                <FileData Type="Normal" Path="lockedArea.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptSmallArea3" ActionTag="534705327" Tag="184" IconVisible="False" LeftMargin="537.1869" RightMargin="141.8131" TopMargin="59.0342" BottomMargin="57.9658" LeftEage="30" RightEage="30" TopEage="29" BottomEage="29" Scale9OriginX="30" Scale9OriginY="29" Scale9Width="30" Scale9Height="32" ctype="ImageViewObjectData">
                <Size X="47.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="560.6869" Y="81.4658" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7723" Y="0.4967" />
                <PreSize X="0.0647" Y="0.2866" />
                <FileData Type="Normal" Path="africaIcon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptSmallArea4" ActionTag="-2037487000" Tag="181" IconVisible="False" LeftMargin="350.6673" RightMargin="328.3327" TopMargin="113.8787" BottomMargin="3.1213" LeftEage="30" RightEage="30" TopEage="29" BottomEage="29" Scale9OriginX="30" Scale9OriginY="29" Scale9Width="30" Scale9Height="32" ctype="ImageViewObjectData">
                <Size X="47.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="374.1673" Y="26.6213" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5154" Y="0.1623" />
                <PreSize X="0.0647" Y="0.2866" />
                <FileData Type="Normal" Path="africaIcon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptSmallArea5" ActionTag="1259313736" Tag="183" IconVisible="False" LeftMargin="536.5520" RightMargin="142.4480" TopMargin="113.8787" BottomMargin="3.1213" LeftEage="30" RightEage="30" TopEage="29" BottomEage="29" Scale9OriginX="30" Scale9OriginY="29" Scale9Width="30" Scale9Height="32" ctype="ImageViewObjectData">
                <Size X="47.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="560.0520" Y="26.6213" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7714" Y="0.1623" />
                <PreSize X="0.0647" Y="0.2866" />
                <FileData Type="Normal" Path="africaIcon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptSmallLine1" ActionTag="1757798503" Tag="178" IconVisible="False" LeftMargin="341.3832" RightMargin="20.6168" TopMargin="53.8328" BottomMargin="108.1672" LeftEage="30" RightEage="30" Scale9OriginX="30" Scale9Width="304" Scale9Height="2" ctype="ImageViewObjectData">
                <Size X="364.0000" Y="2.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="523.3832" Y="109.1672" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7209" Y="0.6657" />
                <PreSize X="0.5014" Y="0.0122" />
                <FileData Type="Normal" Path="mapSmallLine.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptSmallLine0" ActionTag="1465443625" Tag="179" IconVisible="False" LeftMargin="341.3831" RightMargin="20.6169" TopMargin="109.0000" BottomMargin="53.0000" LeftEage="30" RightEage="30" Scale9OriginX="30" Scale9Width="304" Scale9Height="2" ctype="ImageViewObjectData">
                <Size X="364.0000" Y="2.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="523.3831" Y="54.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7209" Y="0.3293" />
                <PreSize X="0.5014" Y="0.0122" />
                <FileData Type="Normal" Path="mapSmallLine.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblSmallArea0" ActionTag="-1983574763" Tag="185" IconVisible="False" LeftMargin="406.0000" RightMargin="228.0000" TopMargin="16.1897" BottomMargin="124.8103" FontSize="20" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="92.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="406.0000" Y="136.3103" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5592" Y="0.8312" />
                <PreSize X="0.1267" Y="0.1402" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblSmallArea1" ActionTag="-684819819" Tag="186" IconVisible="False" LeftMargin="593.0407" RightMargin="40.9593" TopMargin="16.1897" BottomMargin="124.8103" FontSize="20" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="92.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="593.0407" Y="136.3103" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8169" Y="0.8312" />
                <PreSize X="0.1267" Y="0.1402" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblSmallArea2" ActionTag="335472022" Tag="187" IconVisible="False" LeftMargin="406.0000" RightMargin="228.0000" TopMargin="71.0342" BottomMargin="69.9658" FontSize="20" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="92.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="406.0000" Y="81.4658" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5592" Y="0.4967" />
                <PreSize X="0.1267" Y="0.1402" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblSmallArea3" ActionTag="-1282447590" Tag="188" IconVisible="False" LeftMargin="593.0407" RightMargin="40.9593" TopMargin="71.0342" BottomMargin="69.9658" FontSize="20" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="92.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="593.0407" Y="81.4658" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8169" Y="0.4967" />
                <PreSize X="0.1267" Y="0.1402" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblSmallArea4" ActionTag="-133038651" Tag="189" IconVisible="False" LeftMargin="406.0000" RightMargin="228.0000" TopMargin="125.8787" BottomMargin="15.1213" FontSize="20" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="92.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="406.0000" Y="26.6213" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5592" Y="0.1623" />
                <PreSize X="0.1267" Y="0.1402" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblSmallArea5" ActionTag="1449692825" Tag="190" IconVisible="False" LeftMargin="593.0407" RightMargin="40.9593" TopMargin="125.8787" BottomMargin="15.1213" FontSize="20" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="92.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="593.0407" Y="26.6213" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8169" Y="0.1623" />
                <PreSize X="0.1267" Y="0.1402" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position X="12.0000" Y="512.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0090" Y="0.6827" />
            <PreSize X="0.5442" Y="0.2187" />
            <FileData Type="Normal" Path="worldBgSquare.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="svMap" ActionTag="2072928739" Tag="4" IconVisible="False" RightMargin="584.0000" TopMargin="408.0000" BottomMargin="94.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="750.0000" Y="248.0000" />
            <AnchorPoint />
            <Position Y="94.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.1253" />
            <PreSize X="0.5622" Y="0.3307" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="750" Height="419" />
          </AbstractNodeData>
          <AbstractNodeData Name="testNode" ActionTag="-2141880691" Tag="2330" IconVisible="True" LeftMargin="-1344.8198" RightMargin="2678.8198" TopMargin="1667.0842" BottomMargin="-917.0842" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="lblR" ActionTag="-1105483684" Tag="111" IconVisible="False" LeftMargin="470.0529" RightMargin="-487.0529" TopMargin="-720.8494" BottomMargin="686.8494" FontSize="30" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="17.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="470.0529" Y="703.8494" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblG" ActionTag="-190197711" Tag="112" IconVisible="False" LeftMargin="471.2036" RightMargin="-488.2036" TopMargin="-676.3372" BottomMargin="642.3372" FontSize="30" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="17.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="471.2036" Y="659.3372" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblB" ActionTag="564146473" Tag="113" IconVisible="False" LeftMargin="471.2036" RightMargin="-488.2036" TopMargin="-634.7454" BottomMargin="600.7454" FontSize="30" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="17.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="471.2036" Y="617.7454" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="sliderR" ActionTag="-75341441" Tag="108" IconVisible="False" LeftMargin="154.1715" RightMargin="-354.1715" TopMargin="-712.0020" BottomMargin="698.0020" TouchEnable="True" ctype="SliderObjectData">
                <Size X="200.0000" Y="14.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="254.1715" Y="705.0020" />
                <Scale ScaleX="2.0000" ScaleY="2.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <BackGroundData Type="Default" Path="Default/Slider_Back.png" Plist="" />
                <ProgressBarData Type="Default" Path="Default/Slider_PressBar.png" Plist="" />
                <BallNormalData Type="Default" Path="Default/SliderNode_Normal.png" Plist="" />
                <BallPressedData Type="Default" Path="Default/SliderNode_Press.png" Plist="" />
                <BallDisabledData Type="Default" Path="Default/SliderNode_Disable.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sliderG" ActionTag="1675927554" Tag="109" IconVisible="False" LeftMargin="151.8870" RightMargin="-351.8870" TopMargin="-669.6391" BottomMargin="655.6391" TouchEnable="True" ctype="SliderObjectData">
                <Size X="200.0000" Y="14.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="251.8870" Y="662.6391" />
                <Scale ScaleX="2.0000" ScaleY="2.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <BackGroundData Type="Default" Path="Default/Slider_Back.png" Plist="" />
                <ProgressBarData Type="Default" Path="Default/Slider_PressBar.png" Plist="" />
                <BallNormalData Type="Default" Path="Default/SliderNode_Normal.png" Plist="" />
                <BallPressedData Type="Default" Path="Default/SliderNode_Press.png" Plist="" />
                <BallDisabledData Type="Default" Path="Default/SliderNode_Disable.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sliderB" ActionTag="-1040399343" Tag="110" IconVisible="False" LeftMargin="152.2991" RightMargin="-352.2991" TopMargin="-623.6086" BottomMargin="609.6086" TouchEnable="True" ctype="SliderObjectData">
                <Size X="200.0000" Y="14.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="252.2991" Y="616.6086" />
                <Scale ScaleX="2.0000" ScaleY="2.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <BackGroundData Type="Default" Path="Default/Slider_Back.png" Plist="" />
                <ProgressBarData Type="Default" Path="Default/Slider_PressBar.png" Plist="" />
                <BallNormalData Type="Default" Path="Default/SliderNode_Normal.png" Plist="" />
                <BallPressedData Type="Default" Path="Default/SliderNode_Press.png" Plist="" />
                <BallDisabledData Type="Default" Path="Default/SliderNode_Disable.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblR_0" ActionTag="539014099" Tag="114" IconVisible="False" LeftMargin="961.7181" RightMargin="-978.7181" TopMargin="-717.0243" BottomMargin="683.0243" FontSize="30" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="17.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="961.7181" Y="700.0243" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblG_0" ActionTag="803072362" Tag="115" IconVisible="False" LeftMargin="962.8688" RightMargin="-979.8688" TopMargin="-672.5118" BottomMargin="638.5118" FontSize="30" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="17.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="962.8688" Y="655.5118" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblB_0" ActionTag="1888971561" Tag="116" IconVisible="False" LeftMargin="962.8688" RightMargin="-979.8688" TopMargin="-630.9205" BottomMargin="596.9205" FontSize="30" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="17.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="962.8688" Y="613.9205" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="sliderR_0" ActionTag="-250225700" Tag="117" IconVisible="False" LeftMargin="645.8366" RightMargin="-845.8366" TopMargin="-708.1764" BottomMargin="694.1764" TouchEnable="True" PercentInfo="100" ctype="SliderObjectData">
                <Size X="200.0000" Y="14.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="745.8366" Y="701.1764" />
                <Scale ScaleX="2.0000" ScaleY="2.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <BackGroundData Type="Default" Path="Default/Slider_Back.png" Plist="" />
                <ProgressBarData Type="Default" Path="Default/Slider_PressBar.png" Plist="" />
                <BallNormalData Type="Default" Path="Default/SliderNode_Normal.png" Plist="" />
                <BallPressedData Type="Default" Path="Default/SliderNode_Press.png" Plist="" />
                <BallDisabledData Type="Default" Path="Default/SliderNode_Disable.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sliderG_0" ActionTag="1643830211" Tag="118" IconVisible="False" LeftMargin="643.5526" RightMargin="-843.5526" TopMargin="-665.8136" BottomMargin="651.8136" TouchEnable="True" PercentInfo="100" ctype="SliderObjectData">
                <Size X="200.0000" Y="14.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="743.5526" Y="658.8136" />
                <Scale ScaleX="2.0000" ScaleY="2.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <BackGroundData Type="Default" Path="Default/Slider_Back.png" Plist="" />
                <ProgressBarData Type="Default" Path="Default/Slider_PressBar.png" Plist="" />
                <BallNormalData Type="Default" Path="Default/SliderNode_Normal.png" Plist="" />
                <BallPressedData Type="Default" Path="Default/SliderNode_Press.png" Plist="" />
                <BallDisabledData Type="Default" Path="Default/SliderNode_Disable.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sliderB_0" ActionTag="-1356468925" Tag="119" IconVisible="False" LeftMargin="643.9641" RightMargin="-843.9641" TopMargin="-619.7833" BottomMargin="605.7833" TouchEnable="True" PercentInfo="100" ctype="SliderObjectData">
                <Size X="200.0000" Y="14.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="743.9641" Y="612.7833" />
                <Scale ScaleX="2.0000" ScaleY="2.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <BackGroundData Type="Default" Path="Default/Slider_Back.png" Plist="" />
                <ProgressBarData Type="Default" Path="Default/Slider_PressBar.png" Plist="" />
                <BallNormalData Type="Default" Path="Default/SliderNode_Normal.png" Plist="" />
                <BallPressedData Type="Default" Path="Default/SliderNode_Press.png" Plist="" />
                <BallDisabledData Type="Default" Path="Default/SliderNode_Disable.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblR_1" ActionTag="-1331469368" Tag="120" IconVisible="False" LeftMargin="6.8457" RightMargin="-28.8457" TopMargin="-720.4799" BottomMargin="686.4799" FontSize="30" LabelText="R" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="22.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="6.8457" Y="703.4799" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblG_1" ActionTag="577101376" Tag="121" IconVisible="False" LeftMargin="7.9964" RightMargin="-31.9964" TopMargin="-675.9669" BottomMargin="641.9669" FontSize="30" LabelText="G" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="24.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="7.9964" Y="658.9669" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblB_1" ActionTag="1937868139" Tag="122" IconVisible="False" LeftMargin="7.9964" RightMargin="-28.9964" TopMargin="-634.3749" BottomMargin="600.3749" FontSize="30" LabelText="B" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="7.9964" Y="617.3749" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblR_0_0" ActionTag="1240216164" Tag="123" IconVisible="False" LeftMargin="510.8952" RightMargin="-532.8952" TopMargin="-716.6542" BottomMargin="682.6542" FontSize="30" LabelText="R" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="22.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="510.8952" Y="699.6542" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblG_0_0" ActionTag="978439473" Tag="124" IconVisible="False" LeftMargin="512.0459" RightMargin="-536.0459" TopMargin="-672.1416" BottomMargin="638.1416" FontSize="30" LabelText="G" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="24.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="512.0459" Y="655.1416" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblB_0_0" ActionTag="1750905787" Tag="125" IconVisible="False" LeftMargin="512.0459" RightMargin="-533.0459" TopMargin="-630.5501" BottomMargin="596.5501" FontSize="30" LabelText="B" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="512.0459" Y="613.5501" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblR_1_0" ActionTag="966144551" Tag="126" IconVisible="False" LeftMargin="77.3719" RightMargin="-216.3719" TopMargin="-750.3726" BottomMargin="716.3726" FontSize="30" LabelText="ADDITIVE" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="139.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="77.3719" Y="733.3726" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblR_0_0_0" ActionTag="-240762751" Tag="127" IconVisible="False" LeftMargin="581.4200" RightMargin="-792.4200" TopMargin="-746.5470" BottomMargin="712.5470" FontSize="30" LabelText="SUBTRACTIVE" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="211.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="581.4200" Y="729.5470" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-1344.8198" Y="-917.0842" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-1.0081" Y="-1.2228" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="pnlStatus" ActionTag="1098329180" Tag="209" IconVisible="False" LeftMargin="12.0000" RightMargin="596.0000" TopMargin="182.0000" BottomMargin="518.0000" Scale9Enable="True" LeftEage="4" RightEage="4" TopEage="4" BottomEage="4" Scale9OriginX="4" Scale9OriginY="4" Scale9Width="6" Scale9Height="6" ctype="ImageViewObjectData">
            <Size X="726.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="lblTap" ActionTag="412314487" Tag="222" IconVisible="False" LeftMargin="52.5405" RightMargin="631.4595" TopMargin="14.5000" BottomMargin="10.5000" FontSize="22" LabelText="234" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="52.5405" Y="23.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0724" Y="0.4600" />
                <PreSize X="0.0579" Y="0.5000" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="icon" ActionTag="-1319640448" Tag="221" IconVisible="False" LeftMargin="13.2600" RightMargin="678.7400" TopMargin="10.4999" BottomMargin="9.5001" LeftEage="11" RightEage="11" TopEage="9" BottomEage="9" Scale9OriginX="11" Scale9OriginY="9" Scale9Width="12" Scale9Height="12" ctype="ImageViewObjectData">
                <Size X="34.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="30.2600" Y="24.5001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0417" Y="0.4900" />
                <PreSize X="0.0468" Y="0.6000" />
                <FileData Type="Normal" Path="icon_lifetab.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblTime" ActionTag="-2119005299" Tag="223" IconVisible="False" LeftMargin="237.2072" RightMargin="446.7928" TopMargin="14.5000" BottomMargin="10.5000" FontSize="22" LabelText="999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="237.2072" Y="23.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3267" Y="0.4600" />
                <PreSize X="0.0579" Y="0.5000" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="icon_0" ActionTag="-357840556" Tag="224" IconVisible="False" LeftMargin="198.5066" RightMargin="493.4934" TopMargin="10.4999" BottomMargin="9.5001" LeftEage="11" RightEage="11" TopEage="9" BottomEage="9" Scale9OriginX="11" Scale9OriginY="9" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
                <Size X="34.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="215.5066" Y="24.5001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2968" Y="0.4900" />
                <PreSize X="0.0468" Y="0.6000" />
                <FileData Type="Normal" Path="icon_time.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblRelic" ActionTag="-848525607" Tag="225" IconVisible="False" LeftMargin="421.8739" RightMargin="262.1261" TopMargin="14.5000" BottomMargin="10.5000" FontSize="22" LabelText="888" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="421.8739" Y="23.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5811" Y="0.4600" />
                <PreSize X="0.0579" Y="0.5000" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="iconRelic" ActionTag="902728802" Tag="226" IconVisible="False" LeftMargin="385.2533" RightMargin="309.7467" TopMargin="9.5000" BottomMargin="9.5000" LeftEage="11" RightEage="11" TopEage="9" BottomEage="9" Scale9OriginX="11" Scale9OriginY="9" Scale9Width="9" Scale9Height="13" ctype="ImageViewObjectData">
                <Size X="31.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="400.7533" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5520" Y="0.5000" />
                <PreSize X="0.0427" Y="0.6200" />
                <FileData Type="Normal" Path="icon_relic.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblGem" ActionTag="492697731" Tag="227" IconVisible="False" LeftMargin="606.5405" RightMargin="77.4595" TopMargin="14.5000" BottomMargin="10.5000" FontSize="22" LabelText="666" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="606.5405" Y="23.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8355" Y="0.4600" />
                <PreSize X="0.0579" Y="0.5000" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="iconGem" ActionTag="-1567390282" Tag="228" IconVisible="False" LeftMargin="570.4999" RightMargin="124.5001" TopMargin="13.4999" BottomMargin="9.5001" LeftEage="11" RightEage="11" TopEage="9" BottomEage="9" Scale9OriginX="11" Scale9OriginY="9" Scale9Width="10" Scale9Height="9" ctype="ImageViewObjectData">
                <Size X="31.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="585.9999" Y="23.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8072" Y="0.4600" />
                <PreSize X="0.0427" Y="0.5400" />
                <FileData Type="Normal" Path="icon_dia.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblLifeCount" ActionTag="-1676325176" Tag="42" IconVisible="False" LeftMargin="53.4780" RightMargin="598.5220" TopMargin="-46.1949" BottomMargin="61.1949" FontSize="30" LabelText="9999" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="74.0000" Y="35.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="53.4780" Y="78.6949" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="100" B="69" />
                <PrePosition X="0.0737" Y="1.5739" />
                <PreSize X="0.1019" Y="0.7000" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptLife" ActionTag="-1323397388" Tag="23" IconVisible="False" LeftMargin="1.2750" RightMargin="682.7250" TopMargin="-44.6948" BottomMargin="62.6948" LeftEage="13" RightEage="13" TopEage="15" BottomEage="15" Scale9OriginX="13" Scale9OriginY="15" Scale9Width="16" Scale9Height="2" ctype="ImageViewObjectData">
                <Size X="42.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.7012" ScaleY="0.4987" />
                <Position X="30.7234" Y="78.6536" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0423" Y="1.5731" />
                <PreSize X="0.0579" Y="0.6400" />
                <FileData Type="Normal" Path="icon_life.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="12.0000" Y="518.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0090" Y="0.6907" />
            <PreSize X="0.5442" Y="0.0667" />
            <FileData Type="Normal" Path="ui_num_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnAppear" ActionTag="2027397775" VisibleForFrame="False" Tag="292" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="340.8761" RightMargin="949.1239" TopMargin="132.4950" BottomMargin="553.5050" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="14" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="44.0000" Y="64.0000" />
            <Children>
              <AbstractNodeData Name="lbTime" ActionTag="326982838" VisibleForFrame="False" Tag="293" IconVisible="False" LeftMargin="1.0489" RightMargin="-87.9015" TopMargin="67.8296" BottomMargin="-31.3656" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="130.8526" Y="27.5360" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="66.4752" Y="-17.5976" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.5108" Y="-0.2750" />
                <PreSize X="2.9739" Y="0.4302" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblDescription" ActionTag="1769908170" VisibleForFrame="False" Tag="294" IconVisible="False" LeftMargin="13.9752" RightMargin="-74.9752" TopMargin="92.0102" BottomMargin="-60.0102" FontSize="28" LabelText="Auto tap" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="105.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="66.4752" Y="-44.0102" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.5108" Y="-0.6877" />
                <PreSize X="2.3864" Y="0.5000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblTime" ActionTag="-901958260" VisibleForFrame="False" Tag="295" IconVisible="False" LeftMargin="42.9733" RightMargin="-45.9733" TopMargin="64.3658" BottomMargin="-32.3658" FontSize="28" LabelText="999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="47.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5069" ScaleY="0.4885" />
                <Position X="66.7976" Y="-16.7338" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="1.5181" Y="-0.2615" />
                <PreSize X="1.0682" Y="0.5000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="384.8761" Y="585.5050" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2885" Y="0.7807" />
            <PreSize X="0.0330" Y="0.0853" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="btn_appear.png" Plist="" />
            <PressedFileData Type="Normal" Path="btn_appear.png" Plist="" />
            <NormalFileData Type="Normal" Path="btn_appear.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnHidden" ActionTag="1677691277" Tag="296" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="340.8761" RightMargin="949.1239" TopMargin="132.4950" BottomMargin="553.5050" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="14" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="44.0000" Y="64.0000" />
            <Children>
              <AbstractNodeData Name="lbTime" ActionTag="1621256509" VisibleForFrame="False" Tag="297" IconVisible="False" LeftMargin="1.0489" RightMargin="-87.9015" TopMargin="67.8296" BottomMargin="-31.3656" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="130.8526" Y="27.5360" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="66.4752" Y="-17.5976" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.5108" Y="-0.2750" />
                <PreSize X="2.9739" Y="0.4302" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblDescription" ActionTag="1632026413" VisibleForFrame="False" Tag="298" IconVisible="False" LeftMargin="13.9752" RightMargin="-74.9752" TopMargin="92.0102" BottomMargin="-60.0102" FontSize="28" LabelText="Auto tap" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="105.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="66.4752" Y="-44.0102" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.5108" Y="-0.6877" />
                <PreSize X="2.3864" Y="0.5000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblTime" ActionTag="-1401602618" VisibleForFrame="False" Tag="299" IconVisible="False" LeftMargin="42.9733" RightMargin="-45.9733" TopMargin="64.3658" BottomMargin="-32.3658" FontSize="28" LabelText="999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="47.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5069" ScaleY="0.4885" />
                <Position X="66.7976" Y="-16.7338" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="1.5181" Y="-0.2615" />
                <PreSize X="1.0682" Y="0.5000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="384.8761" Y="585.5050" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2885" Y="0.7807" />
            <PreSize X="0.0330" Y="0.0853" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="btn_hidden.png" Plist="" />
            <PressedFileData Type="Normal" Path="btn_hidden.png" Plist="" />
            <NormalFileData Type="Normal" Path="btn_hidden.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnUp" ActionTag="2050753123" VisibleForFrame="False" Alpha="173" IconVisible="False" LeftMargin="337.8220" RightMargin="815.6008" TopMargin="100.6309" BottomMargin="510.4571" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.5772" Y="138.9120" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="428.1106" Y="579.9131" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3209" Y="0.7732" />
            <PreSize X="0.1354" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnDown" ActionTag="-798792727" VisibleForFrame="False" Alpha="173" Tag="2" IconVisible="False" LeftMargin="304.2817" RightMargin="849.1411" TopMargin="431.2414" BottomMargin="179.8466" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.5772" Y="138.9120" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="394.5703" Y="249.3026" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2958" Y="0.3324" />
            <PreSize X="0.1354" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnRight" ActionTag="1380710790" VisibleForFrame="False" Alpha="173" Tag="1" IconVisible="False" LeftMargin="537.8130" RightMargin="615.6098" TopMargin="284.7898" BottomMargin="326.2982" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.5772" Y="138.9120" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="628.1016" Y="395.7542" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4708" Y="0.5277" />
            <PreSize X="0.1354" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnLeft" ActionTag="1364007593" VisibleForFrame="False" Alpha="173" Tag="3" IconVisible="False" LeftMargin="96.5831" RightMargin="1056.8397" TopMargin="263.1247" BottomMargin="347.9633" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.5772" Y="138.9120" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="186.8717" Y="417.4193" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1401" Y="0.5566" />
            <PreSize X="0.1354" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnUp_0" ActionTag="-402086815" VisibleForFrame="False" Alpha="173" Tag="4" IconVisible="False" LeftMargin="336.9439" RightMargin="816.4789" TopMargin="-52.5829" BottomMargin="663.6709" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.5772" Y="138.9120" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="427.2325" Y="733.1269" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3203" Y="0.9775" />
            <PreSize X="0.1354" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnDown_0" ActionTag="-883215960" VisibleForFrame="False" Alpha="173" Tag="6" IconVisible="False" LeftMargin="317.0035" RightMargin="836.4193" TopMargin="593.5713" BottomMargin="17.5167" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.5772" Y="138.9120" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="407.2921" Y="86.9727" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3053" Y="0.1160" />
            <PreSize X="0.1354" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnRight_0" ActionTag="-381989952" VisibleForFrame="False" Alpha="173" Tag="5" IconVisible="False" LeftMargin="738.2336" RightMargin="415.1892" TopMargin="275.7454" BottomMargin="335.3426" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.5772" Y="138.9120" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="828.5222" Y="404.7986" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6211" Y="0.5397" />
            <PreSize X="0.1354" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnLeft_0" ActionTag="633867971" VisibleForFrame="False" Alpha="173" Tag="7" IconVisible="False" LeftMargin="-113.7513" RightMargin="1267.1741" TopMargin="267.6820" BottomMargin="343.4060" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.5772" Y="138.9120" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-23.4627" Y="412.8620" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.0176" Y="0.5505" />
            <PreSize X="0.1354" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnChange" ActionTag="-793966292" VisibleForFrame="False" Alpha="173" Tag="9" IconVisible="False" LeftMargin="-42.1863" RightMargin="1195.6091" TopMargin="504.0511" BottomMargin="107.0369" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.5772" Y="138.9120" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="48.1023" Y="176.4929" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0361" Y="0.2353" />
            <PreSize X="0.1354" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnChange_0" ActionTag="-508466000" VisibleForFrame="False" Alpha="173" Tag="8" IconVisible="False" LeftMargin="740.0714" RightMargin="413.3515" TopMargin="606.7298" BottomMargin="4.3582" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.5772" Y="138.9120" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="830.3599" Y="73.8142" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6225" Y="0.0984" />
            <PreSize X="0.1354" Y="0.1852" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="svCollection" ActionTag="-339813080" Tag="191" IconVisible="False" RightMargin="584.0000" TopMargin="236.0000" BottomMargin="94.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="750.0000" Y="420.0000" />
            <AnchorPoint />
            <Position Y="94.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.1253" />
            <PreSize X="0.5622" Y="0.5600" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="750" Height="527" />
          </AbstractNodeData>
          <AbstractNodeData Name="svShop" ActionTag="703926174" Tag="5" IconVisible="False" RightMargin="584.0000" TopMargin="237.0000" BottomMargin="94.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="750.0000" Y="419.0000" />
            <AnchorPoint />
            <Position Y="94.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.1253" />
            <PreSize X="0.5622" Y="0.5587" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="750" Height="474" />
          </AbstractNodeData>
          <AbstractNodeData Name="itemAchievement" ActionTag="1850474264" Tag="283" IconVisible="False" LeftMargin="12.5500" RightMargin="594.5000" TopMargin="237.5204" BottomMargin="347.7796" Scale9Enable="True" LeftEage="4" RightEage="4" TopEage="4" BottomEage="4" Scale9OriginX="4" Scale9OriginY="4" Scale9Width="718" Scale9Height="96" ctype="ImageViewObjectData">
            <Size X="726.9500" Y="164.7000" />
            <Children>
              <AbstractNodeData Name="lblPlayTime" ActionTag="1954449213" Tag="287" IconVisible="False" LeftMargin="28.6934" RightMargin="633.2566" TopMargin="17.4445" BottomMargin="122.2555" FontSize="22" LabelText="Status" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="65.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="28.6934" Y="134.7555" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0395" Y="0.8182" />
                <PreSize X="0.0894" Y="0.1518" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblRelicPoint" ActionTag="-1927140239" Tag="310" IconVisible="False" LeftMargin="28.6934" RightMargin="633.2566" TopMargin="51.2944" BottomMargin="88.4056" FontSize="22" LabelText="Status" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="65.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="28.6934" Y="100.9056" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0395" Y="0.6127" />
                <PreSize X="0.0894" Y="0.1518" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblAnimalCreate" ActionTag="966772093" Tag="311" IconVisible="False" LeftMargin="28.6934" RightMargin="633.2566" TopMargin="85.1443" BottomMargin="54.5557" FontSize="22" LabelText="Status" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="65.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="28.6934" Y="67.0557" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0395" Y="0.4071" />
                <PreSize X="0.0894" Y="0.1518" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblRareAnimalCreate" ActionTag="-1991354655" Tag="312" IconVisible="False" LeftMargin="28.6934" RightMargin="633.2566" TopMargin="118.9943" BottomMargin="20.7057" FontSize="22" LabelText="Status" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="65.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="28.6934" Y="33.2057" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0395" Y="0.2016" />
                <PreSize X="0.0894" Y="0.1518" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblTotalTap" ActionTag="1481081037" Tag="313" IconVisible="False" LeftMargin="372.6921" RightMargin="289.2579" TopMargin="17.4446" BottomMargin="122.2554" FontSize="22" LabelText="Status" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="65.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="372.6921" Y="134.7554" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5127" Y="0.8182" />
                <PreSize X="0.0894" Y="0.1518" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblBestHunterLevel" ActionTag="1234696756" Tag="314" IconVisible="False" LeftMargin="372.6921" RightMargin="289.2579" TopMargin="51.2945" BottomMargin="88.4055" FontSize="22" LabelText="Status" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="65.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="372.6921" Y="100.9055" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5127" Y="0.6127" />
                <PreSize X="0.0894" Y="0.1518" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblSkillUse" ActionTag="-1040616222" Tag="315" IconVisible="False" LeftMargin="372.6921" RightMargin="289.2579" TopMargin="85.1443" BottomMargin="54.5557" FontSize="22" LabelText="Status" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="65.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="372.6921" Y="67.0557" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5127" Y="0.4071" />
                <PreSize X="0.0894" Y="0.1518" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="svAchievement" ActionTag="-294616402" Tag="282" IconVisible="False" RightMargin="-2.0500" TopMargin="169.7000" BottomMargin="-255.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                <Size X="729.0000" Y="250.0000" />
                <Children>
                  <AbstractNodeData Name="btnTemp" ActionTag="-1410670769" Tag="316" IconVisible="False" LeftMargin="-205.0686" RightMargin="799.0686" TopMargin="156.8190" BottomMargin="285.1810" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="108" Scale9Height="70" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="160.0000" Y="32.0000" />
                    <Children>
                      <AbstractNodeData Name="bg" ActionTag="1983358080" Tag="406" IconVisible="False" TopMargin="-200.0000" BottomMargin="32.0000" Scale9Enable="True" LeftEage="239" RightEage="239" TopEage="34" BottomEage="34" Scale9OriginX="239" Scale9OriginY="34" Scale9Width="248" Scale9Height="36" ctype="ImageViewObjectData">
                        <Size X="160.0000" Y="200.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="80.0000" Y="132.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="4.1250" />
                        <PreSize X="1.0000" Y="6.2500" />
                        <FileData Type="Normal" Path="ui_list_bg.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="sptIconBg" ActionTag="-1861761454" Tag="401" IconVisible="False" LeftMargin="37.5248" RightMargin="30.4752" TopMargin="-191.5615" BottomMargin="131.5615" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="110" Scale9Height="110" ctype="ImageViewObjectData">
                        <Size X="92.0000" Y="92.0000" />
                        <AnchorPoint />
                        <Position X="37.5248" Y="131.5615" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2345" Y="4.1113" />
                        <PreSize X="0.5750" Y="2.8750" />
                        <FileData Type="Normal" Path="icon_frame_1_120X120.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="sptBigIcon" ActionTag="1200244014" Tag="402" IconVisible="False" LeftMargin="37.5200" RightMargin="30.4800" TopMargin="-191.5600" BottomMargin="131.5600" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="82" Scale9Height="82" ctype="ImageViewObjectData">
                        <Size X="92.0000" Y="92.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="37.5200" Y="177.5600" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2345" Y="5.5487" />
                        <PreSize X="0.5750" Y="2.8750" />
                        <FileData Type="Normal" Path="make_normal.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="sptIcon" ActionTag="1413257353" Tag="322" IconVisible="False" LeftMargin="35.1061" RightMargin="101.8939" TopMargin="7.0673" BottomMargin="5.9327" LeftEage="7" RightEage="7" TopEage="6" BottomEage="6" Scale9OriginX="7" Scale9OriginY="6" Scale9Width="9" Scale9Height="7" ctype="ImageViewObjectData">
                        <Size X="23.0000" Y="19.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="46.6061" Y="15.4327" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2913" Y="0.4823" />
                        <PreSize X="0.1437" Y="0.5938" />
                        <FileData Type="Normal" Path="icon_dia_white.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblPrice" ActionTag="-255618213" Tag="318" IconVisible="False" LeftMargin="63.0182" RightMargin="33.9818" TopMargin="4.0673" BottomMargin="2.9327" FontSize="22" LabelText="30/50" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="63.0000" Y="25.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="94.5182" Y="15.4327" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5907" Y="0.4823" />
                        <PreSize X="0.3938" Y="0.7813" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="sptIconBg_0" ActionTag="-1722866423" Tag="405" IconVisible="False" LeftMargin="9.8925" RightMargin="6.7422" TopMargin="-39.3757" BottomMargin="42.1769" Scale9Enable="True" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="110" Scale9Height="110" ctype="ImageViewObjectData">
                        <Size X="143.3653" Y="29.1988" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="81.5752" Y="56.7763" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5098" Y="1.7743" />
                        <PreSize X="0.8960" Y="0.9125" />
                        <FileData Type="Normal" Path="icon_frame_1_120X120.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lbProgress" ActionTag="1211504499" Tag="403" IconVisible="False" LeftMargin="12.6696" RightMargin="9.5194" TopMargin="-36.3829" BottomMargin="45.1697" ProgressInfo="100" ctype="LoadingBarObjectData">
                        <Size X="137.8109" Y="23.2132" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="81.5751" Y="56.7763" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="246" G="237" B="172" />
                        <PrePosition X="0.5098" Y="1.7743" />
                        <PreSize X="0.8613" Y="0.7254" />
                        <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblDescription_0" ActionTag="-220764799" Tag="319" IconVisible="False" LeftMargin="30.9779" RightMargin="29.0221" TopMargin="-37.5720" BottomMargin="44.5720" FontSize="18" LabelText="1000/1000" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="100.0000" Y="25.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="80.9779" Y="57.0720" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5061" Y="1.7835" />
                        <PreSize X="0.6250" Y="0.7813" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="149" G="140" B="106" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblTitle" ActionTag="1462568865" Tag="320" IconVisible="False" LeftMargin="2.4739" RightMargin="3.8510" TopMargin="-90.1911" BottomMargin="78.1921" IsCustomSize="True" FontSize="22" LabelText="Upgrade main tree" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="153.6751" Y="43.9990" />
                        <AnchorPoint ScaleX="0.5284" ScaleY="0.5022" />
                        <Position X="83.6829" Y="100.2874" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="94" G="93" B="73" />
                        <PrePosition X="0.5230" Y="3.1340" />
                        <PreSize X="0.9605" Y="1.3750" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblButtonText" ActionTag="-663070600" Tag="323" IconVisible="False" LeftMargin="49.0179" RightMargin="47.9821" TopMargin="3.0675" BottomMargin="3.9325" FontSize="22" LabelText="30/50" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="63.0000" Y="25.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="80.5179" Y="16.4325" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5032" Y="0.5135" />
                        <PreSize X="0.3938" Y="0.7813" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-125.0686" Y="301.1810" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.1659" Y="0.6354" />
                    <PreSize X="0.2122" Y="0.0675" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="btnDisabled.png" Plist="" />
                    <PressedFileData Type="Normal" Path="btnUpgradeDown.png" Plist="" />
                    <NormalFileData Type="Normal" Path="btnUpgrade.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="-255.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="-1.5483" />
                <PreSize X="1.0028" Y="1.5179" />
                <SingleColor A="255" R="255" G="150" B="100" />
                <FirstColor A="255" R="255" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
                <InnerNodeSize Width="754" Height="474" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position X="12.5500" Y="512.4796" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.6833" />
            <PreSize X="0.5449" Y="0.2196" />
            <FileData Type="Normal" Path="ui_list_bg_locked.png.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="svRelic" ActionTag="1666051856" Tag="3" IconVisible="False" RightMargin="584.0000" TopMargin="347.0000" BottomMargin="94.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="750.0000" Y="309.0000" />
            <AnchorPoint />
            <Position Y="94.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.1253" />
            <PreSize X="0.5622" Y="0.4120" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="750" Height="474" />
          </AbstractNodeData>
          <AbstractNodeData Name="itemRelic" ActionTag="-1640813626" Tag="271" IconVisible="False" LeftMargin="12.0000" RightMargin="596.0000" TopMargin="238.0000" BottomMargin="408.0000" Scale9Enable="True" LeftEage="4" RightEage="4" TopEage="4" BottomEage="4" Scale9OriginX="4" Scale9OriginY="4" Scale9Width="718" Scale9Height="96" ctype="ImageViewObjectData">
            <Size X="726.0000" Y="104.0000" />
            <Children>
              <AbstractNodeData Name="btnUpgrade" ActionTag="-417686608" Tag="272" IconVisible="False" LeftMargin="582.0000" RightMargin="6.0000" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="3" RightEage="3" TopEage="3" BottomEage="3" Scale9OriginX="3" Scale9OriginY="3" Scale9Width="132" Scale9Height="86" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="138.0000" Y="92.0000" />
                <Children>
                  <AbstractNodeData Name="lblDescription" ActionTag="-582409209" Tag="273" IconVisible="False" LeftMargin="29.5000" RightMargin="29.5000" TopMargin="22.5000" BottomMargin="46.5000" FontSize="20" LabelText="생명창조" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="79.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="69.0000" Y="58.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="241" B="127" />
                    <PrePosition X="0.5000" Y="0.6304" />
                    <PreSize X="0.5725" Y="0.2500" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptIcon" ActionTag="-267884180" Tag="274" IconVisible="False" LeftMargin="23.0851" RightMargin="87.9149" TopMargin="47.5000" BottomMargin="17.5000" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="1" Scale9Height="7" ctype="ImageViewObjectData">
                    <Size X="27.0000" Y="27.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="36.5851" Y="31.0000" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2651" Y="0.3370" />
                    <PreSize X="0.1957" Y="0.2935" />
                    <FileData Type="Normal" Path="icon_relic_white.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblPrice" ActionTag="-410942111" Tag="275" IconVisible="False" LeftMargin="52.0000" RightMargin="52.0000" TopMargin="50.5000" BottomMargin="20.5000" FontSize="18" LabelText="8.7j" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="34.0000" Y="21.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="69.0000" Y="31.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.3370" />
                    <PreSize X="0.2464" Y="0.2283" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" />
                <Position X="720.0000" Y="6.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9917" Y="0.0577" />
                <PreSize X="0.1901" Y="0.8846" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Normal" Path="btnUnlockDown.png" Plist="" />
                <NormalFileData Type="Normal" Path="btnUnlock.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="slot0" ActionTag="-1205735911" IconVisible="False" LeftMargin="6.0000" RightMargin="628.0000" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" Scale9Enable="True" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="6" Scale9Height="6" ctype="ImageViewObjectData">
                <Size X="92.0000" Y="92.0000" />
                <Children>
                  <AbstractNodeData Name="sptIcon" ActionTag="-591059616" Tag="207" IconVisible="False" LeftMargin="12.9114" RightMargin="47.0886" TopMargin="58.5247" BottomMargin="6.4753" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="6" Scale9Height="7" ctype="ImageViewObjectData">
                    <Size X="32.0000" Y="27.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="28.9114" Y="19.9753" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3143" Y="0.2171" />
                    <PreSize X="0.3478" Y="0.2935" />
                    <FileData Type="Normal" Path="icon_dia.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblPrice" ActionTag="-1439961649" Tag="208" IconVisible="False" LeftMargin="44.3266" RightMargin="13.6734" TopMargin="60.9747" BottomMargin="10.0253" FontSize="18" LabelText="8.7j" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="34.0000" Y="21.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="61.3266" Y="20.5253" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6666" Y="0.2231" />
                    <PreSize X="0.3696" Y="0.2283" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptLock" ActionTag="-919239864" Tag="221" IconVisible="False" LeftMargin="23.0000" RightMargin="23.0000" TopMargin="2.0915" BottomMargin="27.9085" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="20" Scale9Height="42" ctype="ImageViewObjectData">
                    <Size X="46.0000" Y="62.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="46.0000" Y="58.9085" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6403" />
                    <PreSize X="0.5000" Y="0.6739" />
                    <FileData Type="Normal" Path="sptLock.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="6.0000" Y="6.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0083" Y="0.0577" />
                <PreSize X="0.1267" Y="0.8846" />
                <FileData Type="Normal" Path="ui_picture_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="slot1" ActionTag="-1412831835" Tag="1" IconVisible="False" LeftMargin="103.0155" RightMargin="530.9845" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" Scale9Enable="True" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="6" Scale9Height="6" ctype="ImageViewObjectData">
                <Size X="92.0000" Y="92.0000" />
                <Children>
                  <AbstractNodeData Name="sptLock" ActionTag="85213103" Tag="222" IconVisible="False" LeftMargin="23.0000" RightMargin="23.0000" TopMargin="2.0914" BottomMargin="27.9086" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="20" Scale9Height="42" ctype="ImageViewObjectData">
                    <Size X="46.0000" Y="62.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="46.0000" Y="58.9086" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6403" />
                    <PreSize X="0.5000" Y="0.6739" />
                    <FileData Type="Normal" Path="sptLock.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptIcon" ActionTag="-1593601130" Tag="203" IconVisible="False" LeftMargin="10.3506" RightMargin="49.6494" TopMargin="58.5247" BottomMargin="6.4753" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="6" Scale9Height="7" ctype="ImageViewObjectData">
                    <Size X="32.0000" Y="27.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="26.3506" Y="19.9753" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2864" Y="0.2171" />
                    <PreSize X="0.3478" Y="0.2935" />
                    <FileData Type="Normal" Path="icon_dia.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblPrice" ActionTag="-2036377055" Tag="204" IconVisible="False" LeftMargin="41.7658" RightMargin="16.2342" TopMargin="60.9745" BottomMargin="10.0255" FontSize="18" LabelText="8.7j" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="34.0000" Y="21.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="58.7658" Y="20.5255" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6388" Y="0.2231" />
                    <PreSize X="0.3696" Y="0.2283" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="103.0155" Y="6.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1419" Y="0.0577" />
                <PreSize X="0.1267" Y="0.8846" />
                <FileData Type="Normal" Path="ui_picture_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="slot2" ActionTag="1560797253" Tag="2" IconVisible="False" LeftMargin="200.0311" RightMargin="433.9689" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" Scale9Enable="True" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="6" Scale9Height="6" ctype="ImageViewObjectData">
                <Size X="92.0000" Y="92.0000" />
                <Children>
                  <AbstractNodeData Name="sptIcon" CanEdit="False" ActionTag="1555547386" Tag="215" IconVisible="False" LeftMargin="0.8936" RightMargin="59.1064" TopMargin="58.5247" BottomMargin="6.4753" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="6" Scale9Height="7" ctype="ImageViewObjectData">
                    <Size X="32.0000" Y="27.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="16.8936" Y="19.9753" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1836" Y="0.2171" />
                    <PreSize X="0.3478" Y="0.2935" />
                    <FileData Type="Normal" Path="icon_dia.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblPrice" ActionTag="247405539" Tag="216" IconVisible="False" LeftMargin="32.3079" RightMargin="25.6921" TopMargin="60.9745" BottomMargin="10.0255" FontSize="18" LabelText="8.7j" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="34.0000" Y="21.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.3079" Y="20.5255" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5360" Y="0.2231" />
                    <PreSize X="0.3696" Y="0.2283" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptLock" ActionTag="-96244154" Tag="223" IconVisible="False" LeftMargin="23.0000" RightMargin="23.0000" TopMargin="2.0914" BottomMargin="27.9086" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="20" Scale9Height="42" ctype="ImageViewObjectData">
                    <Size X="46.0000" Y="62.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="46.0000" Y="58.9086" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6403" />
                    <PreSize X="0.5000" Y="0.6739" />
                    <FileData Type="Normal" Path="sptLock.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="200.0311" Y="6.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2755" Y="0.0577" />
                <PreSize X="0.1267" Y="0.8846" />
                <FileData Type="Normal" Path="ui_picture_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="slot3" ActionTag="-2003683076" Tag="3" IconVisible="False" LeftMargin="298.0467" RightMargin="335.9533" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" Scale9Enable="True" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="6" Scale9Height="6" ctype="ImageViewObjectData">
                <Size X="92.0000" Y="92.0000" />
                <Children>
                  <AbstractNodeData Name="sptIcon" ActionTag="1729651228" Tag="217" IconVisible="False" LeftMargin="8.0309" RightMargin="51.9691" TopMargin="58.5247" BottomMargin="6.4753" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="6" Scale9Height="7" ctype="ImageViewObjectData">
                    <Size X="32.0000" Y="27.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="24.0309" Y="19.9753" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2612" Y="0.2171" />
                    <PreSize X="0.3478" Y="0.2935" />
                    <FileData Type="Normal" Path="icon_dia.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblPrice" ActionTag="-508376920" Tag="218" IconVisible="False" LeftMargin="39.4467" RightMargin="18.5533" TopMargin="60.9745" BottomMargin="10.0255" FontSize="18" LabelText="8.7j" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="34.0000" Y="21.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="56.4467" Y="20.5255" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6136" Y="0.2231" />
                    <PreSize X="0.3696" Y="0.2283" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptLock" ActionTag="-1328928180" Tag="224" IconVisible="False" LeftMargin="23.0000" RightMargin="23.0000" TopMargin="2.0914" BottomMargin="27.9086" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="20" Scale9Height="42" ctype="ImageViewObjectData">
                    <Size X="46.0000" Y="62.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="46.0000" Y="58.9086" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6403" />
                    <PreSize X="0.5000" Y="0.6739" />
                    <FileData Type="Normal" Path="sptLock.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="298.0467" Y="6.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4105" Y="0.0577" />
                <PreSize X="0.1267" Y="0.8846" />
                <FileData Type="Normal" Path="ui_picture_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="slot4" ActionTag="939161843" Tag="4" IconVisible="False" LeftMargin="394.0622" RightMargin="239.9378" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" Scale9Enable="True" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="6" Scale9Height="6" ctype="ImageViewObjectData">
                <Size X="92.0000" Y="92.0000" />
                <Children>
                  <AbstractNodeData Name="sptIcon" ActionTag="-75662224" Tag="219" IconVisible="False" LeftMargin="10.0215" RightMargin="49.9785" TopMargin="58.5247" BottomMargin="6.4753" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="6" Scale9Height="7" ctype="ImageViewObjectData">
                    <Size X="32.0000" Y="27.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="26.0215" Y="19.9753" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2828" Y="0.2171" />
                    <PreSize X="0.3478" Y="0.2935" />
                    <FileData Type="Normal" Path="icon_dia.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblPrice" ActionTag="-350840680" Tag="220" IconVisible="False" LeftMargin="41.4366" RightMargin="16.5634" TopMargin="60.9745" BottomMargin="10.0255" FontSize="18" LabelText="8.7j" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="34.0000" Y="21.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="58.4366" Y="20.5255" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6352" Y="0.2231" />
                    <PreSize X="0.3696" Y="0.2283" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptLock" ActionTag="1726629641" Tag="225" IconVisible="False" LeftMargin="23.0000" RightMargin="23.0000" TopMargin="2.0914" BottomMargin="27.9086" LeftEage="13" RightEage="13" TopEage="10" BottomEage="10" Scale9OriginX="13" Scale9OriginY="10" Scale9Width="20" Scale9Height="42" ctype="ImageViewObjectData">
                    <Size X="46.0000" Y="62.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="46.0000" Y="58.9086" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6403" />
                    <PreSize X="0.5000" Y="0.6739" />
                    <FileData Type="Normal" Path="sptLock.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="394.0622" Y="6.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5428" Y="0.0577" />
                <PreSize X="0.1267" Y="0.8846" />
                <FileData Type="Normal" Path="ui_picture_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn0" ActionTag="-1629437030" Alpha="0" IconVisible="False" LeftMargin="7.5077" RightMargin="631.8153" TopMargin="8.0356" BottomMargin="10.2482" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="86.6770" Y="85.7162" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="50.8462" Y="53.1063" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0700" Y="0.5106" />
                <PreSize X="0.1194" Y="0.8242" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
                <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btnManage" ActionTag="363439101" Tag="284" IconVisible="False" LeftMargin="497.2189" RightMargin="154.7811" TopMargin="9.4914" BottomMargin="66.5086" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="3" RightEage="3" TopEage="3" BottomEage="3" Scale9OriginX="3" Scale9OriginY="3" Scale9Width="68" Scale9Height="22" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="74.0000" Y="28.0000" />
                <Children>
                  <AbstractNodeData Name="lblDescription" ActionTag="-1729344225" Tag="285" IconVisible="False" LeftMargin="-2.5000" RightMargin="-2.5000" TopMargin="2.5000" BottomMargin="2.5000" FontSize="20" LabelText="Manage" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="79.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="37.0000" Y="14.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0676" Y="0.8214" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" />
                <Position X="571.2189" Y="66.5086" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7868" Y="0.6395" />
                <PreSize X="0.1019" Y="0.2692" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Normal" Path="btnManage.png" Plist="" />
                <NormalFileData Type="Normal" Path="btnManage.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn1" ActionTag="-1322693449" Alpha="0" Tag="1" IconVisible="False" LeftMargin="103.3813" RightMargin="535.9417" TopMargin="7.8662" BottomMargin="10.4176" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="86.6770" Y="85.7162" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="146.7198" Y="53.2757" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2021" Y="0.5123" />
                <PreSize X="0.1194" Y="0.8242" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
                <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn2" ActionTag="-1416001762" Alpha="0" Tag="2" IconVisible="False" LeftMargin="200.5538" RightMargin="438.7692" TopMargin="8.9960" BottomMargin="9.2878" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="86.6770" Y="85.7162" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="243.8923" Y="52.1459" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3359" Y="0.5014" />
                <PreSize X="0.1194" Y="0.8242" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
                <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn3" ActionTag="2121983135" Alpha="0" Tag="3" IconVisible="False" LeftMargin="297.7266" RightMargin="341.5964" TopMargin="8.9960" BottomMargin="9.2878" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="86.6770" Y="85.7162" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="341.0651" Y="52.1459" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4698" Y="0.5014" />
                <PreSize X="0.1194" Y="0.8242" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
                <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn4" ActionTag="-752510545" Alpha="0" Tag="4" IconVisible="False" LeftMargin="396.0290" RightMargin="243.2940" TopMargin="7.8658" BottomMargin="10.4180" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="86.6770" Y="85.7162" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="439.3675" Y="53.2761" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6052" Y="0.5123" />
                <PreSize X="0.1194" Y="0.8242" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
                <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position X="12.0000" Y="512.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0090" Y="0.6827" />
            <PreSize X="0.5442" Y="0.1387" />
            <FileData Type="Normal" Path="ui_list_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="itemAnimal" ActionTag="1073264837" Tag="1" IconVisible="False" LeftMargin="12.5500" RightMargin="594.5000" TopMargin="237.5203" BottomMargin="347.7797" Scale9Enable="True" LeftEage="4" RightEage="4" TopEage="4" BottomEage="4" Scale9OriginX="4" Scale9OriginY="4" Scale9Width="718" Scale9Height="96" ctype="ImageViewObjectData">
            <Size X="726.9500" Y="164.7000" />
            <Children>
              <AbstractNodeData Name="lbProgressBack" ActionTag="2014739352" Tag="246" IconVisible="False" LeftMargin="137.2729" RightMargin="166.1243" TopMargin="101.1007" BottomMargin="38.8489" LeftEage="80" RightEage="80" TopEage="8" BottomEage="8" Scale9OriginX="80" Scale9OriginY="8" Scale9Width="85" Scale9Height="11" ctype="ImageViewObjectData">
                <Size X="423.5528" Y="24.7504" />
                <AnchorPoint />
                <Position X="137.2729" Y="38.8489" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1888" Y="0.2359" />
                <PreSize X="0.5826" Y="0.1503" />
                <FileData Type="Normal" Path="progressBackground.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbProgress" ActionTag="-461947640" Tag="270" IconVisible="False" LeftMargin="137.8578" RightMargin="165.5422" TopMargin="100.4628" BottomMargin="38.3620" ProgressInfo="100" ctype="LoadingBarObjectData">
                <Size X="423.5500" Y="25.8752" />
                <AnchorPoint />
                <Position X="137.8578" Y="38.3620" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1896" Y="0.2329" />
                <PreSize X="0.5826" Y="0.1571" />
                <ImageFileData Type="Normal" Path="progressForeground.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblPercent" ActionTag="-13599023" Tag="244" IconVisible="False" LeftMargin="325.6329" RightMargin="353.3171" TopMargin="104.4004" BottomMargin="42.2996" FontSize="16" LabelText="23/40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="48.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="349.6329" Y="51.2996" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4810" Y="0.3115" />
                <PreSize X="0.0660" Y="0.1093" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblAnimalCount" ActionTag="1584885310" Tag="261" IconVisible="False" LeftMargin="137.9829" RightMargin="525.9671" TopMargin="28.8437" BottomMargin="110.8563" FontSize="22" LabelText="23/40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="63.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="137.9829" Y="123.3563" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1898" Y="0.7490" />
                <PreSize X="0.0867" Y="0.1518" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="sptIcon" ActionTag="1805556947" Tag="262" IconVisible="False" LeftMargin="17.5329" RightMargin="626.4171" TopMargin="29.7566" BottomMargin="31.9434" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="73" Scale9Height="93" ctype="ImageViewObjectData">
                <Size X="83.0000" Y="103.0000" />
                <AnchorPoint />
                <Position X="17.5329" Y="31.9434" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0241" Y="0.1939" />
                <PreSize X="0.1142" Y="0.6254" />
                <FileData Type="Normal" Path="animal_info.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="btnManageAnimal" ActionTag="1875832943" Tag="242" IconVisible="False" LeftMargin="582.2084" RightMargin="6.7416" TopMargin="34.3380" BottomMargin="38.3620" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="3" RightEage="3" TopEage="3" BottomEage="3" Scale9OriginX="3" Scale9OriginY="3" Scale9Width="132" Scale9Height="86" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="138.0000" Y="92.0000" />
                <Children>
                  <AbstractNodeData Name="lblDescription" ActionTag="-531251891" Tag="243" IconVisible="False" LeftMargin="29.5000" RightMargin="29.5000" TopMargin="34.5000" BottomMargin="34.5000" FontSize="20" LabelText="Manage" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="79.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="69.0000" Y="46.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.5725" Y="0.2500" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" />
                <Position X="720.2084" Y="38.3620" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9907" Y="0.2329" />
                <PreSize X="0.1898" Y="0.5586" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="btnDisabled.png" Plist="" />
                <PressedFileData Type="Normal" Path="btnUnlockDown.png" Plist="" />
                <NormalFileData Type="Normal" Path="btnUnlock.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="itemAnimal_0" ActionTag="-1625251152" Tag="231" IconVisible="False" LeftMargin="0.0406" RightMargin="0.9594" TopMargin="184.6654" BottomMargin="-123.9654" Scale9Enable="True" LeftEage="4" RightEage="4" TopEage="4" BottomEage="4" Scale9OriginX="4" Scale9OriginY="4" Scale9Width="718" Scale9Height="96" ctype="ImageViewObjectData">
                <Size X="725.9500" Y="104.0000" />
                <Children>
                  <AbstractNodeData Name="btnUpgrade" ActionTag="524511280" Tag="233" IconVisible="False" LeftMargin="582.5101" RightMargin="5.4399" TopMargin="6.0179" BottomMargin="5.9821" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="3" RightEage="3" TopEage="3" BottomEage="3" Scale9OriginX="3" Scale9OriginY="3" Scale9Width="132" Scale9Height="86" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="138.0000" Y="92.0000" />
                    <Children>
                      <AbstractNodeData Name="sptIcon" ActionTag="539004993" Tag="468" IconVisible="False" LeftMargin="42.4600" RightMargin="73.5400" TopMargin="50.1553" BottomMargin="25.8447" ctype="SpriteObjectData">
                        <Size X="22.0000" Y="16.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="53.4600" Y="33.8447" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3874" Y="0.3679" />
                        <PreSize X="0.1594" Y="0.1739" />
                        <FileData Type="Normal" Path="icon_leaf_white.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblDescription" ActionTag="-735926095" Tag="234" IconVisible="False" LeftMargin="23.9999" RightMargin="24.0001" TopMargin="23.5600" BottomMargin="45.4400" FontSize="20" LabelText="관리하기d" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="90.0000" Y="23.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="68.9999" Y="56.9400" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.6189" />
                        <PreSize X="0.6522" Y="0.2500" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblPrice" ActionTag="1712129977" Tag="467" IconVisible="False" LeftMargin="63.4287" RightMargin="35.5713" TopMargin="46.6553" BottomMargin="22.3447" FontSize="20" LabelText="300" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="39.0000" Y="23.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="82.9287" Y="33.8447" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6009" Y="0.3679" />
                        <PreSize X="0.2826" Y="0.2500" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="1.0000" />
                    <Position X="720.5101" Y="5.9821" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9925" Y="0.0575" />
                    <PreSize X="0.1901" Y="0.8846" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="btnDisabled.png" Plist="" />
                    <PressedFileData Type="Normal" Path="btnCreateDown.png" Plist="" />
                    <NormalFileData Type="Normal" Path="btnCreate.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblDescription" ActionTag="601508627" Tag="236" IconVisible="False" LeftMargin="109.0370" RightMargin="553.9130" TopMargin="22.8800" BottomMargin="56.1200" FontSize="22" LabelText="23/40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="63.0000" Y="25.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="109.0370" Y="68.6200" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="94" G="93" B="73" />
                    <PrePosition X="0.1502" Y="0.6598" />
                    <PreSize X="0.0868" Y="0.2404" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblDescription_0" ActionTag="1804765205" Tag="245" IconVisible="False" LeftMargin="108.8419" RightMargin="158.9152" TopMargin="50.0000" BottomMargin="14.0000" IsCustomSize="True" FontSize="18" LabelText="사바나 사막의 꽃 눈이 " VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="458.1929" Y="40.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="108.8419" Y="34.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="94" G="93" B="73" />
                    <PrePosition X="0.1499" Y="0.3269" />
                    <PreSize X="0.6312" Y="0.3846" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptIconBg" ActionTag="-396384187" Tag="237" IconVisible="False" LeftMargin="6.8260" RightMargin="627.1240" TopMargin="4.8019" BottomMargin="7.1981" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="110" Scale9Height="110" ctype="ImageViewObjectData">
                    <Size X="92.0000" Y="92.0000" />
                    <AnchorPoint />
                    <Position X="6.8260" Y="7.1981" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0094" Y="0.0692" />
                    <PreSize X="0.1267" Y="0.8846" />
                    <FileData Type="Normal" Path="icon_frame_1_120X120.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptIcon" ActionTag="-972604924" Tag="471" IconVisible="False" LeftMargin="6.8262" RightMargin="627.1238" TopMargin="4.8019" BottomMargin="7.1981" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="82" Scale9Height="82" ctype="ImageViewObjectData">
                    <Size X="92.0000" Y="92.0000" />
                    <AnchorPoint />
                    <Position X="6.8262" Y="7.1981" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0094" Y="0.0692" />
                    <PreSize X="0.1267" Y="0.8846" />
                    <FileData Type="Normal" Path="make_normal.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position X="0.0406" Y="-19.9654" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0001" Y="-0.1212" />
                <PreSize X="0.9986" Y="0.6315" />
                <FileData Type="Normal" Path="ui_list_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lblSurvivalPower" ActionTag="-1173927619" Tag="460" IconVisible="False" LeftMargin="140.5117" RightMargin="523.4383" TopMargin="60.6709" BottomMargin="79.0291" FontSize="22" LabelText="23/40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="63.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="140.5117" Y="91.5291" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1933" Y="0.5557" />
                <PreSize X="0.0867" Y="0.1518" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="itemAnimal_1" ActionTag="1069918958" Tag="461" IconVisible="False" LeftMargin="-0.7386" RightMargin="1.7386" TopMargin="296.6530" BottomMargin="-235.9530" Scale9Enable="True" LeftEage="4" RightEage="4" TopEage="4" BottomEage="4" Scale9OriginX="4" Scale9OriginY="4" Scale9Width="718" Scale9Height="96" ctype="ImageViewObjectData">
                <Size X="725.9500" Y="104.0000" />
                <Children>
                  <AbstractNodeData Name="btnUpgrade" ActionTag="-848841766" Tag="462" IconVisible="False" LeftMargin="583.2892" RightMargin="4.6608" TopMargin="5.4518" BottomMargin="6.5482" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="3" RightEage="3" TopEage="3" BottomEage="3" Scale9OriginX="3" Scale9OriginY="3" Scale9Width="132" Scale9Height="86" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="138.0000" Y="92.0000" />
                    <Children>
                      <AbstractNodeData Name="sptIcon" ActionTag="-32737777" VisibleForFrame="False" Tag="469" RotationSkewX="1.7569" RotationSkewY="1.7569" IconVisible="False" LeftMargin="41.8050" RightMargin="74.1950" TopMargin="49.6792" BottomMargin="26.3208" ctype="SpriteObjectData">
                        <Size X="22.0000" Y="16.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="52.8050" Y="34.3208" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3826" Y="0.3731" />
                        <PreSize X="0.1594" Y="0.1739" />
                        <FileData Type="Normal" Path="icon_leaf_white.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblPrice" ActionTag="1491880716" VisibleForFrame="False" Tag="470" IconVisible="False" LeftMargin="62.7742" RightMargin="36.2258" TopMargin="46.1792" BottomMargin="22.8208" FontSize="20" LabelText="300" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="39.0000" Y="23.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="82.2742" Y="34.3208" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5962" Y="0.3731" />
                        <PreSize X="0.2826" Y="0.2500" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="lblDescription" ActionTag="1822150663" Tag="463" IconVisible="False" LeftMargin="30.5003" RightMargin="30.4997" TopMargin="31.5000" BottomMargin="37.5000" FontSize="20" LabelText="관리하기" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="77.0000" Y="23.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="69.0003" Y="49.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5326" />
                        <PreSize X="0.5580" Y="0.2500" />
                        <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="1.0000" />
                    <Position X="721.2892" Y="6.5482" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9936" Y="0.0630" />
                    <PreSize X="0.1901" Y="0.8846" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="btnDisabled.png" Plist="" />
                    <PressedFileData Type="Normal" Path="btnCreateDown.png" Plist="" />
                    <NormalFileData Type="Normal" Path="btnCreate.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblDescription" ActionTag="-899925365" Tag="464" IconVisible="False" LeftMargin="109.0370" RightMargin="553.9130" TopMargin="23.5000" BottomMargin="55.5000" FontSize="22" LabelText="23/40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="63.0000" Y="25.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="109.0370" Y="68.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="94" G="93" B="73" />
                    <PrePosition X="0.1502" Y="0.6538" />
                    <PreSize X="0.0868" Y="0.2404" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lblDescription_0" ActionTag="160738171" Tag="465" IconVisible="False" LeftMargin="108.8419" RightMargin="154.8618" TopMargin="50.0000" BottomMargin="14.0000" IsCustomSize="True" FontSize="18" LabelText="사바나 사막의 꽃 눈이 ]" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="462.2463" Y="40.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="108.8419" Y="34.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="94" G="93" B="73" />
                    <PrePosition X="0.1499" Y="0.3269" />
                    <PreSize X="0.6367" Y="0.3846" />
                    <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptIconBg" ActionTag="-1574089765" Tag="472" IconVisible="False" LeftMargin="6.8265" RightMargin="627.1235" TopMargin="4.8019" BottomMargin="7.1981" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="110" Scale9Height="110" ctype="ImageViewObjectData">
                    <Size X="92.0000" Y="92.0000" />
                    <AnchorPoint />
                    <Position X="6.8265" Y="7.1981" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0094" Y="0.0692" />
                    <PreSize X="0.1267" Y="0.8846" />
                    <FileData Type="Normal" Path="icon_frame_1_120X120.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="sptIcon" ActionTag="-334896468" Tag="473" IconVisible="False" LeftMargin="6.8265" RightMargin="627.1235" TopMargin="4.8019" BottomMargin="7.1981" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="82" Scale9Height="82" ctype="ImageViewObjectData">
                    <Size X="92.0000" Y="92.0000" />
                    <AnchorPoint />
                    <Position X="6.8265" Y="7.1981" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0094" Y="0.0692" />
                    <PreSize X="0.1267" Y="0.8846" />
                    <FileData Type="Normal" Path="make_rare.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position X="-0.7386" Y="-131.9530" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.0010" Y="-0.8012" />
                <PreSize X="0.9986" Y="0.6315" />
                <FileData Type="Normal" Path="ui_list_bg.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position X="12.5500" Y="512.4797" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0094" Y="0.6833" />
            <PreSize X="0.5449" Y="0.2196" />
            <FileData Type="Normal" Path="ui_list_bg_locked.png.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu0" ActionTag="-1325794831" IconVisible="False" RightMargin="1227.0000" TopMargin="656.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="107.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="981121819" Tag="240" IconVisible="False" LeftMargin="29.0000" RightMargin="29.0000" TopMargin="8.5000" BottomMargin="36.5000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="31" Scale9Height="19" ctype="ImageViewObjectData">
                <Size X="49.0000" Y="49.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="61.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6489" />
                <PreSize X="0.4579" Y="0.5213" />
                <FileData Type="Normal" Path="menu_1.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbl" ActionTag="170428052" Tag="242" IconVisible="False" LeftMargin="34.0000" RightMargin="34.0000" TopMargin="65.5000" BottomMargin="5.5000" FontSize="20" LabelText="초원" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="39.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="17.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1809" />
                <PreSize X="0.3645" Y="0.2447" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="53.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0401" />
            <PreSize X="0.0802" Y="0.1253" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="MainBtn_Off_pixel.png" Plist="" />
            <NormalFileData Type="Normal" Path="MainBtn_On_pixel.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu1" ActionTag="474209550" Tag="1" IconVisible="False" LeftMargin="107.0000" RightMargin="1120.0000" TopMargin="656.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="107.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="-91327871" Tag="244" IconVisible="False" LeftMargin="34.5000" RightMargin="34.5000" TopMargin="9.5000" BottomMargin="37.5000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="20" Scale9Height="17" ctype="ImageViewObjectData">
                <Size X="38.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="61.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6489" />
                <PreSize X="0.3551" Y="0.5000" />
                <FileData Type="Normal" Path="menu_2.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbl" ActionTag="2076527382" Tag="245" IconVisible="False" LeftMargin="33.0000" RightMargin="33.0000" TopMargin="65.5000" BottomMargin="5.5000" FontSize="20" LabelText="동물" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="17.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1809" />
                <PreSize X="0.3832" Y="0.2447" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="160.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1203" />
            <PreSize X="0.0802" Y="0.1253" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="MainBtn_Off_pixel.png" Plist="" />
            <NormalFileData Type="Normal" Path="MainBtn_On_pixel.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu2" ActionTag="-311188376" Tag="2" IconVisible="False" LeftMargin="214.0000" RightMargin="1013.0000" TopMargin="656.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="107.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="318037365" Tag="246" IconVisible="False" LeftMargin="30.0000" RightMargin="30.0000" TopMargin="11.5000" BottomMargin="39.5000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="29" Scale9Height="13" ctype="ImageViewObjectData">
                <Size X="47.0000" Y="43.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="61.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6489" />
                <PreSize X="0.4393" Y="0.4574" />
                <FileData Type="Normal" Path="menu_3.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbl" ActionTag="-1241108945" Tag="247" IconVisible="False" LeftMargin="6.5000" RightMargin="6.5000" TopMargin="65.5000" BottomMargin="5.5000" FontSize="20" LabelText="Collection" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="94.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="17.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1809" />
                <PreSize X="0.8785" Y="0.2447" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="267.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2005" />
            <PreSize X="0.0802" Y="0.1253" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="MainBtn_Off_pixel.png" Plist="" />
            <NormalFileData Type="Normal" Path="MainBtn_On_pixel.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu3" ActionTag="-1133888455" Tag="3" IconVisible="False" LeftMargin="321.0000" RightMargin="906.0000" TopMargin="656.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="107.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="313208089" Tag="248" IconVisible="False" LeftMargin="30.5000" RightMargin="30.5000" TopMargin="8.5000" BottomMargin="36.5000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="28" Scale9Height="19" ctype="ImageViewObjectData">
                <Size X="46.0000" Y="49.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="61.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6489" />
                <PreSize X="0.4299" Y="0.5213" />
                <FileData Type="Normal" Path="menu_4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbl" ActionTag="-480009677" Tag="249" IconVisible="False" LeftMargin="33.0000" RightMargin="33.0000" TopMargin="65.5000" BottomMargin="5.5000" FontSize="20" LabelText="유물" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="17.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1809" />
                <PreSize X="0.3832" Y="0.2447" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="374.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2807" />
            <PreSize X="0.0802" Y="0.1253" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="MainBtn_Off_pixel.png" Plist="" />
            <NormalFileData Type="Normal" Path="MainBtn_On_pixel.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu4" ActionTag="1612111464" Tag="4" IconVisible="False" LeftMargin="428.0000" RightMargin="799.0000" TopMargin="656.0000" BottomMargin="0.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="107.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="-355746642" Tag="250" IconVisible="False" LeftMargin="31.5000" RightMargin="31.5000" TopMargin="10.5000" BottomMargin="38.5000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="26" Scale9Height="15" ctype="ImageViewObjectData">
                <Size X="44.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="61.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6489" />
                <PreSize X="0.4112" Y="0.4787" />
                <FileData Type="Normal" Path="menu_5.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbl" ActionTag="1157298672" Tag="251" IconVisible="False" LeftMargin="33.0000" RightMargin="33.0000" TopMargin="65.5000" BottomMargin="5.5000" FontSize="20" LabelText="지도" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="17.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1809" />
                <PreSize X="0.3832" Y="0.2447" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="481.5000" Y="0.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3609" Y="0.0000" />
            <PreSize X="0.0802" Y="0.1253" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="MainBtn_Off_pixel.png" Plist="" />
            <NormalFileData Type="Normal" Path="MainBtn_On_pixel.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu6" ActionTag="917985612" Tag="5" IconVisible="False" LeftMargin="642.0000" RightMargin="584.0000" TopMargin="656.0000" BottomMargin="0.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="108.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="1303406888" Tag="280" IconVisible="False" LeftMargin="35.0000" RightMargin="36.0000" TopMargin="10.0000" BottomMargin="38.0000" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="19" Scale9Height="16" ctype="ImageViewObjectData">
                <Size X="37.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="61.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4954" Y="0.6489" />
                <PreSize X="0.3426" Y="0.4894" />
                <FileData Type="Normal" Path="menu_7.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbl" ActionTag="-765951183" Tag="281" IconVisible="False" LeftMargin="43.5000" RightMargin="44.5000" TopMargin="65.5000" BottomMargin="5.5000" FontSize="20" LabelText="업" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="20.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="17.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4954" Y="0.1809" />
                <PreSize X="0.1852" Y="0.2447" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="696.0000" Y="0.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5217" Y="0.0000" />
            <PreSize X="0.0810" Y="0.1253" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="MainBtn_Off_pixel.png" Plist="" />
            <NormalFileData Type="Normal" Path="MainBtn_On_pixel.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu5" ActionTag="1877570489" Tag="4" IconVisible="False" LeftMargin="535.0000" RightMargin="692.0000" TopMargin="656.0000" BottomMargin="0.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="1" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="107.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="-655953023" Tag="252" IconVisible="False" LeftMargin="29.0000" RightMargin="29.0000" TopMargin="9.0017" BottomMargin="36.9983" LeftEage="9" RightEage="9" TopEage="15" BottomEage="15" Scale9OriginX="9" Scale9OriginY="15" Scale9Width="31" Scale9Height="18" ctype="ImageViewObjectData">
                <Size X="49.0000" Y="48.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.3951" />
                <Position X="53.5000" Y="55.9631" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5954" />
                <PreSize X="0.4579" Y="0.5106" />
                <FileData Type="Normal" Path="menu_6.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbl" ActionTag="-1354504942" Tag="253" IconVisible="False" LeftMargin="34.0000" RightMargin="34.0000" TopMargin="65.5000" BottomMargin="5.5000" FontSize="20" LabelText="상점" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="39.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="17.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1809" />
                <PreSize X="0.3645" Y="0.2447" />
                <FontResource Type="Normal" Path="GodoB.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="588.5000" Y="0.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4412" Y="0.0000" />
            <PreSize X="0.0802" Y="0.1253" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="MainBtn_Off_pixel.png" Plist="" />
            <PressedFileData Type="Normal" Path="MainBtn_Off_pixel.png" Plist="" />
            <NormalFileData Type="Normal" Path="MainBtn_On_pixel.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>