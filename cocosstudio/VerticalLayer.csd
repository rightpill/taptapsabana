<GameFile>
  <PropertyGroup Name="VerticalLayer" Type="Scene" ID="60919cd9-e611-4378-8985-60370426094c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="178" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="pnlField" ActionTag="721388631" Alpha="249" Tag="3" IconVisible="False" LeftMargin="-468.1422" RightMargin="-115.8221" TopMargin="-25.1844" BottomMargin="609.8837" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="PanelObjectData">
            <Size X="1333.9644" Y="749.3008" />
            <Children>
              <AbstractNodeData Name="building0" ActionTag="-1288576486" Tag="4" IconVisible="False" LeftMargin="239.6686" RightMargin="1000.2958" TopMargin="124.8447" BottomMargin="364.4561" LeftEage="31" RightEage="31" TopEage="85" BottomEage="85" Scale9OriginX="31" Scale9OriginY="85" Scale9Width="32" Scale9Height="90" ctype="ImageViewObjectData">
                <Size X="94.0000" Y="260.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="286.6686" Y="494.4561" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2149" Y="0.6599" />
                <PreSize X="0.0705" Y="0.3470" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="building1" ActionTag="-1664446840" Tag="5" IconVisible="False" LeftMargin="1055.3052" RightMargin="79.6592" TopMargin="159.5616" BottomMargin="336.7392" LeftEage="31" RightEage="31" TopEage="85" BottomEage="85" Scale9OriginX="31" Scale9OriginY="85" Scale9Width="137" Scale9Height="83" ctype="ImageViewObjectData">
                <Size X="199.0000" Y="253.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1154.8052" Y="463.2392" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8657" Y="0.6182" />
                <PreSize X="0.1492" Y="0.3376" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="building2" ActionTag="890891726" Tag="6" IconVisible="False" LeftMargin="1165.1022" RightMargin="13.8622" TopMargin="444.2354" BottomMargin="210.0654" LeftEage="31" RightEage="31" TopEage="31" BottomEage="31" Scale9OriginX="31" Scale9OriginY="31" Scale9Width="93" Scale9Height="33" ctype="ImageViewObjectData">
                <Size X="155.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1242.6022" Y="257.5654" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9315" Y="0.3437" />
                <PreSize X="0.1162" Y="0.1268" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="theTree" ActionTag="-517914925" Tag="16" IconVisible="False" LeftMargin="698.2382" RightMargin="391.7261" TopMargin="115.1825" BottomMargin="293.1183" LeftEage="18" RightEage="18" TopEage="23" BottomEage="23" Scale9OriginX="18" Scale9OriginY="23" Scale9Width="208" Scale9Height="295" ctype="ImageViewObjectData">
                <Size X="244.0000" Y="341.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="820.2382" Y="463.6183" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6149" Y="0.6187" />
                <PreSize X="0.1829" Y="0.4551" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="198.8400" Y="984.5341" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2651" Y="0.7380" />
            <PreSize X="1.7786" Y="0.5617" />
            <FileData Type="Normal" Path="" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="svCharacter" ActionTag="1999689555" Tag="12" IconVisible="False" LeftMargin="33.0050" RightMargin="354.9950" TopMargin="698.3359" BottomMargin="304.6641" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="362.0000" Y="331.0000" />
            <AnchorPoint />
            <Position X="33.0050" Y="304.6641" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0440" Y="0.2284" />
            <PreSize X="0.4827" Y="0.2481" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="362" Height="570" />
          </AbstractNodeData>
          <AbstractNodeData Name="gemBar" ActionTag="-495669485" Tag="17" IconVisible="False" LeftMargin="214.1498" RightMargin="369.8502" TopMargin="1075.5632" BottomMargin="180.4368" LeftEage="80" RightEage="80" TopEage="25" BottomEage="25" Scale9OriginX="80" Scale9OriginY="25" Scale9Width="6" Scale9Height="28" ctype="ImageViewObjectData">
            <Size X="166.0000" Y="78.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="297.1498" Y="219.4368" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3962" Y="0.1645" />
            <PreSize X="0.2213" Y="0.0585" />
            <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu0" ActionTag="1443724717" Tag="2" IconVisible="False" LeftMargin="24.1139" RightMargin="600.8861" TopMargin="1181.3340" BottomMargin="27.6661" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="95" Scale9Height="103" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="125.0000" Y="125.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="86.6139" Y="90.1661" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1155" Y="0.0676" />
            <PreSize X="0.1667" Y="0.0937" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu1" ActionTag="-1066697905" Tag="7" IconVisible="False" LeftMargin="126.6592" RightMargin="499.3408" TopMargin="1178.0978" BottomMargin="30.9022" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="94" Scale9Height="103" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="124.0000" Y="125.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="188.6592" Y="93.4022" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2515" Y="0.0700" />
            <PreSize X="0.1653" Y="0.0937" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu2" ActionTag="-1867226264" Tag="8" IconVisible="False" LeftMargin="250.3593" RightMargin="373.6407" TopMargin="1180.3340" BottomMargin="26.6661" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="105" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="126.0000" Y="127.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="313.3593" Y="90.1661" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4178" Y="0.0676" />
            <PreSize X="0.1680" Y="0.0952" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu3" ActionTag="-991906813" Tag="9" IconVisible="False" LeftMargin="365.3492" RightMargin="258.6508" TopMargin="1184.0701" BottomMargin="23.9299" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="104" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="126.0000" Y="126.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="428.3492" Y="86.9299" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5711" Y="0.0652" />
            <PreSize X="0.1680" Y="0.0945" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu4" ActionTag="-2019608364" Tag="10" IconVisible="False" LeftMargin="492.7866" RightMargin="130.2134" TopMargin="1185.0701" BottomMargin="24.9299" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="97" Scale9Height="102" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="127.0000" Y="124.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="556.2866" Y="86.9299" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7417" Y="0.0652" />
            <PreSize X="0.1693" Y="0.0930" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainMenu5" ActionTag="-203510789" Tag="11" IconVisible="False" LeftMargin="618.4847" RightMargin="6.5153" TopMargin="1180.3340" BottomMargin="26.6661" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="95" Scale9Height="105" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="125.0000" Y="127.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="680.9847" Y="90.1661" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9080" Y="0.0676" />
            <PreSize X="0.1667" Y="0.0952" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnSetting" ActionTag="559298264" Tag="13" IconVisible="False" LeftMargin="674.3395" RightMargin="19.6605" TopMargin="37.1122" BottomMargin="1241.8878" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="26" Scale9Height="33" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="56.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="702.3395" Y="1269.3878" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9365" Y="0.9516" />
            <PreSize X="0.0747" Y="0.0412" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Normal" Path="btnSetting.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnAchievement" ActionTag="-2026958016" Tag="14" IconVisible="False" LeftMargin="603.0551" RightMargin="89.9449" TopMargin="29.0275" BottomMargin="1232.9725" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="27" Scale9Height="50" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="57.0000" Y="72.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="631.5551" Y="1268.9725" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8421" Y="0.9513" />
            <PreSize X="0.0760" Y="0.0540" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnCommunity" ActionTag="-886130528" Tag="15" IconVisible="False" LeftMargin="522.5599" RightMargin="161.4401" TopMargin="32.3984" BottomMargin="1233.6016" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="36" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="66.0000" Y="68.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="555.5599" Y="1267.6016" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7407" Y="0.9502" />
            <PreSize X="0.0880" Y="0.0510" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Normal" Path="btnCommunity.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="leafBar" ActionTag="1393467328" Tag="18" IconVisible="False" LeftMargin="26.6676" RightMargin="386.3324" TopMargin="594.4102" BottomMargin="683.5898" LeftEage="80" RightEage="80" TopEage="25" BottomEage="25" Scale9OriginX="80" Scale9OriginY="25" Scale9Width="177" Scale9Height="6" ctype="ImageViewObjectData">
            <Size X="337.0000" Y="56.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="195.1676" Y="711.5898" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2602" Y="0.5334" />
            <PreSize X="0.4493" Y="0.0420" />
            <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="titleBar" ActionTag="-176084005" Tag="19" IconVisible="False" LeftMargin="21.4501" RightMargin="379.5499" TopMargin="530.6923" BottomMargin="755.3077" LeftEage="80" RightEage="80" TopEage="15" BottomEage="15" Scale9OriginX="80" Scale9OriginY="15" Scale9Width="189" Scale9Height="18" ctype="ImageViewObjectData">
            <Size X="349.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="195.9501" Y="779.3077" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2613" Y="0.5842" />
            <PreSize X="0.4653" Y="0.0360" />
            <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="footBar" ActionTag="1933317230" Tag="20" IconVisible="False" LeftMargin="45.3338" RightMargin="534.6662" TopMargin="1074.9623" BottomMargin="180.0378" LeftEage="80" RightEage="80" TopEage="25" BottomEage="25" Scale9OriginX="80" Scale9OriginY="25" Scale9Width="10" Scale9Height="29" ctype="ImageViewObjectData">
            <Size X="170.0000" Y="79.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="130.3338" Y="219.5378" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1738" Y="0.1646" />
            <PreSize X="0.2267" Y="0.0592" />
            <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>